<?php

// ----------------------- functions -----------------------

function render($payload = '') {
    $html = file_get_contents('./app.html');
    $content = str_replace("<meta-placeholder/>", $payload, $html);
    echo $content;
    exit;
}

function curlRequest($url) {
    $curlOptions = [
        CURLOPT_URL => $url,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT_MS => 3000,
        CURLOPT_CONNECTTIMEOUT_MS => 3000,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "GET",
        CURLOPT_SSL_VERIFYPEER => false,
        CURLOPT_SSL_VERIFYHOST => false,
        CURLOPT_HTTPHEADER => ["cache-control: no-cache"]
    ];

    $curl = curl_init();
    curl_setopt_array($curl, $curlOptions);

    $response = curl_exec($curl);
    $info = curl_getinfo($curl);
    curl_close($curl);

    if($info['http_code'] !== 200) {
        render();
    }

    return json_decode($response)[0];
}

function createMetaTags($items) {
    $tags = '';
    foreach($items as $name => $content) {
        if(!empty($content)) {
            if($name === 'title') {
                $tags .= "<title>$content</title>";
            } else {
                $itempropTag = in_array($name, ['name', 'description', 'image']) ? "<meta itemprop=\"$name\" content=\"$content\" />" : '';
                $tags .= "<meta name=\"$name\" content=\"$content\" />" . $itempropTag;
            }
        }
    }
    return $tags;
}

// ----------------------- config, params -----------------------

$pattern = '/^\/[^\/]+-[0-9]+$/';
$requestUri = getenv('REQUEST_URI');
$checkIfSlug = preg_match($pattern, $requestUri) === 1;
$slug = $checkIfSlug ? $requestUri : null;

if(!$slug) {
    render();
}

$title = 'trans.INFO';
$defaultImage = 'https://transeu-prod-transinfo-0.s3.amazonaws.com/uploads/2017/09/86d06cfc167f8e3f01657fde7eb.jpg';
$frontendUrl = getenv('FORUMBRIDGE_FRONTEND_URL');
$apiUrl = getenv('FORUMBRIDGE_BACKEND_URL');
$rest = $apiUrl . '/posts' . $slug;

// ----------------------- render with meta tags -----------------------

$post = curlRequest($rest);

$image = !empty($post->featuredImage) ? $post->featuredImage : $defaultImage;
$video = $post->type === 'videos' ? 'https://youtube.com/watch?v=' . $post->video : '';
$tags = '';

foreach($post->tags as $tag) {
    $tags .= $tag->name . ', ';
}

$metaTags = createMetaTags([
    "title" => $post->title . ' - ' . $title,
    "og:url" => $frontendUrl . '/' . $post->slug,
    "og:type" => "website",
    "og:title" => $post->title,
    "og:description" => $post->beforeContent,
    "og:image" => $image,
    "og:site_name" => $title,
    "og:video" => $video,
    "description" => $post->beforeContent,
    "name" => $post->title,
    "image" => $image,
    "twitter:card" => $post->title,
    "twitter:description" => $post->beforeContent,
    "twitter:site" => "@TranseuSystem",
    "twitter:image:src" => $image,
    "twitter:player" => $video,
    "og:locale" => $post->langName,
    "news_keywords" => $tags,
    "Googlebot-News" => !$post->googleNews ? "noindex, nofollow" : ''
]);

render($metaTags);