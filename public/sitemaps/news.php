<?php header('Content-type: text/xml'); ?>
<?xml version="1.0" encoding="UTF-8"?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"
		xmlns:news="http://www.google.com/schemas/sitemap-news/0.9"
		xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
		xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9
       http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd
       http://www.google.com/schemas/sitemap-news/0.9
       http://www.google.com/schemas/sitemap-news/0.9/sitemap-news.xsd">
		<?php
		switch ($_GET['lang']) {
			case 'pl':
				$lang = 'pl_PL';
				break;
			case 'en':
				$lang = 'en_GB';
				break;
			case 'de':
				$lang = 'de_DE';
				break;
			case 'lt':
				$lang = 'lt_LT';
				break;
			case 'hu':
				$lang = 'hu_HU';
				break;
			case 'ro':
				$lang = 'ro_RO';
				break;
			case 'sk':
				$lang = 'sk_SK';
				break;
			case 'cs':
				$lang = 'cs_CZ';
				break;
			case 'ru':
				$lang = 'ru_RU';
				break;
		}
		$json = $_SERVER['FORUMBRIDGE_BACKEND_URL'] . '/posts/gn/recentPosts/' . $lang;
		$json = str_replace(['https://trans', 'https://ti'], ['http://trans', 'http://ti'], $json);
		$json = file_get_contents( $json );
		$json = json_decode($json, true);
		foreach ($json as $key => $val) {
			$lang = explode("_", $val['langName'], 2);
			echo '<url>';
			echo '<loc>https://trans.info/' . $val['slug'] . '</loc>';
			echo '<news:news>';
			echo '<news:publication>';
			echo '<news:name>' . $val['title'] . '</news:name>';
			echo '<news:language>' . $lang[0] . '</news:language>';
			echo '</news:publication>';
			echo '<news:genres>PressRelease, Blog</news:genres>';
			echo '<news:publication_date>' . date('c', $val['dateGmt']/1000) . '</news:publication_date>';
			echo '<news:title>' . $val['title'] . '</news:title>';
			echo '<news:keywords>';
			$tags = array();
			foreach ( $val['tags'] as $tag) {
				$tags[] = $tag['name'];
			}
			echo implode(", ",$tags);
			echo '</news:keywords>';
			echo '</news:news>';
			echo '</url>';
		}
		?>
</urlset>