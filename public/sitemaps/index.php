<?php header('Content-type: text/xml'); ?>
<?xml version="1.0" encoding="UTF-8"?>
<sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
	<sitemap>
		<loc>https://trans.info/sitemap-pl.xml</loc>
		<?php
		$json = $_SERVER['FORUMBRIDGE_BACKEND_URL'] . '/posts/gn/lastPostDate/pl_PL';
		$json = str_replace(['https://trans', 'https://ti'], ['http://trans', 'http://ti'], $json);
		if ( file_get_contents( $json ) !== false ) {
			$filecontent = file_get_contents( $json );
			echo '<lastmod>' . date( 'c', $filecontent / 1000 ) . '</lastmod>';
		}
		?>
	</sitemap>
	<sitemap>
		<loc>https://trans.info/sitemap-en.xml</loc>
		<?php
		$json = $_SERVER['FORUMBRIDGE_BACKEND_URL'] . '/posts/gn/lastPostDate/en_GB';
		$json = str_replace(['https://trans', 'https://ti'], ['http://trans', 'http://ti'], $json);
		if ( file_get_contents( $json ) !== false ) {
			$filecontent = file_get_contents( $json );
			echo '<lastmod>' . date( 'c', $filecontent / 1000 ) . '</lastmod>';
		}
		?>
	</sitemap>
	<sitemap>
		<loc>https://trans.info/sitemap-de.xml</loc>
		<?php
		$json = $_SERVER['FORUMBRIDGE_BACKEND_URL'] . '/posts/gn/lastPostDate/de_DE';
		$json = str_replace(['https://trans', 'https://ti'], ['http://trans', 'http://ti'], $json);
		if ( file_get_contents( $json ) !== false ) {
			$filecontent = file_get_contents( $json );
			echo '<lastmod>' . date( 'c', $filecontent / 1000 ) . '</lastmod>';
		}
		?>
	</sitemap>
	<sitemap>
		<loc>https://trans.info/sitemap-cs.xml</loc>
		<?php
		$json = $_SERVER['FORUMBRIDGE_BACKEND_URL'] . '/posts/gn/lastPostDate/cs_CZ';
		$json = str_replace(['https://trans', 'https://ti'], ['http://trans', 'http://ti'], $json);
		if ( file_get_contents( $json ) !== false ) {
			$filecontent = file_get_contents( $json );
			echo '<lastmod>' . date( 'c', $filecontent / 1000 ) . '</lastmod>';
		}
		?>
	</sitemap>
	<sitemap>
		<loc>https://trans.info/sitemap-lt.xml</loc>
		<?php
		$json = $_SERVER['FORUMBRIDGE_BACKEND_URL'] . '/posts/gn/lastPostDate/lt_LT';
		$json = str_replace(['https://trans', 'https://ti'], ['http://trans', 'http://ti'], $json);
		if ( file_get_contents( $json ) !== false ) {
			$filecontent = file_get_contents( $json );
			echo '<lastmod>' . date( 'c', $filecontent / 1000 ) . '</lastmod>';
		}
		?>
	</sitemap>
	<sitemap>
		<loc>https://trans.info/sitemap-hu.xml</loc>
		<?php
		$json = $_SERVER['FORUMBRIDGE_BACKEND_URL'] . '/posts/gn/lastPostDate/hu_HU';
		$json = str_replace(['https://trans', 'https://ti'], ['http://trans', 'http://ti'], $json);
		if ( file_get_contents( $json ) !== false ) {
			$filecontent = file_get_contents( $json );
			echo '<lastmod>' . date( 'c', $filecontent / 1000 ) . '</lastmod>';
		}
		?>
	</sitemap>
	<sitemap>
		<loc>https://trans.info/sitemap-ro.xml</loc>
		<?php
		$json = $_SERVER['FORUMBRIDGE_BACKEND_URL'] . '/posts/gn/lastPostDate/ro_RO';
		$json = str_replace(['https://trans', 'https://ti'], ['http://trans', 'http://ti'], $json);
		if ( file_get_contents( $json ) !== false ) {
			$filecontent = file_get_contents( $json );
			echo '<lastmod>' . date( 'c', $filecontent / 1000 ) . '</lastmod>';
		}
		?>
	</sitemap>
	<sitemap>
		<loc>https://trans.info/sitemap-sk.xml</loc>
		<?php
		$json = $_SERVER['FORUMBRIDGE_BACKEND_URL'] . '/posts/gn/lastPostDate/sk_SK';
		$json = str_replace(['https://trans', 'https://ti'], ['http://trans', 'http://ti'], $json);
		if ( file_get_contents( $json ) !== false ) {
			$filecontent = file_get_contents( $json );
			echo '<lastmod>' . date( 'c', $filecontent / 1000 ) . '</lastmod>';
		}
		?>
	</sitemap>
	<sitemap>
		<loc>https://trans.info/sitemap-ru.xml</loc>
		<?php
		$json = $_SERVER['FORUMBRIDGE_BACKEND_URL'] . '/posts/gn/lastPostDate/ru_RU';
		$json = str_replace(['https://trans', 'https://ti'], ['http://trans', 'http://ti'], $json);
		if ( file_get_contents( $json ) !== false ) {
			$filecontent = file_get_contents( $json );
			echo '<lastmod>' . date( 'c', $filecontent / 1000 ) . '</lastmod>';
		}
		?>
	</sitemap>
</sitemapindex>