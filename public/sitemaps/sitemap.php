<?php header('Content-type: text/xml'); ?>
<?xml version="1.0" encoding="UTF-8"?>

<?php
	$langyearmonth = explode("-", $_GET['lang'], 3);
    $lang = $langyearmonth[0];
    $year = $langyearmonth[1];
    $month = $langyearmonth[2];

    switch ($lang) {
        case 'pl':
            $language = 'pl_PL';
            break;
        case 'en':
            $language = 'en_GB';
            break;
        case 'de':
            $language = 'de_DE';
            break;
        case 'lt':
            $language = 'lt_LT';
            break;
        case 'hu':
            $language = 'hu_HU';
            break;
        case 'ro':
            $language = 'ro_RO';
            break;
        case 'sk':
            $language = 'sk_SK';
            break;
        case 'cs':
            $language = 'cs_CZ';
            break;
        case 'ru':
            $language = 'ru_RU';
            break;
    }
?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
<?php
$start = $month = strtotime('2010-01-01');
$now = date('Y-m-d');
$end = strtotime($now);
while($month < $end)
{
	$year = date('Y', $month);
	$monthcount = date('m', $month);
	$month = strtotime("+1 month", $month);
	$json = $_SERVER['FORUMBRIDGE_BACKEND_URL'] . '/posts/gn/monthlyPosts/' . $language . '/' . $year . '/' . $monthcount;
	$json = str_replace(['https://trans', 'https://ti'], ['http://trans', 'http://ti'], $json);
	$json = file_get_contents( $json );
	$json = json_decode($json, true);
	foreach ($json as $key => $val) {
		echo '<url>';
		echo '<loc>https://trans.info/' . $val['slug'] . '</loc>';
		echo '<lastmod>' . date('Y-m-d', $val['dateGmt']/1000) . '</lastmod>';
		echo '<changefreq>daily</changefreq>';
		echo '<priority>';
		if( true == $val['googleNews'] AND true == $val['sticky'] ) {
			echo '1.0';
		} elseif ( true == $val['googleNews'] OR true == $val['sticky'] ) {
			echo '0.8';
		} else {
			echo '0.5';
		}
		echo '</priority>';
		echo '</url>';
	}
}
?>
</urlset>