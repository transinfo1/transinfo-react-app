(function () {

    const config = {"packageName":"transinfo_frontend_test","restApiHostName":"http://backend-test.transinfo.dev.firetms.io","auth":{"oneLogin":{"authServerUrl":"http://auth.dev-trans.rst.com.pl/oauth2/login","clientId":"14963949470NHDEu4SA74pSgFwk817"},"google":{"clientId":"983191201652-0cum57752r41o66jnbdpi2j3244qvuho.apps.googleusercontent.com"}},"languages":["pl","en","de","cz","hu","lt","sk","ro","ru"]};

    //////////////////
    // Facebook SDK //
    //////////////////
    $(document).ready(function () {
        $('body').append($('<script type="text/javascript" src="https://momentjs.com/downloads/moment.min.js"></script>'));
        forumDropdown();
    });

    window.fbAsyncInit = function () {
        FB.init({
            appId: '399407633738697',
            autoLogAppEvents: true,
            xfbml: true,
            version: 'v2.9'
        });
        FB.AppEvents.logPageView();
    };

    function forumDropdown() {
        const hydePark = '<div>' +
                '<h4><a href="/forum/forum/hyde-park">HydePark</a></h4>' +
                '<ul>' +
                '<li><a href="/forum/forum/hyde-park/hyde-park-aa">Hyde Park</a></li>' +
                '<li><a href="/forum/forum/hyde-park/opinie-i-propozycję-rozwoju-forum">Pomoc, opinie i rozwój forum</a></li>' +
                '</ul></div>',
            systemTransInfo = '<div>' +
                '<h4><a href="/forum">System TRANS.EU</a></h4>' +
                '<ul>' +
                '<li><a href="/forum">Opinie o kontrahentach</a></li>' +
                '<li><a href="/forum">Pomoc techniczna</a></li>' +
                '</ul></div>',
            transId = localStorage.getItem('gtm');

        if (transId === null) {
            $('#forum-hydePark').append(hydePark);
        } else {
            const transIdObj = JSON.parse(transId);
            if (transIdObj.transId === '0-0' || transIdObj.transId === '') {
                $('#forum-hydePark').append(hydePark);
            } else {
                $('#forum-hydePark').append(systemTransInfo);
            }
        }
    }

    (function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) {
            return;
        }
        js = d.createElement(s);
        js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));

    function globalLoaderToggle(state = false) {
        if (state) {
            $('.global-loader').addClass('global-loader-show')
        } else {
            $('.global-loader').removeClass('global-loader-show');
        }
    }

    const getCookie = (name) => {
        let cookies = document.cookie.split(';');
        let selected = cookies.filter(item => name === item.trim().split('=')[0]);
        if (!selected.length) {
            return null;
        }
        let value = selected[0].trim().split('=')[1];
        if (!value) {
            return null;
        }
        return JSON.parse(decodeURIComponent(value));
    }

    // auth objects
    let localStorageAuth = JSON.parse(localStorage.getItem('auth'));
    let cookieAuth = getCookie('forumAuth');

    //functions manage with webstorage/cookies
    const setStorage = (key, value, storage = localStorage) => {
        try {
            let data = JSON.stringify(value);
            storage.setItem(key, data);
        } catch (err) {
            console.error('Fail storage fetching', err)
        }
    }

    const getTimestampModifiedByMinutes = (minutes = 1) => {
        let date = new Date();
        return date.setTime(date.getTime() + (minutes * 60 * 1000))
    }

    const setAuthToLocalStorage = data => setStorage('auth', data);

    const setAuthToCookie = (data = null) => {
        let string = data ? encodeURIComponent(JSON.stringify({
            accessToken: data.accessToken,
            provider: data.provider,
            userId: data.user.id
        })) : '';

        let date = new Date();
        date.setTime(date.getTime() + (30 * 24 * 60 * 60 * 1000)); // 30 days

        document.cookie = "forumAuth=" + string + "; expires=" + date.toUTCString() + "; path=/;";
    }

    function setGtm(userProfile) {
        console.warn('setGTM');
        return ($.ajax({
                url: `${config.restApiHostName}/users/profile/me`,
                type: "GET",
                headers: {
                    "Content-Type": "application/json",
                    "Authorization": userProfile.accessToken,
                    "X-Provider": userProfile.provider
                }
            }).done(data => {
                let transId;
                if (data.transId) {
                    transId = data.transId;
                } else {
                    transId = '0-0';
                }
                const profileInfo = {
                    "transId": transId,
                    "wcw1": data.wcw1,
                    "wcw2": data.wcw2,
                    "wcw3": data.wcw3,
                    "wcw4": data.wcw4,
                    "wcw5": data.wcw5,
                    "wcw6": data.wcw6,
                    "wpw1": data.wpw1,
                    "wpw2": data.wpw2,
                    "wpw3": data.wpw3,
                    "wpw4": data.wpw4,
                    "wpw5": data.wpw5,
                    "wpw6": data.wpw6,
                };
                localStorage.setItem('gtm', JSON.stringify(profileInfo));
            })
        );
    }

    const setStorages = (data, autoLogin = true) => {
        autoLogin = localStorage.getItem('remember') === 'true' ? true : false;
        setGtm(data);

        if (!autoLogin) {
            data = $.extend({expires: getTimestampModifiedByMinutes(1)}, data)
        }
        setAuthToLocalStorage(data);
        setAuthToCookie(data);
    }

    function logout(redirect = true) {
        localStorage.removeItem('auth');
        localStorage.removeItem('remember');
        localStorage.removeItem('gtm');
        localStorageAuth = '';
        cookieAuth = '';

        // vBulletin API logout
        $.ajax({
            url: location.origin + '/forum/logout.php',
            type: 'GET',
            async: false
        });

        document.cookie = "forumAuth=; expires=; path=/;";
        if (redirect) {
            let path = typeof redirect === 'string' ? redirect : '/';
            window.location.href = path;
        }
    }

    // update localStorageAuth when != cookieAuth
    if (localStorageAuth && cookieAuth && localStorageAuth.accessToken !== cookieAuth.accessToken) {
        localStorageAuth.accessToken = cookieAuth.accessToken;
        setStorages(localStorageAuth);
    }

    // logout when accessToken === null
    if (cookieAuth && cookieAuth.accessToken === 'null') {
        logout(false);
    }

    if (localStorageAuth && localStorageAuth.expires) {
        if (localStorageAuth.expires < (new Date().getTime())) {
            logout();
        } else {
            console.warn('local storage', localStorageAuth, 'cookie', cookieAuth)
            setStorages(localStorageAuth, false);
        }
    }

    if (!localStorageAuth) {
        $('.pp').css('display', 'none');
        $('.pp').css('height', $(document).height());
    }

    // Set social account
    function setSocialAccount(userProfile) {
        return ($.ajax({
                url: `${config.restApiHostName}/users/profile/me`,
                type: "GET",
                headers: {
                    "Content-Type": "application/json",
                    "Authorization": userProfile.accessToken,
                    "X-Provider": userProfile.provider
                }
            }).done(data => {
                userProfile.user = data.rpcUserDTO;
                userProfile.user.email = data.email;
                userProfile.user.id = data.id;
                setStorages(userProfile);
                globalLoaderToggle(false);
                location.reload();
            })
        )
    }

//Fetch profile if user is logged
    function userMenu() {
        if (localStorageAuth) {
            let avatar = localStorageAuth.user.avatar || 'css/static/img/default-avatar.png';
            let avatarClass = localStorageAuth.user.avatar ? `"thumbnail rounded-circle"` : `"face-thumbnail"`;
            let userName = localStorageAuth.user.firstName || 'Gość';
            $('.login-panel').html(`
      <div class="dropdown my-auto">
          <button data-test="header.user" class="nav-a dropdown-toggle user" data-toggle="dropdown"
                  aria-haspopup="true" aria-expanded="false">
                  <img
                  class=${avatarClass}
                  src=${avatar}
                  alt='thumbnail'
                  />
              <div class="p-3 text-nowrap">${userName}
                  <i class="fa fa-angle-down hidden-md-down ml-1" aria-hidden="true" />
              </div>
          </button>
          <div class="dropdown-menu hidden-md-down" aria-labelledby="test_Hmd_video">
              <a href="/user/profile" class="dropdown-item" data-test="header.link.profile">
                  Profil
              </a>
              <a href="/contact" class="dropdown-item" data-test="header.link.contact">
                  Kontakt
              </a>
              <a href="/policy" class="dropdown-item" data-test="header.link.policy">
                  Polityka prywatności
              </a>
              <div class="dropdown-divider"></div>
              <a class="dropdown-item logout-button" data-test="header.link.logout">
                  Wyloguj
              </a>
          </div>
      </div>
    `)


            $('.profile-item').html(`
      <a href="/user/profile" class="nav-link white-button" data-test="header.link.profileMobile"><span>Profil</span></a>
      `)

            $('.settings-item').html(`
      <a data-popup="userProfileQuestions" class="nav-link white-button" data-test="header.link.settingsMobile"><span>Ustawienia</span></a>
      `)

            $('.logout-item').html(`
      <a class="nav-link logout-button white-button" data-test="header.link.logoutMobile" style="color: #fff">Wyloguj</a>
      `)

            $('.login-item').html(`
          <button data-test="header.userMobile" class="nav-a dropdown-toggle user" data-toggle="dropdown"
          aria-haspopup="true" aria-expanded="false">
          <img
          class=${avatarClass}
          src=${avatar}
          alt='thumbnail'
          />
          <div class="p-3 text-nowrap">${userName}
          <i class="fa fa-angle-down hidden-md-down ml-1" aria-hidden="true" />
          </div>
          </button>`)

        }
    }

//Go to Search Result
    function goToSearch() {
        let list = $('.suggestions');
        $(document).on('click', '.suggestions li', function (e) {
                let keywords = e.target.innerText;
                window.location.href = '/search?q=' + keywords;
            }
        )
    }


//Get search suggestions
    function fetchSuggestions() {
        let suggestions = [];
        let link = `${config.restApiHostName}/search/autocomplete/pl_PL?keywords=`

        $("#search-text").on('keyup', function (e) {
            let keywords = e.target.value;
            $.ajax({
                url: link + keywords
            }).done(function (data) {
                suggestions = data;
                let list = $('.suggestions');
                list.html('');
                if (suggestions.length > 0) {
                    suggestions.map((word, key) => {
                        let item = '';
                        if (key < 5) {
                            item += `<li>${word}</li>`
                            list.append(item);
                        }
                    })

                }
            });
        })
    }

//fetching data from database
    function fetchData(type) {
        let videoItems = $('.video-item');
        let currentTime = Date.now();
        let postType = type;
        let link = `${config.restApiHostName}/posts/limit/4/before/${currentTime}/langName/pl_PL?type=${postType}`;
        let videos = [];
        $.ajax({
            url: link
        }).done(function (data) {
            videos = [...data];
            if (videos.length) {
                videos.map((video, key) => {
                    let imgSrc = `https://img.youtube.com/vi/${video.video}/hqdefault.jpg` || `css/images/article1.jpg`
                    videoItems.eq(key).html(`
			<div class="video-thumbnail-wrapper">
			    <div class="video__thumbnail">  
				    <a href="/${video.slug}"><img class="search-video__thumbnail__img" src=${imgSrc} alt="videos-thumbnail">
				        <div class="play-video-icon"></div>
				    </a>
				</div>
				<a class="link-color" href="/${video.slug}">${video.title}</a>
			</div>
			`);
                })
            }
        });
    }

    function fillHeaderPeople() {
        const authorContainer = $('#people-row').css({
            "display": "flex",
            "flex-wrap": "no-wrap"
        });
        let authors = [],
            authorLink = `${config.restApiHostName}/users/awardedAuthor/randomList?limit=4&langName=pl_PL`,
            posts;

        $.ajax({
            url: authorLink
        }).done(function (data) {
            let authorContent,
                authorAvatar,
                userCaption;

            authors = [...data];

            authors.map((author, key) => {
                $.ajax({
                    url: `${config.restApiHostName}/posts?limit=2&authorSlug=${author.slug}&langName=pl_PL`,
                    async: true
                }).success(response => {
                    posts = [...response];

                    posts.forEach((post) => {
                        let ans
                        if (post.type === 'videos') {
                            ans = '<div class="mt-2"></div><a href="/' + post.slug + '"><img src="css/static/img/icons/play_small.svg" alt="play video" class="mr-1" />' + post.title + '</a>';
                        } else {
                            ans = '<div class="mt-2"></div><a href="/' + post.slug + '">' + post.title + '</a>';
                        }
                        $('#author-posts-' + author.id).append(ans)
                    });
                });

                if (author.avatar === '') {
                    authorAvatar = "./core/images/default/default_avatar_medium.png";
                } else {
                    authorAvatar = author.avatar;
                }
                if (author.userCaption === null) {
                    userCaption = "";
                } else {
                    userCaption = author.userCaption;
                }

                authorContent = '<div class="col-3 people-block"><div class="vBflex flex-nowrap">' +
                    '<a class="link-user"  href="./../user/' + author.slug + '">' +
                    '<div  class="user-thumbnail" itemprop="name"><img class="rounded-circle mt-1" src="' + authorAvatar + '" alt=""></div>' +
                    '</a>' +
                    '<div class="header-author ml-1"><a class="link-color" href="./../user/' + author.slug + '">' + author.name + '</a>' +
                    '<p class="user-caption">' + userCaption + '</p></div>' +
                    '</div><div class="mt-3 header-author-separate"></div> <div id="author-posts-' + author.id + '"></div></div>'; //'+postsContent()+'

                authorContainer.append(authorContent);
            })
        });
    }

////////////////////////
//      LOGIN         //
////////////////////////

    function facebookLogin() {
        globalLoaderToggle(true)
        $.fblogin({
            fbId: '399407633738697',
            permissions: 'email,user_birthday',
            fields: 'first_name,last_name,email,picture',
        }).done(data => {
            FB.getLoginStatus(response => {
                if (response.status === 'connected') {
                    $.ajax({
                        url: `${config.restApiHostName}/token/fb/getLongLivedToken`,
                        type: "POST",
                        headers: {
                            "Content-Type": "application/x-www-form-urlencoded",
                        },
                        data: {
                            fbExchangeToken: response.authResponse.accessToken
                        }
                    }).done(token => {
                        console.log('face', token);
                        setSocialAccount({
                            accessToken: token,
                            provider: 'facebook',
                        }).then(() => {
                            globalLoaderToggle(false)
                            location.reload();
                        });

                    })

                } else if (response.status === 'not_authorized') {
                    console.log('the user is logged in to Facebook, but has not authenticated your app')
                } else {
                    console.log("the user isn't logged in to Facebook")
                }
            })
        }).fail(function (error) {
            globalLoaderToggle(false)
            console.log('An error occurred.', error);
        });
    }

    // offline (server-side) authorization type
    function getToken(code, provider) {
        return $.ajax({
            url: `${config.restApiHostName}/token/get`,
            type: 'POST',
            data: {
                code: code,
                provider: provider
            },
            headers: {"Content-Type": "application/x-www-form-urlencoded"}
        })
    }

    // google login
    gapi.load('auth2', function () {
        auth2 = gapi.auth2.init({
            client_id: '983191201652-0cum57752r41o66jnbdpi2j3244qvuho.apps.googleusercontent.com',
            cookiepolicy: 'single_host_origin',
            scope: 'profile email',
            // prompt: 'consent'
        });
    });

    $('#google-login').click(function () {
        globalLoaderToggle(true)
        auth2.grantOfflineAccess({
            prompt: 'consent'
        }).then(response => {
            getToken(response.code, 'google').then(resp => {
                setSocialAccount({
                    accessToken: resp,
                    provider: 'google'
                });
            })
        });
    })

    function oneLogin() {
        let params = [
            'redirect_uri=' + location.origin,
            'client_id=' + config.auth.oneLogin.clientId, // client ID z prod z mobilnej trans.INFO: 11474527064r5sXitnNnPnrqN8z6JVB
            'response_type=code',
            'scope=companies.employees.me.read',
            'state=f04uM'
        ];
        window.location.href = config.auth.oneLogin.authServerUrl + "?" + params.join('&'); // DEV url: https://auth.dev-trans.rst.com.pl
    }

    function wordpressLogin(e) {
        e.preventDefault();
        let login = $('#email-trans').val();
        let pass = $('#password-trans').val();
        let rememberMe = $('#remember-me').is(':checked');
        if (!$.trim(login) || !$.trim(pass)) {
            $('#fail-alert').css('display', 'block');
            $('#fail-alert').text('Uzupełnij wymagane pola!');
            setTimeout(function () {
                $('#fail-alert').css('display', 'none');
            }, 3500)
            return;
        }
        let data = {password: pass, username: login};
        $.ajax({
            url: `${config.restApiHostName}/users/login`,
            type: "POST",
            contentType: 'application/json;charset=UTF-8',
            dataType: "json",
            data: JSON.stringify(data)
        }).then(
            globalLoaderToggle(true)
        )
            .done(function (data) {
                globalLoaderToggle(false);
                localStorage.setItem('remember', rememberMe);
                setStorages(data, rememberMe);
                location.reload();
            })
            .fail(function (jqXHR, textStatus, error) {
                globalLoaderToggle(false);
                const lockTime = jqXHR.responseJSON.lockdowntime,
                    failedLoginAttempt = jqXHR.responseJSON.failedLoginAttempt,
                    maxfailedLoginAttempts = jqXHR.responseJSON.maxfailedLoginAttempts;

                let status = JSON.parse(jqXHR.responseText);
                if (status.error === "401 Unauthorized" || status.error === "401 Forbidden") {
                    if (lockTime >= 0) {
                        $('#fail-alert').css('display', 'block');
                        $('#fail-alert').text('Ponowne logowanie możliwe o ' + moment().add(lockTime, 'seconds').format("HH:mm"));
                        setTimeout(function () {
                            $('#fail-alert').css('display', 'none');
                        }, 5000)
                    } else if (failedLoginAttempt >= 1 && failedLoginAttempt <= maxfailedLoginAttempts) {
                        $('#fail-alert').css('display', 'block');
                        $('#fail-alert').text('Wykorzystano ' + failedLoginAttempt + ' z ' + maxfailedLoginAttempts + ' prób!');
                        setTimeout(function () {
                            $('#fail-alert').css('display', 'none');
                        }, 5000)
                    } else {
                        $('#fail-alert').css('display', 'block');
                        $('#fail-alert').text('Login i/lub hasło są niepoprawne!');
                        setTimeout(function () {
                            $('#fail-alert').css('display', 'none');
                        }, 5000)
                    }
                } else {
                    $('#fail-alert').css('display', 'block');
                    $('#fail-alert').text('Ponowne logowanie możliwe o ' + moment().add(lockTime, 'seconds').format("HH:mm"));
                    setTimeout(function () {
                        $('#fail-alert').css('display', 'none');
                    }, 5000)
                }
            });
    };


/////////////////////////////
// INIT ON DOCUMENT READY  //
/////////////////////////////

    $(document).ready(function () {

        fetchData('videos');
        fillHeaderPeople();
        userMenu();
        fetchSuggestions();
        goToSearch();

        $('.login-button').on('click', function () {
            $('.pp').css('display', 'block');
        });

        // closing popup
        $('.login-popup-close').on('click', function (e) {
            $('.pp').css('display', 'none');
        });
        $("body").keydown(function (e) {
            if (e.keyCode === 27) {
                $('.pp').css('display', 'none');
            }
        });
        $(".pp-login").click(function (event) {
            event.stopPropagation();
        });

        $('#wordpress-login').on('click', function (e) {
            wordpressLogin(e);
        });

        $('#facebook-login').on('click', facebookLogin);

        $('#one-login').on('click', oneLogin);

        $('#show-password').on('click', () => {
            let passType = $('#password-trans').attr('type');
            let newType = (passType === "password") ? "text" : "password";
            $('#password-trans').attr('type', newType);
        })

        $('.logout-button').on('click', logout);


    })


////////////////////////
// header transitions //
////////////////////////

    $(window).on('scroll', function (event) {
        var scrollValue = $(window).scrollTop();
        if (scrollValue > 70) {
            $('.navbar .logo').addClass('img-zoom-out');
            $('.navbar').addClass('navbar-zoom-out');
            $('.navbar .thumbnail').addClass('img-zoom-out');
        } else {
            $('.navbar .logo').removeClass('img-zoom-out');
            $('.navbar').removeClass('navbar-zoom-out');
            $('.navbar .thumbnail').removeClass('img-zoom-out');
        }
    });

    $(function () {
        $('[data-toggle="push"]').each(function () {
            var $this = $(this);

            //var data = $this.data();
            var options = {
                target: '#navbar',
                direction: 'left',
                canvas: 'body',
                logo: '#navbar-brand-search',
                mobilemenu: '#mobilemenu'
            };

            //$.extend(options, data);

            var $target = $(options.target);
            $target.css('display', 'block');

            $target.addClass('navbar-push navbar-push-' + options.direction);

            var $canvas = $(options.canvas);
            $canvas.addClass('push-canvas');

            var $logo = $(options.logo);
            //$logo.addClass('on');

            $this.unbind().click(function (e) {
                $this.toggleClass('active');
                $canvas.toggleClass('pushed-' + options.direction);
                $target.toggleClass('in');
                $logo.toggleClass('off');
            });

            var $mobilemenu = $(options.mobilemenu);
            $mobilemenu.unbind().click(function (e) {
                $this.toggleClass('active');
                $canvas.toggleClass('pushed-' + options.direction);
                $target.toggleClass('in');
                $logo.toggleClass('off');
            });
        });
    });
}());