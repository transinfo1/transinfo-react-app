<?php

namespace Transinfobridge\lib;

use Logger;
use vB;
use vB_Api;
use vB_Library;
use Transinfobridge\lib\Config;
use Transinfobridge\lib\DbUsers;

require_once "DbUsers.php";
require_once "Config.php";
require_once "Misc.php";

/**
 * Class IndexRequester
 *
 * @package Transinfobridge\lib
 */
final class IndexRequester
{
    /**
     * @var Logger
     */
    private $logger;

    /**
     * @var Config
     */
    private $config;


    const LANG_NAME = 'langName';
    const FORUM_ID = 'forumId';
    const THREAD_ID = 'threadId';
    const POST_ID = 'postId';

    const TRANSINFO_USER_ID = 'transinfoUserId';
    const THREAD_TITLE = 'threadTitle';
    const POST_CONTENT = 'postContent';
    const THREAD_SLUG = 'threadSlug';
    const POST_TIME = 'postTime';
    const IS_FIRST = 'isFirst';
    const SIGNATURE = 'signature';

    /**
     * IndexRequester constructor.
     */
    public function __construct()
    {
        $this->logger = vB::getLogger('Indexer');
        $this->config = Config::getInstance();
    }

    /**
     * Singleton pattern
     *
     * @return null|IndexRequester
     */
    public static function Instance()
    {
        static $inst = null;
        if ($inst === null) {
            $inst = new IndexRequester();
        }
        return $inst;
    }

    /**
     * Add or update post index by rest
     *
     * @param $nodeId
     */
    public function addPostIndex($nodeId) {
        $forumId = $this->getForumId($nodeId);

        //is node is in private sub forum we do won't index them
        if($this->isNodePrivate($forumId)) {
            return;
        }

        $dbUsers = new DbUsers(vB::get_registry()->db);
        $nodeInfo = vB_Api::instance('node')->getNode((int)$nodeId);
        $isFirst = $nodeInfo['starter'] == $nodeId;

        $pathVariables = [
            self::LANG_NAME => "pl_pl",
            self::FORUM_ID => $forumId,
            self::THREAD_ID => $isFirst ? $nodeId : $nodeInfo['parentid'],
            self::POST_ID => $nodeId
        ];

        $postData = [
            self::TRANSINFO_USER_ID => $dbUsers->getCmsUserId($nodeInfo['userid']),
            self::THREAD_TITLE => $isFirst ? $nodeInfo['title']: $this->getParentThreatTitle($nodeId),
            self::POST_CONTENT => $nodeInfo['description'],
            self::THREAD_SLUG => $this->getSlug($nodeId),
            self::POST_TIME => $nodeInfo['created']."000", // in millisecond
            self::IS_FIRST => $isFirst ? "true" : "false",
            self::SIGNATURE => $this->getSignature()
        ];

        $this->makeAddCURL($postData, $pathVariables);
    }

    /**
     * Delete post index by rest
     *
     * @param $nodeId
     */
    public function deletePostIndex($nodeId) {
        $pathVariables = [
            self::LANG_NAME => "pl_pl",
            self::THREAD_ID => $nodeId,
            self::POST_ID => $nodeId
        ];

        $postData = [
            self::SIGNATURE => $this->getSignature()
        ];

        $this->makeDeleteCURL($postData, $pathVariables);
    }

    /**
     * @return Logger
     */
    public function getLogger()
    {
        return $this->logger;
    }

    /**
     * Check if node is in private sub forum
     *
     * @param $forumId
     * @return bool
     */
    private function isNodePrivate($forumId)
    {
        $nodeList = vB_Api::instance('node')->getGuestChannels();
        return !array_key_exists($forumId, $nodeList);
    }

    /**
     * @param $nodeId
     * @return int
     */
    private function getForumId($nodeId) {
        $parentsInfo = vB_Library::instance('node')->getParents((int)$nodeId);

        for ($i = 0; $i < count($parentsInfo); $i++) {
            if($parentsInfo[$i]['nodeid'] === $parentsInfo[$i]['starter']) {
                return $parentsInfo[$i+1]['nodeid'];
            }
        }
        return $nodeId; //default
    }

    /**
     * @param $postId
     * @return string
     */
    private function getParentThreatTitle($postId) {
        $parentId = $parentId = vB_Library::instance('node')->getParents((int)$postId)[1]['parent'];
        $nodeInfo = vB_Api::instance('node')->getNode((int)$parentId);

        return $nodeInfo['title'];
}

    /**
     * Returns slug
     *
     * @param $postId
     * @return string return slug or false if failed
     */
    private function getSlug($postId)
    {
        $url = vB_Api::instance('route')->getNodeUrl($postId);
        $parsedUrl = parse_url($url);

        $urlGet = "?p=" . $postId . "#post" . $postId;
        return $parsedUrl['path'].$urlGet;
    }

    /**
     * @return string generated signature
     */
    private function getSignature()
    {
        $options = vB::getDatastore()->getValue('options');
        return sha1($options['apikey']);
    }

    /**
     * Converts array to POST fields
     *
     * @param array $array
     * @return string postfields
     */
    private function preparePostFields($array)
    {
        $params = array();

        foreach ($array as $key => $value) {
            $params[] = $key . '=' . urlencode($value);
        }

        return implode('&', $params);
    }

    /**
     * Makes CURL to Indexer with data of modified or new posts
     *
     * @param array $postData
     *      @RequestParam(required = false) Integer transinfoUserId,
     *      @RequestParam String threadTitle,
     *      @RequestParam String postContent,
     *      @RequestParam String threadSlug,
     *      @RequestParam Long postTime,
     *      @RequestParam Boolean isFirst
     *      @RequestParam String signature
     * @param array $pathVariables
     *      @PathVariable String langName,
     *      @PathVariable Integer threadId,
     *      @PathVariable Integer postId,
     *      @PathVariable Integer forumId,
     * @return String curl response
     * @return bool false if failed
     */
    private function makeAddCURL($postData, $pathVariables)
    {
        $restURL = $this->config->get("misc.indexer_endpoint");
        $url = "{$restURL}{$pathVariables[self::LANG_NAME]}/{$pathVariables[self::FORUM_ID]}/{$pathVariables[self::THREAD_ID]}/{$pathVariables[self::POST_ID]}";

        return $this->makeCURL($url, $postData, "POST");
    }

    /**
     * Classic CURL call
     *
     * @param string $url
     * @param array $postData
     * @param string $type
     * @return mixed curl response
     */
    private function makeCURL($url, $postData, $type)
    {
        $options = vB::getDatastore()->getValue('options');
        $curlConnectionTimeout = (int) Misc::getFromArray($options, 'transplaceinfo_curl_connection_timeout', 3000);
        $curlTimeout = (int) Misc::getFromArray($options, 'transplaceinfo_curl_timeout', 3000);

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT_MS => $curlTimeout,
            CURLOPT_CONNECTTIMEOUT_MS => $curlConnectionTimeout,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => $type,
            CURLOPT_POSTFIELDS => $this->preparePostFields($postData),
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_SSL_VERIFYHOST => false,
            CURLOPT_HTTPHEADER => array(
                "cache-control: no-cache",
                "content-type: application/x-www-form-urlencoded",
            ),
        ));

        $response = curl_exec($curl);
        $info = curl_getinfo($curl);
        curl_close($curl);

        $infoHttpCode = $info['http_code'];
        $infoUrl = $info['url'];

        if (empty($response)) {
            $msg = "[INDEXER CURL] backend response null " . $infoHttpCode. " - " . $infoUrl . " - " . $response     ;
            $this->logger->error($msg);
        }
        elseif ($infoHttpCode !== 200) {
            $msg = "[INDEXER CURL] ".$infoHttpCode. " - " . $infoUrl . " - " . $response;
            $this->logger->error($msg);
        }

        return $response;
    }

    /**
     * Makes CURL to Indexer with data of deleted posts
     *
     * @param array $postData only signature
     *      @RequestParam String signature
     * @param array $pathVariables
     *      @PathVariable String langName,
     *      @PathVariable Integer threadId,
     *      @PathVariable Integer postId,
     * @return String curl response
     * @return bool false if failed
     */
    private function makeDeleteCURL($postData, $pathVariables)
    {
        $restURL = $this->config->get("misc.indexer_endpoint");
        $url = "{$restURL}{$pathVariables[self::LANG_NAME]}/{$pathVariables[self::THREAD_ID]}/{$pathVariables[self::POST_ID]}?signature={$postData['signature']}";

        return $this->makeCURL($url, array(), "DELETE");
    }
}