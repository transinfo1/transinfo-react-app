<?php

namespace Transinfobridge\lib;

const TRANSINFO_AUTH_COOKIE = 'forumAuth';

use vB;

class AuthData
{
    /**
     * @var string
     */
    private $accessToken;

    /**
     * @var int
     */
    private $cmsUserId;

    /**
     * @var string
     */
    private $provider;

    /**
     * UserData constructor.
     */
    public function __construct()
    {
        $this->accessToken = null;
        $this->provider = null;
        $this->cmsUserId = null;
    }

    /**
     * @return mixed
     */
    public function getAccessToken()
    {
        return $this->accessToken;
    }

    /**
     * @return mixed
     */
    public function getCmsUserId()
    {
        return $this->cmsUserId;
    }

    /**
     * @return mixed
     */
    public function getProvider()
    {
        return $this->provider;
    }

    /**
     * Reads basic user auth data from cookies, which are set by FE after login operation
     */
    public function readFromCookies()
    {
        if(isset($_COOKIE[TRANSINFO_AUTH_COOKIE])) {
            $cookieAuthData = urldecode( $_COOKIE[TRANSINFO_AUTH_COOKIE]);
            $result = json_decode($cookieAuthData, true);

            if($result) {
                $this->accessToken = $result['accessToken'];
                $this->cmsUserId = $result['userId'];
                $this->provider= $result['provider'];
            }
        }
    }

    /**
     * Checks if cookies were set in front end and the load operation has completed successfully
     *
     * @return bool
     */
    public function isCookiesLoaded()
    {
        return !(empty($this->accessToken) || empty($this->cmsUserId) || empty($this->provider) || $this->accessToken === "null");
    }

    /**
     * Refresh auth token and reset cookie value
     *
     * @param string $newAccessToken
     */
    public function setToken($newAccessToken)
    {
        $this->accessToken = (string)$newAccessToken;

        $options = vB::getDatastore()->getValue('options');
        $cookieLifetime = (int) Misc::getFromArray($options, 'transinfobridge_cookie_lifetime', 3600);

        setcookie("forumAuth", $this->convertToJson(), time()+ $cookieLifetime,"/", $_SERVER['SERVER_NAME'], true, false);
    }

    /**
     * @return string Json
     */
    private function convertToJson()
    {
        return json_encode(array(
                'accessToken'  => $this->accessToken,
                'provider'     => $this->provider,
                'userId'       => $this->cmsUserId
            ));
    }
}