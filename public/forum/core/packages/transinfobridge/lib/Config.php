<?php

namespace Transinfobridge\lib;

use RuntimeException;
use Transinfobridge\lib\Misc;

require_once 'Misc.php';

class Config
{
    /**
     * @var array
     */
    private $config = [];

    /**
     * @var Config
     */
    private static $instance;

    const BACKEND_URL_ENV = 'FORUMBRIDGE_BACKEND_URL';
    const FRONTEND_URL_ENV = 'FORUMBRIDGE_FRONTEND_URL';
    const ADMIN_MD5_ENV = 'FORUMBRIDGE_ADMIN_PWD_MD5';
    const ADMIN_WHITELIST_ENV = 'FORUMBRIDGE_ADMINCP_WHITELIST';
    const LOG_LEVEL = 'ERROR';

    /**
     * SessionMemcache constructor.
     */
    private function __construct()
    {
        $backendUrl = getenv(self::BACKEND_URL_ENV);
        $frontendUrl = getenv(self::FRONTEND_URL_ENV);
        $adminMD5 = getenv(self::ADMIN_MD5_ENV);
        $adminWhiteListString = getenv(self::ADMIN_WHITELIST_ENV);

        if(empty($backendUrl) || empty($frontendUrl) || empty($adminMD5) || empty($adminWhiteListString))
        {
            $logger = Misc::getLogger();
            $logger->fatal("[Config] Server environment variables aren't configured");
            throw new RuntimeException("Missing 'transinfobridge' config environment variables.");
        }

        $configData = array(
            "session" => [
                "profile_endpoint"      => $backendUrl.'/users/profile/',
                "get_refresh_endpoint"  => $backendUrl.'/users/refreshAccessToken/',
                "post_refresh_endpoint" => $backendUrl.'/token/refresh/',
            ],

            "transinfo" => [
                "login_page"            => $frontendUrl.'/#login',
                "logout_page"           => $frontendUrl.'/#logout',
                "profile_page"          => $frontendUrl.'/user/profile',
                "main_page"             => $frontendUrl,
                "register_page"         => $frontendUrl.'/#register'
            ],

            "misc" => [
                "indexer_endpoint"      => $backendUrl.'/forum/index/',
                "log_level"             => self::LOG_LEVEL,
                "admin_md5"             => $adminMD5,
                "admincp_whitelist"     => explode('|', $adminWhiteListString)
            ]
        );

        $this->addConfigData($configData);
    }

    /**
     * @return Config
     */
    public static function getInstance()
    {
        if (!static::$instance) {
            static::$instance = new static();
        }
        return static::$instance;
    }

    /**
     * @param array $data
     * @return Config
     */
    public function addConfigData(array $data)
    {
        $this->config = array_replace_recursive($this->config, $data);
        return $this;
    }

    /**
     * @param string $path
     * @param string $default
     * @return mixed
     */
    public function get($path, $default = null)
    {
        return Misc::getFromArray($this->config, $path, $default);
    }
}
