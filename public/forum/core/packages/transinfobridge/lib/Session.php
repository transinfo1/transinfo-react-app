<?php

namespace Transinfobridge\lib;

use Logger;
use vB;
use vB5_Auth;
use vB5_Cookie;
use vB_User;
use vB_Input_Cleaner;

class Session
{
    /**
     * @var Config
     */
    private $config;

    /**
     * @var Logger
     */
    private $logger;

    /**
     * @var vB_Input_Cleaner
     */
    private $input;

    /**
     * @var int
     */
    private $curlConnectionTimeout;

    /**
     * @var int
     */
    private $curlTimeout;

    /**
     * @var string
     */
    private $logPrefix = "[TOKEN]";

    const ACCESS_TOKEN = 'accessToken';
    const ERROR = 'error';

    /**
     * SessionMemcache constructor.
     *
     * @param Config $config
     * @param Logger $logger
     * @param vB_Input_Cleaner $input
     */
    public function __construct(Config $config, Logger $logger, vB_Input_Cleaner $input)
    {
        $this->config = $config;
        $this->logger = $logger;
        $this->input = $input;

        $options = vB::getDatastore()->getValue('options');
        $this->curlConnectionTimeout = (int) Misc::getFromArray($options, 'transplaceinfo_curl_connection_timeout', 3000);
        $this->curlTimeout = (int) Misc::getFromArray($options, 'transplaceinfo_curl_timeout', 3000);
    }

    /**
     * Makes redirect to Front-End main page
     */
    public function redirectToMainPage() {
        exec_header_redirect($this->config->get('transinfo.main_page'), 307);
    }

    /**
     * Clears entry in session table and deletes cookies
     *
     * @param AuthData $authData
     * @param bool $setLogoutCookie
     */
    public function processLogout($authData, $setLogoutCookie = true) {
        vB_User::processLogout();
        if($setLogoutCookie === true) {
            $authData->setToken('null');
            $msg = "[LOGOUT] Processed logout for userId: {$this->getCurrentUserForumId()}";
            $this->logger->debug($msg);
        }
    }

    /**
     * Create session for given user ID and log in forum.
     *
     * @param int $forumUserId
     */
    public function processNewLogin($forumUserId)
    {
        $result = vB_User::processNewLogin(['userid' => $forumUserId]);

        vB5_Auth::setLoginCookies($result, '', false);

        //do not redirect implicit bridge calls
        if($_SERVER['REQUEST_METHOD'] === 'GET') {
            $this->redirectToCurrentPage();
        }
    }

    /**
     * Return read model user data for given user id
     *
     * @param AuthData $authData
     * @return array | bool
     */
    public function getReadModelUserData($authData)
    {
        $profileResponse = $this->makeProfileCURLCall($authData);
        if(isset($profileResponse[self::ERROR]) || empty($profileResponse)) {
            if ($profileResponse['message'] === 'Access Denied' && $profileResponse['status'] === 403 ) {
                switch ($authData->getProvider()) {
                    case 'wordpress':
                        $refreshResponse = $this->makeWordpressTokenRefreshCall($authData->getAccessToken());
                        break;
                    case 'facebook':
                        $refreshResponse = array(self::ACCESS_TOKEN => '');
                        break;
                    case 'google':
                    case 'onelogin':
                        $refreshResponse = $this->makeGoogleAndOneloginTokenRefreshCall($authData->getAccessToken(), $authData->getProvider());
                        break;
                    default:
                        $this->logger->error("{$this->logPrefix} Invalid provider: {$authData->getProvider()}");
                        $refreshResponse = null;
                        break;
                }
                if(isset($refreshResponse[self::ERROR]) || empty($refreshResponse) || empty($refreshResponse[self::ACCESS_TOKEN])){
                    $msg = "{$this->logPrefix} Token refresh fail, CmsUserId: {$authData->getCmsUserId()}, Provider: {$authData->getProvider()}, Error Msg: ". Misc::multi_implode($refreshResponse,", ");
                    $this->logger->debug($msg);
                    $this->processLogout($authData);
                    return false;
                }
                elseif(isset($refreshResponse[self::ACCESS_TOKEN])) { //granted new access token
                    $msg = "{$this->logPrefix} Granted new access token, CmsUserId:{$authData->getCmsUserId()}, ForumUserId: {$this->getCurrentUserForumId()}, Provider:{$authData->getProvider()}, RefreshResponse: {$refreshResponse[self::ACCESS_TOKEN]}";
                    $this->logger->debug($msg);
                    $authData->setToken($refreshResponse[self::ACCESS_TOKEN]);
                    $profileResponse = $this->makeProfileCURLCall($authData);
                }
            } else {
                $msg = "{$this->logPrefix} Access Denied, CmsUserId:{$authData->getCmsUserId()}, ForumUserId: {$this->getCurrentUserForumId()}, Provider:{$authData->getProvider()}";
                $this->logger->debug($msg);
                $this->processLogout($authData);
                return false;
            }
        }
        return $profileResponse;
    }

    /**
     * Make CURL call to profile rest
     *
     * @param AuthData $authData
     * @return array
     */
    private function makeProfileCURLCall($authData) {
        $curlOptions = array(
//            CURLOPT_URL => $this->config->get('session.profile_endpoint').$authData->getCmsUserId(),
            CURLOPT_URL => $this->config->get('session.profile_endpoint').'me',
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "authorization: ".$authData->getAccessToken(),
                "x-provider: ".$authData->getProvider()
            ),
        );
        return $this->executeCURL($curlOptions);
    }

    /**
     * Make CURL call to refresh tokens rest
     *
     * @param string $accessToken
     * @return array
     */
    private function makeWordpressTokenRefreshCall($accessToken) {
        $curlOptions = array(
            CURLOPT_URL => $this->config->get('session.get_refresh_endpoint').$accessToken,
            CURLOPT_CUSTOMREQUEST => "GET",
        );
        return $this->executeCURL($curlOptions);
    }

    /**
     * Make CURL call to refresh tokens rest
     *
     * @param string $accessToken
     * @param string $provider
     * @return array
     */
    private function makeGoogleAndOneloginTokenRefreshCall($accessToken, $provider) {
        $curlOptions = array(
            CURLOPT_URL => $this->config->get('session.post_refresh_endpoint'),
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "token={$accessToken}&provider={$provider}",
        );
        return $this->executeCURL($curlOptions);
    }

    /**
     * @return int
     */
    public function getCurrentUserForumId()
    {
        return (int) vB::getCurrentSession()->fetch_userinfo()['userid'];
    }

    /**
     * Check is current session a guest session.
     *
     * @return bool
     */
    public function isGuestSession()
    {
        return $this->getCurrentUserForumId() === 0;
    }

    /**
     * Executes CURL, handles CURL errors, returns response as array
     * CURLOPT_URL and CURLOPT_CUSTOMREQUEST are required
     * CURLOPT_HTTPHEADER and CURLOPT_POSTFIELDS are optional
     *
     * @param array $curlOptions
     * @return array CURL response
     */
    private function executeCURL($curlOptions)
    {
        //preparing data
        $curlParsedOptions = array(
            CURLOPT_URL => $curlOptions[CURLOPT_URL],
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT_MS => $this->curlTimeout,
            CURLOPT_CONNECTTIMEOUT_MS => $this->curlConnectionTimeout,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => $curlOptions[CURLOPT_CUSTOMREQUEST],
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_SSL_VERIFYHOST => false
        );

        $httpHeader = array();
        $httpHeader[0] = "cache-control: no-cache";

        if($curlOptions[CURLOPT_CUSTOMREQUEST] === "POST") {
            if(isset($curlOptions[CURLOPT_POSTFIELDS])) {
                $curlParsedOptions[CURLOPT_POSTFIELDS] = $curlOptions[CURLOPT_POSTFIELDS];
            }
            $httpHeader[1] = "content-type: application/x-www-form-urlencoded";
        }

        if(isset($curlOptions[CURLOPT_HTTPHEADER]) && is_array($curlOptions[CURLOPT_HTTPHEADER])) {
            $httpHeader = array_merge($httpHeader, $curlOptions[CURLOPT_HTTPHEADER]);
        }

        $curlParsedOptions[CURLOPT_HTTPHEADER] = $httpHeader;

        //////////////////////
        /// curl execution ///
        //////////////////////

        $curl = curl_init();

        curl_setopt_array($curl, $curlParsedOptions);

        $response = curl_exec($curl);
        $info = curl_getinfo($curl);
        curl_close($curl);

        $infoHttpCode = $info['http_code'];
        $infoUrl = $info['url'];

        if (empty($response)) {
        $msg = "[SESSION CURL] backend response null " . $infoHttpCode . " - " . $infoUrl . " - " . $response;
            $this->logger->error($msg);
            $response['message'] = "Access Denied";
            $response['status'] = $infoHttpCode;
            return array(self::ACCESS_TOKEN => '');
        }
        elseif ($infoHttpCode !== 200) {
            $msg = "[SESSION CURL] ".$infoHttpCode. " - " . $infoUrl . " - " . $response;
            if($infoHttpCode == 403) { // its always happen when token require refresh
                $this->logger->debug($msg);
            } else {
                $this->logger->error($msg);
            }
        }

        $json = $this->convertJSONToArray($response);

        return empty($json) ? array(self::ACCESS_TOKEN => $response) : $json;
    }

    /**
     * Convert JSON response to Array. If TransInfo return unknown format (not json), probably there is problem
     * with session verification endpoint. This problem will be logged and empty array returned.
     * Here we also logged in any other problems like no access (wrong authorization), or response different than
     * status code 200..
     *
     * @param string $responseJSON Json response from TransInfo
     * @return array
     */
    private function convertJSONToArray($responseJSON)
    {
        $response = json_decode($responseJSON);
        if (json_last_error() !== JSON_ERROR_NONE) {
            return array();
        }
        return (array) $response;
    }

    /**
     * Reload user page.
     * We have to set current site to $_POST because vB mechanism work in this way.
     * Whet more we need `exit` at end because vB do not handle this.
     */
    private function redirectToCurrentPage()
    {
        $_POST['url'] = base64_encode($this->getCurrentUri());
        if (Misc::isLoginWidgetPage() && $this->isReferrerSameHost()) {
            $_POST['url'] = base64_encode($this->getCurrentReferrer());
        }
        vB5_Auth::doLoginRedirect();
        exit;
    }

    /**
     * Return current page URI.
     * We need this information when we autologin user and reload page.
     *
     * @return string
     */
    private function getCurrentUri()
    {
        $isSecure = $this->input->fetch_server_value('HTTPS') == 'on';
        $serverProtocol = strtolower($this->input->fetch_server_value('SERVER_PROTOCOL'));
        $protocol = substr($serverProtocol, 0, strpos($serverProtocol, '/')) . (($isSecure) ? 's' : '');

        $requestUri = $this->input->fetch_server_value('REQUEST_URI');

        return $protocol . '://' . $this->getCurrentHost() . $requestUri;
    }

    /**
     * @return string
     */
    private function getCurrentHost()
    {
        $isSecure = $this->input->fetch_server_value('HTTPS') == 'on';

        $port = $this->input->fetch_server_value('SERVER_PORT');
        $port = ((!$isSecure && $port == '80') || ($isSecure && $port == '443')) ? '' : ':' . $port;

        $host = $this->input->fetch_server_value('HTTP_HOST');
        return isset($host) ? $host : $this->input->fetch_server_value('SERVER_NAME') . $port;
    }

    /**
     * @return bool
     */
    private function isReferrerSameHost()
    {
        return strpos($this->getCurrentReferrer(), $this->getCurrentHost()) !== false;
    }

    /**
     * @return bool|string
     */
    private function getCurrentReferrer()
    {
        return $this->input->fetch_server_value('HTTP_REFERER');
    }
}
