<?php

namespace Transinfobridge\lib;

use vB;
use vB_Request_Web;
use vB_User;

/** @var Config $config */
$config = Config::getInstance();

/**
 * Redirect address map, from source to destiny.
 */
$map = [
    '/forum/settings/profile'      => $config->get('transinfo.profile_page'),
    '/forum/auth/logout'           => $config->get('transinfo.logout_page'),
    '/forum/register'              => $config->get('transinfo.register_page'),
];

/**
 * Get request and redirect to required page in TransInfo.
 */
$request = vB::getRequest();
if ($request instanceof vB_Request_Web) {
    $path = $request->getVbUrlPath();
    if (isset($map[$path])) {
        $url = $map[$path];
        exec_header_redirect($url, 301);
    }
}
