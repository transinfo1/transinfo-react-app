<?php

namespace Transinfobridge\lib;

use vB_Database;

/**
 * Class DbUsers
 *
 * @package Transinfobridge\lib
 */
class DbUsers
{
    const TABLE_VB_USERS = 'user';
    const FIELD_VB_ACCOUNT_ID = 'userid';
    const FIELD_VB_ACCOUNT_EMAIL = 'email';

    const TABLE_MAP_USERS = 'transinfo_user';
    const FIELD_MAP_CMS_ID = 'wordpress_id';
    const FIELD_MAP_FORUM_ID = 'forum_userid';

    // Custom field name in VBulletin, where we also keep Account ID from registration request.
    const CMS_ID_CUSTOM_FIELD = 'field5';
    const TRANSPLACE_ID_CUSTOM_FIELD = 'field6';
    const FIRST_NAME_CUSTOM_FIELD = 'field7';
    const LAST_NAME_CUSTOM_FIELD = 'field8';

    //Save basic fields
    const SAVE_USER_ID = 'userid';
    const SAVE_PASS = 'password';
    const SAVE_USER = 'user';
    const SAVE_USERFIELD = 'userfield';

    const TRANSEU_GROUP_ID = 14;//first free number after standard usergroups

    /**
     * @var vB_Database
     */
    protected $database;

    /**
     * DbUsers constructor.
     *
     * @param vB_Database $database
     */
    public function __construct(vB_Database $database)
    {
        $this->database = $database;
    }

    /**
     * Return forum user id (from mapping table) for given cms user id. If user do not exists return null.
     *
     * @param int $cmsUserId
     * @param bool $checkIfExist
     * @return int | null
     */
    public function getForumUserId($cmsUserId, $checkIfExist = false)
    {
        // Perform action only when we request possible account id
        $cmsUserId = intval($cmsUserId);
        if ($cmsUserId > 0) {
            // Prepare arguments and parameters
            $table = $this->getTable(self::TABLE_MAP_USERS);
            $fieldForumUserId = "{$table}." . self::FIELD_MAP_FORUM_ID;
            $fieldCmsUserId = "{$table}." . self::FIELD_MAP_CMS_ID;

            // Run query
            $query = "SELECT {$fieldForumUserId} FROM {$table} WHERE {$fieldCmsUserId}  = '{$cmsUserId}';";
            $result = $this->database->query_first($query);

            // Check query results
            if ($result && isset($result[self::FIELD_MAP_FORUM_ID])) {
                $userId = (int)$result[self::FIELD_MAP_FORUM_ID];
                if(!$checkIfExist || $this->userExist($userId)) {
                    return $userId;
                }else{
                    return 0;
                }
            }
        }
        return 0;
    }

    /**
     * Checks if userid exist in user table
     *
     * @param $userId
     * @return bool
     */
    private function userExist($userId)
    {
        if($userId>0)
        {
            $table = self::TABLE_VB_USERS;

            $column = self::SAVE_USER_ID;

            $query = "SELECT {$column} FROM `{$table}` WHERE {$column}  = '{$userId}';";
            $result = $this->database->query_first($query);
            return (!empty($result) && isset($result[self::FIELD_VB_ACCOUNT_ID]));
        }
        return false;
    }

    /**
     * Return cms user (from mapping table) id for given forum id. If user do not exists return null.
     *
     * @param int $forumUserId
     * @return int | null
     */
    public function getCmsUserId($forumUserId)
    {
        // Perform action only when we request possible account id
        $forumUserId = intval($forumUserId);
        if ($forumUserId > 0) {
            // Prepare arguments and parameters
            $table = $this->getTable(self::TABLE_MAP_USERS);
            $fieldForumUserId = "{$table}." . self::FIELD_MAP_FORUM_ID;
            $fieldCmsUserId = "{$table}." . self::FIELD_MAP_CMS_ID;

            // Run query
            $query = "SELECT {$fieldCmsUserId} FROM {$table} WHERE {$fieldForumUserId}  = '{$forumUserId}';";
            $result = $this->database->query_first($query);

            // Check query results
            if ($result && isset($result[self::FIELD_MAP_CMS_ID])) {
                return (int) $result[self::FIELD_MAP_CMS_ID];
            }
        }
        return 0;
    }

    /**
     * Return forum user id (from vb user table) for given email. If user do not exists return null.
     *
     * @param string $accountEmail
     * @return int
     */
    public function getForumUserIdByEmail($accountEmail)
    {
        if ($accountEmail != null) {
            // Prepare arguments and parameters
            $table = $this->getTable(self::TABLE_VB_USERS);
            $fieldEmail = "{$table}." . self::FIELD_VB_ACCOUNT_EMAIL;
            $fieldForumUserId = "{$table}." . self::FIELD_VB_ACCOUNT_ID;

            // Run query
            $query = "SELECT {$fieldForumUserId} FROM {$table} WHERE {$fieldEmail}  = '{$accountEmail}';";
            $result = $this->database->query_first($query);

            // Check query results
            if ($result && isset($result[self::FIELD_VB_ACCOUNT_ID])) {
                return (int) $result[self::FIELD_VB_ACCOUNT_ID];
            }
        }
        return 0;
    }

    /**
     * Creates mapping for forum user, maps forum user and cms user
     *
     * @param int $forumUserId
     * @param int $cmsUserId
     * @return bool
     */
    public function connectAccount($forumUserId, $cmsUserId)
    {
        //check if mapping doesn't exist
        if( ($oldAccountId = $this->getForumUserId($cmsUserId)) != 0)
        {
            $this->disconnectAccount($oldAccountId);
        }

        if( $this->getCmsUserId($forumUserId) != 0)
        {
            $this->disconnectAccount($forumUserId);
        }

        if ($forumUserId > 0 && $cmsUserId > 0) {

            // Table name
            $table = $this->getTable(self::TABLE_MAP_USERS);

            // Keys
            $keys = [self::FIELD_MAP_FORUM_ID, self::FIELD_MAP_CMS_ID];

            // Values
            $values = [$forumUserId, $cmsUserId];

            // Run query
            $query = "INSERT INTO {$table} (" . implode(',', $keys) . ") VALUES (" . implode(',', $values) . ");";

            return $this->database->query_write($query);

        }
        return false;
    }

    /**
     * Deletes mappings related to user
     *
     * @param int $forumUserId
     * @return bool
     */
    private function disconnectAccount($forumUserId)
    {
        if ($forumUserId > 0) {

            $table = $this->getTable(self::TABLE_MAP_USERS);
            $column = self::FIELD_MAP_FORUM_ID;

            // Run query
            $query = "DELETE FROM {$table} WHERE {$column} = {$forumUserId};";

            return $this->database->query_write($query);
        }
        return false;
    }

    /**
     * Return table name with prefix.
     *
     * @param string $table
     * @return string
     */
    protected function getTable($table)
    {
        return TABLE_PREFIX . $table;
    }
}
