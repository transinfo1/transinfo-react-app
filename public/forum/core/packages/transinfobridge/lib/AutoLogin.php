<?php

namespace Transinfobridge\lib;

use Logger;
use vB5_Auth;
use vB5_Cookie;
use vB_Api;
use vB_User;
use vB_Api_User;
use Api_InterfaceAbstract;
use vB;

/**
 * Class AutoLogin
 *
 * @package Transinfobridge\lib
 */
class AutoLogin
{
    /**
     * @var Session
     */
    private $session;
    /**
     * @var DbUsers
     */
    private $dbUsers;

    /**
     * @var Logger
     */
    private $logger;

    /**
     * @var Config
     */
    private $config;

    const REGISTER_LOG_PREFFIX = '[REGISTER]';

    /**
     * Autologin constructor.
     *
     * @param Session $session
     * @param DbUsers $dbUsers
     * @param Logger $logger
     */
    public function __construct(Session $session, DbUsers $dbUsers, Logger $logger)
    {
        $this->session = $session;
        $this->dbUsers = $dbUsers;
        $this->logger = $logger;
        $this->config = Config::getInstance();
    }

    /**
     * Auto login user currently not logged in to VBulletin forum.
     *
     * @param AuthData $authData
     */
    public function login($authData)
    {
        /** Check if auth data from cookies exist */
        if ($authData->isCookiesLoaded()) {

            /** Check is authData (cookies) is ok by making profile request to API */
            $accountData = $this->session->getReadModelUserData($authData);
            if (empty($accountData)) {
                $msg = "[ACCESS] accountData was empty  - cms user: {$authData->getCmsUserId()}";
                $this->logger->debug($msg);
                return;
            }

            $accountDataDetails = (array)$accountData['rpcUserDTO'];    // details of user profile
            $cmsUserId = $accountData['id'];                            // sometimes user profile object can be null,
            $forumUserId = $this->dbUsers->getForumUserId($cmsUserId, true);

            /** if forum id is equal 0, this means that the user does not exist in the forum database, so we can register him */
            if ($forumUserId === 0) {
                /**
                 * prevent registering user in forum if profile is not filled,
                 * instead force user to fill profile info by redirect
                 */
                if ($accountDataDetails['profileFilled'] == false) {
                    $this->session->redirectToMainPage();
                }
                /** If user dose not exist, create new user and update his forum id*/
                $forumUserId = $this->registerNewUser($accountData);
            }

            /** Setup user language */
            $this->setLoggedInUserLanguage($accountDataDetails['langName']);

            // Log login message
            $msg = "[LOGIN] Logged in - CmsUserId:{$cmsUserId}, ForumUserId:{$forumUserId}, CurrentForumUserId:{$this->session->getCurrentUserForumId()} Provider:{$authData->getProvider()}";
            $this->logger->debug($msg);

            /** Process VBulletin log in */
            $this->session->processNewLogin($forumUserId);
        } else {
            $userIdMsg = ($this->session->getCurrentUserForumId() == 0) ? "" : " - CurrentForumUserId:{$this->session->getCurrentUserForumId()}";
            $msg = "[LOGIN] Auth cookies not exist or null" . $userIdMsg . ", IP: {$_SERVER['REMOTE_ADDR']}";
            $this->logger->debug($msg);
        }
    }

    /**
     * Check user access to VBulletin when user is currently logging in
     * @param AuthData $authData
     */
    public function checkUserAccess($authData)
    {
        /** Checks if auth data from cookies exist */
        if ($authData->isCookiesLoaded()) {

            $cmsUserID = $authData->getCmsUserId();
            $forumUserID = $this->dbUsers->getForumUserId($cmsUserID);

            /** check is authData (cookies) is ok by making profile request to API */
            $accountData = $this->session->getReadModelUserData($authData);
            if (!empty($accountData)) {

                /** Check if the currently logged in user has the same id as the user id in cookie and cookies data is valid*/
                if ($forumUserID !== $this->session->getCurrentUserForumId()) {
                    $msg = "[ACCESS] - Change users - ForumUserId:{$this->session->getCurrentUserForumId()} to ForumUserId:{$forumUserID}, CmsUserId:{$cmsUserID}";
                    $this->logger->debug($msg);

                    $this->session->processLogout($authData,false);
                    $this->login($authData);
                }
            } else {
                $msg = "[ACCESS] accountData was empty  - cms user: {$authData->getCmsUserId()}";
                $this->logger->debug($msg);
                return;
            }
        } else {
            $userIdMsg = ($this->session->getCurrentUserForumId() == 0) ? "" : " - CurrentForumUserId:{$this->session->getCurrentUserForumId()}";
            $msg = "[ACCESS] Auth cookies not exist or null" . $userIdMsg . ", IP: {$_SERVER['REMOTE_ADDR']}";
            $this->logger->debug($msg);
            $this->session->processLogout($authData);
        }
    }

    /**
     * Setup correct user session in VBulletin forum
     *
     * @param $authData
     */
    public function setCorrectUserSession($authData)
    {
        if ($this->session->isGuestSession()) {
            $this->login($authData);
        } else {
            $this->checkUserAccess($authData);
        }
    }

    /**
     * Set correct cookie with user language version.
     *
     * @param string $requiredLanguage Correct language code from TransInfo (en, pl, lt, etc.)
     */
    public function setLoggedInUserLanguage($requiredLanguage)
    {
        static $languageID = 'languageid';

        /** @var \vB_Api_Language $languageApi */
        $languageApi = vB_Api::instance('language');
        foreach ($languageApi->fetchAll() as $language) {
            if (isset($language['languagecode']) && $language['languagecode'] === $this->convertLanguageToForumFormat($requiredLanguage)) {
                vB5_Cookie::set($languageID, $language[$languageID], 365, false);
                return;
            }
        }
        //set default language
        vB5_Cookie::set($languageID, "en", 365, false);
    }

    /**
     * @param array $accountData
     * @return int userForumID
     */
    private function registerNewUser($accountData)
    {
        $rpcUserDTO = (array)$accountData['rpcUserDTO'];
        $username = $rpcUserDTO['name'];
        $firstName = $rpcUserDTO['firstName'];
        $lastName = $rpcUserDTO['lastName'];
        $transID = $accountData['transId'];
        $email = $accountData['email'];

        if ($firstName != null && $lastName != null) {
            $username = "{$firstName} {$lastName} ({$rpcUserDTO['id']})";
        }

        $postdata = array(
            'username' => $username,
            'email' => $email
        );
        if ($transID != null && $transID != "") {
            $usergroupid = array(
                'usergroupid' => DbUsers::TRANSEU_GROUP_ID
            );
            $postdata = array_merge($postdata, $usergroupid);
        }

        $data = array(
            DbUsers::SAVE_USER_ID => 0,
            DbUsers::SAVE_PASS => hash("sha1", $email . rand()),
            DbUsers::SAVE_USER => $postdata,
            array(),
            array(),
            DbUsers::SAVE_USERFIELD => array(
                DbUsers::CMS_ID_CUSTOM_FIELD => $accountData['id'],
                DbUsers::TRANSPLACE_ID_CUSTOM_FIELD => $transID,
                DbUsers::FIRST_NAME_CUSTOM_FIELD => $firstName,
                DbUsers::LAST_NAME_CUSTOM_FIELD => $lastName
            ),
            array(),
            '', //verify that human verify is disable in admincp
            array('registration' => true),
        );

        $abort = false;

        vB::getHooks()->invoke('hookRegistrationBeforeSave', array(
            'this' => $this,
            'data' => &$data,
            'abort' => &$abort,
        ));

        if ($abort) {
            $msg = self::REGISTER_LOG_PREFFIX . " - ForumUserId: {$data[DbUsers::SAVE_USER_ID]}, Hook abort called, unknown problem";
            $this->logger->fatal($msg);
        } else {
            $response = $this->saveUserData($data); // Save the data

            if (isset($response[Misc::ERROR_FIELD])) {
                /** if user exist in user table try only map account and merge wordpress data with vb data */
                if (Misc::recursive_array_search('emailtaken', $response['errors'])) {
                    return $this->mergeAccount($data, $email, $accountData['id']);
                }

                $msg = self::REGISTER_LOG_PREFFIX . " - ForumUserId: {$data[DbUsers::SAVE_USER_ID]}, Error Msg: ".Misc::multi_implode($response, ", ");
                $this->logger->fatal($msg);

            } else {
                return (int)$response;
            }
        }
        return -1;
    }

    /**
     * @param $dataToSave
     * @param $cmsEmail
     * @param $cmsID
     * @return int
     * @internal param $cmsData
     */
    private function mergeAccount($dataToSave, $cmsEmail, $cmsID)
    {
        $userForumID = $this->dbUsers->getForumUserIdByEmail($cmsEmail);

        //If user is super admin, skip merge
        if($userForumID == 1){
            return $userForumID;
        }

        if ($this->dbUsers->connectAccount($userForumID, $cmsID)) {
            $this->logger->debug(self::REGISTER_LOG_PREFFIX . " Created mapping for VBUser: {$userForumID}");
            $dataToSave[DbUsers::SAVE_USER_ID] = $userForumID;
            $dataToSave[4] = array();//adminoptions

            //login admin to make merge and update user data
            $this->saveUserData($dataToSave);

        } else {
            $this->logger->fatal(self::REGISTER_LOG_PREFFIX . " Cannot map user: {$userForumID}");
        }
        return $userForumID;
    }

    /**
     * @param array $data
     */
    private function saveUserData($data)
    {
        $saveApi = Api_InterfaceAbstract::instance();
        $userApi = vB_Api::instance('user');

        $passwords = [
            'password' => '',
            'md5password' => $this->config->get('misc.admin_md5'),
            'md5password_utf' => ''
        ];

        $loginResponse = $userApi->login2('admin', $passwords, '', 'cplogin');
        if(isset($loginResponse[Misc::ERROR_FIELD]))
        {
            $this->logger->error(self::REGISTER_LOG_PREFFIX . "Wrong admin configuration, user data wont't be updated");
        }
        $result = $saveApi->callApi('user', 'save', $data);
        //logout admin
        vB_User::processLogout();

        return $result;
    }

    /**
     * @param string $language
     * @return string
     */
    private function convertLanguageToForumFormat($language)
    {
        return substr($language, 0, strpos($language, '_'));
    }
}
