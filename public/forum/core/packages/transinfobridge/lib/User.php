<?php

namespace Transinfobridge\lib;

use vB;

class User
{
    /**
     * Determine user name changed.
     */
    const FIELD_HAS_USER_NAME_CHANGED = 'field7';

    /**
     * Field name with user id.
     */
    const FIELD_USER_ID = 'userid';

    /**
     * Guest user id.
     */
    const VALUE_GUEST_ACCOUNT = 0;


    /**
     * @return array
     */
    public static function getLoggedInUserData()
    {
        $session = vB::getCurrentSession();

        if (empty($session)) {
            return [];
        }

        return $session->fetch_userinfo();
    }

    /**
     * @param array $userData
     * @return bool
     */
    public static function hasUserNameChanged(array $userData = null)
    {
        if ($userData === null) {
            $userData = static::getLoggedInUserData();
        }

        $fieldName = static::FIELD_HAS_USER_NAME_CHANGED;
        return isset($userData[$fieldName]) && $userData[$fieldName] === '1';
    }

    /**
     * @param array|null $userData
     * @return bool
     */
    public static function isGuest(array $userData = null)
    {
        if ($userData === null) {
            $userData = static::getLoggedInUserData();
        }

        $fieldName = static::FIELD_USER_ID;
        return isset($userData[$fieldName]) && $userData[$fieldName] === static::VALUE_GUEST_ACCOUNT;
    }
}
