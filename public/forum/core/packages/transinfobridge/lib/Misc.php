<?php

namespace Transinfobridge\lib;

use vB;
use vB_Request_Web;

/**
 * Class Misc
 *
 * @package Transinfobridge\lib
 */
class Misc
{
    const PACKAGE_NAME = 'transinfobridge';

    const ERROR_FIELD = 'errors';

    /**
     * Check if TransInfo bridge is turned on and installed.
     * To do this get list of all installed products and iterate through it to find `static::PACKAGE_NAME` package
     * and this package must be active.
     *
     * @return bool
     */
    public static function isTransInfoProductEnabled()
    {
        foreach (vB::getDatastore()->getValue('products') as $product => $status) {
            if ($product === static::PACKAGE_NAME && (int) $status === 1) {
                return true;
            }
        }

        return false;
    }


    /**
     * Returns log4php logger relative to config file
     *
     * @return \Logger
     */
    public static function getLogger() {
        $level = Config::LOG_LEVEL;
        if($level === 'DEBUG') {
            return vB::getLogger('Global_debug');
        } else {
            return vB::getLogger('Global');
        }
    }

    /**
     * Check if we are in admin control panel and redirect if your ip is not on whitelist.
     *
     * @return bool
     */
    public static function isAdminControlPanel()
    {
//        $config = Config::getInstance();
        $isAdmincp = strpos(vB::getRequest()->getScriptPath(), 'admincp/') !== false;

//        if ($isAdmincp) {
//            /** @var array $ipArray */
//            $ipArray = $config->get('misc.admincp_whitelist');
//            foreach ($ipArray as $item) {
//                $remoteIpClassExp = explode('.', $_SERVER['REMOTE_ADDR']);
//                $remoteIpClass = $remoteIpClassExp[0] . '.' . $remoteIpClassExp[1];
//                if($item === $remoteIpClass) {
//                    return $isAdmincp;
//                }
//            }
//            exec_header_redirect('/forum', 307);
//
//        }
        return $isAdmincp;
    }

    /**
     * Check if we are not load scripts from login widget page.
     *
     * @return bool
     */
    public static function isLoginWidgetPage()
    {
        return strpos(vB::getRequest()->getScriptPath(), 'auth/login-form') !== false;
    }

    /**
     * Return value from array using dot notation.
     *
     * @param array $array
     * @param string $value
     * @param mixed $default
     * @return array|mixed|null
     */
    public static function getFromArray(array $array, $value, $default = null)
    {
        if (!empty($value) && is_string($value)) {
            $keys = explode('.', $value);
            foreach ($keys as $key) {
                if (isset($array[$key])) {
                    $array = $array[$key];
                } else {
                    return $default;
                }
            }
        }
        return $array;
    }

    /**
     * @param string $needle
     * @param array $haystack
     * @return bool
     */
    public static function recursive_array_search($needle, $haystack)
    {
        foreach ($haystack as $key => $value) {
            if ($needle === $value || (is_array($value) && Misc::recursive_array_search($needle, $value) !== false)) {
                return true;
            }
        }
        return false;
    }

    /**
     * @param array $array
     * @param string $glue
     * @return bool|string
     */
    public static function multi_implode($array, $glue)
    {
        if(!is_array($array)) {
            return "";
        }

        $ret = '';

        foreach ($array as $item) {
            if (is_array($item)) {
                $ret .= Misc::multi_implode($item, $glue) . $glue;
            } else {
                $ret .= $item . $glue;
            }
        }
        return substr($ret, 0, 0 - strlen($glue));
    }
}
