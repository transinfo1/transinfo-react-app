<?php
function installStyle(){
    try {
        $themeTransInfo = __DIR__ . '/../transinfomisc/import/style/transinfostyle.xml';
        if(file_exists($themeTransInfo)) {
            $overwriteStyleId = -1;
            $styleName = 'TransInfo Style';
            $styleLibrary = vB_Library::instance('Style');
            $styles = $styleLibrary->fetchStyles(true);
            foreach ( $styles as $style) {
                if($style['title'] == $styleName) {
                    $overwriteStyleId = $style['styleid'];
                    break;
                }
            }
            $imported = $styleLibrary->importStyleFromXML(file_get_contents($themeTransInfo), $styleName, -1, $overwriteStyleId, true, 1, true, true);
            vB_Api::instance('style')->buildAllStyles();

            vB_Api::instance('style')->setDefaultStyle($imported['overwritestyleid']);

            vB_Cache::resetCache();
            vB::getDatastore()->resetCache();
        } else {
            throw new vB_Exception_AdminStopMessage("wrong path: ".$themeTransInfo);
        }
    } catch (vB_Exception_AdminStopMessage $e) {
        throw $e;
    }
}
