<?php

namespace {

    use Transinfobridge\lib\DbUsers;
    use Transinfobridge\lib\Misc;

    if (!defined('VB_API')) define('VB_API', false);

    /**
     * Class Transinfobridge_Api_User.
     * vB_Api_User extension.
     */
    class Transinfobridge_Api_User extends vB_Api_Extensions
    {
        public $product = 'transinfobridge';
        public $version = '1.0.0';
        public $developer = 'TransInfo Team';
        public $title = 'Connection between TransInfo and VBulletin forum.';
        public $minver = '5.2.0';
        public $maxver = '5.9.99';
        public $infourl = '1';
        public $checkurl = '';
        public $AutoInstall = 1;
        public $extensionOrder = 10;

        /**
         * Insert or Update an user
         * @param mixed $prev         Result of previous extensions for `user.save` API method
         *
         * @param integer $userid Userid to be updated. Set to 0 if you want to insert a new user.
         * @param string $password Password for the user. Empty means no change.
         *                            May be overwritten by the $extra array
         * @param array $user Basic user information such as email or home page
         *                            * username
         *                            * email
         *                            * usertitle
         *                            * birthday
         *                            * usergroupid (will get no_permissions exception without admin permissions)
         *                            * membergroupids (will get no_permissions exception without admin permissions)
         *                            * list not complete
         * @param array $options vB options for the user
         * @param array $adminoptions Admin Override Options for the user
         * @param array $userfield User's User Profile Field data
         * @param array $notificationOptions
         * @param array $hvinput Human Verify input data. @see vB_Api_Hv::verifyToken()
         * @param array $extra Generic flags or data to affect processing.
         *                            * registration
         *                            * email
         *                            * newpass
         *                            * password
         *                            * acnt_settings
         * @return int New or updated userid.
         */
        public function save(
            $prev,
            $userid,
            $password,
            $user,
            $options,
            $adminoptions,
            $userfield,
            $notificationOptions = [],
            $hvinput = [],
            $extra = []
        )
        {
            // Load dependencies
            $logger = Misc::getLogger();

            // Prepare parameters
            $forumUserId = (int)$prev;
            $cmsUserId = (int)$userfield[DbUsers::CMS_ID_CUSTOM_FIELD];// should be already filled
            $transPlaceId = trim($userfield[DbUsers::TRANSPLACE_ID_CUSTOM_FIELD]);


            // Logger text.php
            $logPrefix = "[USER.SAVE]";
            $logData = "CmsUserId: {$cmsUserId}, TransPlaceId: {$transPlaceId}, ForumUserId: {$forumUserId}";

            // If product is not enable, log this information.
            if (!Misc::isTransInfoProductEnabled()) {
                $logger->fatal("{$logPrefix} TransInfo bridge product not available. {$logData}.");
                return;
            }

            // Get TransInfo - VBulletin user class bridge
            $dbUsers = new DbUsers(vB::get_registry()->db);

            /**
             * First of all we have to check if user already is not connected with account ID.
             * This may happen when we edit user. API 'user.save' method is fired when we add or edit user account.
             */
            $forumUserIdFromMapping = $dbUsers->getForumUserId($cmsUserId, true);
            if ($forumUserIdFromMapping) {
                $msg = "{$logPrefix} User already exists. {$logData}, ForumUserId already connected with CmsUserId: {$forumUserIdFromMapping}";
                $logger->debug($msg);
                return;
            }

            /**
             * Try to add data to database. Catch any exceptions and log them.
             * There might be sql exceptions like duplicate entry for key in table.
             */
            try {
                if ($dbUsers->connectAccount($forumUserId, $cmsUserId)) {
                    $logger->debug("{$logPrefix} {$logData}");
                } else {
                    $logger->error("{$logPrefix} Can not register user. {$logData}");
                }
            } catch (Exception $e) {
                $logger->fatal("{$logPrefix} Exception occurred. {$logData}. Exception: {$e->getMessage()}");
            }
        }
    }
}
