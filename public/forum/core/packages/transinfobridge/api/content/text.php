<?php

namespace {

    use Transinfobridge\lib\DbUsers;
    use Transinfobridge\lib\IndexRequester;

    if (!defined('VB_API')) define('VB_API', false);

    /**
     *  Class Transinfobridge_Api_Text.
     *  vB_Api_Content_Text extension.
     */
    class Transinfobridge_Api_Content_Text extends vB_Api_Extensions
    {
        public $product = 'transinfobridge';
        public $version = '1.0.0';
        public $developer = 'TransInfo Team';
        public $title = 'Connection between TransInfo and VBulletin forum.';
        public $minver = '5.2.0';
        public $maxver = '5.9.99';
        public $infourl = '1';
        public $checkurl = '';
        public $AutoInstall = 1;
        public $extensionOrder = 10;

        /**
         * Adds a new node
         *
         * @param  mixed   Array of field => value pairs which define the record.
         * @param  array   Array of options for the content being created.
         *                 Understands skipTransaction, skipFloodCheck, floodchecktime, many subclasses have skipNotification. See subclasses for more info.
         *
         * @return integer the new nodeid
         */
        public function add($data, $options = array())
        {
            IndexRequester::Instance()->addPostIndex((int)$data);
            return $data;
        }

        /**
         * Updates a record
         *
         * @param  mixed array of nodeids
         * @param  mixed array of permissions that should be checked.
         *
         * @return bool
         */
        public function update($nodeid, $data)
        {
            IndexRequester::Instance()->addPostIndex((int)$data);
            return $data;
        }

        /**
         * Permanently deletes a node, but probably never called
         *
         * @param  integer The nodeid of the record to be deleted
         *
         * @return boolean
         */
        public function delete($nodeid)
        {
            //This method never called but
            assert("Transplacebridge_Api_Content_Text - Delete ");
            return $nodeid;
        }
    }
}
