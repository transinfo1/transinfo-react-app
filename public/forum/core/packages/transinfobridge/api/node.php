<?php

namespace {
    use Transinfobridge\lib\IndexRequester;

    if (!defined('VB_API')) define('VB_API', false);

    /**
     *  Class Transinfobridge_Node.
     *  vB_Api_Node extension.
     */
    class Transinfobridge_Api_Node extends vB_Api_Extensions
    {
        public $product = 'transinfobridge';
        public $version = '1.0.0';
        public $developer = 'TransInfo Team';
        public $title = 'Connection between TransInfo and VBulletin forum.';
        public $minver = '5.2.0';
        public $maxver = '5.9.99';
        public $infourl = '1';
        public $checkurl = '';
        public $AutoInstall = 1;
        public $extensionOrder = 10;

        const NODE_IDS = 'nodeids';
        const CHILD = 'child';

        /**
         * @param $args
         */
        private function undeleteNodes($args)
        {
            $indexRequester = IndexRequester::Instance();
            $nodeIdArray = is_array($args[self::NODE_IDS]) ? $args[self::NODE_IDS] : array(0 => $args[self::NODE_IDS]);

            foreach ($nodeIdArray as $nodeId) {
                $childrenArray = vB_Library::instance('node')->fetchClosurechildren((int)$nodeId)[$nodeId];

                foreach ($childrenArray as $child) {
                    $indexRequester->addPostIndex($child[self::CHILD]);
                }
            }
        }

        /**
         * @param $args array
         */
        private function deleteNodes($args)
        {
            $indexRequester = IndexRequester::Instance();
            $nodeIdArray = is_array($args[self::NODE_IDS]) ? $args[self::NODE_IDS] : array(0 => $args[self::NODE_IDS]);

            foreach ($nodeIdArray as $nodeId) {
                $childrenArray = vB_Library::instance('node')->fetchClosurechildren((int)$nodeId)[$nodeId];

                foreach ($childrenArray as $child) {
                    $indexRequester->deletePostIndex($child[self::CHILD]);
                }
            }
        }

        /**
         * @param $args
         */
        private function deleteNodesAsSpam($args)
        {
            //equals delete index operation
            $this->deleteNodes($args);
        }

        /**
         * @param $args
         */
        private function moveNodes($args)
        {
            //equals update/add index operation
            $this->undeleteNodes($args);
        }

        /**
         * @param $args
         */
        private  function movePosts($args)
        {
            //equals update/add index operation
            $this->undeleteNodes($args);
        }

        /**
         * @param $args
         */
        private function cloneNodes($args)
        {
            //TODO - handle clone operation, not enough information about cloned nodes
            return;
        }

        /**
         * @param $args
         */
        private function mergeTopics($args)
        {
            $indexRequester = IndexRequester::Instance();
            $destinationTopicsId = $args['targetnodeid'];
            $mergeTopicsId = $args[self::NODE_IDS];

            foreach ($mergeTopicsId as $topicId) {
                if($topicId == $destinationTopicsId) {
                    $indexRequester->addPostIndex((int)$topicId);

                    $childrenArray = vB_Library::instance('node')->fetchClosurechildren((int)$topicId)[$topicId];
                    foreach ($childrenArray as $child) {
                        $indexRequester->addPostIndex($child[self::CHILD]);
                    }
                }
                else {
                    $indexRequester->deletePostIndex((int)$topicId);
                }
            }
        }

        /**
         * @param $args
         */
        private function mergePosts($args)
        {
            $indexRequester = IndexRequester::Instance();
            $destinationPostId = $args['input'][6]['value'];
            $mergePostsId = explode(",", $args['input'][5]['value']);

            foreach ($mergePostsId as $postId) {
                if($postId == $destinationPostId) {
                    $indexRequester->addPostIndex((int)$postId);
                }
                else {
                    $indexRequester->deletePostIndex((int)$postId);
                }
            }
        }

        /**
         * @param $args array
         */
        private function manageDeletedNode($args)
        {
            $args[self::NODE_IDS] = array(0 => $args[self::NODE_IDS]);

            switch ((int)$args['params']['deletetype']) { // type of manage operation
                case 0: // e.g. Edit of topic name
                case 2: // Undelete
                    $this->undeleteNodes($args);
                    break;
                case 1: // Keep unchanged
                    // nothing to do
                    break;
                case 3: // Delete
                    $this->deleteNodes($args);
                    break;
            }
        }

        /**
         * ===================================================================
         * Use to suppress call_user_func_array warning
         * ===================================================================
         */
        private function incrementNodeview($args) {}
        private function markRead($args) {}

        /**
         * Calls specific function by his name
         */
        public function callNamed()
        {
            list($current, $method, $args) = func_get_args();
            call_user_func_array(array($this, $method), array($args));
        }
    }
}
