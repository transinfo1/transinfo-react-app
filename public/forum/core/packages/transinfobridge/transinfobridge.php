<?php

namespace {
    // #TPL-3769 prevent 500 error while moving big topics in admin panel
    ini_set('memory_limit', '512M');

    use Transinfobridge\lib\AutoLogin;
    use Transinfobridge\lib\Config;
    use Transinfobridge\lib\DbUsers;
    use Transinfobridge\lib\Misc;
    use Transinfobridge\lib\Session;
    use Transinfobridge\lib\AuthData;

    require_once 'lib/Misc.php';
    require_once 'lib/AutoLogin.php';
    require_once 'lib/AuthData.php';
    require_once 'lib/Session.php';
    require_once 'lib/Config.php';

    /**
     * ===================================================================
     * For first forum run install style
     * ===================================================================
     */
    const INSTALL_STYLE_FILE = 'core/packages/transinfobridge/installStyle.php';
    if(file_exists(INSTALL_STYLE_FILE) && md5($_GET['password']) === $_SERVER['FORUMBRIDGE_ADMIN_PWD_MD5'] && $_GET['tiAction'] === 'installStyle')
    {
        sleep(3);
        require_once INSTALL_STYLE_FILE;
        try {
            $userApi = vB_Api::instance('user');

            $passwords = [
                'password' => '',
                'md5password' => Config::getInstance()->get('misc.admin_md5'),
                'md5password_utf' => ''
            ];

            $userApi->login2('admin', $passwords, '', 'cplogin');
            installStyle();
        } catch (vB_Exception_AdminStopMessage $e) {
            Misc::getLogger()->error("Can't install transinfo style: " . Misc::multi_implode($e->getParams(),","));
        } catch (Exception $ex) {
            Misc::getLogger()->error("Can't install transinfo style: " . $ex->getMessage());
        }
        finally{
            vB_User::processLogout();
            echo "<script>location.href = '/forum/';</script>";
            Misc::getLogger()->error("THIS " . DIR . " OR " . __DIR__);
        }
    }

    /**
     * ===================================================================
     * If package is not enabled or we are in admincp, skip this bridge configuration.
     * ===================================================================
     */
    if (!Misc::isTransInfoProductEnabled() || Misc::isAdminControlPanel()) {
        return;
    }


    /**
     * ===================================================================
     * Catch current url and redirect if necessary
     * ===================================================================
     */
    require_once 'lib/RouteCatch.php';


    /**
     * ===================================================================
     * Read basic user information from cookies set by font end
     * ===================================================================
     */
    $authData = new AuthData();
    $authData->readFromCookies();


    /**
     * ===================================================================
     * Prepare application session.
     * By default we take TransPlace cookie and exchange this cookie
     * data to account id with TransPlace.
     * Connection is through `https`, we use basic auth and we trust
     * our endpoint, so we log in user in vBForum on account which match
     * to account id.
     * ===================================================================
     */
    $loginService = new AutoLogin(
        new Session(Config::getInstance(), Misc::getLogger(), vB::get_registry()->input),
        new DbUsers(vB::get_registry()->db),
        Misc::getLogger()
    );

    $loginService->setCorrectUserSession($authData);
}
