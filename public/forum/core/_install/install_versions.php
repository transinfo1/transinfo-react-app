<?php
/*========================================================================*\
|| ###################################################################### ||
|| # vBulletin 5.3.1 - Licence Number LE11266DD1
|| # ------------------------------------------------------------------ # ||
|| # Copyright 2000-2017 vBulletin Solutions Inc. All Rights Reserved.  # ||
|| # This file may not be redistributed in whole or significant part.   # ||
|| # ----------------- VBULLETIN IS NOT FREE SOFTWARE ----------------- # ||
|| # http://www.vbulletin.com | http://www.vbulletin.com/license.html   # ||
|| ###################################################################### ||
\*========================================================================*/

/**
 *	@package vBInstall
 */

/*
 *	Holds the various minimum versions for vbulletin.  Versions below these will
 *	cause the installer to fail.
 *
 *	If you change these to install vBulletin on an unsupported version, vBulletin
 *	may not work properly or may not work at all.
 */
$install_versions = array(
	'php_required' => '5.6.0',
	'mysql_required' => '5.1.5',
	'mariadb_required' => '5.1.5'
);

/*=========================================================================*\
|| #######################################################################
|| # Downloaded: 01:43, Tue Jun 20th 2017
|| # CVS: $RCSfile$ - $Revision: 93854 $
|| #######################################################################
\*=========================================================================*/
