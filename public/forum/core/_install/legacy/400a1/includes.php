<?php
/*========================================================================*\
|| ###################################################################### ||
|| # vBulletin 5.3.1 - Licence Number LE11266DD1
|| # ------------------------------------------------------------------ # ||
|| # Copyright 2000-2017 vBulletin Solutions Inc. All Rights Reserved.  # ||
|| # This file may not be redistributed in whole or significant part.   # ||
|| # ----------------- VBULLETIN IS NOT FREE SOFTWARE ----------------- # ||
|| # http://www.vbulletin.com | http://www.vbulletin.com/license.html   # ||
|| ###################################################################### ||
\*========================================================================*/

require_once(DIR . '/install/legacy/400a1/vb/datamanager/attachdata.php');
require_once(DIR . '/install/legacy/400a1/vb/datamanager/attachment.php');
require_once(DIR . '/install/legacy/400a1/vb/datamanager/attachmentfiledata.php');
require_once(DIR . '/install/legacy/400a1/packages/vbattach/attach.php');
require_once(DIR . '/install/legacy/400a1/packages/vbforum/attach/album.php');
require_once(DIR . '/install/legacy/400a1/functions.php');

/*=========================================================================*\
|| #######################################################################
|| # Downloaded: 01:43, Tue Jun 20th 2017
|| # CVS: $RCSfile$ - $Revision: 89239 $
|| #######################################################################
\*=========================================================================*/
