<?php if (!defined('VB_ENTRY')) die('Access denied.');
/*========================================================================*\
|| ###################################################################### ||
|| # vBulletin 5.3.1 - Licence Number LE11266DD1
|| # ------------------------------------------------------------------ # ||
|| # Copyright 2000-2017 vBulletin Solutions Inc. All Rights Reserved.  # ||
|| # This file may not be redistributed in whole or significant part.   # ||
|| # ----------------- VBULLETIN IS NOT FREE SOFTWARE ----------------- # ||
|| # http://www.vbulletin.com | http://www.vbulletin.com/license.html   # ||
|| ###################################################################### ||
\*========================================================================*/

/**
* Fetches an array of prefixes for the specified forum. Returned in format:
* [prefixsetid][] = prefixid
*
* @param	integer	Channel Node ID to fetch prefixes from
*
* @return	array
*/
function fetch_prefix_array($nodeid)
{
	global $vbulletin;

	if (isset($vbulletin->prefixcache))
	{
		return (is_array($vbulletin->prefixcache["$nodeid"]) ? $vbulletin->prefixcache["$nodeid"] : array());
	}
	else
	{
		$prefixsets = array();
		$prefix_sql = $vbulletin->db->query_read("
			SELECT prefix.*, prefixpermission.usergroupid AS restriction
			FROM " . TABLE_PREFIX . "channelprefixset AS channelprefixset
			INNER JOIN " . TABLE_PREFIX . "prefixset AS prefixset ON (prefixset.prefixsetid = channelprefixset.prefixsetid)
			INNER JOIN " . TABLE_PREFIX . "prefix AS prefix ON (prefix.prefixsetid = prefixset.prefixsetid)
			LEFT JOIN " . TABLE_PREFIX . "prefixpermission AS prefixpermission ON (prefix.prefixid = prefixpermission.prefixid)
			WHERE channelprefixset.nodeid = " . intval($nodeid) . "
			ORDER BY prefixset.displayorder, prefix.displayorder
		");
		while ($prefix = $vbulletin->db->fetch_array($prefix_sql))
		{
			if (empty($prefixsets["$prefix[prefixsetid]"]["$prefix[prefixid]"]))
			{
				$prefixsets["$prefix[prefixsetid]"]["$prefix[prefixid]"] = array(
					'prefixid' => $prefix['prefixid'],
					'restrictions' => array()
				);
			}

			if ($prefix['restriction'])
			{
				$prefixsets["$prefix[prefixsetid]"]["$prefix[prefixid]"]['restrictions'][] = $prefix['restriction'];
			}
		}

		// Legacy Hook 'prefix_fetch_array' Removed //

		return $prefixsets;
	}
}

/**
 * Prefix Permission Check
 *
 * @param	string	The prefix ID to check
 * @param	array	The restricted usergroups (used when we have the restrictions already)
 *
 * @return 	boolean
 */
function can_use_prefix($prefixid, $restrictions = null)
{
	global $vbulletin;

	if (!is_array($restrictions))
	{
		$restrictions = array();
		$restrictions_db = $vbulletin->db->query_read("
			SELECT prefixpermission.usergroupid
			FROM " . TABLE_PREFIX . "prefixpermission AS prefixpermission
			WHERE prefixpermission.prefixid = '" . $vbulletin->db->escape_string($prefixid) . "'
		");

		while ($restriction = $vbulletin->db->fetch_array($restrictions_db))
		{
			$restrictions[] = intval($restriction['usergroupid']);
		}
	}

	if (empty($restrictions))
	{
		return true;
	}

	$membergroups = fetch_membergroupids_array($vbulletin->userinfo);
	$infractiongroups = explode(',', str_replace(' ', '', $vbulletin->userinfo['infractiongroupids']));

	foreach ($restrictions AS $usergroup)
	{
		if (in_array($usergroup, $infractiongroups))
		{
			return false;
		}
	}

	if (!count(array_diff($membergroups, $restrictions)))
	{
		return false;
	}

	return true;
}

/*=========================================================================*\
|| #######################################################################
|| # Downloaded: 01:43, Tue Jun 20th 2017
|| # CVS: $RCSfile$ - $Revision: 93305 $
|| #######################################################################
\*=========================================================================*/
