<?php
/*========================================================================*\
|| ###################################################################### ||
|| # vBulletin 5.3.1 - Licence Number LE11266DD1
|| # ------------------------------------------------------------------ # ||
|| # Copyright 2000-2017 vBulletin Solutions Inc. All Rights Reserved.  # ||
|| # This file may not be redistributed in whole or significant part.   # ||
|| # ----------------- VBULLETIN IS NOT FREE SOFTWARE ----------------- # ||
|| # http://www.vbulletin.com | http://www.vbulletin.com/license.html   # ||
|| ###################################################################### ||
\*========================================================================*/

// ######################## SET PHP ENVIRONMENT ###########################
error_reporting(E_ALL & ~E_NOTICE);

// ##################### DEFINE IMPORTANT CONSTANTS #######################
define('CVS_REVISION', '$RCSfile$ - $Revision: 91781 $');

// #################### PRE-CACHE TEMPLATES AND DATA ######################
global $phrasegroups, $specialtemplates;
$phrasegroups = array();

$specialtemplates = array();

// ########################## REQUIRE BACK-END ############################
require_once(dirname(__FILE__) . '/global.php');
require_once(DIR . '/includes/class_rss_poster.php');

header('Content-Type: text/xml; charset=utf-8');
$licenseid = 'LE11266DD1';
$config = vB::getConfig();

if (isset($config['Misc']['licenseid']))
{
	$licenseid = $config['Misc']['licenseid'];
}

if ($result = fetch_file_via_socket('https://version.vbulletin.com/news.php?v=' . SIMPLE_VERSION . "&id=$licenseid", array('type' => '')))
{
	echo $result['body'];
}
else
{
	echo 'Error';
}

/*=========================================================================*\
|| #######################################################################
|| # Downloaded: 01:43, Tue Jun 20th 2017
|| # CVS: $RCSfile$ - $Revision: 91781 $
|| #######################################################################
\*=========================================================================*/
