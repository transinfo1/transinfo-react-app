# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [1.39] - 2017-10-11
### Added
- Server Side Rendering (TPM-910)

## [1.40] - 2017-10-11
### Changed
- Gitlab-CI config fixed for RC and PROD enviroment