const env = require('./envList');
const param = process.argv[2];

switch(param){
    case 'dev':
        env.dev();
        break;
    case 'test':
        env.test();
        break;
    case 'awsDev':
        env.awsDev();
        break;
    case 'awsRc':
        env.awsRc();
        break;
    case 'awsRcTibo':
        env.awsRcTibo();
        break;
    case 'awsProd':
        env.awsProd();
        break;
    default:
    return console.log('No enviroment selected');
}