const fs = require('fs');
const config = require('./configList');

module.exports = {
    dev: function () {writeEverythingWeNeed(config.devConf)},
    test: function () {writeEverythingWeNeed(config.testConf)},
    awsDev: function () {writeEverythingWeNeed(config.awsDevConf)},
    awsRc: function () {writeEverythingWeNeed(config.awsRcConf)},
    awsRcTibo: function () {writeEverythingWeNeed(config.awsRcTiboConf)},
    awsProd: function () {writeEverythingWeNeed(config.awsProdConf)}
};

function writeEverythingWeNeed (config) {
    fs.writeFile("./config.json", JSON.stringify(config), (err) => {
        if (err) {
            console.error(err);
            return;
        }
        console.log("File has been created");
    });

    let forumJSfile = "public/forum/js/TransInfo-extras.js";
    fs.readFile(forumJSfile, 'utf8', function (err,data) {
        if (err) {
            return console.log(err);
        }
        //be sure that this string exists in file
        let oldConfig = '{"packageName":"transinfo_frontend_test","restApiHostName":"http://backend-test.transinfo.dev.firetms.io","auth":{"oneLogin":{"authServerUrl":"http://auth.dev-trans.rst.com.pl/oauth2/login","clientId":"14963949470NHDEu4SA74pSgFwk817"},"google":{"clientId":"983191201652-0cum57752r41o66jnbdpi2j3244qvuho.apps.googleusercontent.com"}},"languages":["pl","en","de","cz","hu","lt","sk","ro","ru"]}';
        let result = data.replace(oldConfig, JSON.stringify(config));
        fs.writeFile(forumJSfile, result, 'utf8', function (err) {
            if (err) return console.log(err);
        });
        console.log("Rest url replaced");
    });

    let cardboxFile = "cardbox.conf";
    fs.readFile(cardboxFile, 'utf8', function (err,data) {
        if (err) {
            return console.log(err);
        }
        //be sure that this string exists in file
        let result = data.replace("transinfo_frontend_test", config.packageName);
        fs.writeFile(cardboxFile, result, 'utf8', function (err) {
            if (err) return console.log(err);
        });
        console.log("Package name replaced");
    });
}
