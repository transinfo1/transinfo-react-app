import React, {Component} from 'react';
import {Link} from 'react-router'
import construction from '../../../public/static/img/construction-500.png';


class Code500 extends Component {
    render() {
        return (
            <div className="error-500 d-flex align-items-center justify-content-center flex-column">
                <div className="error--wrapper">
                        <h1 className="code-500">500
                        <div className="error-info-wrapper">
                            <p className="info-500">500 interval Server Error Page</p>
                            <p className="info-details-500 mb-1">We are working towards creating something better. We won’t
                                be long</p>
                            <p className="info-details-500 mt-1">Go back to <Link to="/">homepage</Link>.</p></div></h1>
                    <img src={construction} alt="" className="constructor"/>
                </div>
            </div>
        );
    }
}

export default Code500;
