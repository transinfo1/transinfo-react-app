import React, {Component} from 'react';
import { Link } from 'react-router';
import translate from '../../utils/translate';

class Code404 extends Component {
    render() {
        return (
            <div className="error-404 d-flex align-items-center justify-content-center flex-column">
                    <p className="error-info mb-1">{translate('message.errors.error404')}</p>
                    <p className="error-go-homepage"><Link to="/">{translate('navigation.goHome')}</Link>.</p>
            </div>
        );
    }
}

export default Code404;
