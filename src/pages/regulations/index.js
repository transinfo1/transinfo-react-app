import React, { Component } from 'react';
import translate from '../../utils/translate';

class Regulations extends Component {
  render(){
    return(
        <section className="regulations">
            <article className="regulations_content">
                <h1>
                    {translate('regulationsContent.header')}
                </h1>
                <h2>
                    {translate('regulationsContent.regAndTerms')}
                </h2>
                <p>
                    {translate('regulationsContent.acceptRegulations')}
                </p>
                <h3>
                    {translate('regulationsContent.partnersMarketing')}
                </h3>
                <p>
                    {translate('regulationsContent.acceptPersonalData')}
                </p>
                <h3>
                    {translate('regulationsContent.serviceRegulations')}
                </h3>
                <p>
                    {translate('regulationsContent.contents')}
                </p>
                <ul>
                    <li>{translate('regulationsContent.general.generalProvisions')}
                        <ul>
                            <li>{translate('regulationsContent.general.Definitions')}
                                <br/> {translate('regulationsContent.general.generalProvisionsText1')}
                                <br/> {translate('regulationsContent.general.generalProvisionsText2')}
                                <br/> {translate('regulationsContent.general.generalProvisionsText3')}
                                <br/> {translate('regulationsContent.general.generalProvisionsText4')}
                                <br/> {translate('regulationsContent.general.generalProvisionsText5')}
                                <br/> {translate('regulationsContent.general.generalProvisionsText6')}
                                <br/> {translate('regulationsContent.general.generalProvisionsText7')}
                                <br/> {translate('regulationsContent.general.generalProvisionsText8')}
                                <br/> {translate('regulationsContent.general.generalProvisionsText9')}
                                <br/>
                            </li>
                        </ul>
                    </li>
                    <li>{translate('regulationsContent.registration.registrationSubtitle')}
                        <ul>
                            <li>{translate('regulationsContent.registration.regulationText1')}
                                <ul>
                                    <li>{translate('regulationsContent.registration.regulationText2')}
                                        <br/> {translate('regulationsContent.registration.regulationText3')}
                                        <br/> {translate('regulationsContent.registration.regulationText4')}
                                        <br/> {translate('regulationsContent.registration.regulationText5')}
                                        <br/> {translate('regulationsContent.registration.regulationText7')}
                                        <br/>
                                    </li>
                                    <li>{translate('regulationsContent.registration.regulationText8')}</li>
                                </ul>
                            </li>
                            <li>{translate('regulationsContent.registration.regulationText9')}</li>
                            <li>{translate('regulationsContent.registration.regulationText10')}</li>
                            <li>{translate('regulationsContent.registration.regulationText11')}</li>
                            <li>{translate('regulationsContent.registration.regulationText12')}</li>
                            <li>{translate('regulationsContent.registration.regulationText13')}</li>
                            <li>{translate('regulationsContent.registration.regulationText14')}</li>
                            <li>{translate('regulationsContent.registration.regulationText15')}
                                <ul>
                                    <li>-{translate('regulationsContent.registration.regulationText16')}</li>
                                    <li>-{translate('regulationsContent.registration.regulationText17')}</li>
                                    <li>-{translate('regulationsContent.registration.regulationText18')}</li>
                                    <li>-{translate('regulationsContent.registration.regulationText19')}</li>
                                    <li>-{translate('regulationsContent.registration.regulationText20')}</li>
                                    <li>-{translate('regulationsContent.registration.regulationText21')}</li>
                                    <li>-{translate('regulationsContent.registration.regulationText22')}</li>
                                    <li>-{translate('regulationsContent.registration.regulationText23')}</li>
                                    <li>-{translate('regulationsContent.registration.regulationText24')}</li>
                                    <li>-{translate('regulationsContent.registration.regulationText25')}</li>
                                    <li>-{translate('regulationsContent.registration.regulationText26')}</li>
                                </ul>
                            </li>
                        </ul>
                    </li>
                    <li>{translate('regulationsContent.forum.forumHeader')}
                        <ul>
                            <li>{translate('regulationsContent.forum.forumText1')}</li>
                            <li>{translate('regulationsContent.forum.forumText2')}
                                <ul>
                                    <li>{translate('regulationsContent.forum.forumText3')}</li>
                                    <li>{translate('regulationsContent.forum.forumText4')}</li>
                                    <li>{translate('regulationsContent.forum.forumText5')}</li>
                                    <li>
                                        {translate('regulationsContent.forum.forumText6')}<br/>
                                        {translate('regulationsContent.forum.forumText7')}
                                    </li>
                                </ul>
                            </li>
                            <li>{translate('regulationsContent.forum.forumText8')}</li>
                            <li>{translate('regulationsContent.forum.forumText9')}
                                <ul>
                                    <li>{translate('regulationsContent.forum.forumText10')}</li>
                                    <li>{translate('regulationsContent.forum.forumText11')}</li>
                                    <li>{translate('regulationsContent.forum.forumText12')}</li>
                                </ul>
                            </li>
                            <li>{translate('regulationsContent.forum.forumText13')}
                                <ul>
                                    <li>{translate('regulationsContent.forum.forumText14')}
                                        <li>{translate('regulationsContent.forum.forumText15')}</li>
                                        <li>{translate('regulationsContent.forum.forumText16')}</li>
                                        <li>{translate('regulationsContent.forum.forumText17')}</li>
                                        <li>{translate('regulationsContent.forum.forumText18')}</li>
                                        <li>{translate('regulationsContent.forum.forumText19')}</li>
                                        <li>{translate('regulationsContent.forum.forumText20')}</li>
                                        <li>{translate('regulationsContent.forum.forumText21')}<br/>
                                                {translate('regulationsContent.forum.forumText22')}<br/>
                                                {translate('regulationsContent.forum.forumText23')}<br/>
                                                {translate('regulationsContent.forum.forumText24')}<br/>
                                                {translate('regulationsContent.forum.forumText25')}<br/>
                                                {translate('regulationsContent.forum.forumText26')}<br/>
                                        </li>
                                            <li>{translate('regulationsContent.forum.forumText27')}</li>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </li>
                    <li>
                        {translate('regulationsContent.rightsProtection.rightProtectionHeader')}
                        <ul>
                            <li>{translate('regulationsContent.rightsProtection.rightProtectionText1')}</li>
                            <li>{translate('regulationsContent.rightsProtection.rightProtectionText2')}</li>
                            <li>{translate('regulationsContent.rightsProtection.rightProtectionText3')}</li>
                            <li>{translate('regulationsContent.rightsProtection.rightProtectionText4')}</li>
                            <li>{translate('regulationsContent.rightsProtection.rightProtectionText5')}</li>
                            <li>{translate('regulationsContent.rightsProtection.rightProtectionText6')}</li>
                            <li>{translate('regulationsContent.rightsProtection.rightProtectionText7')}</li>
                        </ul>
                    </li>
                    <li>{translate('regulationsContent.accountSecurity.accountSecurityHeader')}
                        <ul>
                            <li>{translate('regulationsContent.accountSecurity.accountSecurityText1')}</li>
                            <li>{translate('regulationsContent.accountSecurity.accountSecurityText2')}</li>
                            <li>{translate('regulationsContent.accountSecurity.accountSecurityText3')}</li>
                            <li>{translate('regulationsContent.accountSecurity.accountSecurityText4')}</li>
                            <li>{translate('regulationsContent.accountSecurity.accountSecurityText5')}</li>
                            <li>{translate('regulationsContent.accountSecurity.accountSecurityText6')}</li>
                            <li>{translate('regulationsContent.accountSecurity.accountSecurityText7')}</li>
                            <li>{translate('regulationsContent.accountSecurity.accountSecurityText8')}</li>
                            <li>{translate('regulationsContent.accountSecurity.accountSecurityText9')}</li>
                            <li>{translate('regulationsContent.accountSecurity.accountSecurityText10')}</li>
                        </ul>
                    </li>
                    <li>{translate('regulationsContent.protectionPersonal.protectionPersonalHeader')}
                        <ul>
                            <li>{translate('regulationsContent.protectionPersonal.protectionPersonalText1')}</li>
                            <li>{translate('regulationsContent.protectionPersonal.protectionPersonalText2')}</li>
                        </ul>
                    </li>
                    <li>{translate('regulationsContent.infringements.infringementsHeader')}
                        <ul>
                            <li>{translate('regulationsContent.infringements.infringementsText1')}</li>
                        </ul>
                    </li>
                    <li>
                        {translate('regulationsContent.responsibility.responsibilityHeader')}
                        <ul>
                            <li>{translate('regulationsContent.responsibility.responsibilityText1')}</li>
                            <li>{translate('regulationsContent.responsibility.responsibilityText2')}</li>
                            <li>{translate('regulationsContent.responsibility.responsibilityText3')}</li>
                            <li>{translate('regulationsContent.responsibility.responsibilityText4')}</li>
                        </ul>
                    </li>
                    <li>{translate('regulationsContent.finalProvisions.finalProvisionsHeader')}
                        <ul>
                            <li>{translate('regulationsContent.finalProvisions.finalProvisionsText1')}</li>
                        </ul>
                    </li>
                </ul>
            </article>
        </section>
  )
  }
}

export default Regulations;
