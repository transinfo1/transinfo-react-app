import React,  { Component } from 'react';
import AuthorLink from '../../components/user/atoms/AuthorLink';
import CreatedDate from '../../components/createdDate/index.js';
import Amount from '../../components/comments/Amount';
import ViewMore from '../../components/viewMore/index.js';
import { Link } from 'react-router';
import {authorName} from '../../utils/helpers';
import translate from '../../utils/translate';

class VideoItem extends Component {

    videoThumbnail(){
        let {post} = this.props;
        let image = post.featuredImage ? `${post.featuredImage}` : post.video ? `https://img.youtube.com/vi/${post.video}/hqdefault.jpg` : '/static/img/default_image.jpg';
        return (
            <img className="search-video__thumbnail__img" src={image} alt="videos-thumbnail"/>
        )
    }

    createContent(str){
        function trim(str){
            return str.replace(/^[ ),.-]+|\s*$/g, '');
        }
        let string = trim(str)

        let cleanText = string.replace(/<\/?[^>]+(>|$)/g, "");
        if(cleanText[0].toUpperCase() === cleanText[0]) {
            return {__html: string + '...'}
        } else {
            return {__html: '...' + string + '...'}
        }
    }

    createTags(str){
        let cleanTag = str.replace(/<\/?[^>]+(>|$)/g, "");
        return cleanTag;
    }


    render(){
        let {post} = this.props;
        let testString = 'search.video';
        let testKey = 0;


        return(
            <div className="search-article">
                <a href={'/' + post.postSlug}>
                    {post.highlight.content ?
                        post.highlight.postTitle ?
                            <h4 className="search-article__header" data-test="article.title" dangerouslySetInnerHTML={{__html: post.highlight.postTitle[0]}}></h4>
                                :
                            <h4 className="search-article__header" data-test="article.title" dangerouslySetInnerHTML={{__html: post.postTitle}}></h4>
                    :
                    null
                    }
                </a>

                <div className="search-article__desc">
                        <a href={'/' + post.postSlug}>
                            <div className="search-video__thumbnail">
                                {this.videoThumbnail()}
                            </div>
                        </a>

                    {post.highlight.content ?

                            <p data-test="videos.title">
                                <span dangerouslySetInnerHTML={this.createContent(post.highlight.content[0])}></span>
                                {post.highlight.tags ?
                                    <span className="mt-2 tag-container">{translate('posts.tags')}
                                                {post.highlight.tags.map( (tag, key) => {
                                                    return (key < 3 ?  <Link className={"btn btn-tag "} key={key}><span className="trim m-0 tags" dangerouslySetInnerHTML={ {__html: '#' + this.createTags(tag)} }/></Link> : null )
                                                }) }
                                        </span>
                                    : null
                                }
                            </p>
                            :
                            post.highlight.postTitle ?
                                <div data-test="videos.title">
                                    <a href={'/' + post.postSlug}>
                                        <h4 className="search-article__header search-article__header--video" data-test="videos.title" dangerouslySetInnerHTML={{__html: post.highlight.postTitle[0]}}></h4>
                                    </a>
                                    {post.highlight.tags ?
                                        <span className="mt-2 tag-container">{translate('posts.tags')}
                                                {post.highlight.tags.map( (tag, key) => {
                                                    return (key < 3 ?  <Link className={"btn btn-tag "} key={key}><span className="trim m-0 tags" dangerouslySetInnerHTML={ {__html: '#' + this.createTags(tag)} }/></Link> : null )
                                                }) }
                                        </span>
                                        : null
                                    }
                                </div>

                                :
                                <div data-test="videos.title">
                                    <a href={'/' + post.postSlug}>
                                        <h4 className="search-article__header search-article__header--video" data-test="videos.title" dangerouslySetInnerHTML={{__html: post.postTitle}}></h4>
                                    </a>
                                    {post.highlight.tags ?
                                        <span className="tag-container">{translate('posts.tags')}
                                            {post.highlight.tags.map( (tag, key) => {
                                                return (key < 3 ?  <Link className={"btn btn-tag "} key={key}><span className="trim m-0 tags" dangerouslySetInnerHTML={ {__html: '#' + this.createTags(tag)} }/></Link> : null )
                                            }) }
                                        </span>
                                        : null
                                    }
                                </div>
                    }
                </div>
                <div className="search-article__links">
                    <AuthorLink data={post.author} testString={testString +'.author'} testKey={testKey}>
                        {authorName(post.author)}
                    </AuthorLink>
                    <CreatedDate date={post.dateGmt} testString={testString} testKey={testKey}/>
                    <Amount value={post.commentsCount} description={true} testString={testString + '.comments'} testKey={testKey}/>
                    <ViewMore color path={post.postSlug} testString={testString} testKey={testKey} normalLink />
                </div>
            </div>
        )
    }
}

export default VideoItem;