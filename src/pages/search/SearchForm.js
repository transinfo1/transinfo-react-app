import React,  { Component } from 'react';
import translate from '../../utils/translate';
import {connect} from 'react-redux';
import {fetchSearchResult, setKeywords} from '../../actions/search';
import {debounce} from '../../utils/filters';
import * as storage from '../../utils/storage';
import {browserHistory} from 'react-router';
import { plural } from '../../utils/helpers';
import { getLang } from '../../utils/storeData';

class SearchForm extends Component {
  constructor(props){
      super(props);
      this.fetchResult = debounce(this.fetchResult, 500);

  }

  fetchResult( searchOnlyTitle = false) {
      const {store} = this.props;
      this.props.setKeywords(this.searchText.value)
      this.props.fetchSearchResult({keywords: this.searchText.value, type: store.type, onlyTitle: searchOnlyTitle, sortBy: store.sortBy});
      browserHistory.push('/search?q=' + this.searchText.value);
  }

  componentDidMount(){
      const {store, location} = this.props;
      if(typeof(location.query.q) === "undefined"){
          this.props.setKeywords('')
          this.props.fetchSearchResult({keywords: ''});
          return;
      }
      let previousSearch = storage.get('searchParams', sessionStorage);
      if(!previousSearch && location.query){
          this.props.setKeywords(location.query.q)
          this.props.fetchSearchResult({keywords: location.query.q});
          return;
      }

      if(!store.fetched && location.query.q !== previousSearch.keywords) {
          this.props.setKeywords(location.query.q)
          this.props.fetchSearchResult({keywords: location.query.q});
          return;
      }

      if(previousSearch && !store.redirectFromHeader) {
          this.props.setKeywords(previousSearch.keywords)
          this.searchText.value = previousSearch.keywords;
          this.props.fetchSearchResult(previousSearch);
      }

  }

  componentDidUpdate(){
      const {store} = this.props;
      if(store.keywords !== this.searchText.value ){
          this.searchText.value = store.keywords;
      };
  }


  countSearchResults(){
      const {store} = this.props;
      let countResult = 0;
      if(store.data.comment) {
          countResult += store.data.comment.total;
      }
      if(store.data.forum) {
          countResult += store.data.forum.total;
      }
      if(store.data.post){
          countResult += store.data.post.total;
      }
      return countResult;
  }


  render(){
      const {store} = this.props;
      let testString = 'search.input';
      return(
            <div className="search-form">
              <form onSubmit={(e) => {e.preventDefault();}}>
                  <input type="text" data-test={testString + '.searchText'}
                         className="search-form__input" placeholder={translate('search.inputSearchValue')} ref={input => this.searchText = input} required maxLength="35" defaultValue={store.keywords} onChange={() => {
                      this.fetchResult();
                  }}/>
                  <div className="search-form__button-container">
                  <button className="search-form__reset-button" data-test="search.button-reset" type="reset" onClick={(e) => {
                      this.props.setKeywords('');
                  }}>X</button>
                  </div>
              </form>
              <div className="search-form__checkbox">
                <div className="search-form__checkbox-container">
                  <input id="searchOnlyByTitle" type="checkbox" data-test={testString + '.searchOnlyByTitle'} className="search-form__checkbox-input" onChange={
                      (e) => {
                          let searchOnlyTitle = e.target.checked;
                          this.fetchResult(searchOnlyTitle);}}/>
                  <label htmlFor="searchOnlyByTitle" className="search-form__checkbox-label" >{translate('search.onlyTitles')}</label>
                </div>
                <div>
                  <span data-test="search.countResult">{this.countSearchResults()} { plural(this.countSearchResults(), getLang(), 'results')}</span>
                </div>
              </div>
            </div>

    )
  }
}


const mapStateToProps = (store) => {
    return {
        store: store.search
    };
};
export default connect(mapStateToProps, {fetchSearchResult, setKeywords})(SearchForm);
