import React,  { Component } from 'react';
import translate from '../../utils/translate';
import PostItem from './PostItem'
import {connect} from 'react-redux';
import {fetchSearchResult} from '../../actions/search';


class SearchPostsList extends Component {
        showMorePosts(e){
            e.preventDefault();
            const {store} = this.props;
            this.props.fetchSearchResult({keywords: store.keywords, type: 'forum'})
        }

        render() {
            let {data, type, keywords} = this.props;
            return(
                <div>
                    {data.length ?
                        type === 'forum' ?
                            (<div className="comments-list comments-list--optional-view">
                                <div className="row justify-content-start">
                                    {data.map((post, key) => <div className="col-22 col-lg-18" key={key}><PostItem post={post} type={type}/></div>)}
                                </div>
                            </div>)
                            :
                            <div className="comments-list mb-3 mt-3">
                                <h2 className="heading--semibold">Wyniki z forum dla zapytania: {keywords}</h2>
                                <div className="row justify-content-start mt-3">
                                    {data.map((post, key) => <div className="col-22 col-lg-8" key={key}><PostItem post={post} type={type}/></div>)}
                                </div>
                                <div className="comments-list__show-more mt-1">
                                    <div className='comments-list__show-more__link' onClick={(e) => {this.showMorePosts(e);}}>
                                        <a href="">{translate('search.seeMoreForum')}
                                            <img className="ml-2 comments-list__show-more__icon" src={`/static/img/icons/arrow.svg`} alt="arrow icon"/>
                                        </a>
                                    </div>
                                </div>
                            </div>


                        :
                        null
                    }
                </div>
            )
        }

}

const mapStateToProps = (store) => {
    return {
        store: store.search
    };
};

export default connect(mapStateToProps, {fetchSearchResult})(SearchPostsList);