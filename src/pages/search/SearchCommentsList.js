import React, {Component} from 'react';
import CommentItem from './CommentItem'
import {connect} from 'react-redux';
import {fetchSearchResult} from '../../actions/search';
import translate from '../../utils/translate';


class SearchCommentsList extends Component {
    showMoreComments(e){
        e.preventDefault();
        const {store} = this.props;
        this.props.fetchSearchResult({keywords: store.keywords, type: 'comment'})
    }

    render(){
        const {data, page, keywords, type} = this.props;

        return(
            <div>
                {data.length ?
                    (<div>
                        {type === 'comment' ?
                            <div className="comments-list comments-list--optional-view">
                                <div className="row justify-content-start">
                                    {data.map((post, key) => <div className="col-22 col-lg-18" key={key}><CommentItem post={post} type={type}/></div>)}
                                </div>
                            </div>
                            : page ? null :
                                <div className="comments-list">
                                    <h2 className="heading--semibold comments-list__header">{translate('posts.phrase')}: <span data-test="search.phrase">{keywords} {translate('search.inComments')}</span></h2>
                                    <div className="row justify-content-start">
                                        {data.map((post, key) => <div className="col-22 col-lg-8" key={key}><CommentItem post={post} /></div>)}
                                    </div>
                                    <div className="comments-list__show-more">
                                        <div className='comments-list__show-more__link' onClick={(e) => {
                                        this.showMoreComments(e);
                                        }
                                        }>
                                            <a href="">{translate('search.seeMoreComments')} <img className="ml-2 comments-list__show-more__icon" src={`/static/img/icons/arrow.svg`} alt="arrow icon"/></a>
                                        </div>
                                    </div>
                                </div>
                        }
                    </div>)
                    :
                    null
                }
            </div>
        )
    }
}


const mapStateToProps = (store) => {
    return {
        store: store.search
    };
};

export default connect(mapStateToProps, {fetchSearchResult})(SearchCommentsList);