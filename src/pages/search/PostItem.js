import React,  { Component } from 'react';
import AuthorLink from '../../components/user/atoms/AuthorLink';
import CreatedDate from '../../components/createdDate/index.js';
import {authorName} from '../../utils/helpers';
import Truncate from 'react-truncate';
import translate from '../../utils/translate';

class PostItem extends Component {
    createContent(str){
        function trim(str){
            return str.replace(/^[ ),.-]+|\s*$/g, '');
        }
        let string = trim(str)

        let cleanText = string.replace(/<\/?[^>]+(>|$)/g, "");
        if(cleanText[0].toUpperCase() === cleanText[0]) {
            return {__html: string + '...'}
        } else {
            return {__html: '...' + string + '...'}
        }
    }

    render(){
        let testString = 'search.forum-post';
        let testKey = 0;
        let {post, type} = this.props;
        return(
                <div className="search-comment-item">
                    <a className="search-comment-item__link" href={"/forum" + post.postSlug}>
                    {post.highlight.postTitle ?
                        <h4 className="search-post-header" data-test="post.title" dangerouslySetInnerHTML={{__html: post.highlight.postTitle[0]}}/>
                        :
                        <h4 className="search-post-header" data-test="post.title">{post.postTitle}</h4>
                    }
                    </a>
                    <span className="post-icon"></span>
                    <a className="search-comment-item__link" href={"/forum" + post.postSlug}>
                        {post.highlight.content ?
                            <p className="comment-item__content forum-content" data-test="post.content" dangerouslySetInnerHTML={this.createContent(post.highlight.content[0])}/>
                        :
                            post.commentContent ?
                                <p className="comment-item__content forum-content" data-test="post.content">
                                    <Truncate lines={3} ><span dangerouslySetInnerHTML={{__html: post.commentContent}}></span></Truncate>
                                </p>
                                :
                                <p className="comment-item__content forum-content" data-test="post.content">
                                    Brak zawartości do wyświetlenia
                                </p>
                        }
                    </a>

                    <div className="search-comment-item__info mt-2">
                        <AuthorLink disable={true} data={post.author} testString={testString + '.author'} testKey={testString}>
                            {authorName(post.author)}
                        </AuthorLink >
                        <CreatedDate date={post.dateGmt} testString={testString} testKey={testKey}/>
                        {type === 'forum' &&
                        <a className='view-more' href={'/forum' + post.postSlug} data-test={testString + '.readMore' + testKey}>
                            {translate('main.seeMore')} <img className="ml-2" src={`/static/img/icons/arrow.svg`} alt="arrow icon"/>
                        </a>
                        }
                    </div>
                </div>
        )
    }
}

export default PostItem;