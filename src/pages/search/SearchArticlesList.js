import React from 'react';
import ArticleItem from "./ArticleItem";
import VideoItem from './VideoItem';

const SearchArticlesList = ({data}) => {
    return(
      <div className="articles-list">
        <div className="row justify-content-start">
            {data.length > 0 ? data.map( (post, key) => {
                return (post.postType === 'videos' ?
                        <div key={key} className="col-24 col-lg-18"><VideoItem post={post}/></div>
                        :
                        <div key={key} className="col-24 col-lg-18"><ArticleItem post={post}/></div>
                        )
            }) : ''}
        </div>
      </div>
    )
}

export default SearchArticlesList;
