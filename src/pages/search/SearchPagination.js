import React,  { Component } from 'react';
import {connect} from 'react-redux';
import ReactPaginate from 'react-paginate';
import {fetchSearchResult} from '../../actions/search';


class SearchPagination extends Component {

    constructor(props) {
        super(props);
        this.handlePageChange = this.handlePageChange.bind(this)
    }

    handlePageChange(data) {
        const {store} = this.props;
        this.props.fetchSearchResult({
            keywords: store.keywords,
            type: store.type,
            sortBy: store.sortBy,
            onlyTitle: store.onlyTitle,
            page: data.selected
        })
    }

    render() {
        const {store} = this.props;
        let totalResults = store.fetched && store.type === "all" ? store.data.post.total : store.data.comment.total + store.data.post.total;
        if (store.type === 'forum') {
            totalResults = store.data.forum.total;
        }
        let pageCount = Math.ceil(totalResults/10);
        return (
             pageCount > 1 ?
                (<ReactPaginate
                    previousLabel={null}
                    nextLabel={null}
                    breakLabel={<a href="">...</a>}
                    breakClassName={"break-me"}
                    pageCount={pageCount}
                    marginPagesDisplayed={2}
                    pageRangeDisplayed={4}
                    onPageChange={this.handlePageChange}
                    containerClassName={"pagination"}
                    subContainerClassName={"pages pagination"}
                    activeClassName={"active"}
                    forcePage={store.page}
                />)
                : null
            )
    }
}

const mapStateToProps = (store) => {
    return {
        store: store.search
    };
};

export default connect(mapStateToProps, {fetchSearchResult})(SearchPagination);

