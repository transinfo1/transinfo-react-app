import React,  { Component } from 'react';
import translate from '../../utils/translate';
import SearchCommentsList from './SearchCommentsList'
import SearchArticlesList from './SearchArticlesList'
import SearchPagination from './SearchPagination';
import SearchPostsList from './SearchPostsList';
import SearchFilter from './SearchFilter';
import {connect} from 'react-redux';
import {fetchSearchResult} from '../../actions/search';
import Loader from '../../components/asyncData/Loader';

class SearchResultsList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            showSortBy: false,
        };
    }


    handleOnClick(){
        this.setState({
            showSortBy: !this.state.showSortBy
        });
    }

    sortSearchResult(e){
        const {store} = this.props;
        let sortValue = e.target.value;
        this.setState({
            showSortBy: false
        });
        this.props.fetchSearchResult({keywords: store.keywords, type: store.type, sortBy: sortValue, onlyTitle: store.onlyTitle})
    }

    render(){

    const {store} = this.props;
    let testString = 'search.radioButton';

    if (this.props.store.status === undefined) {
        document.location.href = '/404'
    }

    return(
      <div className="col-22 col-lg-18">
        <div className="search-results-list">
          <div className="row justify-content-center search-results-list__navigation">
              <div className="col-24 col-lg-12 push-lg-12 pagination-container">
                  <SearchPagination/>
              </div>
              <div className="col-12 hidden-lg-up mobile-filter">
                <SearchFilter/>
              </div>
              <div className="col-12 col-lg-12 pull-lg-12 sort-by">
                  <span onClick={this.handleOnClick.bind(this)} className="sort-by__header">{translate('search.sortBy')}</span>
                  <ul className={this.state.showSortBy ? "sort-by__list sort-by__list--show" : "sort-by__list"} >
                      <form onClick={this.sortSearchResult.bind(this)}>
                            <li className="sort-by__list__item">
                                <label className={store.sortBy === 'date' ? "sort-by__list__label sort-by__list__label--active" : "sort-by__list__label" }>
                                    <input className="sort-by__list__input" type="radio" name="sortBy" value="date" data-test={testString + '.date'} />{translate('search.sortByOptions.date')}
                                </label>
                            </li>
                            <li className="sort-by__list__item">
                                <label className={store.sortBy === 'popularity' ? "sort-by__list__label sort-by__list__label--active" : "sort-by__list__label" }>
                                    <input className="sort-by__list__input" type="radio" name="sortBy" value="popularity"  data-test={testString + '.popularity'} />{translate('search.sortByOptions.popularity')}
                                    </label>
                            </li>
                            <li className="sort-by__list__item">
                                <label className={store.sortBy === 'relevance' ? "sort-by__list__label sort-by__list__label--active" : "sort-by__list__label" }>
                                    <input className="sort-by__list__input" type="radio" name="sortBy" value="relevance"  data-test={testString + '.relevance'} />{translate('search.sortByOptions.accuracy')}
                                </label>
                            </li>
                      </form>
                  </ul>
              </div>
          </div>
            {store.fetching ?  <Loader/> :
                (store.data.comment.searchResults.length || store.data.post.searchResults.length || store.data.forum.searchResults.length) ?
                    (<div>
                        <SearchCommentsList data={store.data.comment.searchResults} keywords={store.keywords} page={store.page} type={store.type}/>
                        <SearchPostsList data={store.data.forum.searchResults} keywords={store.keywords} type={store.type} />
                        <SearchArticlesList data={store.data.post.searchResults} />
                        <div className="search-results-list__navigation search-results-list__navigation--bottom">
                            <div className="col-24 col-lg-18 pagination-container">
                                <SearchPagination/>
                            </div>
                        </div>
                    </div>)
                    :
                    store.keywords ?
                        <h5 className="no-results-header">{translate('search.noResult')}</h5>
                    :
                        <h5 className="no-results-header">{translate('search.inputSearchValue')}</h5>
            }
        </div>
      </div>
    )
  }
}

const mapStateToProps = (store) => {
    return {
        store: store.search
    };
};

export default connect(mapStateToProps, {fetchSearchResult})(SearchResultsList);
