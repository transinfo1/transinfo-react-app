import React,  { Component } from 'react';
import AuthorLink from '../../components/user/atoms/AuthorLink';
import CreatedDate from '../../components/createdDate/index.js';
import {authorName} from '../../utils/helpers';
import translate from '../../utils/translate';

class CommentItem extends Component {

    createContent(str){
        function trim(str){
            return str.replace(/^[ ),.-]+|\s*$/g, '');
        }
        let string = trim(str)

        let cleanText = string.replace(/<\/?[^>]+(>|$)/g, "");
        if(cleanText[0].toUpperCase() === cleanText[0]) {
            return {__html: string + '...'}
        } else {
            return {__html: '...' + string + '...'}
        }
    }

  render(){
    let {post, type} = this.props;
    let testString = 'search.comment';
    let testKey = 0;
    return(
        <div className="search-comment-item">
            {type === 'comment' ? <h4 className="search-comment-item__header" data-test="comment.title">{post.postTitle}</h4> : null}
            <div>
            <span className="search-comment-item__icon" />
            <a className="search-comment-item__link" href={'/' + post.postSlug + '#comments'}>
                { post.highlight.content ?
                    <p className="search-comment-item__content" data-test="comment.content" dangerouslySetInnerHTML={this.createContent(post.highlight.content[0])} />
                    :
                    <p className="search-comment-item__content" data-test="comment.content">{translate('message.emptyContentToView')}</p>
                }

            </a>
            </div>
            <div className="search-comment-item__info">
                    <AuthorLink disable={true} data={post.author} testString={testString + '.author'} testKey={testString}>
                        {authorName(post.author)}
                    </AuthorLink >
                    <CreatedDate date={post.dateGmt} testString={testString} testKey={testKey}/>
            </div>
        </div>
    )
  }
}

export default CommentItem;
