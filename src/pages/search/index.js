import React, { Component } from 'react';
import SearchForm from './SearchForm';
import SearchResultsList from './SearchResultsList';
import SearchFilter from './SearchFilter';
// import Sticky from 'react-sticky-el';
import {connect} from 'react-redux';
import {fetchWallPosts} from '../../actions/posts';

class Search extends Component {


      render(){
        let {location} = this.props;
        return(
          <section className="search-container">
              <div className="container">
                  <div className="row justify-content-center">
                      <div className="col-22 offset-lg-3 col-lg-18">
                        <SearchForm location={location}/>
                      </div>
                  </div>
              </div>
            <div className="container">
              <div className="row justify-content-center">
                  <div className="col-22 col-md-3 hidden-md-down">
                        <SearchFilter/>
                  </div>
                  <SearchResultsList/>
              </div>
            </div>
          </section>
        )
      }
}

const mapStateToProps = (store) => {
    return {
        store: store.posts
    };
};

export default connect(mapStateToProps, {fetchWallPosts})(Search);
