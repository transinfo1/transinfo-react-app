import React, {Component} from 'react';
import translate from '../../utils/translate';
import {connect} from 'react-redux';
import {fetchSearchResult} from '../../actions/search';


class SearchFilter extends Component {
    constructor(props) {
        super(props);
        this.state = {
            showFilter: false,
        };
    }

    handleOnClick() {
        this.setState({
            showFilter: !this.state.showFilter
        });
    }

    filterSearchResult(e) {
        const {store} = this.props;
        let filterValue = e.target.value
        this.setState({
            showFilter: false
        });
        this.props.fetchSearchResult({keywords: store.keywords, type: filterValue})
    }

    render() {
        const {store} = this.props;
        let testString = 'search.radioButton';
        return (
            <div className="search-filter">
                <h5 className="search-filter__header"
                    onClick={this.handleOnClick.bind(this)}>{translate('search.filter')}</h5>
                <ul className={this.state.showFilter ? "d-flex search-filter__list search-filter__list--show" : "d-flex search-filter__list"}>
                    <form className="search-filter__form" onChange={this.filterSearchResult.bind(this)}>
                        <li className="search-filter__form__item"><label className="search-filter__form__label"><input
                            className="search-filter__form__radio-button" type="radio" name="filterBy" data-test={testString + '.all'}
                            checked={store.type === 'all'} id="all" value="all"/>{translate('search.all')}</label>
                        </li>
                        <li className="search-filter__form__item"><label className="search-filter__form__label"><input
                            className="search-filter__form__radio-button" type="radio" name="filterBy" data-test={testString + '.post'}
                            checked={store.type === 'post'} id="post" value="post"/>{translate('search.inArticle')}</label>
                        </li>
                        <li className="search-filter__form__item"><label className="search-filter__form__label"><input
                            className="search-filter__form__radio-button" type="radio" name="filterBy" data-test={testString + '.comment'}
                            checked={store.type === 'comment'} id="comment"
                            value="comment"/>{translate('search.inComments')}</label>
                        </li>
                        <li className="search-filter__form__item"><label className="search-filter__form__label"><input
                            className="search-filter__form__radio-button" type="radio" name="filterBy" data-test={testString + '.forum'}
                            checked={store.type === 'forum'} id="forum" value="forum"/>{translate('search.inForum')}</label>
                        </li>
                        <li className="search-filter__form__item"><label className="search-filter__form__label"><input
                            className="search-filter__form__radio-button" type="radio" name="filterBy" data-test={testString + '.videos'}
                            checked={store.type === 'videos'} id="videos" value="videos"/>{translate('search.inVideo')}</label>
                        </li>
                    </form>
                </ul>
            </div>
        )
    }
}


const mapStateToProps = (store) => {
    return {
        store: store.search
    };
};
export default connect(mapStateToProps, {fetchSearchResult})(SearchFilter);
