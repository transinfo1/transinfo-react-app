import React,  { Component } from 'react';
import AuthorLink from '../../components/user/atoms/AuthorLink';
import CreatedDate from '../../components/createdDate/index.js';
import Amount from '../../components/comments/Amount';
import ViewMore from '../../components/viewMore/index.js';
import { Link } from 'react-router';
import Truncate from 'react-truncate';
import {authorName} from '../../utils/helpers';
import translate from '../../utils/translate';

class ArticleItem extends Component {

    createContent(str){
        function trim(str){
            return str.replace(/^[ ),.-]+|\s*$/g, '');
        }
        let string = trim(str)

        let cleanText = string.replace(/<\/?[^>]+(>|$)/g, "");
        if(cleanText[0].toUpperCase() === cleanText[0]) {
            return {__html: string + '...'}
        } else {
            return {__html: '...' + string + '...'}
        }
    }

    createTags(str){
        let cleanTag = str.replace(/<\/?[^>]+(>|$)/g, "");
        return cleanTag;
    }


    render(){
        let {post} = this.props;
        let testString = 'search.article';
        let testKey = 0;

        return(
            <div className="search-article">
                <a href={'/' + post.postSlug}>
                    <h4 className="search-article__header" data-test="article.title" dangerouslySetInnerHTML={{__html: post.highlight.postTitle ? post.highlight.postTitle[0] : post.postTitle}}/>
                </a>

                <div className="search-article__desc">
                    <div className="search-article__thumbnail">
                        <a href={'/' + post.postSlug}>
                            <img className="search-article__thumbnail__img" src={post.postFeaturedImage ? post.postFeaturedImage :"/static/img/default_image.jpg" } alt="post-thumbnail"/>
                        </a>
                    </div>


                    {post.highlight.content ?
                            <p data-test="article.content">
                                <span dangerouslySetInnerHTML={this.createContent(post.highlight.content[0])}/>
                                {post.highlight.tags ?
                                    <span className="mt-2 tag-container">{translate('posts.tags')}
                                        {post.highlight.tags.map( (tag, key) => {
                                            return (key < 3 ?  <Link className={"btn btn-tag "} key={key}><span className="trim m-0 tags-semibold tags" dangerouslySetInnerHTML={ {__html: '#' + this.createTags(tag)} }/></Link> : null )
                                        }) }

                                    </span>
                                    : null
                                }
                            </p>
                            : post.highlight.postTitle ? <p data-test="article.content"><Truncate lines={3}><span dangerouslySetInnerHTML={{__html: post.postBeforeContent}}></span></Truncate></p>
                            : post.highlight.tags ?
                                <p data-test="article.content">
                                    <Truncate lines={3} ><span dangerouslySetInnerHTML={{__html: post.postBeforeContent}}></span></Truncate>
                                    <span className="mt-2 tag-container tags-semibold">{translate('posts.tags')}
                                        {post.highlight.tags.map( (tag, key) => {
                                                    return (key < 3 ?  <Link className={"btn btn-tag "} key={key}><span className="trim m-0 tags" dangerouslySetInnerHTML={ {__html: '#' + this.createTags(tag)} }/></Link> : null )
                                                }) }
                                    </span>
                                </p>
                                : translate('message.emptyContentToView')
                    }


                </div>
                <div className="search-article__links">
                        <AuthorLink data={post.author} testString={testString +'.author'} testKey={testKey}>
                            {authorName(post.author)}
                        </AuthorLink>
                    <CreatedDate date={post.dateGmt} testString={testString} testKey={testKey}/>
                    <Amount value={post.commentsCount} description={true} testString={testString + '.comments'} testKey={testKey}/>
                    <ViewMore color path={post.postSlug} testString={testString} testKey={testKey} normalLink />
                </div>
            </div>
        )
    }
}

export default ArticleItem;