import React from 'react';
import Template from './Template';
import _ from 'lodash';
import Loader from '../../components/asyncData/Loader';
import AddNews from './atoms/AddNews';
import {StickyContainer} from 'react-sticky';

import Quote from './QuoteSingle';
import QuoteSet from './QuoteSet';
import TransTV from './TransTV';
import config from '../../config';

const List = ({ store, wall, featuredComments, transTV, postQuote }) => {

    const transTVMobLimit = config.wallBanners.transTVMobile;
    const featuredComMobLimit = config.wallBanners.featuredCommentsMobile;
    const transTVMobile = !(window.innerWidth > transTVMobLimit);
    const featuredCommentsMobile = !(window.innerWidth > featuredComMobLimit);

    const banner2Data = featuredComments ? featuredComments.data : null;
    const banner3Data =  transTV ? transTV.data : null;
    const banner1 = postQuote ? _.chunk(postQuote.data) : null;
    const banner2 = featuredCommentsMobile ? _.chunk(banner2Data, 2) : _.chunk(banner2Data, 3);
    const banner3 = transTVMobile ? _.chunk(banner3Data) : _.chunk(banner3Data, 4);

    let nextBanner = 1,
        banner1Counter = 0,
        banner1ListItem= -1,
        banner2Counter = 0,
        banner2ListItem = -1,
        banner3Counter = 0,
        banner3ListItem = -1,
        windowWidth = window.innerWidth,
        timeout,
        resizeWidth;

    function refresh() {
        windowWidth = window.innerWidth;
        windowWidth < transTVMobLimit ? (location.reload(true)) :  ( windowWidth > transTVMobLimit ? (location.reload(true)) : windowWidth = transTVMobLimit );
        windowWidth < featuredComMobLimit ? (location.reload(true)) :  ( windowWidth > featuredComMobLimit ? (location.reload(true)) : windowWidth = featuredComMobLimit );
    }

    window.onresize = function() {
        resizeWidth = window.innerWidth;
        clearTimeout(timeout);
        if ( (windowWidth > transTVMobLimit && resizeWidth < transTVMobLimit) || (windowWidth < transTVMobLimit && resizeWidth > transTVMobLimit)
            || (windowWidth > featuredComMobLimit && resizeWidth < featuredComMobLimit) || (windowWidth < featuredComMobLimit && resizeWidth > featuredComMobLimit) ) {
            timeout = setTimeout(refresh, 100);
        }
    };

    return (
        <StickyContainer>
            <AddNews />
            <section className="wall">
                {wall ?
                    store.data.slice(3).map((article, key) => {

                        const position = key + 1;

                        if(position && (position %  4 === 0)) {
                            switch(nextBanner){
                                case 1:
                                    nextBanner = 2;
                                    banner1Counter += 1;
                                    banner1ListItem += 1;
                                    if(banner1.length >= banner1Counter){
                                        if(!banner1[banner1ListItem]){
                                            return null;
                                        }
                                        return(
                                            <section key={key}>
                                                <div className="container">
                                                    <Template data={article} testKey={key}/>
                                                </div>
                                                <Quote key={'quote-'+ key} data={banner1[banner1ListItem]} />
                                            </section>
                                        )
                                    } else {
                                        if(banner2.length >= banner2Counter) {
                                            banner2Counter += 1;
                                            banner2ListItem += 1;
                                            if(banner2.length >= banner2Counter){
                                                if(!banner2[banner2ListItem]){
                                                    return null;
                                                }
                                                return(
                                                    <section key={key}>
                                                        <div className="container">
                                                            <Template data={article} testKey={key}/>
                                                        </div>
                                                        <QuoteSet key={'quoteSet-'+ key} data={banner2[banner2ListItem]} />
                                                    </section>
                                                )
                                            } else {
                                                if(banner3.length >= banner3Counter) {
                                                    banner3Counter += 1;
                                                    banner3ListItem += 1;
                                                    if(banner3.length >= banner3Counter){
                                                        if(!banner3[banner3ListItem]){
                                                            return null;
                                                        }
                                                        return(
                                                            <section key={key}>
                                                                <div className="container">
                                                                    <Template data={article} testKey={key}/>
                                                                </div>
                                                                <TransTV key={'transTv-'+ key} data={banner3[banner3ListItem]} />
                                                            </section>
                                                        )
                                                    }
                                                }
                                            }
                                        } else {
                                            if(banner3.length >= banner3Counter) {
                                                banner3Counter += 1;
                                                banner3ListItem += 1;
                                                if(banner3.length >= banner3Counter){
                                                    if(!banner3[banner3ListItem]){
                                                        return null;
                                                    }
                                                    return(
                                                        <section key={key}>
                                                            <div className="container">
                                                                <Template data={article} testKey={key}/>
                                                            </div>
                                                            <TransTV key={'transTv-'+ key} data={banner3[banner3ListItem]} />
                                                        </section>
                                                    )
                                                }
                                            }
                                        }
                                    }
                                    break;
                                case 2:
                                    nextBanner = 3;
                                    banner2Counter += 1;
                                    banner2ListItem += 1;
                                    if(banner2.length >= banner2Counter){
                                        if(!banner2[banner2ListItem]){
                                            return null;
                                        }
                                        return(
                                            <section key={key}>
                                                <div className="container">
                                                    <Template data={article} testKey={key}/>
                                                </div>
                                                <QuoteSet key={'quoteSet-'+ key} data={banner2[banner2ListItem]} />
                                            </section>
                                        )
                                    } else {
                                        if(banner3.length >= banner3Counter) {
                                            banner3Counter += 1;
                                            banner3ListItem += 1;
                                            if(banner3.length >= banner3Counter){
                                                if(!banner3[banner3ListItem]){
                                                    return null;
                                                }
                                                return(
                                                    <section key={key}>
                                                        <div className="container">
                                                            <Template data={article} testKey={key}/>
                                                        </div>
                                                        <TransTV key={'transTv-'+ key} data={banner3[banner3ListItem]} />
                                                    </section>
                                                )
                                            } else {
                                                if(banner1.length >= banner1Counter) {
                                                    banner1Counter += 1;
                                                    banner1ListItem += 1;
                                                    if(banner1.length >= banner1Counter){
                                                        if(!banner1[banner1ListItem]){
                                                            return null;
                                                        }
                                                        return(
                                                            <section key={key}>
                                                                <div className="container">
                                                                    <Template data={article} testKey={key}/>
                                                                </div>
                                                                <Quote key={'quote-'+ key} data={banner1[banner1ListItem]} />
                                                            </section>
                                                        )
                                                    }
                                                }
                                            }
                                        } else {
                                            if(banner1.length >= banner1Counter) {
                                                banner1Counter += 1;
                                                banner1ListItem += 1;
                                                if(banner1.length >= banner1Counter){
                                                    if(!banner1[banner1ListItem]){
                                                        return null;
                                                    }
                                                    return(
                                                        <section key={key}>
                                                            <div className="container">
                                                                <Template data={article} testKey={key}/>
                                                            </div>
                                                            <Quote key={'quote-'+ key} data={banner1[banner1ListItem]} />
                                                        </section>
                                                    )
                                                }
                                            }
                                        }
                                    }
                                    break;
                                case 3:
                                    nextBanner = 1;
                                    banner3Counter += 1;
                                    banner3ListItem += 1;
                                    if(banner3.length >= banner3Counter){
                                        if(!banner3[banner3ListItem]){
                                            return null;
                                        }
                                        return(
                                            <section key={key}>
                                                <div className="container">
                                                    <Template data={article} testKey={key}/>
                                                </div>
                                                <TransTV key={'transTv-'+ key} data={banner3[banner3ListItem]} />
                                            </section>
                                        )
                                    } else {
                                        if(banner1.length >= banner1Counter) {
                                            banner1Counter += 1;
                                            banner1ListItem += 1;
                                            if(banner1.length >= banner1Counter){
                                                if(!banner1[banner1ListItem]){
                                                    return null;
                                                }
                                                return(
                                                    <section key={key}>
                                                        <div className="container">
                                                            <Template data={article} testKey={key}/>
                                                        </div>
                                                        <Quote key={'quote-'+ key} data={banner1[banner1ListItem]} />
                                                    </section>
                                                )
                                            } else {
                                                if(banner2.length >= banner2Counter) {
                                                    banner2Counter += 1;
                                                    banner2ListItem += 1;
                                                    if(banner2.length >= banner2Counter){
                                                        if(!banner2[banner2ListItem]){
                                                            return null;
                                                        }
                                                        return(
                                                            <section key={key}>
                                                                <div className="container">
                                                                    <Template data={article} testKey={key}/>
                                                                </div>
                                                                <QuoteSet key={'quoteSet-'+ key} data={banner2[banner2ListItem]} />
                                                            </section>
                                                        )
                                                    }
                                                }
                                            }
                                        } else {
                                            if(banner2.length >= banner2Counter) {
                                                banner2Counter += 1;
                                                banner2ListItem += 1;
                                                if(banner2.length >= banner2Counter){
                                                    if(!banner2[banner2ListItem]){
                                                        return null;
                                                    }
                                                    return(
                                                        <section key={key}>
                                                            <div className="container">
                                                                <Template data={article} testKey={key}/>
                                                            </div>
                                                            <QuoteSet key={'quoteSet-'+ key} data={banner2[banner2ListItem]} />
                                                        </section>
                                                    )
                                                }
                                            }
                                        }
                                    }
                                    break;
                                default:
                                    return null;
                            }
                        }

                        return(
                            <section key={key}>
                                <div className="container">
                                    <Template data={article} testKey={key}/>
                                </div>
                            </section>
                        )

                    })
                    :
                    store.map((article, key) => {
                        return(
                            <div key={key} className="container">
                                <Template data={article} testKey={key}/>
                            </div>
                        )
                    })
                }
                {store.fetching ?
                    <Loader />
                    : null }
            </section>
        </StickyContainer>
    )
};

export default List;
