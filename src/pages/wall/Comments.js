import React, {Component} from 'react';
import translate from '../../utils/translate';
import {connect} from 'react-redux';
import {Link} from 'react-router';
import {sortByCreatedDateDesc} from '../../utils/sort';

import Thumbnail from '../../components/user/atoms/Thumbnail';
import AuthorLink from '../../components/user/atoms/AuthorLink';

import CreatedDate from '../../components/createdDate';
import AddComment from '../../components/comments/AddForm';
import Amount from '../../components/comments/Amount';
import { authorName } from '../../utils/helpers';
import replaceEntities from '../../utils/commentEntities';

// import Likes from '../../components/social/Likes';

class Comments extends Component {
    getComments() {
      return this.props.store.data.concat(this.props.comments)
          .filter(item => item.postId === this.props.postId && item.parent === 0)
          .sort(sortByCreatedDateDesc)
    }

    render() {
        const {testString, testKey, amount, slug} = this.props;
        return (
            <div className="wall-comments">

                <div className="head-mobile d-flex flex-row py-2 my-4 hidden-lg-up">
                    <Amount className="ml-auto mr-2" value={amount + this.getComments().filter(item => item.justAdded).length} description type="comments" testString={testString + '.mobile'} testKey={testKey} />
                    <Link to={'/' + slug + '#comments'}>{translate('functionalities.addComment')}</Link>
                </div>

                <div className="head-desktop hidden-md-down d-flex flex-row align-items-center border-bottom pb-2 mb-2">
                  <h2 className="mb-0 mr-auto">{translate('posts.lastComments')}:</h2>
                  <Amount value={amount + this.getComments().filter(item => item.justAdded).length} description type="comments" testString={testString} testKey={testKey} />
                </div>

                {/*{this.getComments().length === 0 ? <small className="ml-1">{translate('posts.noComments')}</small> : ''}*/}

                <ul>
                    {this.getComments()
                        .slice(0, 2)
                        .map((item, key) => {

                            let author = item.author || {slug: null, avatar: ''};

                            let {slug, avatar} = author;

                            let maxLength = 180;
                            let deleted = item.status !== 'approved';

                            let content = deleted ? translate('message.deletedComment') : item.content
                            content = content.length > maxLength ? content.slice(0, maxLength) + '...' : content;

                            let deletedCommentClass = deleted ? ' deleted-comment' : '';

                            return (
                                <li className={'comment-item d-flex flex-column mb-1' + deletedCommentClass} key={key}>

                                    <div className="d-flex flex-row align-items-center">
                                        <AuthorLink slug={slug} disable={true} testString={testString + '.comment.thumbnail'} testKey={testKey + '' + key}>
                                            <Thumbnail path={avatar} name={authorName(item.author)} className="m-2" />
                                        </AuthorLink>
                                        <AuthorLink slug={slug} disable={true} className="m-2" testString={testString + '.comment'} testKey={testKey + '' + key}>
                                            {authorName(item.author)}
                                        </AuthorLink>
                                        <CreatedDate date={item.dateGmt} className="ml-auto m-2" testString={testString + '.comment'} testKey={testKey + '' + key} />
                                    </div>
                                    <span className="px-3 comment-text" data-test={testString + '.comment.content' + testKey + '' + key}>{ replaceEntities(content) }</span>


                                    {/* <div className="d-flex flex-row align-items-center justify-content-end">
                                        <Likes colorful data={item.likes} type="comment" id={item.id} testString={testString + '.comment'} testKey={testKey + '' + key} />
                                        <Amount value={item.repliesCount} type="replies" className="ml-1" testString={testString + '.comment'} testKey={testKey + '' + key} />
                                    </div> */}
                                </li>
                            )
                    })}
                </ul>
                {this.getComments().length === 0 ? '' :
                  <div className="view-more-comments d-flex hidden-md-down py-2">
                      <Link className="ml-auto" to={'/' + slug + '#comments'} data-test={testString + '.seeMoreComments' + testKey}> {translate('search.seeMoreComments')}</Link>
                  </div>
                }

                <div className="hidden-md-down">
                    <AddComment postId={this.props.postId} testString={testString} testKey={testKey} />
                </div>
            </div>
        );
    }
}

const mapStateToProps = (store) => ({
    store: store.comments
});

export default connect(mapStateToProps, {})(Comments);
