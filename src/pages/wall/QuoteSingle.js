import React from 'react';
import PostAuthor from '../../components/user/PostAuthor';
import SocialBar from '../../components/social/SocialBar';
import ViewMore from '../../components/viewMore';

const QuoteSingle = ({ data }) => {

    const testString = 'singleQuote';

    const quotes = data.map((item, index) => {
        return(
            <div key={'quote-single-' + index} className="d-flex flex-column flex-md-row align-items-md-center">
                <div className="quote-user">
                    <PostAuthor author={item.author} className="mr-md-5 py-2 px-4" testString={testString + '.user'} />
                </div>
                <div className="quote-content pr-4">
                    <p data-test={testString + '.content'} className="quote-text">
                        <span dangerouslySetInnerHTML={ {__html: item.excerpt } } />
                    </p>
                    <div className="d-flex flex-column flex-md-row align-items-center">
                        <SocialBar data={item} colorful horizontalLeft />
                        <ViewMore className="ml-md-auto" path={item.slug} testString={testString} testKey={0} color normalLink />
                    </div>
                </div>
            </div>
        )
    });

    return(
        <section className="quote quote-single mb-3 mb-lg-5 py-3 py-lg-5">
            <div className="container">
                {quotes}
            </div>
        </section>
    )
};

export default QuoteSingle;
