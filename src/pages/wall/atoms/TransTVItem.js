import React from 'react';
import Thumbnail from '../../../components/thumbnail';
// import {Link} from 'react-router';

const TransTVItem = ({ video }) => {
    return(
      <li className="col-md-6">
          <Thumbnail data={video} path={"/" + video.slug} linked />
          <a href={'/' + video.slug} className="video-description" dangerouslySetInnerHTML={ {__html: video.title} } />
      </li>
    );
};

export default TransTVItem;
