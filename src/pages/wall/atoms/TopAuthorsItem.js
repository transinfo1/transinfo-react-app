import React from 'react';
import AuthorLink from '../../../components/user/atoms/AuthorLink';
import {authorName} from '../../../utils/helpers';

const TopAuthorItem = ({ author, testString, testKey }) => {

  const authorThumbnail = author.avatar ? author.avatar : ''; // /static/img/topauthor.png

  return(
    <li style={{"backgroundImage": `url(${authorThumbnail})`}}>
      <AuthorLink data={author} testString={testString} testKey={testKey} className="w-50">
          <p data-test={testString + '.name' + testKey} className="m-0" dangerouslySetInnerHTML={{__html: authorName(author, true)}} />
      </AuthorLink>
      <div className="role mt-2 w-50" data-test={testString + '.role' + testKey}>
          {author.userCaption || null}
      </div>
    </li>
  );
}

export default TopAuthorItem;
