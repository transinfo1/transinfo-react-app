import React from 'react';
//import { Link } from 'react-router';
import Thumbnail from '../../../components/user/atoms/Thumbnail';
import GoToPost from '../../../components/goToPost';
import { truncate, authorName } from '../../../utils/helpers';

const QuoteSetItem = ({ testString, testKey, comment }) => {
    return(
      <li className="d-flex flex-column flex-sm-row col-lg-8">
        <div className="post-author mr-sm-4" data-test={testString + '.thumbnail' + testKey}>
            {comment.author ?
                <Thumbnail path={comment.author.avatar} name={authorName(comment.author)} />
            : null }
        </div>
        <div className="d-flex flex-column w-100">
            <div className="post-author mb-2" data-test={testString + '.user' + testKey}>
                {authorName(comment.author)}
            </div>
            <p data-test={testString + '.content' + testKey} className="mb-4 quote-text pr-lg-3">
                <a href={'/' + comment.postSlug}><span dangerouslySetInnerHTML={ {__html: truncate(comment.content, 24)  } } /></a>
            </p>
            <GoToPost className="quote-gotopost align-self-center align-self-sm-end align-self-lg-start" path={comment.postSlug} testString={testString} testKey={testKey} />
        </div>
      </li>
    );
};

export default QuoteSetItem;
