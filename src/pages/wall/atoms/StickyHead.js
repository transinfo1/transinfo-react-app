import React from 'react';
import {Link} from 'react-router';
import PostAuthor from '../../../components/user/PostAuthor';
import SocialBar from '../../../components/social/SocialBar';

const StickyHead = ({ data, headingClass = "", testString = "", testKey = "" }) => {
  return(
    <div className="head" data-test={testString + testKey + '.title'}>
        <Link className={"mb-3 " + headingClass} to={'/' + data.slug}><span dangerouslySetInnerHTML={ {__html: data.title} } /></Link>
        <div className="d-flex flex-column align-items-start flex-sm-row align-items-sm-center">
            <PostAuthor author={data.author} className="d-flex align-items-center mr-3" testString={testString + testKey} />
            <SocialBar data={data} testString={testString + testKey} sticky />
        </div>
    </div>
  );
};

export default StickyHead;
