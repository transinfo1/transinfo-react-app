import React from 'react';
import {Sticky} from 'react-sticky';
import translate from '../../../utils/translate';

const AddNews = () => {
  return(
    <Sticky className="add-news-sticky" stickyStyle={{top: 80}} >
      <section data-popup="addNews" className="wall-add-news">
        <i data-popup="addNews" className="icon fa fa-plus" aria-hidden="true"></i>
        <p data-popup="addNews">{translate('popups.news.addNews')}</p>
      </section>
    </Sticky>
  );
}

export default AddNews;
