import React from 'react';
import ViewMore from '../../components/viewMore';
import TransTVItem from './atoms/TransTVItem';
import translate from '../../utils/translate';

const TransTV = ({ data }) => {

    const testString = "transTV";
    const videos = data.map((item, index) => {
        return <TransTVItem key={"transtvItem-" + index} video={item} />
    });

    return(
        <div className="transtv mb-3 mb-lg-5 py-4 py-lg-5">
            <div className="container">
                <h2 className="mb-3 mb-lg-5">Trans.INFO TV</h2>
                <ul className="d-md-flex flex-md-row">
                    {videos}
                </ul>
                <div className="d-flex flex-column align-items-end">
                    <ViewMore className="align-self-end" testString={testString} color path={"videos"} modified modifiedTxt={translate('main.seeAll')} />
                </div>
            </div>
        </div>
    )
};

export default TransTV;
