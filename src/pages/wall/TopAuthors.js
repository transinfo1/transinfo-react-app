import React, { Component } from 'react';
import { connect } from 'react-redux';
import translate from '../../utils/translate';
import { fetchTopAuthors } from '../../actions/topAuthors';
import TopAuthorsItem from './atoms/TopAuthorsItem';

class TopAuthors extends Component {

    renderList(authors, testString){
        return authors.map((item, index) => {
            return <TopAuthorsItem key={"topauthor-" + index} author={item} testString={testString} testKey={index} />;
        });
    }
    render(){

        const testString = 'topAuthors';
        const { topAuthorsStore } = this.props;
        const authors = topAuthorsStore.fetched ? topAuthorsStore.data : false;

        if(!authors) {
            return null;
        }

        return(
            <section className="top-authors container-fluid hidden-md-down my-md-5">
                <span className="btn btn-tag"><a href="/people">{translate('label.topAuthor')}</a></span>
                <ul className="d-flex flex-row">
                    {authors ? this.renderList(authors, testString) : null}
                </ul>
            </section>
        );
    }
}

const mapStateToProps = (store) => {
    return{
        topAuthorsStore: store.topAuthors
    }
};

export default connect(mapStateToProps, { fetchTopAuthors })(TopAuthors);
