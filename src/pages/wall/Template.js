import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Link} from 'react-router';
import RelatedPosts from '../../components/relatedPosts';
import PostAuthor from '../../components/user/PostAuthor';
import SocialBar from '../../components/social/SocialBar';
import Comments from './Comments';
import CreatedDate from '../../components/createdDate';
import Tags from '../../components/tags';
import ViewMore from '../../components/viewMore';
import Thumbnail from '../../components/thumbnail';

import * as artUtils from '../../utils/posts';

class Template extends Component {

    render() {
        const {data, testKey = 0} = this.props;
        let testString = 'articles.wall.article';
        return (
            <article className="wall-post-item row mb-3 mb-lg-5">

                <div className="col-24 col-lg-3">
                    <div
                        className="user-card d-flex align-items-center flex-lg-column align-items-lg-start"> {/*user-card d-flex align-items-center flex-lg-column align-items-lg-start*/}
                        <PostAuthor author={data.author}
                                    className="d-flex align-items-center flex-lg-column align-items-lg-baseline"
                                    testString={testString + '.author'} testKey={testKey}/>
                        <CreatedDate date={data.dateGmt} testString={testString} testKey={testKey}/>
                        <Tags cropped className="ml-auto ml-lg-0" data={data.tags} testString={testString}
                              testKey={testKey}/>
                    </div>
                </div>

                <div className="col-24 col-lg-12">

                    <div className="wall-post-content">

                        <div className="wrapper">
                            {(data.type === 'videos') ?
                                <div data-test={testString + '.link' + testKey}>
                                    <Thumbnail data={data}  />
                                </div>
                                :
                                <Link data-test={testString + '.link' + testKey}
                                      to={'/' + data.slug}
                                      style={artUtils.getFeaturedImage(data.featuredImage)}
                                      className="post-thumbnail"/>
                            }
                            <Link className="gradient-link-img" to={'/' + data.slug}></Link>
                            {(!data.beforeContent) ?
                                <div className="head-on-image">
                                    <Link to={"/" + data.slug}>
                                        <h2 data-test={testString + '.title' + testKey}
                                            dangerouslySetInnerHTML={ {__html: data.title} }/>
                                    </Link>
                                    <div className="d-sm-flex justify-content-sm-between mt-3">
                                        <SocialBar data={data} testString={testString} testKey={testKey}/>
                                        <ViewMore className="mt-3 mt-sm-0 hidden-sm-down" path={data.slug}
                                                  testString={testString} testKey={testKey}/>
                                    </div>
                                </div>
                                :
                                <div className="head-on-image">
                                    <Link to={"/" + data.slug}>
                                        <h2 data-test={testString + '.title' + testKey}
                                            dangerouslySetInnerHTML={ {__html: data.title} }/>
                                    </Link>
                                </div>
                            }
                        </div>

                        {(data.beforeContent) ?
                            <div className="head mt-3">
                                <div dangerouslySetInnerHTML={ {__html: data.beforeContent} }
                                     data-test={testString + '.contentBefore' + testKey}/>
                                <div className="d-sm-flex justify-content-sm-between mt-3">
                                    <SocialBar data={data} testString={testString} testKey={testKey} colorful/>
                                    <ViewMore className="mt-3 mt-sm-0" path={data.slug} testString={testString}
                                              testKey={testKey} color/>
                                </div>
                            </div>
                            : null}

                    </div>

                </div>

                <div className="col-24 col-lg-9">
                    <Comments postId={data.id} slug={data.slug} amount={data.commentsCount}
                              testString={testString + '.comments'} testKey={testKey} comments={data.lastComments} />
                    {
                        this.props.store.data.filter(item => item.postId === data.id).length + data.commentsCount === 0 &&
                            <RelatedPosts post={data} type={data.type === 'videos' ? 'videos' : 'post'} thumb="small" vertical className="wall-comments mt-3"/>
                    }
                </div>

            </article>
        );
    }
}

const mapStateToProps = (store) => {
    return {
        store: store.comments
    };
};

export default connect(mapStateToProps)(Template);
