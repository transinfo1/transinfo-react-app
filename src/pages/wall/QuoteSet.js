import React from 'react';
import QuoteSetItem from './atoms/QuoteSetItem';
import translate from '../../utils/translate';

const QuoteSet = ({ data }) => {
    const testString = "setQuote";
    const comments = data.map((item, index) => {
        return <QuoteSetItem key={'quote-set-' + index} comment={item} testString={testString} testKey={index} />
    });
    return(
        <section className="quote quote-set mb-3 mb-lg-5 py-3 py-lg-5">
            <div className="container">
                <h2 className="py-3">{translate('label.userQuote')}</h2>
                <ul className="d-lg-flex flex-lg-row">
                    {comments}
                </ul>
            </div>
        </section>
    )
};
export default QuoteSet;