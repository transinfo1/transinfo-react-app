import config from '../../config.json';

import React, {Component} from 'react';
import {connect} from 'react-redux';
import moment from 'moment';
import {fetchWallPosts, fetchNextPosts} from '../../actions/posts';
import { fetchFeaturedComments } from '../../actions/comments';
import { fetchBannerVideos } from '../../actions/videos';
import {setComments} from '../../actions/comments';
import { fetchQuotePosts } from '../../actions/posts';

import {getLastCreatedDate} from '../../utils/posts';

import Waypoint from 'react-waypoint';
import AsyncData from '../../components/asyncData/AsyncData';

import List from './List';
import Sticky from './Sticky';
import TopAuthors from './TopAuthors';

import seo from '../../utils/seo';
import translate from '../../utils/translate';

class Wall extends Component {

    constructor(props){
        super(props);
        this.state = {
            seedSession: Math.random().toString(36).substring(7),
            lazyLoadCount: 0
        }
    }

    componentDidMount() {
        if(!this.props.postsStore.fetched) {
            this.props.fetchWallPosts()
        }
    }


    lazyLoad() {
        const {postsStore} = this.props;
        const dateNow = new Date();

        // Featured Comments
        const comments = this.props.featuredCommentsStore.data;
        const commentDate = comments.length ? comments.slice(-1)[0].dateGmt : moment(dateNow).format('x');

        // Banner Videos
        const videos = this.props.transtvStore.data;
        const videoDate = videos.length ? videos.slice(-1)[0].dateGmt : moment(dateNow).format('x');

        // Loading banners data on lazyload
        this.props.fetchFeaturedComments(3, commentDate);
        this.props.fetchBannerVideos(4, videoDate);
        this.props.fetchQuotePosts(1, this.state.lazyLoadCount, 90, this.state.seedSession);

        if (postsStore.fetched && !postsStore.fetching) {
            this.props.fetchNextPosts(
              getLastCreatedDate(postsStore.data.slice(3)),
              config.articlesWall.lazyLoadAmount
            );
        }

        this.setState({ lazyLoadCount: this.state.lazyLoadCount + 1 });
    }

    render() {
        const {postsStore, featuredCommentsStore, transtvStore, quoteStore} = this.props;

        if(postsStore.fetched && postsStore.data.length === 0) {
            return <div className="no-data-msg">{translate('message.noPosts')}</div>
        }

        // wall
        return (
            <div className="home">

              { seo({
                title: translate('main.title'),
                description: translate('main.description'),
                avatar: 'avatar.jpg',
                tags: ''
              }) }

                <AsyncData store={postsStore} component={<Sticky data={postsStore.data}/>} showLoader={false} />
                <TopAuthors />
                <List store={postsStore} featuredComments={featuredCommentsStore} transTV={transtvStore} postQuote={quoteStore} wall />
                <Waypoint onEnter={() => this.lazyLoad()} />
            </div>
        )
    }
}

const mapStateToProps = (store) => ({
    postsStore: store.posts,
    commentsStore: store.comments,
    featuredCommentsStore: store.featuredComments,
    transtvStore: store.bannerVideos,
    quoteStore: store.postsQuote
});

export default connect(mapStateToProps, {fetchWallPosts, fetchNextPosts, setComments, fetchFeaturedComments, fetchBannerVideos, fetchQuotePosts})(Wall);
