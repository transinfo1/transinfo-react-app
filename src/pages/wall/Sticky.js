import React from 'react';
import * as postUtils from '../../utils/posts';
import {Link} from 'react-router';
import VideoThumbnail from '../../components/thumbnail';
import {getLang} from '../../utils/storeData';

import StickyHead from './atoms/StickyHead';

const Sticky = ({data}) => {

    const testString = "sticky";

    function generateStickyContent (dataId, header) {
        if(data[dataId].type !== 'videos') {
            return <span>
                     <Link to={'/' + data[dataId].slug} className="sticky-thumbnail" style={postUtils.getFeaturedImage(data[dataId].featuredImage)} />
                     <Link className="gradient-link-img" to={'/' + data[dataId].slug}></Link>
                     <StickyHead data={data[dataId]} headingClass={header} testString={testString + ".article"} testKey={dataId + 1} />
                   </span>
        } else {
            return <span>
                    <VideoThumbnail data={data[2]} linkClass="sticky-thumbnail" linked wall />
                    <Link className="gradient-link-img" to={'/' + data[dataId].slug}></Link>
                    <StickyHead data={data[2]} headingClass="h2" testString={testString + ".video"} />
                    <Link to="/videos" className="btn btn-tag-sticky">trans.INFO TV</Link>
                   </span>
                }
    }



    return (
        <section className="wall-sticky container-fluid mb-4 mb-lg-0">
            <div className="content d-flex flex-row">
                <article className="sticky-main">
                    <div className="wrapper" data-test="sticky.article1" data-testLang={getLang()}>
                        {generateStickyContent(0, 'h1')}
                    </div>
                </article>
                <aside className="sticky-sub d-flex flex-column hidden-lg-down">
                    <div className="sticky-sub-item wrapper" data-test="sticky.video">
                        {generateStickyContent(2, 'h2')}
                    </div>
                    <div className="sticky-sub-item wrapper" data-test="sticky.article1">
                        {generateStickyContent(1, 'h2')}
                    </div>
                </aside>
            </div>
        </section>
    )
}

export default Sticky;
