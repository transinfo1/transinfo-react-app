import React, {Component} from 'react';
import SocialBar from '../../components/social/SocialBar';
import RelatedPosts from '../../components/relatedPosts';
import Comments from './Comments';
import * as postUtils from '../../utils/posts';
import  { tableConverter }  from '../../utils/tableConverter';
import ImageGallery from 'react-image-gallery';
import { StickyContainer, Sticky } from 'react-sticky';
import PostAuthor from '../../components/user/PostAuthor';
import CreatedDate from '../../components/createdDate';
import Tags from '../../components/tags';
import translate from '../../utils/translate';
import {authorName} from '../../utils/helpers';
import AddNews from '../wall/atoms/AddNews';

class Template extends Component {
    componentDidMount() {
        tableConverter();
    }
    renderGallery(gallery){

      if(!gallery){
        return null;
      }

      let images = [];

      gallery.map((image) => {
        return images.push({ original: image, thumbnail: image });
      });

      return(
        <div className="post-gallery mt-5">
          <ImageGallery items={images} slideInterval={2000} lazyLoad={true} showPlayButton={false} showIndex={true} />
        </div>
      )

    }

    render() {
        let {data} = this.props;
        let testString = 'posts.article';
        let testKey = 0;
        let datePublished = new Date(data.dateGmt).toISOString();
        let dateModified = new Date(data.modifiedGmt).toISOString();

        const schema = {
            "@context": "http://schema.org",
            "@type": "NewsArticle",
            "mainEntityOfPage": {
                "@type": "WebPage",
                "@id": 'http://trans.info/'+data.slug
            },
            "headline": data.title,
            "image": {
                "@type": "ImageObject",
                "url": data.featuredImage || 'http://trans.info/default.jpg',
                "height": 800,
                "width": 800
            },
            "datePublished": datePublished,
            "dateModified": dateModified,
            "author": {
                "@type": "Person",
                "name": authorName(data.author)
            },
            "publisher": {
                "@type": "Organization",
                "name": "transINFO",
                "logo": {
                    "@type": "ImageObject",
                    "url": "http://transinfo.dev.firetms.io/static/img/logo.gif",
                    "width": 600,
                    "height": 60
                }
            },
            "description": data.content
        };

        return (
        <section className="post article">
                <script type="application/ld+json">
                    {JSON.stringify(schema)}
                </script>

              <StickyContainer>

                <header className="featured-image" style={postUtils.getFeaturedImage(data.featuredImage)}>
                    <div className="gradient-link-img"></div>
                    <div className="container">
                        <div className="row">
                            <h1 className="col-24 col-lg-18 offset-lg-3" data-test="article.title">{data.title}</h1>
                        </div>
                    </div>
                </header>

                <div className="mt-5">
                    <AddNews />
                </div>

                <section className="container content">
                    <div className="row">
                        <aside className="col-24 col-lg-3">
                            <div className="user-card d-flex align-items-center flex-lg-column align-items-lg-start">
                                <PostAuthor author={data.author} className="d-flex align-items-center flex-lg-column align-items-lg-baseline" testString={testString + '.author'} testKey={testKey} />
                                <CreatedDate date={data.dateGmt} testString={testString} testKey={testKey} />
                                {/* <Tags className="ml-auto ml-lg-0" data={data.tags} testString={testString} testKey={testKey} /> */}
                            </div>
                            <Sticky stickyStyle={{top: 50, zIndex: 4}}>
                                <div className="social-bar-side hidden-md-down">
                                  <SocialBar data={data} colorful vertical wordShare testString="article"/>
                                </div>
                            </Sticky>
                        </aside>
                        <article className="col-24 col-lg-18">
                            <div className="htmlOutput mt-3 ml-lg-3 mt-lg-0" itemProp="description" dangerouslySetInnerHTML={ {__html: data.content} }
                                 data-test="article.content"></div>
                               <div className="social-bar-mobile hidden-lg-up mt-3">
                                   <CreatedDate date={data.dateGmt} testString={testString} testKey={testKey} className="float-right" />
                                   <div className="clearfix"></div>
                                <SocialBar data={data} colorful wordShare testString="article"/>
                            </div>
                            {this.renderGallery(data.gallery)}
                        </article>

                        <section className="col-24 col-lg-21 offset-lg-3 mt-5">
                          {data.tags.length > 0 ?
                          <div className="border-bottom pb-3 mb-5 d-flex flex-row align-items-center">
                              <h2 className="mb-0 py-0">{translate('posts.tags')}:</h2>
                              <Tags horizontal className="ml-auto ml-lg-2" data={data.tags} testString={testString} testKey={testKey} />
                          </div>
                          : null}
                          <RelatedPosts post={data} limit={3} type="post" slider />
                        </section>
                    </div>
                </section>

                <section className="container mt-5">
                  <div className="row">
                    <div className="col-24 col-lg-21 offset-lg-3">
                        <Comments postId={data.id} amount={data.commentsCount} testString="article.comments" />
                    </div>
                  </div>
                </section>
              </StickyContainer>

            </section>
        );
    };
};

export default Template;
