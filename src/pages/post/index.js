import React, {Component} from 'react';
import {connect} from 'react-redux';
import {fetchOnePost} from '../../actions/posts';

import ArticleTemplate from './ArticleTemplate';
import VideoTemplate from './VideoTemplate';
import Loader from '../../components/asyncData/Loader';
import {authorName} from '../../utils/helpers';
import {browserHistory} from 'react-router';

import seo from '../../utils/seo';

class Post extends Component {

    componentWillMount() {
        const { params, store } = this.props;
        if(!store.fetched || !this.selectPost().length) {
            this.props.fetchOnePost(params.postSlug)
        }
    }

    selectPost() {
      const {store, params} = this.props;
      return store.data.filter(
        item => item.slug === params.postSlug
      )
    }

    render() {
        const {store} = this.props;
        //error
        if (this.props.store.status === 404) {
            browserHistory.push('/404');
        }

        if(store.fetched && this.selectPost().length) {
            let post = this.selectPost()[0]

            let description = post.content.substr(0, 180);
            description = description.substr(0, Math.min(description.length, description.lastIndexOf(" ")))+' ...';
            let tagsListTemp = [];
            post.tags.slice(0,10).forEach(item => {
              tagsListTemp.push(item.name);
            });
            let tagsList = tagsListTemp.join(', ');

            return (
              <div>
                { seo({
                  title: post.title,
                  author: authorName(post.author),
                  description: description.replace(/<p[^>]*>/g, ""),
                  avatar: post.featuredImage || "avatar.jpg",
                  tags: tagsList,
                  googleNews: post.googleNews,
                }) }

                  {post.type === 'videos' ? <VideoTemplate data={post} /> : <ArticleTemplate data={post} />}
                </div>

            );
        } else {
            return <Loader />
        }
    }
}

const mapStateToProps = (store) => {
    return {
        store: store.posts
    };
};

export default connect(mapStateToProps, {fetchOnePost})(Post);
