import React, {Component} from 'react';
import translate from '../../utils/translate';
import {connect} from 'react-redux';
import {sortByCreatedDateAsc, sortByCreatedDateDesc} from '../../utils/sort';
import {getAllIds} from '../../utils/storeData';
import {getLastCreatedDate} from '../../utils/posts';
import replaceEntities from '../../utils/commentEntities';
import {fetchComments} from '../../actions/comments';

import AddComment from '../../components/comments/AddForm';
import Likes from '../../components/social/Likes';
import Thumbnail from '../../components/user/atoms/Thumbnail';
import AuthorLink from '../../components/user/atoms/AuthorLink';
import Amount from '../../components/comments/Amount';
import CreatedDate from '../../components/createdDate';
import { authorName } from '../../utils/helpers';

class Comments extends Component {

    componentWillMount() {
        let lastCommentCreatedDate = getLastCreatedDate(this.getComments())
        this.props.fetchComments(this.props.postId, lastCommentCreatedDate)
        .then(() => {
          getAllIds(this.props.commentsStore.data.filter(item => item.repliesCount > 0 && item.postId === this.props.postId)).forEach(parentId => {
            let lastReplyCreatedDate = getLastCreatedDate(this.getComments(this.props.postId));
            this.props.fetchComments(this.props.postId, lastReplyCreatedDate, parentId, 2);
          })
        })
    }

    componentDidMount() {
        // go to anchor '#comments'
        if(location.hash === "#comments") {
            let $ = window.jQuery;
            $( document ).ready(() =>{
                $('html, body').animate({ scrollTop: $('#comments').offset().top }, 'slow');
            })
        }
    }

    getComments(parentId = 0) {
        return this.props.commentsStore.data
            .filter(item => item.postId === this.props.postId && item.parent === parentId)
            .sort(!parentId ? sortByCreatedDateDesc : sortByCreatedDateAsc)
    }

    getReplies(parentId, limit = 1000) {
      let lastDate = this.getComments(parentId).length ? this.getComments(parentId).slice(-1)[0].dateGmt : null;
        this.props.fetchComments(
          this.props.postId,
          lastDate,
          parentId,
          limit
        )
    }

    commentTemplate(data, key, testString, testKey, commentKey = '') {

      let author = data.author || {slug: null, avatar: ''};

      let {slug, avatar} = author;
      let deleted = data.status !== 'approved';
      let content = deleted ? translate('message.deletedComment') : data.content
      let deletedCommentClass = deleted ? ' deleted-comment' : '';
      let showClass = data.repliesCount === 0 || data.justAdded ? '' : ' show';

      return (
        <div key={key} className="comments-item mb-4">
          <div className={'d-flex flex-row' + deletedCommentClass}>

            <AuthorLink slug={slug} disable={true} testString={testString + '.comment.thumbnail'} testKey={commentKey + '' + key}>
                <Thumbnail path={avatar} name={authorName(data.author)} className="ml-auto" testString={testString + '.comment'} testKey={commentKey + '' + key} />
            </AuthorLink>

            <div className="ml-3">
                <AuthorLink slug={slug} disable={true} testString={testString + '.comment'} testKey={commentKey + '' + key}>
                    {authorName(data.author)}
                </AuthorLink>
                <p className="comment-text" data-test={testString + '.comment.content' + commentKey + '' + key}>{replaceEntities(content).replace('&#8243;', '"')}</p>
                <div className="d-flex flex-row align-items-center flex-wrap">
                    <Likes colorful type="comment" data={data.likes} id={data.id} testString={testString + '.comment'} testKey={commentKey + '' + key} />
                    <CreatedDate date={data.dateGmt} className="mr-3" testString={testString + '.comment'} testKey={commentKey + '' + key} />
                    {data.parent === 0 ?
                      <div className="d-flex flex-row" data-toggle="collapse" data-target={'#repliesContainer-' + data.id}>
                        <Amount className="d-flex flex-row align-items-center" type="replies" value={data.repliesCount + this.getComments(data.id).filter(item => item.justAdded).length} description testString={testString + '.comment'} testKey={commentKey + '' + key} />
                        {/*{this.getComments(data.id).length === 0 ? <div className="amount ml-3" data-test={testString + '.writeReplyButton' + key}>{translate('label.placeholder.answer')}</div> : ''}*/}
                        {this.getComments(data.id).length === 0 ? <div className="amount hidden-md-down ml-3" data-test={testString + '.writeReplyButton' + key}>{translate('label.placeholder.answer')}</div> : ''}
                    </div>
                    : ''}
                </div>
            </div>

          </div>

          {data.parent === 0 ?

          <div className={'children collapse out' + showClass} id={'repliesContainer-' + data.id}>
            {this.getComments(data.id).map((item, replyKey) => {
                  return this.commentTemplate(item, replyKey, this.props.testString + '.reply', this.props.testKey, key)
                }
            )}

            {data.repliesCount > this.getComments(data.id).length ?
              <div className="toggle-replies toggle-replies-show" onClick={() => this.getReplies(data.id)}>
                {translate('main.seeMoreAnswers')}
              </div>
            :
              <div className="toggle-replies toggle-replies-hide" data-toggle="collapse" data-target={'#repliesContainer-' + data.id}>
                  {translate('main.hideAnswers')}
              </div>
            }

            <AddComment postId={this.props.postId} parentId={data.id} testString={testString} testKey={key} beforeSendCallback={() => {this.getReplies(data.id)}} />

          </div>

          : '' }

        </div>
      )

    } 

    render() {

        const testString = this.props.testString;

        return (
            <div className="comments">

                <div className="head border-bottom d-flex justify-content-between">
                    <h2 id="comments">
                      {translate('posts.comments')}
                    </h2>
                    <Amount value={this.props.amount + this.props.commentsStore.data.filter(item => item.postId === this.props.postId && item.justAdded).length} description testString={testString} />
                </div>

                <div>

                  <div className="d-flex flex-row align-items-center my-4">
                    <AuthorLink className="mr-3" slug={this.props.authStore.data.user ? this.props.authStore.data.user.avatar : null} >
                        <Thumbnail path={this.props.authStore.data.user ? this.props.authStore.data.user.avatar : null} />
                    </AuthorLink>
                    <AddComment postId={this.props.postId} testString={testString} />
                  </div>

                  {this.getComments().map((item, key) => {
                        return this.commentTemplate(item, key, this.props.testString, this.props.testKey)
                      }
                  )}

                </div>
            </div>
        );
    }
}

const mapStateToProps = (store) => ({
    commentsStore: store.comments,
    authStore: store.auth
});

export default connect(mapStateToProps, {fetchComments})(Comments);
