import React, { Component } from 'react';

import SocialBar from '../../components/social/SocialBar';
import RelatedPosts from '../../components/relatedPosts';
import Comments from './Comments';
import PostAuthor from '../../components/user/PostAuthor';
import CreatedDate from '../../components/createdDate';
import Tags from '../../components/tags';
import Player from '../../pages/video/atoms/Player';
import translate from '../../utils/translate';

class Template extends Component {
    render(){

        let {data} = this.props;
        let testString = 'posts.video';
        let testKey = 0;

        return(
            <section data-test="video" className="post video">

                <Player video={data} testString={testString + '.player'} />

                <section className="container content">
                    <div className="row">
                        <aside className="col-24 col-lg-3">
                          <div className="user-card d-flex align-items-center flex-lg-column align-items-lg-start">
                              <PostAuthor author={data.author} className="d-flex align-items-center flex-lg-column align-items-lg-baseline" testString={testString + '.author'} testKey={testKey} />
                              <CreatedDate date={data.dateGmt} testString={testString} testKey={testKey} />
                              {/*<Tags className="ml-auto ml-lg-0" data={data.tags} testString={testString} testKey={testKey} />*/}
                          </div>
                        </aside>
                        <article className="col-24 col-lg-14">
                            <div className="article-head mt-3 mt-lg-0">
                                <h1 data-test={testString + '.title'} className="mt-3 mb-0 mb-lg-2" dangerouslySetInnerHTML={ {__html: data.title} }></h1>
                                <div className="hidden-md-down">
                                    <SocialBar data={data} colorful wordShare horizontalLeft data-test={testString + '.social'}/>
                                </div>
                            </div>
                            <div className="htmlOutput" dangerouslySetInnerHTML={ {__html: data.content} }
                                 data-test={testString + '.content'}></div>

                            {data.tags.length > 0 ?
                                <div className="pb-3 mt-3 d-flex flex-row align-items-center">
                                    <h2 className="mb-0">{translate('posts.tags')}:</h2>
                                    <Tags horizontal className="ml-auto ml-lg-2" data={data.tags} testString={testString} testKey={testKey} />
                                </div>
                                : null}


                            <div className="social-bar-mobile hidden-lg-up mt-3">
                                <SocialBar data={data} colorful wordShare horizontalRight data-test={testString + '.social'} />
                            </div>


                        </article>

                        <aside className="col-24 col-lg-7 mt-lg-0">
                            <RelatedPosts post={data} vertical type="videos" />
                        </aside>
                    </div>
                </section>

                <section className="container mt-5" id="comments">
                    <div className="col-24 col-lg-21 offset-lg-3">
                        <Comments postId={data.id} amount={data.commentsCount} testString="article.comments" />
                    </div>
                </section>

            </section>
        );
    }
}

export default Template;
