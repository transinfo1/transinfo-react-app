import React, { Component } from 'react';
import translate from '../../../utils/translate';
import { connect } from 'react-redux';
import AboutForm from './atoms/AboutDetailsForm';
import CareerForm from './atoms/CareerDetailsForm';
import InterestsForm from './atoms/InterestsDetailsForm';
import LoginForm from './atoms/LoginDetailsForm';
import UserForm from './atoms/UserDetailsForm';
import ProfileNav from './atoms/ProfileNav';
import ProfileNavBottom from './atoms/ProfileNavBottom';
import UserAvatar from './atoms/UserAvatar';

import { updateProfile } from '../../../actions/profile';
import { setAuthenticatedUser } from '../../../actions/auth';

import { requestAsAuthenticatedUser } from '../../../utils/authentication';
import { sendNotification } from '../../../utils/notification';
import {whilePending} from '../../../utils/helpers';
import Notification from '../../../components/notification';
import Loader from '../../../components/asyncData/Loader';

class Template extends Component {

    // componentDidMount(){
    //     requestAsAuthenticatedUser('/users/profile/me', null, 'get', response => {
    //         this.props.updateProfile(response);
    //     });
    // }

    request(path, data, callback = () => {}, method = 'put', extraParams) {

        whilePending(true, '.btn-update');
        requestAsAuthenticatedUser(path, data, method, () => {
            window.scrollTo(0,0);
            sendNotification(translate('message.success.changesSaved'), 'success', 3000);
            callback();
            whilePending(false, '.btn-update');
        }, (error) => {
            if(error.response.status !== 403) {
                sendNotification(translate('message.errors.savingFailure'), 'error', 3000);
            }
            whilePending(false, '.btn-update');
        }, null, null, extraParams)
    }

    render(){
        const {profileStore, authStore} = this.props;
        const user = (profileStore.fetched && !profileStore.fetching) ? profileStore.data : false;
        const social = (authStore.data.provider !== 'wordpress' && authStore.data.provider !== 'onelogin');
        const testString = 'editProfile';

        return(
            <div className="edit-profile">
                <div className="head-dark head">
                  <div className="container">
                      <h2 className="mb-0">
                        {translate('account.myAccount')}
                      </h2>
                  </div>
                </div>
                <div className="container mt-5">
                    <div className="row">
                        <aside className="col-24 col-lg-3 mb-3">
                          <div className="wrapper">
                            {user ? <UserAvatar user={user} social={social} testString={testString} request={this.request} updateAuthObj={this.props.setAuthenticatedUser} updateProfile={this.props.updateProfile} /> : null }
                            <ProfileNav social={social} />
                          </div>
                        </aside>
                        <div className="col-24 col-lg-21">
                              <Notification data={this.props.notificationStore} />
                          {!social ?
                              <div className="row mb-3">
                                  {/*d-lg-flex flex-lg-row*/}
                                  <div className="col-24 col-md-12">
                                    <div id="block-user"  className="profile-block m-1">
                                        {/*w-100 w-lg-50 mb-3 mb-lg-0 mr-lg-3 d-flex flex-column*/}
                                        <h2>
                                          {translate('account.profile.headers.aboutYou')}
                                        </h2>
                                        {user ? <UserForm user={user} testString={testString} updateProfile={this.props.updateProfile} request={this.request} /> : <Loader />}
                                    </div>
                                  </div>
                                  <div className="col-24 col-md-12">
                                    <div id="block-login" className="profile-block m-1">
                                        {/*w-100 w-lg-50 d-flex flex-column*/}
                                        <h2>
                                          {translate('label.login')}
                                        </h2>
                                        {user ? <LoginForm user={user} testString={testString} request={this.request} /> : <Loader /> }
                                    </div>
                                  </div>
                              </div>
                            : null }
                            <div className="row">
                                {/*d-lg-flex flex-lg-row*/}
                                <div className="col-24 col-md-12">
                                    <div id="block-interests" className="profile-block m-1">
                                        {/*w-100 w-lg-50 mb-3 mb-lg-0 mr-lg-3 d-flex flex-column p-0*/}
                                        <h2>
                                            {translate('account.profile.headers.interests')}
                                        </h2>
                                        {user ? <InterestsForm user={user} testString={testString} request={this.request} /> : <Loader />}
                                    </div>
                                </div>
                                <div className="col-24 col-md-12">
                                    {/*<div className=" p-0">*/}
                                        <div id="block-career" className="profile-block m-1 mb-3">
                                            <h2>
                                              {translate('account.profile.headers.companyAndPosition')}
                                            </h2>
                                            {user ? <CareerForm user={user} testString={testString} request={this.request} /> : <Loader /> }
                                        </div>
                                        <div id="block-about" className="profile-block m-1">
                                            <h2>
                                              {translate('account.profile.headers.aboutMe')}
                                            </h2>
                                            {user ? <AboutForm user={user} testString={testString} request={this.request} /> : <Loader />}
                                        </div>
                                    {/*</div>*/}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="row hidden-lg-up">
                      <div className="col-24">
                        <ProfileNavBottom />
                      </div>
                    </div>
                </div>
            </div>
        );
    }
}


const mapStateToProps = (store) => {
  return {
    profileStore: store.profile,
    authStore: store.auth,
    notificationStore: store.notification
  }
}

export default connect(mapStateToProps, { updateProfile, setAuthenticatedUser })(Template);
