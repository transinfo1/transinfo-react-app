import React, { Component } from 'react';

import { onInputChange } from '../../../../utils/onInputChange';
import translate from '../../../../utils/translate';
import config from '../../../../config';
import InputText from '../../../../components/form/InputText';
import Button from '../../../../components/form/Button';

class CareerDetailsForm extends Component {
  constructor(props){
    super(props);

    this.state = {
      companyName: '',
      positionName: '',
      transId: ''
    }

    this.onChange = this.onChange.bind(this);
  }
  componentWillMount(){
    const { user } = this.props;

    this.setState({
      id: user.id ? user.id : '',
      companyName: user.companyName ? user.companyName : '',
      positionName: user.positionName ? user.positionName : '',
      transId: user.transId ? user.transId : ''
    })

  }
  onChange(event){
    const user = onInputChange(event, this.state);
    this.setState({ user });
  }
  onFormSubmit(event, state){
      event.preventDefault();
      this.props.request("/users/profile/" + this.state.id + "/companyPosition/edit", state)
  }
  render(){

    const { testString } = this.props;

    return(
      <form className="info-form" onSubmit={event => this.onFormSubmit(event, this.state)}>
          {/*d-flex flex-column*/}
          <InputText groupClasses="row" //d-flex flex-row align-items-center
                     labelClasses="col-7 my-auto "
                     wrapperClasses="col-17"
                     labelTxt={translate('label.company')}
                     htmlFor="companyName"
                     value={this.state.companyName}
                     onInputChange={this.onChange}
                     testString={testString + '.careerDetails'}
                     pattern={config.regex.noSpecials}
                     validationMessage={translate('message.validation.noSpecials')}
                     maxLength="35"
                     required />
           <InputText groupClasses="row" //d-flex flex-row align-items-center
                      labelClasses="col-7 my-auto "
                      wrapperClasses="col-17"
                      labelTxt={translate('label.role')}
                      htmlFor="positionName"
                      value={this.state.positionName}
                      onInputChange={this.onChange}
                      testString={testString + '.careerDetails'}
                      pattern={config.regex.noSpecials}
                      validationMessage={translate('message.validation.noSpecials')}
                      maxLength="35"
                      required />
          <InputText groupClasses="row"
                     labelClasses="col-7 my-auto "
                     wrapperClasses="col-17"
                     labelTxt="Trans-id"
                     htmlFor="transId"
                     value={this.state.transId}
                     onInputChange={this.onChange}
                     testString={testString + '.careerDetails'}
                     disabled />
          <div className="form-group mb-0 d-flex flex-row align-items-end">
              <div className="btn-wrapper d-flex flex-column align-items-end">
                  <div className="d-flex flex-row align-items-center">
                      {/*<em className="mr-4">* {translate('label.optional')}</em>*/}
                      <Button className="btn-color btn-padding btn-update"
                              buttonTxt={translate('label.buttons.update')}
                              testString={testString + '.careerDetails'} />
                  </div>
              </div>
          </div>
      </form>
    )
  }
}

export default CareerDetailsForm;
