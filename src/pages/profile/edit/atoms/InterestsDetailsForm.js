import React, { Component } from 'react';
import { sendNotification } from '../../../../utils/notification';
import { onInputChange } from '../../../../utils/onInputChange';
import { checkForTrueVal } from '../../../../utils/helpers';
import translate from '../../../../utils/translate';
import Checkbox from '../../../../components/form/Checkbox';
import Button from '../../../../components/form/Button';

class InterestsDetailsForm extends Component {
  constructor(props){
    super(props);
    // Initial component state
    this.state = {
      companyAnswers: {
        "wcw1": false,
        "wcw2": false,
        "wcw3": false,
        "wcw4": false,
        "wcw5": false,
        "wcw6": false
      },
      positionAnswers: {
        "wpw1": false,
        "wpw2": false,
        "wpw3": false,
        "wpw4": false,
        "wpw5": false,
        "wpw6": false
      },
      labels: {
        "wcw1": translate('label.profile.aboutCompany.companyAnswer1'),
        "wcw2": translate('label.profile.aboutCompany.companyAnswer2'),
        "wcw3": translate('label.profile.aboutCompany.companyAnswer3'),
        "wcw4": translate('label.profile.aboutCompany.companyAnswer4'),
        "wcw5": translate('label.profile.aboutCompany.companyAnswer5'),
        "wcw6": translate('label.profile.aboutCompany.companyAnswer6'),
        "wpw1": translate('label.profile.aboutPosition.positionAnswer1'),
        "wpw2": translate('label.profile.aboutPosition.positionAnswer2'),
        "wpw3": translate('label.profile.aboutPosition.positionAnswer3'),
        "wpw4": translate('label.profile.aboutPosition.positionAnswer4'),
        "wpw5": translate('label.profile.aboutPosition.positionAnswer5'),
        "wpw6": translate('label.profile.aboutPosition.positionAnswer6')
      }
    }
    // Bindings
    this.onChange = this.onChange.bind(this);
  }

  componentDidMount(){

    const {user} = this.props;

    // Updating state after user data comes in
    this.setState({
      id: user.id ? user.id : false,
      companyAnswers: {
        "wcw4": user.wcw4 ? user.wcw4 : false,
        "wcw2": user.wcw2 ? user.wcw2 : false,
        "wcw3": user.wcw3 ? user.wcw3 : false,
        "wcw1": user.wcw1 ? user.wcw1 : false,
        "wcw5": user.wcw5 ? user.wcw5 : false,
        "wcw6": user.wcw6 ? user.wcw6 : false
      },
      positionAnswers: {
        "wpw1": user.wpw1 ? user.wpw1 : false,
        "wpw2": user.wpw2 ? user.wpw2 : false,
        "wpw3": user.wpw3 ? user.wpw3 : false,
        "wpw4": user.wpw4 ? user.wpw4 : false,
        "wpw5": user.wpw5 ? user.wpw5 : false,
        "wpw6": user.wpw6 ? user.wpw6 : false
      }
    });

  }
  onFormSubmit(event, state){
    event.preventDefault();

    // Prepare object to send
    const obj = {
      "wcw1": state.companyAnswers.wcw1,
      "wcw2": state.companyAnswers.wcw2,
      "wcw3": state.companyAnswers.wcw3,
      "wcw4": state.companyAnswers.wcw4,
      "wcw5": state.companyAnswers.wcw5,
      "wcw6": state.companyAnswers.wcw6,
      "wpw1": state.positionAnswers.wpw1,
      "wpw2": state.positionAnswers.wpw2,
      "wpw3": state.positionAnswers.wpw3,
      "wpw4": state.positionAnswers.wpw4,
      "wpw5": state.positionAnswers.wpw5,
      "wpw6": state.positionAnswers.wpw6
    };

    // Validate if at least one value is selected
    let companyList = !!checkForTrueVal(state.companyAnswers);
    let positionList = !!checkForTrueVal(state.positionAnswers);
    // Show error message if all values are empty
    if(!companyList || !positionList){
        return sendNotification(translate('message.errors.error') + ' ' + translate('message.errorSelectVal'), 'error', 3000);
    }

    // Proceed if validation is passed
      this.props.request("/users/profile/" + state.id + "/interests/edit", obj);


  }
  onChange(event, state, listType){
    const info = onInputChange(event, state);
    const answerList = {};
    answerList[listType] = info;
    this.setState(answerList);
  }
  renderCompanyAnswers(list, testString){
    return Object.keys(list.companyAnswers).map((item) => {
      return(
        <li key={item}>
          <Checkbox checkboxTxt={list.labels[item]}
                    htmlFor={item}
                    onInputChange={event => this.onChange(event, list.companyAnswers, 'companyAnswers')}
                    testString={testString + '.companyDetails'}
                    value={list.companyAnswers[item]} />
        </li>
      )
    })
  }
  renderPositionAnswers(list, testString){
    return Object.keys(list.positionAnswers).map((item) => {
      return(
        <li key={item}>
          <Checkbox checkboxTxt={list.labels[item]}
                    htmlFor={item}
                    onInputChange={event => this.onChange(event, list.positionAnswers, 'positionAnswers')}
                    testString={testString + '.positionDetails'}
                    value={list.positionAnswers[item]} />
        </li>
      )
    })
  }
  render(){

    const {testString} = this.props;

    return(
      <form className="info-form d-flex flex-column" onSubmit={event => this.onFormSubmit(event, this.state)}>
          <div className="form-group">
              <h3 className="mb-3 pb-2 border-bottom">{translate('label.profile.aboutCompany.companyQuestion')}</h3>
              <ul className="form-check form-check-inline">
                  {this.renderCompanyAnswers(this.state, testString)}
              </ul>
          </div>
          <div className="form-group">
              <h3 className="mt-4 mb-3 pb-2 border-bottom">{translate('label.profile.aboutPosition.positionQuestion')}</h3>
              <ul className="form-check form-check-inline">
                  {this.renderPositionAnswers(this.state, testString)}
              </ul>
          </div>
          <div className="form-group mb-0 d-flex flex-row align-items-end">
              <div className="btn-wrapper d-flex flex-column align-items-end">
                <Button className="btn-color btn-padding btn-update"
                        buttonTxt={translate('label.buttons.update')}
                        testString={testString + '.interestsDetails'} />
              </div>
          </div>
      </form>
    )
  }
}

export default InterestsDetailsForm;
