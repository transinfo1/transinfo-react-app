import React from 'react';
import { Link } from 'react-router';
import translate from '../../../../utils/translate';

const ProfileNav = ({ social }) => {

  return(
    <nav className="profile-nav mt-3">
      <ul className="mb-0 desktop hidden-md-down">
          <li className="pt-0"><Link to="/user/profile" activeClassName="active" >{translate('navigation.profile')}</Link></li>
          <li><Link to="/contact" activeClassName="active" >{translate('navigation.contact')}</Link></li>
          <li><Link to="/policy" activeClassName="active">{translate('navigation.policy')}</Link></li>
      </ul>
      <ul className="mb-0 mobile hidden-lg-up">
          <li className="pt-0 profile">
            <Link to="#mobilenavprofile" data-toggle="collapse">
              <span className="d-flex flex-row align-items-center">
                {translate('navigation.profile')}
                <img className="icon ml-2" src="/static/img/icons/arrow_down.svg" alt="arrow down" />
              </span>
            </Link>
          </li>
          <li className="collapse" id="mobilenavprofile">
            <ul>
              {!social ? <li><a href="#block-user">{translate('popups.EP.EPuser')}</a></li> : null }
              {!social ? <li><a href="#block-login">{translate('popups.EP.EPlogin')}</a></li> : null }
              <li><a href="#block-career">{translate('popups.EP.EPcareer')}</a></li>
              <li><a href="#block-about">{translate('popups.EP.EPabout')}</a></li>
              <li><a href="#block-interests">{translate('popups.EP.EPinterests')}</a></li>
            </ul>
          </li>
      </ul>
    </nav>
  )
}

export default ProfileNav;
