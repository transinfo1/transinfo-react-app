import React, { Component } from 'react';
import moment from 'moment';
import { connect } from 'react-redux';

import { updateUserInfo } from '../../../../utils/authentication';
import { onInputChange } from '../../../../utils/onInputChange';
import translate from '../../../../utils/translate';
import config from '../../../../config';
import InputText from '../../../../components/form/InputText';
import InputDate from '../../../../components/form/InputDate';
import Button from '../../../../components/form/Button';

class UserDetailsForm extends Component  {
  constructor(props){
    super(props); 
    // Initial component state
    this.state = {
        id: '',
        firstName: '',
        lastName: '',
        birthDate: ''
    }

    // Bindings
    this.onChange = this.onChange.bind(this);
    this.onFormSubmit = this.onFormSubmit.bind(this);
  }



  componentWillMount(){

    const {user} = this.props;

    // Updating state after user data comes in
    this.setState({
        id: user.id ? user.id : '',
        firstName: user.rpcUserDTO.firstName ? user.rpcUserDTO.firstName : '',
        lastName: user.rpcUserDTO.lastName ? user.rpcUserDTO.lastName : '',
        birthDate: user.birthDate ? moment(user.birthDate).format('YYYY-MM-DD') : ''
    })

  }

  onChange(event){
    const info = onInputChange(event, this.state);
    this.setState(info);
  }

  onFormSubmit(event, state){
    let birthValue = document.getElementsByName('birthDate')[0].value;
    event.preventDefault();

    const {updateProfile, user} = this.props;

    const newInfo = {
      firstName: state.firstName,
      lastName: state.lastName,
      birthDate: birthValue ? birthValue : null
    };
      // birthDate: state.birthDate ? moment(state.birthDate).format("YYYY-MM-DD[T]HH:mm:ss.SSS[Z]") : null
    const newUser = {
        ...user,
        rpcUserDTO: {
            ...user.rpcUserDTO,
            firstName: state.firstName,
            lastName: state.lastName,
            birthDate: birthValue ? birthValue : null
        }
    };

      this.props.request("/users/profile/" + state.id + "/aboutYou/edit", newInfo, () => {
          updateProfile(newUser);
          updateUserInfo(newInfo);
      })

  }

  render(){

    const { testString } = this.props;
    const maxBirthDate = moment(new Date()).format('YYYY-MM-DD');

    return(
      <form className="info-form" onSubmit={event => this.onFormSubmit(event, this.state)}>
          {/*d-flex flex-column*/}
          <InputText groupClasses="row" //d-flex flex-row align-items-center
                     labelClasses="col-7 my-auto "
                     wrapperClasses="col-17"
                     labelTxt={translate('label.name')}
                     htmlFor="firstName"
                     value={this.state.firstName}
                     onInputChange={this.onChange}
                     testString={testString + '.userDetails'}
                     pattern={config.regex.noSpecialsAndNumbers}
                     validationMessage={translate('message.validation.noSpecialsAndNumbers')}
                     maxLength="35"
                     required />
         <InputText groupClasses="row" //d-flex flex-row align-items-center
                    labelClasses="col-7 my-auto "
                    wrapperClasses="col-17"
                    labelTxt={translate('label.surname')}
                    htmlFor="lastName"
                    value={this.state.lastName}
                    onInputChange={this.onChange}
                    testString={testString + '.userDetails'} 
                    pattern={config.regex.noSpecialsAndNumbers}
                    validationMessage={translate('message.validation.noSpecialsAndNumbers')}
                    maxLength="35"
                    required />
        <InputDate groupClasses="row" //d-flex flex-row align-items-center
                   labelClasses="col-7 my-auto "
                   wrapperClasses="col-17"
                   htmlFor="birthDate"
                   labelTxt={translate('label.dob')}
                   value={this.state.birthDate}
                   onInputChange={this.onChange}
                   testString={testString + '.userDetails'}
                   maxDate={maxBirthDate} />


          <div className="form-group mb-0 d-flex flex-row align-items-end">
              <div className="btn-wrapper d-flex flex-column align-items-end">
                  <Button className="btn-color btn-padding btn-update"
                          buttonTxt={translate('label.buttons.update')}
                          testString={testString + '.userDetails'} />
              </div>
          </div>
      </form>
    )
  }
}

const mapStateToProps = (store) => {
    return {
        profileStore: store.profile,
    }
}

export default connect(mapStateToProps)(UserDetailsForm);
