import React, { Component } from 'react';

import { onInputChange } from '../../../../utils/onInputChange';
import translate from '../../../../utils/translate';
import InputEmail from '../../../../components/form/InputEmail';
import InputPassword from '../../../../components/form/InputPassword';
import Button from '../../../../components/form/Button';

class LoginDetailsForm extends Component {
  constructor(props){
    super(props);

    this.state = {
      email: '',
      password: '',
      oldPassword: ''
    }

    this.onChange = this.onChange.bind(this);
  }
  componentWillMount(){
    const { user } = this.props;
    this.setState({
      email: user.email ? user.email : '',
      id: user.id ? user.id : ''
    });
  }
  onChange(event){
    const user = onInputChange(event, this.state);
    this.setState({ user });
  }
  onFormSubmit(event, state){

    event.preventDefault();

    const obj = {
      password: state.password,
      oldPassword: state.oldPassword
    }

      this.props.request("/users/profile/" + state.id + "/password/edit", obj)
  }
  render(){

    const {testString} = this.props;

    return(
      <form className="info-form" onSubmit={event => this.onFormSubmit(event, this.state)}>
        {/*d-flex flex-column*/}
          <InputEmail groupClasses="d-flex flex-row align-items-center"
                      labelClasses="col-7"
                      inputClasses="col-17"
                      labelTxt={translate('label.email')}
                      htmlFor="email"
                      testString={testString + '.loginDetails'}
                      value={this.state.email}
                      disabled
                      required />
        <InputPassword groupClasses="d-flex flex-row align-items-center"
                       labelClasses="col-7"
                       inputClasses="col-17"
                       labelTxt={translate('label.oldPassword')}
                       testString={testString + '.loginForm'}
                       htmlFor="oldPassword"
                       onInputChange={this.onChange}
                       value={this.state.oldPassword}
                       required />
       <InputPassword groupClasses="d-flex flex-row align-items-center"
                      labelClasses="col-7"
                      inputClasses="col-17"
                      htmlFor="password"
                      pattern="(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{5,}$"
                      validationMessage={translate('message.validation.passwordRequirements')}
                      labelTxt={translate('label.newPassword')}
                      testString={testString + '.loginForm'}
                      onInputChange={this.onChange}
                      value={this.state.password}
                      required/>
          <div className="form-group mb-0 d-flex flex-row align-items-end">
              <div className="btn-wrapper d-flex flex-column align-items-end">
                <Button className="btn-color btn-padding btn-update"
                        buttonTxt={translate('label.buttons.update')}
                        testString={testString + '.loginDetails'} />
              </div>
          </div>
      </form>
    )
  }
}


// <div className="form-group form-password d-flex flex-row align-items-center">
//     <label htmlFor="example-text-input" className="col-form-label col-6">Stare hasło</label>
//     <input className="form-control" type={this.state.passwordType} />
//     <img onClick={this.onPasswordShow} src="/static/img/icons/password.svg" alt="Password Icon" />
// </div>
// <div className="form-group form-password d-flex flex-row align-items-center">
//     <label htmlFor="example-text-input" className="col-form-label col-6">Nowe hasło</label>
//     <input className="form-control" type={this.state.passwordType} />
//     <img onClick={this.onPasswordShow} src="/static/img/icons/password.svg" alt="Password Icon" />
// </div>


export default LoginDetailsForm;
