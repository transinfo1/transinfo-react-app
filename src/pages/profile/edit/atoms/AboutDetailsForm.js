import React, { Component } from 'react';
import { onInputChange } from '../../../../utils/onInputChange';
import translate from '../../../../utils/translate';
import TextArea from '../../../../components/form/TextArea';
import Button from '../../../../components/form/Button';

class AboutDetailsForm extends Component {
  constructor(props){
    super(props);

    this.state = {
      id: '',
      description: ''
    }

    this.onChange = this.onChange.bind(this);
  }
  componentWillMount(){
    const { user } = this.props;

    this.setState({
      id: user.id ? user.id : '',
      description: user.rpcUserDTO.description ? user.rpcUserDTO.description : ''
    })

  }
  onChange(event){
    const user = onInputChange(event, this.state);
    this.setState(user);
  }
  onFormSubmit(event, state){
    event.preventDefault();
    this.props.request("/users/profile/" + this.state.id + "/description/edit", state.description)
  }
  render(){
    const {testString} = this.props;

      return(
    <form className="info-form d-flex flex-column" onSubmit={event => this.onFormSubmit(event, this.state)}>
        <TextArea rows="3"
                  htmlFor="description"
                  onInputChange={this.onChange}
                  testString={testString + '.aboutDetails'}
                  value={this.state.description}
                  placeholder={translate('label.placeholder.writeSomething')}
                  maxLength="300"
                  required />
          <div className="form-group mb-0 d-flex flex-row align-items-end">
              <div className="btn-wrapper d-flex flex-column align-items-end">
                <Button className="btn-color btn-padding btn-update"
                        buttonTxt={translate('label.buttons.update')}
                        testString={testString + '.aboutDetails'} />
              </div>
          </div>
      </form>
    )
  }
}

export default AboutDetailsForm;
