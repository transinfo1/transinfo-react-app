import React from 'react';
import { Link } from 'react-router';
import translate from '../../../../utils/translate';

const ProfileNavBottom = () => {
  return(
    <nav className="profile-nav-bottom hidden-lg-up">
      <ul>
        <li>
          <Link to="contact">{translate('navigation.contact')}</Link>
        </li>
        <li>
          <Link to="policy">{translate('navigation.policy')}</Link>
        </li>
      </ul>
    </nav>
  );
}

export default ProfileNavBottom;
