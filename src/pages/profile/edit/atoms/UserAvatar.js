import React from 'react';
import ReactDom from "react-dom";

import { sendNotification } from '../../../../utils/notification';
import translate from '../../../../utils/translate';
import {authorName} from '../../../../utils/helpers';
import { getAuthObj, getUser } from '../../../../utils/authentication';
import AvatarCropper from "react-avatar-cropper";

var UserAvatar = React.createClass({
    getInitialState: function() {
        return {
            cropperOpen: false,
            img: null,
            croppedImg: getUser().avatar || '/static/img/default-avatar.png'
        };
    },

    buttonTranslate: function() {
        const modalBoxButtons = document.querySelectorAll('.modal-footer button');

        modalBoxButtons[0].setAttribute('class', 'fa fa-times close btn-close');
        modalBoxButtons[0].innerText = '';

        modalBoxButtons[1].setAttribute('class', 'btn btn-color btn-padding');
        modalBoxButtons[1].innerText = translate('label.buttons.save');
    },
    canvasMobile() {
        const canvas = document.querySelector('.row canvas');

        canvas.setAttribute('class', 'canvas')
    },

    handleFileChange: function(dataURI) {
        this.setState({
            img: dataURI,
            croppedImg: this.state.croppedImg,
            cropperOpen: true
        });
        this.buttonTranslate();
        this.canvasMobile();
    },

    handleCrop: function(dataURI) {
        this.setState({
            cropperOpen: false,
            img: null,
            croppedImg: dataURI
        });
        function urltoFile(url, filename, mimeType, request){
            return (fetch(url)
                    .then(function(res){return res.arrayBuffer();})
                    .then(function(buf){return {
                        request: request,
                        file: new File([buf], filename, {type:mimeType})
                    }
                    })
            );
        }

        urltoFile(dataURI, 'avatar.png', 'image/png', this.props.request)
            .then(resp => {

                let data = new FormData();
                data.append('file', resp.file);

                resp.request("/users/profile/" + this.props.user.id + "/avatar/", data, () => {
                    this.props.updateAuthObj({
                        ...getAuthObj(),
                        user: {
                            ...getAuthObj().user,
                            avatar: dataURI
                        }
                    })
                    sendNotification(translate('message.success.changesSaved'), 'success', 3000);
                }, 'post', 'fileUpload')
            })
    },
    handleRequestHide: function() {
        this.setState({
            cropperOpen: false
        });
    },

    render () {
        let userName = authorName(getAuthObj().user);
        return (
                <div className="avatar">
                    <div className="avatar-wrapper">
                        <img src={this.state.croppedImg} alt={userName} />

                        {!this.props.social &&
                            <div className="avatar-edit">
                                {translate('label.buttons.change')}
                                <FileUpload handleFileChange={this.handleFileChange}/>
                            </div>
                        }
                    </div>
                    {this.state.cropperOpen &&
                    <AvatarCropper
                        onRequestHide={this.handleRequestHide}
                        cropperOpen={this.state.cropperOpen}
                        onCrop={this.handleCrop}
                        image={this.state.img}
                        width={100}
                        height={100}
                    />
                    }
                    <p className="name mb-0 mt-2">{userName}</p>
                </div>


        );
    }
});

var FileUpload = React.createClass({

    handleFile: function(e) {
        var reader = new FileReader();
        var file = e.target.files[0];

        if (!file) return;

        reader.onload = function(img) {
            ReactDom.findDOMNode(this.refs.in).value = '';
            this.props.handleFileChange(img.target.result);
        }.bind(this);
        reader.readAsDataURL(file);
    },

    render: function() {
        return (
            <input ref="in" type="file" accept="image/*" onChange={this.handleFile} />
        );
    }
});

export default UserAvatar;