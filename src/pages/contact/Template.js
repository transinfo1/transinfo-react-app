import React, { Component } from 'react';
import translate from '../../utils/translate';
import axios from 'axios';
import seo from '../../utils/seo';

import {connect} from 'react-redux';
import getConfig from '../../utils/getConfig';
import { showPopup } from '../../actions/popups';

import {getLang} from '../../utils/storeData';

class Template extends Component {
    constructor(props) {
        super(props);
        this.state = {
            checkboxState: true
        }
    }

    onFormSubmit(event){
        event.preventDefault();
        const inputs = event.target.querySelectorAll('input');
        const textArea = event.target.querySelectorAll('textarea');

        let obj = {
            attachments: [null],
            email: inputs[2].value,
            firstName: inputs[0].value,
            lastName: inputs[1].value,
            message: textArea[0].value,
        };
        axios.post(getConfig().restApiHostName + '/email/contact', obj).then(() => {
            inputs.forEach(item => item.value = '');
            textArea[0].value = '';
            this.props.showPopup('contactMsgSuccess');
        }).catch(() => {
            this.props.showPopup('contactMsgFail');
        })
    }

    toggle(event) {
        this.setState({
            checkboxState: !this.state.checkboxState
        });
    }

    render() {
        return(
            <div className="contact">
                { seo({
                    title: translate('navigation.contact'),
                    description: translate('main.description'),
                    avatar: 'avatar.jpg',
                    tags: ''
                }) }
                <section className="contact-head">
                    <div className="container no-padding-x">
                        <div className="col-md-14 col-lg-6 no-padding-x">
                            <h1 className="heading--bold mb-4">{translate('contact.about')}</h1>
                            <p>{translate('main.contactHeader')}</p>
                        </div>
                    </div>
                </section>
                <section className="contact-info container mt-5 d-md-flex flex-md-row justify-content-around">

                    <form className="contact-info-form d-flex flex-column col-24 col-md-10 col-lg-10" onSubmit={event => this.onFormSubmit(event, this.state)}>
                        <h2 className="heading--bold mb-3">{translate('main.contactMe')}</h2>
                        <p className="mb-3">{translate('main.contactTime')}</p>
                        <div className="form-group">
                            <label className="ml-1" htmlFor="name">{translate('label.name')} </label>
                            <input type="text" id="name" className="form-control" required pattern="^[^<>%$!@#&*()+_0-9]*$" title={translate('message.validation.noSpecialsAndNumbers')}/>
                        </div>
                        <div className="form-group">
                            <label className="ml-1" htmlFor="name">{translate('label.surname')} </label>
                            <input id="surname" type="text" className="form-control"  required pattern="^[^<>%$!@#&*()+_0-9]*$" title={translate('message.validation.noSpecialsAndNumbers')}/>
                        </div>
                        <div className="form-group">
                            <label className="ml-1" htmlFor="email">{translate('label.email')} </label>
                            <input id="email" type="email" className="form-control" required/>
                        </div>
                        <div className="form-group">
                            <label className="ml-1" htmlFor="message">{translate('label.placeholder.message')}  </label>
                            <textarea id="message" className="form-control" rows="7" required></textarea>
                        </div>
                        <div className="form-group">
                            <span><input type="checkbox" onClick={this.toggle.bind(this)} /> {translate('regulationsContent.acceptPersonalData')} </span>
                        </div>
                        <div className="form-group d-flex flex-column align-items-end">
                            <button className="btn btn-color btn-padding" disabled={this.state.checkboxState}>{translate('label.buttons.sendMessage')}</button>
                        </div>
                    </form>
                    <div className="d-flex d-flex-row col-24 col-md-9 col-lg-8">
                        <div className="contact-info-company mt-5 mt-md-0 col-24 col-md-24 col-lg-24">
                            <h2 className="heading--bold mb-3">Logintrans Sp. z.o.o.</h2>
                            <p className="mb-3">{getLang() === 'pl' ? 'ul. Chabrowa 4, 52-200 Wysoka' : '4 Chabrowa Street, 52-200 Wysoka'}</p>
                            <ul>
                                <li>{translate('contact.phone')} 71 734 17 00</li>
                                <li className="mb-3">{translate('contact.fax')} 71 722 01 22</li>
                                <li><a href="mailto:redakcja@trans.info">redakcja@trans.info</a></li>
                            </ul>
                        </div>
                    </div>
                </section>
            </div>
        );
    }
}

function mapStateToProps(store){
    return {
        auth: store.auth,
        profileStore: store.profile,
        notificationStore: store.notification
    }
}


export default  connect(mapStateToProps, { showPopup })(Template);
