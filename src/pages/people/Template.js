import React, { Component } from 'react';
import PostAuthor from '../../components/user/PostAuthor';
import translate from '../../utils/translate';
import RecentPosts from '../../components/RecentPosts';

import axios from 'axios';
import getConfig from '../../utils/getConfig';
import {getLocale} from '../../utils/storeData';
import Loader from '../../components/asyncData/Loader';

import VisibilitySensor from 'react-visibility-sensor';
import {browserHistory} from 'react-router';
import seo from '../../utils/seo';

class People extends Component {

    constructor() {
        super();
        this.state = {
            authors: [],
            fetched: false
        }
        this.limit = 1000;

    }

    componentWillMount() {
        axios.get(getConfig().restApiHostName + '/users', {
            params: {
                limit: this.limit,
                langName: getLocale(),
                hasPosts: true
            }
        }).then(response => {
            this.setState({
                authors: response.data,
                fetched: true
            })
        }).catch(() => {
            browserHistory.push('/500');
        })
    }
    showRecentPosts(author) {
        return (
            <div id={'recentArticle-'+author.id} className="header-author-article">
                <RecentPosts author={author} limit={5} title />
            </div>
            )
    }
    checkVisibility(author) {
        let getVisibleArticle = document.getElementById('recentArticle-' + author.id);
        return getVisibleArticle ? this.showRecentPosts(author) : null ;
    }

    showAuthor(author, key) {
        return (
            <li className="people-item col-24 col-md-12 col-lg-8" key={key} id={'author-' + key}>
                <div className="wrapper">
                    <div className="header-author" id={'author-' + author.id}>
                        <PostAuthor author={author} />
                    </div>
                    <VisibilitySensor scrollCheck={true} offset={{bottom:-200 }} partialVisibility={true} resizeCheck={true}>
                           {
                               ({isVisible}) =>
                                   <span>
                                       {
                                           isVisible ? this.showRecentPosts(author) : this.checkVisibility(author)
                                       }
                                   </span>
                           }
                    </VisibilitySensor>

                </div>
            </li>
        );
    }


    render(){
        return(
            <section className="people container">
                { seo({
                    title: translate('navigation.dropDown.people.title'),
                    description: translate('navigation.dropDown.people.description'),
                    avatar: 'avatar.jpg',
                    tags: ''
                }) }
                <div className="row pt-5 pb-5">
                    <h1 className="heading--semibold col-24">
                        {translate('navigation.people')}
                    </h1>
                </div>
                <ul className="people-items row d-flex align-items-stretch justify-content-center">
                    {
                        this.state.fetched
                            ?
                            this.state.authors.map((item, key) => this.showAuthor(item, key))
                            :
                            <Loader />
                    }
                </ul>
            </section>
        );
    }
}

export default People;
