import React from 'react';
import VideoItem from './VideoItem';
import Slider from 'react-slick';
import config from '../../config';

const VideoList = ({ videos, category = false,  selectVideo, moreVideos, testString, testKey = '' }) => {

  let sliderLazyLoad = {
    afterChange: (currentSlide) =>  { moreVideos(videos, currentSlide, category) }
  }

  const items = videos.map((video, index) => {
    return(
      <div key={index}>
        <VideoItem video={video} index={index} selectVideo={selectVideo} testString={testString} testKey={testKey + '' + index} />
      </div>
    )
  });

  return(
    <Slider {...config.videosSliderConfig} {...sliderLazyLoad}>
      {items}
    </Slider>
  )
};

export default VideoList;
