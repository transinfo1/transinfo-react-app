import React, { Component } from 'react';
import {connect} from 'react-redux';
import moment from 'moment';
import {fetchVideos, fetchCategorisedVideos, saveCategorisedVideos, updateCategorisedVideos} from '../../actions/videos';
import {fetchVideoCategories} from '../../actions/categories';

import VideoPreview from './VideoPreview';
import VideoList from './VideoList';

import seo from '../../utils/seo';
import translate from '../../utils/translate';

class Template extends Component {
  constructor(props){
    super(props);
    this.state = {
      allCurrentPos: 5,
      allPrevSlide: null
    }
  }
  componentWillMount(){

    const dateNow = new Date();
    const unixDate = moment(dateNow).format('x');

    // Load all videos
        // moved to header (loaded on app start)

    // Load categorized videos and save them in one reducer
    this.props.fetchVideoCategories(20).then((response) => {

      const categories = response.value.data;
      const promises = [];
      const catVideos = {};
      let catObj = {};


      categories.forEach((category) => {
          promises.push(this.props.fetchCategorisedVideos(5, unixDate, category.slug))
          // Set initial state for displayed videos by category
          catObj[category.slug + 'CurrentPos'] = 5;
          catObj[category.slug + 'PrevSlide'] = null;
          this.setState(catObj);
      });


      Promise.all(promises).then((response, index) => {

        response.forEach((list, index) => {

          catVideos[categories[index].slug] = [];

          if(list.value.data.length){
              list.value.data.forEach((item) => {
                catVideos[categories[index].slug].push(item);
              })
          }

        });

      }).then(() => {
        this.props.saveCategorisedVideos(catVideos);
      });

    });
  }
  getOneCatVideo(lastVideo, category){
    let nextVid = [];

    this.props.fetchCategorisedVideos(1, lastVideo, category).then((response) => {

      const video = response.value.data[0];

      if(video){
        nextVid.push(video);
      }

    }).then(() => {
      this.props.updateCategorisedVideos(nextVid, category);
    });
  }
  loadCatVideos(category, testString, key){

    const videosInCat = this.props.catVidStore.videos[category.slug];

    if(!videosInCat){
      return false;
    } else {
      if(!videosInCat.length){
        return false;
      }
    }

    return(
       <li key={'category-' + key} className="mb-5">
           <h2>{category.name}</h2>
             <VideoList videos={videosInCat}
                        category={category}
                        moreVideos={this.loadNextVideos.bind(this)}
                        testString={testString + '.categorized'}
                        testKey={key} />
       </li>
     )

  }
  loadNextVideos(videos, currentSlide, category){

    const lastVideo = videos.slice(-1)[0].dateGmt;

    const catPrevSlide = this.state[category.slug + 'PrevSlide'];
    const catCurrentPos = this.state[category.slug + 'CurrentPos'];
    const catRemainNo = videos.length - catCurrentPos;

    const allPrevSlide = this.state.allPrevSlide;
    const allCurrentPos = this.state.allCurrentPos;
    const allRemainNo = videos.length - allCurrentPos;


    if(category.slug){

       let catObj = {};

       if(currentSlide > catPrevSlide){

         catObj[category.slug + 'PrevSlide'] = currentSlide;
         catObj[category.slug + 'CurrentPos'] = catCurrentPos + 1;
         this.setState(catObj);

         if(catRemainNo !== 0){
          return false;
         }

         this.getOneCatVideo(lastVideo, category.slug);

      } else {
        catObj[category.slug + 'PrevSlide'] = currentSlide;
        catObj[category.slug + 'CurrentPos'] = catCurrentPos - 1;
        this.setState(catObj);
      }

    } else {

      if(currentSlide > allPrevSlide){

        this.setState({
          allPrevSlide: currentSlide,
          allCurrentPos: allCurrentPos + 1
        });


        if(allRemainNo !== 0){
         return false;
        }

        this.props.fetchVideos(1, lastVideo);

     } else {
       this.setState({
         allPrevSlide: currentSlide,
         allCurrentPos: allCurrentPos - 1
       });
     }

    }
  }
  render(){

      const testString = "videos";
      const { allVidStore, catStore } = this.props;
      const videos = (allVidStore.fetched) ? allVidStore.data : false;
      const categories = (catStore.fetched) ? catStore.data : false;

      return(
          <section className="videos">
            { seo({
                title: translate('navigation.dropDown.video.title'),
                description: translate('navigation.dropDown.video.description'),
                avatar: 'avatar.jpg',
                tags: '',
            }) }
              {videos ? <VideoPreview video={videos[0]} testString={testString + '.preview'} /> : null }
              <ul className="video-categories container">
                <li className="mb-5">
                  <h2>Wszystkie wideo</h2>
                    {videos ? <VideoList videos={videos} moreVideos={this.loadNextVideos.bind(this)} testString={testString + '.all'} />
                    : <div>No videos avaiable.</div> }
                </li>
                {categories ? categories.map((category, index) => {
                  return this.loadCatVideos(category, testString, index)
                }) : null }
              </ul>
          </section>
      );
  }

}


const mapStateToProps = (store) => {
  return {
    allVidStore: store.videos,
    catStore: store.videoCategories,
    catVidStore: store.saveCatVid
  }
}

export default connect(mapStateToProps, {fetchVideos, fetchVideoCategories, fetchCategorisedVideos, saveCategorisedVideos, updateCategorisedVideos})(Template);
