import React from 'react';

const Player = ({ video, testString }) => {

    function toggleVideo(state) {
        // if state == 'hide', hide. Else: show video
        var div = document.getElementById("popupVid");
        var iframe = div.getElementsByTagName("iframe")[0].contentWindow;
        div.style.display = state === 'hide' ? 'none' : '';
        let func = state === 'hide' ? 'pauseVideo' : 'playVideo';
        iframe.postMessage('{"event":"command","func":"' + func + '","args":""}','*');

        // var player = document.getElementById("player");
        // player.style.display = 'show';
    }

    const featuredImageThumb = video.featuredImage;
    const image = featuredImageThumb ? featuredImageThumb : video.video ? `https://img.youtube.com/vi/${video.video}/maxresdefault.jpg` : '/static/img/default_image.jpg';

  return (
    <div data-test={testString} className="video-player">
        <div className="container">
            <div className="col-24 offset-lg-3 col-lg-18">
                <div id="popupVid" className="embed-responsive embed-responsive-16by9" style={{position:'absolute', display:'none', zIndex:200 }}>
                    <iframe className="embed-responsive-item" src={`https://www.youtube.com/embed/${video.video}?enablejsapi=1`} frameBorder="0" allowFullScreen></iframe>
                </div>
                <div id="thumbnail_container" className="thumbnail_container">
                    <a className="start-video" onClick={()=>toggleVideo()}><img width="64" src="/static/img/icons/play.svg" alt="play" /></a>
                    <img className="img-fluid" src={image} alt={video.title} />
                </div>
            </div>
        </div>
    </div>
  )

}

export default Player;
