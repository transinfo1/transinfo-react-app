import React from 'react';
import SocialBar from '../../../components/social/SocialBar';
import CreatedDate from '../../../components/createdDate';

const Header = ({ video, testString }) => {
  return(
    <header data-test={testString} className="container">
      <div className="d-flex flex-column flex-sm-row align-items-sm-center mb-sm-3">
        <CreatedDate date={video.dateGmt} className="mr-2" testString={testString} />
        <SocialBar colorful data={video} testString={testString} wordShare horizontalLeft />
      </div>
      <h1 data-test={testString + '.title'} className="mb-0 heading--semibold" dangerouslySetInnerHTML={ {__html:video.title} } />
    </header>
  )
};

export default Header;
