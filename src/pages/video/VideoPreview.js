import React from 'react';
import Player from './atoms/Player';
import Header from './atoms/Header';

const VideoPreview = ({video, testString}) => {
  return(
    <div className="video-preview">
      <Player video={video} testString={testString + '.player'} />
      <Header video={video} testString={testString + '.header'} />
    </div>
  );
};

export default VideoPreview;
