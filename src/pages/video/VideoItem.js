import React from 'react';
import Likes from '../../components/social/Likes';
import CreatedDate from '../../components/createdDate';
import Thumbnail from '../../components/thumbnail';

const VideoItem = ({ video, index, testString, testKey }) => {
    let maxLength = 100;
    let title = video.title > maxLength ? video.title.slice(0, maxLength) + '...' : video.title;

    return(
    <div className="video-list-item d-flex flex-column">
        <div data-test={testString + '.image' + testKey}>
          <Thumbnail data={video} />
        </div>
        <div data-test={testString + '.title' + testKey} className="htmlOutput mb-2" dangerouslySetInnerHTML={ {__html: title } }></div>
        <div data-test={testString + '.social' + testKey} className="video-social d-flex align-items-end">
          <div className="d-flex flex-row align-items-center">
            <CreatedDate date={video.dateGmt} className="mr-2" testString={testString} testKey={testKey}/>
            <Likes colorful type="post" data={video.likes} id={video.id} testString={testString} testKey={testKey} />
          </div>
        </div>
    </div>
  )
}

export default VideoItem;
