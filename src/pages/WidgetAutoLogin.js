import React, {Component} from 'react';
import {connect} from 'react-redux';

import { socialLoginFailed, setAuthenticatedUser, setWidgetAutoLogin } from '../actions/auth';
import { isAuthenticated, fetchUserData } from '../utils/authentication';
import axios from 'axios';
import getConfig from '../utils/getConfig';
import Loader from '../components/asyncData/Loader';

class WidgetAutoLogin extends Component {

    constructor() {
        super();
        this.queryParams = {};
        document.location.search.replace('?', '').split('&').forEach(item => {
            let splittedUp = item.split('=');
            this.queryParams[splittedUp[0]] = splittedUp[1];
        });
    }

    redirect() {
        return window.location.replace(decodeURIComponent(this.queryParams.target_uri || '/'));
    }

    isOpenedInHardCommunicator() {
        return navigator.userAgent.match('Trans');
    }

    componentDidMount() {
        if(this.isOpenedInHardCommunicator()) {
            window.close();
            window.open(window.location.href);
        } else {
            if (!isAuthenticated() && this.queryParams.code) {
                this.getTokenByCode(this.queryParams.code, 'onelogin').then(response => {
                    fetchUserData(response.data, 'onelogin').then(() => {
                        this.props.setWidgetAutoLogin();
                        this.redirect();
                        window.dataLayer.push({
                            provider: 'OneLogin',
                            message: 'widget autologin - success',
                            event: 'LogIn',
                        });

                    }).catch(() => {
                        this.redirect();
                        window.dataLayer.push({
                            provider: 'OneLogin',
                            message: 'widget autologin - failure while fetching user data with OneLogin access token',
                            event: 'LogErr',
                        });
                    })
                }).catch(() => {
                    this.redirect();
                    window.dataLayer.push({
                        provider: 'OneLogin',
                        message: 'widget autologin - failure while getting access token by access code',
                        event: 'LogErr',
                    });
                })
            } else {
                this.redirect();
            }
        }
    }

    getTokenByCode(code, provider) {
        let $ = window.jQuery;
        return axios.post(getConfig().restApiHostName + '/token/get?autoLogin=true', $.param({
            code: code,
            provider: provider
        }), {
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        })
    }

    render() {
        return <Loader />
    }
}

const mapStateToProps = (store) => ({
    postsStore: store.posts,
});

export default connect(mapStateToProps, {socialLoginFailed, setAuthenticatedUser, setWidgetAutoLogin})(WidgetAutoLogin);
