import React, { Component } from 'react';
import moment from 'moment';
import translate from '../../utils/translate';
import {getLang} from '../../utils/storeData';
import AuthorThumbnail from '../../components/user/atoms/Thumbnail';
import {authorName} from '../../utils/helpers';
import seo from '../../utils/seo';

class AuthorPosts extends Component {
    // constructor(props){
    //     super(props);
    //     this.state = {
    //         author: ''
    //     }
    // }
    // componentWillMount(){
    //     const {params} = this.props;
    //     this.getAuthor(params.slug);
    // }

    render(){

        const { author } = this.props;

        if(!author){
            return null;
        }


        return(
            <section className="author-posts">
                {seo({
                    title: translate('posts.userPosts') + ' ' + authorName(author),
                    description: translate('main.description'),
                    avatar: 'avatar.jpg',
                    tags: ''
                }) }
                <div className="author-info container d-flex flex-column flex-md-row px-3">
                    <div data-test="authorPosts.author.thumbnail" className="author-avatar p-0">
                        {/* col-24 col-md-3 */}
                        <AuthorThumbnail path={author.avatar} name={authorName(author)} />
                    </div>
                    <div className="author-name px-3">
                        {/* col-24 col-md-21 d-flex flex-column*/}
                        <div className="d-flex align-items-center flex-md-row flex-column mb-2">
                            <h3 data-test="authorPosts.author.name" className="mt-3 mt-md-0">{authorName(author)}</h3>
                            <span data-test="authorPosts.author.createdDate">
                                {translate('posts.memberSince')}:
                                    <time>
                                    {moment(author.createdDate).locale(getLang()).format(' D MMMM YYYY')}
                                    </time>
                            </span>
                        </div>
                        <p data-test="authorPosts.author.description" dangerouslySetInnerHTML={ {__html: author.userCaption} } />
                        <p data-test="authorPosts.author.description" dangerouslySetInnerHTML={ {__html: author.description} } />
                    </div>
                </div>
                <div className="author-posts-head head-dark mb-5">
                    <h2 className="mb-0 container" data-test="authorPosts.author.nameHeading">{translate('posts.userPosts')} {authorName(author)}</h2>
                </div>
            </section>
        )
    }
}

export default AuthorPosts;
