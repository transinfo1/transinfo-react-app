import React, { Component } from 'react';
import translate from '../../utils/translate';
import seo from '../../utils/seo';

class Template extends Component {
  //   constructor(props){
  //       super(props);
  //       this.state = { tagName: '' }
  //
  //   }
  //   componentWillMount(){
  //     const { params } = this.props;
  //     this.getTag(params);
  // }
  // componentWillReceiveProps(newProps){
  //   const currentParams = this.props.params;
  //   const nextParams = newProps.params;
  //
  //   if(currentParams.slug === nextParams.slug){
  //       return false;
  //   }
  //
  //    this.getTag(nextParams)
  // }

  render(){

      const {tagName} = this.props;

      return(
          <section className="tag-posts-head head-dark mb-5">
              {seo({
                  title: translate('posts.taggedPosts') + ': ' + tagName,
                  description: translate('main.description'),
                  avatar: 'avatar.jpg',
                  tags: tagName
              })}
              <div className="container d-flex flex-row alig-items-center">
                  <h2 className="mb-0">
                      {translate('posts.taggedPosts')}:
                  </h2>
                  <span data-test="articles.wall.article.header.tagName" className="btn btn-tag ml-3">
                    #{tagName}
                  </span>
              </div>
          </section>
      )
  }
}

export default Template;
