import React, { Component } from 'react';
import {connect} from 'react-redux';
import Waypoint from 'react-waypoint';
import { filterPostsByTag, filterPostsByAuthor } from '../../utils/filters';
import {fetchTagPosts, fetchNextTagPosts, fetchAuthorPosts, fetchNextAuthorPosts} from '../../actions/posts';
import List from '../../pages/wall/List';
import AuthorPosts from './AuthorPostsTemplate';
import TagPosts from './TagPostsTemplate';
import translate from '../../utils/translate';
import Info from '../../components/messages/Info';
import Loader from '../../components/asyncData/Loader';
import axios from 'axios';
import getConfig from '../../utils/getConfig';
import {browserHistory} from 'react-router';

// import {fetchData, setNestedDataToStore, getLocale} from '../../utils/storeData';

class PostFilter extends Component {
    // constructor(props) {
    //     super(props);
    //     this.state = {
    //         posts: [],
    //         author: {},
    //         tagName: ''
    //     }
    //
    //     const { type, slug } = this.props.params;
    //     this.type = type;
    //     this.slug = slug;
    //
    //     this.initialPostsLimit = 10;
    //     this.lazyLoadingLimit = 10;
    //     this.locale = getLocale();
    //
    //     this.typeParams = {
    //         user: {
    //             fetchPosts: {
    //                 limit: this.initialPostsLimit,
    //                 authorSlug: this.slug,
    //                 langName: this.locale
    //             },
    //             fetchNextPosts: {
    //                 authorSlug: this.slug
    //             }
    //         },
    //         tag: {
    //             fetchPosts: {
    //                 limit: this.initialPostsLimit,
    //                 tagSlug: this.slug,
    //                 langName: this.locale
    //             },
    //             fetchNextPosts: {
    //                 tagSlug: this.slug
    //             }
    //         }
    //     }
    // }
    //
    // componentWillMount() {
    // }
    //
    // fetchPosts() {
    //     fetchData('slugPosts', '/posts', {
    //         params: this.typeParams[this.type].fetchPosts
    //     }).then(response => {
    //         setNestedDataToStore(response.data, 'post')
    //         return response
    //     })
    // }
    //
    // fetchNextPosts(lastDate) {
    //   fetchData('slugPosts', '/posts/limit/' + this.lazyLoadingLimit + '/before/' + lastDate + '/langName/' + this.locale, {
    //         params: this.typeParams[this.type].fetchNextPosts
    //     }).then(response => {
    //         setNestedDataToStore(response.data, 'post')
    //         return response
    //     })
    // }
    //
    // render() {
    //
    //
    //
    //
    //     console.log('PARAMSSS', this.props.params)
    //
    //
    //     return (
    //         <div>
    //             test
    //         </div>
    //     )
    // }



 constructor(props){
     super(props);
     this.state = {
         tagName: '',
         author: '',
         slug: ''
     }
 }
  componentWillMount() {
      const {params} = this.props;
      switch(params.type){
          case 'user':
              this.props.fetchAuthorPosts(10, params.slug);
              this.getAuthor(params.slug);
              break;
          case 'tag':
              this.props.fetchTagPosts(10, params.slug);
              this.getTag(params);
              break;
          default:
              browserHistory.push('/404');
      }
      this.setState({slug: this.props.params.slug});

  }

    getAuthor(user){
        axios.get(getConfig().restApiHostName + '/users/' + user).then((response) => {
            const obj = response.data;
            if(obj){
                this.setState({ author: obj });
            }
        }).catch(() => {
            browserHistory.push('/500');
        })
    }

    getTag(params){
        axios.get(getConfig().restApiHostName + '/tag/' + params.slug).then((response) => {
            this.setState({ tagName: response.data.name });
        }).catch(() => {
            browserHistory.push('/500');
        });
    }

  lazyLoad(store, params) {
      switch(params.type){
        case 'user':
          if (store.fetched && !store.fetching) {
              let lastDate = store.data.slice(-1)[0].dateGmt;
              this.props.fetchNextAuthorPosts(2, lastDate, params.slug);
          }
          break;
        case 'tag':
          if (store.fetched && !store.fetching) {
              let lastDate = store.data.slice(-1)[0].dateGmt;
              this.props.fetchNextTagPosts(2, lastDate, params.slug);
          }
          break;
        default:
          return null
      }
  }
  renderView(params){
    switch(params.type){
      case 'user':
          return <AuthorPosts author={this.state.author}  /> ;
      case 'tag':
          return <TagPosts tagName={this.state.tagName} />;
      default:
        return null
    }
  }
  renderList(posts, params, store){
    switch(params.type){
      case 'user':
        const byAuthor = posts.length ? filterPostsByAuthor(posts, params.slug) : false;
        return byAuthor ? <List store={byAuthor}/> : store.fetched ? <Info message={translate('message.errors.noAuthorPosts')} /> : null;
      case 'tag':
        const byTag = posts.length ? filterPostsByTag(posts, params.slug) : false;
        return byTag ? <List store={byTag} /> : store.fetched ? <Info message={translate('message.errors.noTagPosts')} /> : null;
      default:
        return null
    }
  }
  render() {


    // zaslepka....
    if(this.props.routingStore.locationBeforeTransitions.action === 'PUSH') {
        location.reload();
    }


    const {store, params} = this.props;
    const posts = (store.fetched) ? store.data : false;

    return(
      <div className="filter-posts">
         {this.renderView(params)}
        {this.renderList(posts, params, store)}
        {store.fetching && <Loader />}
        <Waypoint onEnter={() => this.lazyLoad(store, params)}/>
      </div>
    )

  }

};

const mapStateToProps = (store) => ({
    store: store.slugPosts,
    routingStore: store.routing
});

export default connect(mapStateToProps, {fetchTagPosts, fetchNextTagPosts, fetchAuthorPosts, fetchNextAuthorPosts})(PostFilter);
