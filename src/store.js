import promise from 'redux-promise-middleware';
import thunk from 'redux-thunk';
import reduxLogger from 'redux-logger';
import { createStore, applyMiddleware } from 'redux';
import reducers from './reducers';
import getConfig from './utils/getConfig';

const middleware = getConfig().env === 'prod' ? applyMiddleware(promise(), thunk) : applyMiddleware(promise(), thunk, reduxLogger());
const store = createStore(reducers, middleware);

export default store;