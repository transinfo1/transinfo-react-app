import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import { Router , browserHistory } from 'react-router';
import store from './store';
import routes from './routes';
import * as storage from './utils/storage';

import { syncHistoryWithStore } from 'react-router-redux';

import './styles/base.scss'

window.jQuery = require('jquery');
window.Tether = require('tether');
require('bootstrap');


let deviceIdName = 'deviceId';
if(!storage.get(deviceIdName)) {
    let uniqueId = Math.random().toString().slice(2,19) + '-' + new Date().getTime();
    storage.set(deviceIdName, uniqueId)
}

const history = syncHistoryWithStore(browserHistory, store);

render(
    <Provider store={store}>
        <Router history={history} routes={routes} onUpdate={() => window.scrollTo(0, 0)} />
    </Provider>,
    document.getElementById('root')
);
