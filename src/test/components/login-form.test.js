// Testing Enviroment setup

import React from 'react';
import chai, { expect } from 'chai';
import chaiEnzyme from 'chai-enzyme';
import { shallow, mount, render } from 'enzyme';

chai.use(chaiEnzyme());

// Import Components for testing
import {Trans} from '../../components/popups/login/WordPress';

const props = { auth: { response: false, fetching: false, isAuthenticated: false, data: {}, status: null, error: null } };

// Defining the test
describe('TESTING components -> Trans', () => {

  // Testing username //////////////////////////////////////////////////////////////
  it('Username state exist and should be empty', function () {

    const wrapper = shallow(<Trans {...props} />);
    expect(wrapper.state().credentials.username).to.equal('');

  });

  it('Username field should be required', function () {

    const wrapper = shallow(<Trans {...props} />);
    expect(wrapper.find('[data-enzyme="username"]')).to.have.attr('required');

  });

  it('Username should be of type text', function () {

    const wrapper = shallow(<Trans {...props} />);
    expect(wrapper.find('[data-enzyme="username"]')).to.have.attr('type', 'text');

  });

  // Testing password //////////////////////////////////////////////////////////////
  it('Password state exist and should be empty', function () {

    const wrapper = shallow(<Trans {...props} />);
    expect(wrapper.state().credentials.password).to.equal('');

  });

  it('Password field should be required', function () {

    const wrapper = shallow(<Trans {...props} />);
    expect(wrapper.find('[data-enzyme="password"]')).to.have.attr('required');

  });

  it('Password should be of type password', function () {

    const wrapper = shallow(<Trans {...props} />);
    expect(wrapper.find('[data-enzyme="password"]')).to.have.attr('type', 'password');

  });


  // Testing Checkbox Field //////////////////////////////////////////////////////////////
  it('Checkbox state exist and should equal to true', function () {

    const wrapper = shallow(<Trans {...props} />);
    expect(wrapper.state().credentials.remember).to.equal(true);

  });

  it('Checkbox should be of type checkbox', function () {

    const wrapper = shallow(<Trans {...props} />);
    expect(wrapper.find('[data-enzyme="checkbox"]')).to.have.attr('type', 'checkbox');

  });

  it('Checkbox should be checked', function () {

    const wrapper = shallow(<Trans {...props} />);
    expect(wrapper.find('[data-enzyme="checkbox"]')).to.be.checked();

  });

  // Testing login button //////////////////////////////////////////////////////////////
  it('Button should be type of a button', function () {

    const wrapper = shallow(<Trans {...props} />);
    expect(wrapper.find('[data-enzyme="login-btn"]').type()).to.equal('button');

  });

});
