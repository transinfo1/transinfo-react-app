// Testing Enviroment setup
import React from 'react';
import chai, { expect } from 'chai';
import chaiEnzyme from 'chai-enzyme';
import { shallow, mount, render } from 'enzyme';
import axios from 'axios';

chai.use(chaiEnzyme());

// Import Reducer for testing
import auth from '../../reducers/auth';

const initialState = {
  response: false,
  fetching: false,
  isAuthenticated: false,
  data: {},
  status: null,
  error: null
}

// Defining the test
describe('TESTING reducers -> auth', () => {

  // LOGIN_PENDING action type //////////////////////////////////////////////////////////////
  it('LOGIN_PENDING', () => {

    const action = {
      type: 'LOGIN_PENDING',
      payload: {
        ...initialState,
        fetching: true
      }
    }

    const newState = auth(initialState, action);

    expect(newState).to.deep.equal(action.payload);

  });

  // LOGIN_FULFILLED action type //////////////////////////////////////////////////////////////
  it('LOGIN_FULFILLED', () => {

    const action = {
      type: 'LOGIN_FULFILLED',
      payload: {
        ...initialState,
        response: true,
        fetching: false,
        isAuthenticated: true,
        data: {},
        status: 200,
        error: null
      }
    }

    const newState = auth(initialState, action);

    expect(newState).to.deep.equal(action.payload);

  });


})
