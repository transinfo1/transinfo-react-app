// Testing Enviroment setup
import React from 'react';
import chai, { expect } from 'chai';
import chaiEnzyme from 'chai-enzyme';
import { shallow, mount, render } from 'enzyme';
import axios from 'axios';
import getConfig from '../../utils/getConfig';

chai.use(chaiEnzyme());

// Import Action for testing
import * as actions from '../../actions/posts';

const types = {
  posts: 'FETCH_POSTS',
  videos: 'FETCH_VIDEOS'
}

// Defining the test
describe('TESTING actions -> posts', () => {

  // Fetching wall posts test //////////////////////////////////////////////////////////////
  it('Fetching wall posts', () => {

    const request = axios.get(getConfig().restApiHostName + '/posts/wall', {wall: true});

    const expectedAction = {
      type: types.posts,
      payload: request
    }

    expect(actions.fetchWallPosts()).to.deep.equal(expectedAction);

  });

  // Fetching posts test //////////////////////////////////////////////////////////////
  it('Fetching posts', () => {

    const amount = 5;
    const request = axios.get(config.restApiDomain + '/posts/amount/' + amount);

    const expectedAction = {
      type: types.posts,
      payload: request
    }

    expect(actions.fetchWallPosts()).to.deep.equal(expectedAction);

  });

  // Fetching one post test //////////////////////////////////////////////////////////////
  it('Fetching one post', () => {

    const slug = 'uwaga-hiszpanie-wykorzystujac-luke-w-przepisach-i-wlepiaja-mandaty-za-brak-zaswiadczenia-o-dzialalnosci-49299';
    const request = axios.get(config.restApiDomain + '/posts/' + slug);

    const expectedAction = {
      type: types.posts,
      payload: request
    }

    expect(actions.fetchWallPosts()).to.deep.equal(expectedAction);

  });

  // Fetching next post test //////////////////////////////////////////////////////////////
  it('Fetching next post', () => {

    const data = {
      amount: 2,
      lastDate: 1490273891000
    }

    const request = axios.get(config.restApiDomain + '/posts/amount/' + data.amount + '/before/' + data.lastDate);

    const expectedAction = {
      type: types.posts,
      payload: request
    }

    expect(actions.fetchWallPosts()).to.deep.equal(expectedAction);

  });

  // Fetching videos test //////////////////////////////////////////////////////////////
  it('Fetching videos', () => {

    const amount = 5;
    const request = axios.get(config.restApiDomain + '/videos/amount/' + amount);

    const expectedAction = {
      type: types.posts,
      payload: request
    }

    expect(actions.fetchWallPosts()).to.deep.equal(expectedAction);

  });

});
