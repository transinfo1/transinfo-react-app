// Testing Enviroment setup
import React from 'react';
import chai, { expect } from 'chai';
import chaiEnzyme from 'chai-enzyme';
import { shallow, mount, render } from 'enzyme';
import axios from 'axios';
import getConfig from '../../utils/getConfig';

chai.use(chaiEnzyme());

// Import Action for testing
import * as actions from '../../actions/auth';

// Defining the test
describe('TESTING actions -> auth ', () => {

  // Testing login action //////////////////////////////////////////////////////////////
  it('Login the user', () => {

    const data = {
      username: 'user2',
      password: 'user2'
    }

    const request = axios.post(getConfig().restApiHostName + '/user/login', {
        username: data.username,
        password: data.password
    });

    const expectedAction = {
      type: 'LOGIN',
      payload: request
    }

    expect(actions.login(data)).to.deep.equal(expectedAction);

  });

})
