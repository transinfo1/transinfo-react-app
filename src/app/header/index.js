import React, {Component} from 'react';
import translate from '../../utils/translate';
import {connect} from 'react-redux';
import Thumbnail from '../../components/thumbnail';
import {sortByCreatedDateDesc} from '../../utils/sort';
import {Link} from 'react-router';
import {logout} from '../../actions/auth';

import UserMenu from './UserMenu';
import MainMenuItem from './MainMenuItem';
import SocialLinks from '../../components/social/Links';
import Search from './Search';
import LangSelector from './LangSelector';
import PostAuthor from '../../components/user/PostAuthor';
import moment from 'moment';
import {fetchVideos} from '../../actions/videos';
import {fetchTopAuthors} from '../../actions/topAuthors';
import RecentPosts from '../../components/RecentPosts';

class Header extends Component {

    componentDidMount() {
        this.props.fetchVideos(5, moment(new Date()).format('x'));
        this.props.fetchTopAuthors();
    }

    pipeline() {
        return <span className="navbar-text pipeline"/>
    }

    forumItem (transId) {
        if(transId === '0-0' || transId === null) {
            return (
                <div className="col-6 forum-block">
                    <h4><a href="/forum/forum/hyde-park">HydePark</a></h4>
                    <ul>
                        <li><a href="/forum/forum/hyde-park/hyde-park-aa">Hyde Park</a></li>
                        <li><a href="/forum/forum/hyde-park/opinie-i-propozycję-rozwoju-forum">Pomoc, opinie i rozwój forum</a></li>
                    </ul>
                </div>
            )
        } else {
            return (
                <div className="col-6 forum-block">
                    <h4><a href="/forum/">System TRANS.EU</a></h4>
                    <ul>
                        <li><a href="/forum/">Opinie o kontrahentach</a></li>
                        <li><a href="/forum/">Pomoc techniczna</a></li>
                    </ul>
                </div>
            )
        }
    }

    render() {

        let $ = window.jQuery
        $(window).on('scroll', function (event) {
            var scrollValue = $(window).scrollTop();
            if (scrollValue > 70) {
                // $('.logoCropper img').attr('src', '/static/img/logo.jpg');
                $('.navbar .logo').addClass('img-zoom-out');
                $('.navbar').addClass('navbar-zoom-out');
                $('.navbar .thumbnail').addClass('img-zoom-out');
            } else {
                // $('.logoCropper img').attr('src', '/static/img/logo.gif');
                $('.navbar .logo').removeClass('img-zoom-out');
                $('.navbar').removeClass('navbar-zoom-out');
                $('.navbar .thumbnail').removeClass('img-zoom-out');
            }
        });

        $(function () {
            var scrollValue = $(window).scrollTop();
            if(scrollValue > 70) {
                $('.navbar .thumbnail').addClass('img-zoom-out');
            }
            $('[data-toggle="push"]').each(function () {
                var $this = $(this);

                //var data = $this.data();
                var options = {
                    target: '#navbar',
                    direction: 'left',
                    canvas: 'body',
                    logo: '#navbar-brand-search',
                    mobilemenu: '#mobilemenu'
                };

                //$.extend(options, data);

                var $target = $(options.target);
                $target.css('display', 'block');
                $target.addClass('navbar-push navbar-push-' + options.direction);

                var $canvas = $(options.canvas);
                $canvas.addClass('push-canvas');

                var $logo = $(options.logo);
                //$logo.addClass('on');

                $this.unbind().click(function (e) {
                    $this.toggleClass('active');
                    $canvas.toggleClass('pushed-' + options.direction);
                    $target.toggleClass('in');
                    $logo.toggleClass('off');
                });

                var $mobilemenu = $(options.mobilemenu);
                $mobilemenu.unbind().click(function (e) {
                    $this.toggleClass('active');
                    $canvas.toggleClass('pushed-' + options.direction);
                    $target.toggleClass('in');
                    $logo.toggleClass('off');
                });
            });
        });


        const {videosStore, authStore, langStore, topAuthorsStore, userTransId} = this.props;

        return (
            <nav className="navbar fixed-top navbar-toggleable-md navbar-light">
                <button className="navbar-toggler navbar-toggler-right " type="button" data-toggle="push"
                        data-target="#navbar" aria-controls="navbar" aria-expanded="false"
                        aria-label="Toggle navigation">
                    <span className="my-1 mx-2 close">X</span>
                    <span className="navbar-toggler-icon"></span>
                </button>
                <div className="navbar-brand" id="navbar-brand-search">
                    <a href="/" className="banner-wrapper"><img src="/static/img/logo.svg" alt={translate("main.title")} className="banner" /></a>
                    <div className="hidden-lg-up pull-right">
                        {/*<Search />*/}
                        <div className="mobile-search">
                            <a href="/search"><img src="/static/img/icons/search.svg"
                                                   alt={translate("label.placeholder.search")}/></a>
                        </div>
                    </div>
                </div>

                <div className="navbar-collapse" id="navbar">
                    <div className="d-flex flex-column flex-lg-row justify-content-around">
                        <div className="flex-last flex-lg-first my-auto" id="mobilemenu">
                            <ul className="navbar-nav mr-auto ml-5 text-left">

                                <li className="nav-item menu-item login-item hidden-lg-up p-4">
                                    <UserMenu mobile/>
                                </li>

                                {langStore.lang === 'pl' &&

                                <MainMenuItem link="videos">
                                    {videosStore.data.length > 0 ? videosStore.data
                                        .sort(sortByCreatedDateDesc)
                                        .slice(0, 4)
                                        .map((item, key) => (
                                            <div className="col-6" key={key}>
                                                <Thumbnail data={item} className="p-3" linked/>
                                                <a href={"/" + item.slug}
                                                   className=" pb-3 w-100 text-center link-silver">
                                                    <span dangerouslySetInnerHTML={ {__html: item.title} }/>
                                                </a>
                                            </div>

                                        )) : <div
                                        className="col-6 forum-block no-data-msg">{translate('message.noPosts')}</div>}
                                </MainMenuItem>
                                }

                                {langStore.lang === 'pl' &&

                                <MainMenuItem link="forum">
                                    <div className="col-6 forum-block">
                                        <h4><a href="/forum/forum/dział-prawny">Prawo i finanse</a></h4>
                                        <ul>
                                            <li><a href="/forum/forum/branża/przepisy-transportowe">Przepisy transportowe</a></li>
                                            <li><a href="/forum/forum/branża/milog-i-marcon-wszystkie-wątki">Milog i macron</a></li>
                                        </ul>
                                    </div>
                                    <div className="col-6 forum-block">
                                        <h4><a href="/forum/forum/branża">Branża</a></h4>
                                        <ul>
                                            <li><a href="/forum/forum/branża/dyskusje-ogólne">Dyskusje ogólne</a></li>
                                            <li><a href="/forum/forum/busiarze/dla-busów">Busiarze</a></li>
                                        </ul>
                                    </div>
                                    <div className="col-6 forum-block">
                                        <h4><a href="/forum/forum/ogłoszenia">Ogłoszenia</a></h4>
                                        <ul>
                                            <li><a href="/forum/forum/ogłoszenia/dam-pracę-szukam-pracy">Dam pracę</a></li>
                                            <li><a href="/forum/forum/ogłoszenia/kupię-sprzedam-wynajmę">Sprzedam / Kupię / Zamienię</a></li>
                                        </ul>
                                    </div>
                                    { this.forumItem(userTransId) }
                                </MainMenuItem>

                                }

                                {langStore.lang === 'pl' &&

                                <MainMenuItem link="people">
                                    {topAuthorsStore.data.length > 0 ?
                                        topAuthorsStore.data.slice(0, 4).map((item, key) => {
                                            return (
                                                <div key={key} className="col-6 people-block">
                                                    <div className="header-author mb-3 pb-3">
                                                        <PostAuthor author={item}
                                                                    className="d-flex flex-row align-items-center"/>
                                                    </div>
                                                    <div className="header-author-article">
                                                        <RecentPosts author={item}/>
                                                    </div>
                                                </div>
                                            )
                                        })
                                        : null
                                    }
                                </MainMenuItem>

                                }

                                <li className="nav-item menu-item addnews-item hidden-lg-up px-4 py-4">
                                    <a href="#" className="nav-link white-button"
                                       data-popup="addNews"><span>{translate('functionalities.addNews')}</span></a>
                                </li>

                                {authStore.isAuthenticated ? <li className="nav-item menu-item hidden-lg-up px-4">
                                    <Link to="/user/profile" className="nav-link white-button"
                                          data-test="header.link.profileMobile"><span>{translate('navigation.profile')}</span></Link>
                                </li> : ''}

                                {/*{authStore.isAuthenticated ? */}
                                {/*<li className="nav-item menu-item hidden-lg-up px-4">*/}
                                {/*<a data-popup="userProfileQuestions" className="nav-link white-button"*/}
                                {/*data-test="header.link.settingsMobile"><span>{translate('navigation.settings')}</span></a>*/}
                                {/*</li> : ''}*/}

                                <li className="nav-item menu-item hidden-lg-up px-4">
                                    <Link to="/contact" className="nav-link white-button"
                                          data-test="header.link.contactMobile"><span>{translate('navigation.contact')}</span></Link>
                                </li>

                                <li className="nav-item menu-item hidden-lg-up px-4">
                                    <Link to="/policy" className="nav-link white-button"
                                          data-test="header.link.policyMobile"><span>{translate('navigation.policy')}</span></Link>
                                </li>

                                {authStore.isAuthenticated ? <li className="nav-item menu-item hidden-lg-up px-4">
                                    <a href="#" onClick={() => logout()} className="nav-link white-button"
                                       data-test="header.link.logoutMobile"><span>{translate('navigation.logout')}</span></a>
                                </li> : ''}

                            </ul>
                        </div>

                        <div
                            className="flex-first flex-lg-last d-flex flex-md-row flex-sm-row justify-content-between ">
                            <div className="d-flex flex-row ml-4 mr-2">

                                <SocialLinks classes="navbar-text hidden-md-down" color="mono"/>
                                <SocialLinks classes="navbar-text hidden-lg-up" color="white"/>
                                <span className="navbar-text pipeline hidden-md-down"></span>
                                <div className="hidden-md-down d-flex align-items-center ">
                                    <Search />
                                </div>
                            </div>
                            <div className="d-flex flex-row ">
                                {this.pipeline()}

                                <LangSelector />

                                {this.pipeline()}

                                <div className={ authStore.isAuthenticated ? 'hidden-md-down my-auto' : 'hidden-md-down user-menu' } >

                                    <UserMenu/>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </nav>
        )
    }
}

const mapStateToProps = (store) => ({
    langStore: store.language,
    authStore: store.auth,
    videosStore: store.videos,
    topAuthorsStore: store.topAuthors,
    userTransId: store.profile.data.transId,
})

export default connect(mapStateToProps, {fetchVideos, fetchTopAuthors})(Header)
