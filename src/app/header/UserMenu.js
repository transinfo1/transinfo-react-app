import React, {Component} from 'react';
import {connect} from 'react-redux';
import translate from '../../utils/translate';
import { Link } from 'react-router';
import {logout} from '../../actions/auth';
import {showPopup, hidePopup} from '../../actions/popups';
import {updateProfile, setFetchingProfile} from '../../actions/profile';
import {authorName} from '../../utils/helpers';

import Thumbnail from '../../components/user/atoms/Thumbnail';

import Social from '../../components/popups/login/Social';
import { requestAsAuthenticatedUser } from '../../utils/authentication';

class UserMenu extends Component{

    render() {

        // forum links
        setTimeout(() => {
            switch (location.hash) {
                case '#login':
                    if(this.props.popupsStore.name !== 'login') {
                        this.props.showPopup('login');
                    }
                    break;
                case '#remindPassword':
                    if(this.props.popupsStore.name !== 'remindPassword') {
                        this.props.showPopup('remindPassword');
                    }
                    break;
                case '#register':
                    if(this.props.popupsStore.name !== 'register') {
                        this.props.showPopup('register');
                    }
                    break;
                case '#logout':
                    this.props.logout();
                    break;
                default:
                    break;
            }
        }, 500)

        const {authStore, popupsStore, profileStore, mobile} = this.props;

        if(authStore.isAuthenticated) {
            // welcome popup
            setTimeout(() => {
                if (!authStore.data.user.profileFilled) {
                    if(popupsStore.name !== 'userProfileQuestions')
                    this.props.showPopup('userProfileQuestions')
                } else {
                    if (popupsStore.visibility && (popupsStore.name === "login" || popupsStore.name === "register")) {
                        this.props.hidePopup();
                    }
                }
            }, 1);

            // fetching profile when not fetched
            if(!profileStore.fetched && !profileStore.fetching) {
                this.props.setFetchingProfile();
                requestAsAuthenticatedUser('/users/profile/me', {}, 'get', response => {
                    this.props.updateProfile(response.data)
                })
            }
        }

        const {data} = authStore;
        let defaultAvatar = <img src="/static/img/icons/person.svg" className="face-thumbnail" alt="Me"/>

        if(!authStore.isAuthenticated) {
            return (
                <div data-popup="login" data-test={'header.loginButton' + (mobile ? 'Mobile' : '')} className="my-auto login-button text-nowrap">
                    {defaultAvatar}
                    {' ' + translate('navigation.login')}
                </div>
            )
        }



        return (
            <div className="dropdown my-auto hover-btn pt-0">
                <button data-test={'header.user' + (mobile ? 'Mobile' : '')} className="nav-link dropdown-toggle user" data-toggle="dropdown"
                        aria-haspopup="true" aria-expanded="false">
                    <Thumbnail path={data.user.avatar} name={authorName(data.user)} />
                    <div className="p-3 text-nowrap">{authorName(data.user, false, true)}
                        <i className="fa fa-angle-down hidden-md-down ml-1" aria-hidden="true" />
                    </div>
                </button>
                <div className="dropdown-menu hidden-md-down dropdown-profile" aria-labelledby="test_Hmd_video">
                    <Link to="/user/profile" className="dropdown-item" data-test={mobile ? '' : 'header.link.profile'}>
                        {translate('navigation.profile')}
                    </Link>
                    {/*<a className="dropdown-item" data-test={mobile ? '' : 'header.link.settings'}>*/}
                        {/*{translate('navigation.settings')}*/}
                    {/*</a>*/}
                    <Link to="/contact" className="dropdown-item" data-test={mobile ? '' : 'header.link.contact'}>
                        {translate('navigation.contact')}
                    </Link>
                    <Link to="/policy" className="dropdown-item" data-test={mobile ? '' : 'header.link.policy'}>
                        {translate('navigation.policy')}
                    </Link>
                    <div className="dropdown-divider"></div>
                    <a className="dropdown-item" onClick={this.props.logout} data-test={mobile ? '' : 'header.link.logout'}>
                        {translate('navigation.logout')}
                    </a>
                </div>

                <div className="hidden-social-buttons">
                    <Social />
                </div>

            </div>
        )
    }
}

const mapStateToProps = (store) => ({
    authStore: store.auth,
    profileStore: store.profile,
    popupsStore: store.popups
})

export default connect(mapStateToProps, {logout, updateProfile, setFetchingProfile, showPopup, hidePopup})(UserMenu);
