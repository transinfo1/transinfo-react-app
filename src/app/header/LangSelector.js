import getConfig from '../../utils/getConfig';
import React, {Component} from 'react';
import {connect} from 'react-redux';
import {setLang} from '../../actions/language';

class LangSelector extends Component {

    getOtherLangs() {
        let langs = getConfig().languages
        return langs.filter((lang) => (lang !== this.props.store.lang))
    }

    render() {

        let testString = 'header.langSelector';

        return (
            <div className="dropdown pt-0 d-flex align-items-center ">
                <div className="nav-link dropdown-toggle smaller text-nowrap" data-test={testString} data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    {this.props.store.lang.toUpperCase()}
                    <i className="fa fa-angle-down ml-1" aria-hidden="true"></i>
                </div>
                <div className="dropdown-menu lang-switch" aria-labelledby="test_Hmd_people">
                    {this.getOtherLangs().map((lang, key) => (
                        <a className="dropdown-item" href={'/' + lang} key={key} onClick={() => {
                            //this.props.setLang(lang);
                            //location.reload();
                          }} data-test={testString + '.' + lang}>
                            {lang.toUpperCase()}
                        </a>
                    ))}
                </div>
            </div>
        )
    }
}

const mapStateToProps = (store) => ({
    store: store.language,
})

export default connect(mapStateToProps, {setLang})(LangSelector)
