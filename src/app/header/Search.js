import React, {Component} from 'react';
import translate from '../../utils/translate'
import {browserHistory} from 'react-router';
import {fetchSearchResult, setKeywords, fetchSuggestions} from '../../actions/search';
import {connect} from 'react-redux'

class Search extends Component {
    constructor(props) {
        super(props)
        this.state = {
            placeholderText : translate("label.placeholder.search"),
        }
    }

    redirectToSearchPage(e, bySuggestion = false) {
        e.preventDefault();
        let searchText;
        if(bySuggestion){
            searchText = e.target.outerHTML.replace(/<\/?[^>]+(>|$)/g, "");
        } else {
            searchText = this.refs.searchText.value
        }
        this.props.setKeywords(searchText, true);
        this.props.fetchSearchResult({keywords: searchText, saveKeywords: true});
        browserHistory.push('/search?q=' + searchText);
        this.refs.searchText.value = "";

    }

    onFormChange(e){
        this.props.fetchSuggestions(e.target.value);
    }

    handleFocus(text) {
        this.setState({
            placeholderText: text,
        })
    }
    handleBlur(text =  translate("label.placeholder.search")) {
        this.setState({
            placeholderText: text,
        })
    }
    render() {
        let {store} = this.props;
        return (
            <form onSubmit={(e) => {
                this.redirectToSearchPage(e);
            }}  onChange={this.onFormChange.bind(this)} className="form-inline my-2 d-flex flex-nowrap main-search-form" id="searchForm" data-test="header.search.form">
                <input className="form-control mr-sm-2" ref="searchText" type="text" maxLength="35" placeholder={this.state.placeholderText} onFocus={() => this.handleFocus('')} onBlur={() => this.handleBlur()}/>
                <button type="submit"  aria-hidden="true">
                  <img src="/static/img/icons/search.svg" alt={translate("label.placeholder.search")} />
                </button>
                <ul className="suggestions">
                    {store.suggestions ?
                        store.suggestions.map( (word, key) => {
                            return (key < 5 ?  <li key={key} onClick={(e) => {
                                this.redirectToSearchPage(e, true)
                            }} dangerouslySetInnerHTML={{__html: word}}></li> : null )
                        })
                    : null
                    }
                </ul>


            </form>
        )
    }
}

const mapStateToProps = (store) => {
    return {
        store: store.search
    };
};

export default connect(mapStateToProps, {fetchSearchResult, setKeywords, fetchSuggestions})(Search);
