import React, {Component} from 'react';
import translate from '../../utils/translate';
import { Link } from 'react-router';

class MainMenuItem extends Component {

    render() {
        const {link} = this.props;
        return (
            <li className="nav-item menu-item px-4 px-lg-0">
                {link === 'forum' ?
                    <a href={'/' + link} className="nav-link white-button py-lg-3" data-test={'header.mainMenu.link.' + link}>
                        <span>{translate('navigation.' + link)}</span>
                        <i className="fa fa-angle-down" aria-hidden="true"></i>
                    </a>
                    :
                    <Link to={'/' + link} className="nav-link white-button py-lg-3" data-test={'header.mainMenu.link.' + link}>
                        <span>{translate('navigation.' + link)}</span>
                        <i className="fa fa-angle-down" aria-hidden="true"></i>
                    </Link>
                }
                <div className="hover-menu hidden-md-down">
                    <div className="box">
                        <div className="container">
                            <div className="row">
                                <div className="col-24">
                                    <div className="row">
                                        {this.props.children}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-24 see-all">
                                {link === 'forum' ?
                                    <a href={'/' + link}>
                                        {translate("main.seeAll")}
                                    </a>
                                    :
                                    <Link to={'/' + link}>
                                        {translate("main.seeAll")}
                                    </Link>
                                }
                            </div>
                        </div>
                    </div>
                </div>
            </li>
        )
    }
}

export default MainMenuItem
