import React, {Component} from 'react';
import { connect } from 'react-redux';
import Header from './header';
import Footer from './Footer';
import Popup from '../components/popups/index';
import Loader from '../components/asyncData/Loader'
import { setAuthenticatedUser, logout } from '../actions/auth';
import { setLang } from '../actions/language';
import { showPopup, hidePopup } from '../actions/popups';
import * as storage from '../utils/storage';
import {replace} from 'react-router-redux';


class App extends Component {
    componentWillMount() {
        this.props.setLang();

        // register confirm
        let credentials = this.props.params;
        if (credentials.key && credentials.username) {
            this.props.showPopup('changePassword', credentials)
        }

        // auto login
        let localStorageAuthData = storage.get('auth');

        if(localStorageAuthData) {
            if (localStorageAuthData.expires && localStorageAuthData.expires < (new Date().getTime())) {
                // auto logout if 'remember me' is unchecked
                this.props.logout();
            } else {
                this.props.setAuthenticatedUser(localStorageAuthData, !localStorageAuthData.expires);
            }
        }

        // auth2 redirect with 'code'
        let queryStr = location.search;
        if(queryStr.match(/state=/)) {
            let pieces = queryStr.replace('?code=', '').split('&state=');
            this.props.showPopup('login', {
                code: pieces[0],
                state: pieces[1]
            });
            window.history.pushState(null, null, window.location.pathname); // removing GET params from URL
        }
    }
    setProfileStorage (set = false) {
        const profileInfo = {
            "transId" : this.props.profileStore.data.transId || '0-0',
            "wcw1" : this.props.profileStore.data.wcw1,
            "wcw2" : this.props.profileStore.data.wcw2,
            "wcw3" : this.props.profileStore.data.wcw3,
            "wcw4" : this.props.profileStore.data.wcw4,
            "wcw5" : this.props.profileStore.data.wcw5,
            "wcw6" : this.props.profileStore.data.wcw6,
            "wpw1" : this.props.profileStore.data.wpw1,
            "wpw2" : this.props.profileStore.data.wpw2,
            "wpw3" : this.props.profileStore.data.wpw3,
            "wpw4" : this.props.profileStore.data.wpw4,
            "wpw5" : this.props.profileStore.data.wpw5,
            "wpw6" : this.props.profileStore.data.wpw6,
        };
        if(set) {
            storage.set('gtm', profileInfo)

        }else {
            storage.clear('gtm');
        }
    }

    render() {

        this.setProfileStorage(this.props.profileStore.fetched);

        return (
            <div>
                <div className="global-loader">
                    <Loader />
                </div>

                <Popup />
                <Header />
                    <section className="main-content">
                        {this.props.children}
                    </section>
                <Footer />
            </div>
        );
    }
}

const mapStateToProps = (store) => {
    return {
        langStore: store.language,
        notificationStore: store.notification,
        authStore: store.auth,
        profileStore: store.profile
    };
};

export default connect(mapStateToProps, {setLang, showPopup, hidePopup, logout, setAuthenticatedUser, replace})(App);
