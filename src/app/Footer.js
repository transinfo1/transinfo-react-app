import React from 'react';
import { Link } from 'react-router';
import SocialLinks from '../components/social/Links';
import translate from '../utils/translate';

const Footer = () => (
    // className="mt-5"
    <footer>
      <div className="container d-flex flex-column align-items-center flex-md-row justify-content-md-between">
        <p className="copyright mb-md-0 mt-2 mt-md-0">&copy; 2010-2017 {translate('main.rightsReserved')}</p>
        <nav className="secondary-nav p-0">
          <ul className="d-flex my-auto">
              <li><Link to="/contact">{translate('navigation.contact')}</Link></li>
              <li><Link to="/policy">{translate('navigation.policy')}</Link></li>
              <li><Link to="/regulations">{translate('navigation.rules')}</Link></li>
          </ul>
        </nav>
        <SocialLinks color="white" />
      </div>
    </footer>
);

export default Footer;
