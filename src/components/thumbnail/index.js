import React from 'react';
import { Link } from 'react-router';

const Thumbnail = ({ data, linked = false, wall = false, thumb = false, linkClass = '', className = ''}) => {
    // console.warn("THUMBNAIL: ", data.featuredImage)
    // if(data.featuredImage) {
    //     console.warn("THUMBNAIL IF ")
    //     return null
    // }
    // console.warn("THUMBNAIL else ")
    const str = String(data.featuredImage);
    const image_path = str.substring(0, str.lastIndexOf("/") + 1);
    const image_full_size = str.substring(str.lastIndexOf("/") + 1, str.length);
    const image_url = thumb === 'small' ? image_path + image_full_size.split('.')[0] + '-85x64.' + image_full_size.split('.')[1] : image_path + image_full_size.split('.')[0] + '-400x300.' + image_full_size.split('.')[1];

    const featuredImageThumb = data.featuredImage ? image_url : null;

  const image = featuredImageThumb ? featuredImageThumb : data.video ? `https://img.youtube.com/vi/${data.video}/hqdefault.jpg` : '/static/img/default_image.jpg';
  const bg = { "backgroundImage": "url('" + image + "')" };

    let isVideo = data.type === 'videos';
    let videoPrefix  = isVideo ? 'video-' : '';

  if(wall) {
      return (
          <div className={videoPrefix + "thumbnail-wrapper " + className}>
              {linked ?
                  <Link to={'/' + data.slug} className={videoPrefix + "thumbnail " + linkClass} style={bg}>
                      {isVideo && <div className="play" />}
                  </Link>
                  :
                  <a href={'/' + data.slug}><div className={videoPrefix + "thumbnail"} style={bg}/></a>
              }

          </div>
      )
  }

  return (
          <div className={videoPrefix + "thumbnail " + className}>
              <a href={'/' + data.slug}>
                 <img src={image} alt={data.title} className="img-fluid" />
                  {isVideo && <div className="play" />}
              </a>
          </div>
         )

};

export default Thumbnail;
