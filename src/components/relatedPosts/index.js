import React, {Component} from 'react';
import translate from '../../utils/translate';
import getConfig from '../../utils/getConfig';
import {getLocale} from '../../utils/storeData';
import Loader from '../asyncData/Loader';
import axios from 'axios';
import Thumbnail from '../thumbnail';
import Slider from 'react-slick';
import config from '../../config';

class RelatedPosts extends Component {

    constructor(tags) {
        super(tags);
        this.tagsSlugsList = this.tagsSlugsList();
        this.keywords = this.props.post.title; // post title as keywords - search analyzator take this
        this.defaultLimit = 2;
        this.limit = this.props.limit || this.defaultLimit;
        this.state = {
            byTagData: [],
            byTagFetched: false,
            bySearchData: [],
            bySearchFetched: false
        };
        this.relatedPosts = this.props.post.relatedPosts;
    }

    request(url, params) {
        return axios.get(getConfig().restApiHostName + url, params)
    }


    componentWillMount() {
        if (this.emptyRelatedPost() && this.defaultLimit === this.limit) {
            this.setState({
                    bySearchData: this.relatedPosts,
                    bySearchFetched: true
                }
            );
        } else {
            if (this.keywords || this.tagsSlugsList) {
                this.request('/search/' + getLocale(), {
                    params: {
                        type: this.props.type,
                        keywords: (this.tagsSlugsList || '') + ' ' + (this.keywords || ''),
                        limit: this.limit + 1,
                        randomRescore: 'true',
                        saveKeywords: 'false'
                    }
                }).then(response => {
                    this.setState({
                        bySearchData: response.data.searchResultsMap.post.searchResults,
                        bySearchFetched: true
                    });
                }).catch(() => {
                    this.setState({
                        bySearchFetched: true
                    });
                });
            }
        }
    }

    emptyRelatedPost() {
        if (!this.relatedPosts.length) {
            return false;
        };
        return true
    }

    getRelatedPosts() {
        return this.resultFilter(this.state.byTagData.concat(this.state.bySearchData))
    }

    resultFilter(result) {
        return result.filter(item => item.id !== this.props.post.id).slice(0, this.limit);
    }

    tagsSlugsList() {
        const {tags} = this.props.post;
        if (!tags || !tags.length) {
            return null
        }
        let list = [];

        tags.forEach(item => {
            list.push(item.slug);
        });

        return list.join(' '); // changed to 'space'
    }

    render() {
        let {className, type, slider} = this.props;
        let isFetched = this.state.bySearchFetched; // changed

        // no related posts
        if (isFetched && this.getRelatedPosts().length === 0) {
            return null;
        }
        className = this.emptyRelatedPost() ? className : className + ' no-display ';
        // render list
        let list = this.getRelatedPosts().map((item, key) => {
            let commonItem = {
                    ...item,
                    type: item.type || item.postType,
                    slug: item.slug || item.postSlug,
                    title: item.title || item.postTitle,
                    featuredImage: item.featuredImage || item.postFeaturedImage,
                },
                title = commonItem.title.length > 80 ? commonItem.title.slice(0, 80) + '...' : commonItem.title;

            return (
                <div key={ key } className="item">
                    <Thumbnail data={commonItem} wall={this.props.slider} thumb={this.props.thumb}/>
                    <a href={'/' + commonItem.slug} className="link-silver" dangerouslySetInnerHTML={{__html: title}}/>
                </div>
            )
        });

        return (
                <div className={'related-posts ' + className}>
                <h2 className="border-bottom pb-3 mb-3">
                    {type === 'videos' ? translate('posts.similarVideos') : translate('posts.similarArticles')}
                </h2>

                {isFetched ?
                    <div className="list">
                        {slider ?
                            <Slider {...config.relatedPostsSliderConfig}>
                                {list}
                            </Slider>
                            : this.emptyRelatedPost() ?
                                <div>
                                    {list}
                                </div> : translate('message.emptyContentToView')
                        }
                    </div> : <Loader />
                }
            </div>
        )
    }
}

export default RelatedPosts;