import React from 'react';
// import {Link} from 'react-router';

export default ({data, horizontal = false, className = '', testString, testKey = '', cropped = false}) => {

    const tags = (data) => (horizontal ? data : data.slice(0, 3));
    //let classes = horizontal ? 'flex-row tags ' : 'd-flex flex-row flex-lg-column flex-wrap flex-sm-nowrap tags mb-0 justify-content-end ';
    let classes = horizontal ? 'tags-semibold flex-row ' : 'tags mb-0 ';
    let itemClasses = horizontal ? 'm-1' : '';

    return (
        <div className={classes + className}>
          {tags(data).map((tag, key) =>
            <a key={key} href={`/tag/${tag.slug}`} className={"btn btn-tag " + itemClasses} data-test={testString + '.tag' + testKey + key}>
              <span className="trim m-0" dangerouslySetInnerHTML={ {__html: '#' + tag.name} } />
            </a>
          )}
        </div>
    )
}
