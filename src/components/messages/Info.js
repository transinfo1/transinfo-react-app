import React from 'react';

const Info = ({ message }) => {
  return(
    <div className="container">
      <div data-test="message.global.info" className="alert alert-info" role="alert">
        <strong>Info!</strong> { message }
      </div>
    </div>
  );
}

export default Info;
