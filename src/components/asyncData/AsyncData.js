import React, {Component} from 'react';
import {browserHistory} from 'react-router';
import Loader from './Loader';

class AsyncData extends Component {

    render() {
        let {store, component, showLoader = true} = this.props;
        let {response, fetching, fetched, error} = store;

        if (fetching && showLoader) {
            return <Loader />
        }

        if (response) {
            if (error) {
                browserHistory.push('500');
            }

            if (fetched) {
                return component
            }
        }

        return null;
    }

}

export default AsyncData;
