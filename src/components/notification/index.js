import React from 'react';

const Notification = ({ data, className = '', id = null}) => {

    if(id !== data.id) {
        return null;
    }
  return(
    <div className={"notification " + className}>
        <div data-test="global.notification.message" className={`alert ${data.type === 'success' ? 'alert-success alert-active'
                                : data.type === 'error' ? 'alert-danger alert-active'
                                : 'alert-disabled' }`} role="alert">
          {data.alert}
        </div>
    </div>
  );
}

export default Notification;
