import React from 'react';
import moment from 'moment';
import {getLang} from '../../utils/storeData';

const CreatedDate = ({date = new Date().getTime(), testString = '', testKey = '', className = ''}) => {
    let nowTimestamp = moment().unix() * 1000;
        if(date > nowTimestamp) {
            date -= date - nowTimestamp;
        }

    let weekAgo = new Date();
    weekAgo.setDate(weekAgo.getDate() - 7)
    let thisWeek = date > weekAgo.getTime()
    let format = thisWeek ? moment(date).locale(getLang() === 'cz' ? 'cs' : getLang()).fromNow() : moment(date).format('DD.MM.YYYY')  //  HH:mm
    return (
        <time data-test={testString + '.createdDate' + testKey} className={'created-date ' + className}>
            <img src="/static/img/icons/clock.svg" className="mr-1" alt="clock" />
            <span>{format}</span>
        </time>
    )
}

export default CreatedDate;
