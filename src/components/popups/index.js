import React, {Component} from 'react';
import {connect} from 'react-redux';
import {showPopup, hidePopup} from '../../actions/popups';
import {browserHistory} from 'react-router';

import Login from './login/Template';
import Register from './register/Template';
import RegisterMessageSuccess from './register/MessageSuccess';
import NewsMessageSuccess from './news/MessageSuccess';
import RemindPassword from './password/RemindPassword';
import ChangePassword from './password/ChangePassword';
import PasswordChanged from './password/PasswordChanged';
import UserProfileQuestions from './profile/index';
import AddNews from './news/Template';
import MsgContactSuccess from './contact/MsgContactSuccess';
import MsgContactFail from './contact/MsgContactFail';
import {isAuthenticated} from '../../utils/authentication';

const myPopups = {
    login: Login,
    register: Register,
    registerSuccess: RegisterMessageSuccess,
    newsSuccess: NewsMessageSuccess,
    remindPassword: RemindPassword,
    changePassword: ChangePassword,
    passwordChanged: PasswordChanged,
    userProfileQuestions: UserProfileQuestions,
    addNews: AddNews,
    contactMsgSuccess: MsgContactSuccess,
    contactMsgFail: MsgContactFail,
};

class Popup extends Component {
  componentDidMount(){
      let root = document.querySelector("#root");
      root.addEventListener("click", this.toggle.bind(this) , false);
      window.addEventListener("keydown", this.escape.bind(this) , false);
  }

  componentDidUpdate(){
    const {visibility, name} = this.props.store;
    return (name === 'login' && isAuthenticated() && visibility) ? this.props.hidePopup() : null;
  }

  checkPending() {
      let $ = window.jQuery;
      return !$('.global-loader').hasClass('global-loader-show');
  }

  render(){
    return(
        <div>
            {this.showPopup()}
        </div>
    )
  }

  escape(e) {
    if(this.props.store.name !== 'userProfileQuestions' && this.checkPending()) {
      return (e.key === "Escape" || e.code === "Escape" || e.keyCode === 27) ? this.props.hidePopup() : null;
    }
    return;
  }
  toggle(e){
    const {showPopup, hidePopup, store} = this.props;

    let typeClicked = e.target.dataset.popup;

    if(!typeClicked) {
        typeClicked = e.target.parentElement.dataset.popup
    }

    let currentType = store.name;
    let popupActive = store.visibility;
    let popupAction = e.target.dataset.action;
    let popupClass = e.target.className;

    if(typeClicked){
      (currentType === typeClicked && popupActive) ? hidePopup() : showPopup(typeClicked);
    } else {
       if ((popupClass === 'pp' || popupAction === 'close') && store.name !== 'userProfileQuestions') {
           hidePopup();
           if(['#login', '#register', '#remindPassword'].indexOf(location.hash) >= 0) {
               browserHistory.push('/');
           }
       }
    }

  }
  showPopup(){
    const {visibility, name} = this.props.store;
    let Component = myPopups[name];

    if(!visibility || (name === 'login' && isAuthenticated()) || ((name === 'register' && isAuthenticated()))  ) {
        return null;
    } else {
        if(!visibility){
            return null;
        }
        return <Component />
    }

  }

}

const mapStateToProps = (state) => ({
  store: state.popups,
});

export default connect(mapStateToProps, {showPopup, hidePopup})(Popup);
