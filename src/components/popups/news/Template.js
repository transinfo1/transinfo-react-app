import React, {Component} from 'react';
import axios from 'axios';
import {connect} from 'react-redux';
import DropzoneComponent from 'react-dropzone-component';
import ReactDOMServer from 'react-dom/server';

import { showPopup } from '../../../actions/popups';
import getConfig from '../../../utils/getConfig';
import translate from '../../../utils/translate';
import { onInputChange } from '../../../utils/onInputChange';
import InputText from '../../form/InputText';
import InputEmail from '../../form/InputEmail';
import TextArea from '../../form/TextArea';
// import Checkbox from '../../form/Checkbox';
import Button from '../../form/Button';
import { sendNotification } from '../../../utils/notification';
import Notification from '../../../components/notification';
import {whilePending} from '../../../utils/helpers';
import {getLang} from '../../../utils/storeData';
import {authorName} from '../../../utils/helpers';
import config from '../../../config';
import moment from 'moment';

class Template extends Component {
    constructor(props){
        super(props);

        this.state = {
            name:  "",
            email: "",
            body: "",
            title: "",
            files: [],
            agree: true,
            djsConfig: {
                autoProcessQueue: false,
                //maxFiles: 6, // limit ilosci plikow zostawic na przyszlosc
                acceptedFiles: "image/*",
                maxFilesize: 1, // MB
                previewTemplate: ReactDOMServer.renderToStaticMarkup(
                    <div className="file-uploader-preview mt-4 col-8">
                        <img data-dz-thumbnail="true" alt="File preview" />
                        <div className="mt-2 file-name" data-dz-name="true"></div>
                    </div>
                )
            },
            config: {
                postUrl: 'no-url'
            },
            eventHandlers: {
                init: dz => this.dropzone = dz,
                addedfile: file => {
                    const fileSize = file.size < 1048576; // 1MB
                    const fileType = file.type.match('image.*');

                    if(fileType && fileSize){
                        this.setState({ files: this.state.files.concat([file]) })
                    } else if(!fileSize){
                        sendNotification(translate('message.errors.fileTooLarge') + '1MB', 'error', 3000)
                    } else if(!fileType){
                        sendNotification(translate('message.errors.wrongFileType') + 'JPG, PNG, GIF, BMP', 'error', 3000)
                    }
                },
                maxfilesexceeded: () => {
                   // this.setState({ files: this.state.files.slice(0,6) }); limit plikow obsluga bledu itp zostawic na przyszlosc
                   // sendNotification(translate('message.error') + ' ' + translate('popups.news.maxFiles'), 'error', 3000)
                },
                error: file => this.dropzone.removeFile(file)
            }
        };

        // Bindings
        this.onChange = this.onChange.bind(this);
        this.onBlur = this.onBlur.bind(this);

    }
    onChange(event){
        const info = onInputChange(event, this.state);
        this.setState(info);
    }

    onBlur() {
        this.setState({
            ...this.state,
            name: this.state.name.trim(),
            title: this.state.title.trim()
        })
    }

    onFormSubmit(event, state){
        event.preventDefault();

        let {auth, profileStore} = this.props;

        let obj = {
            timestamp: moment().format('YYYY-MM-DDTHH:mm:ss'),
            name: state.name.trim() || authorName(auth.data.user),
            email: state.email.trim() || profileStore.data.email,
            body: state.body.trim(),
            title: state.title.trim(),
            lang: getLang()
        };

        let formatedData = JSON.stringify(obj);

        let data = new FormData();

        let blob = new Blob([formatedData], { type: "application/json" });
        data.append('alert', blob);

        state.files.forEach((file) => {
            data.append('files', file);
        });

        whilePending(true, '.btn-padding');
        axios.post(getConfig().restApiHostName + '/alerts', data).then(() => {
            this.props.showPopup('newsSuccess');
            whilePending(false, '.btn-padding');
        }).catch((error) => {
            sendNotification(translate('message.errors.error'), 'error', 3000);
            whilePending(false, '.btn-padding');
        })
    }
    render(){

        const testString = 'popup.newsForm';
        return(
            <div className="pp">
                <div className="wrapper pp-article-send">
                    <div className="head-dark">
                        <h1 className="mb-3">{translate('popups.news.newsTitle')}</h1>
                        <p className="mb-0">
                            {translate('popups.news.newsDescription')}
                        </p>
                    </div>
                    <Notification data={this.props.notificationStore} />
                    <form data-test="popup.newsForm" className="row mt-4" onSubmit={event => this.onFormSubmit(event, this.state)}>
                        <div className="form-group col-24 col-md-16 mb-0 pr-md-5">
                            {(!this.props.auth.isAuthenticated && !this.props.auth.data.user) ?
                            <div className="d-flex justify-content-between">
                                <InputText groupClasses="w-100 mr-2"
                                           htmlFor="name"
                                           labelTxt={translate('label.name')}
                                           testString={testString}
                                           onInputChange={this.onChange}
                                           onBlur={this.onBlur}
                                           value={this.state.name}
                                           pattern={config.regex.noSpecialsAndNumbers}
                                           validationMessage={translate('message.validation.noSpecialsAndNumbers')}
                                           required />
                                <InputEmail groupClasses="w-100"
                                            htmlFor="email"
                                            labelTxt={translate('label.email')}
                                            testString={testString}
                                            onInputChange={this.onChange}
                                            value={this.state.email}
                                            required />
                            </div>
                            : null}
                            <InputText groupClasses="w-100 title-wrapper mt-3"
                                       inputClasses="mb-4 title p-0"
                                       htmlFor="title"
                                       removeLabel
                                       placeholder={translate('label.placeholder.articleTitle')}
                                       testString={testString}
                                       onInputChange={this.onChange}
                                       onBlur={this.onBlur}
                                       value={this.state.title}
                                       required />
                            <TextArea groupClasses="mb-0"
                                      htmlFor="body"
                                      onInputChange={this.onChange}
                                      placeholder={translate('label.placeholder.articleContent')}
                                      value={this.state.body}
                                      rows="12"
                                      required/>
                        </div>
                        <div className="form-group col-24 col-md-8 d-flex flex-column mb-0 mt-4 mt-md-0">
                            <DropzoneComponent config={this.state.config}
                                               eventHandlers={this.state.eventHandlers}
                                               djsConfig={this.state.djsConfig}
                                               className="file-uploader mb-4"
                                               data-test="popup.newsForm.file.upload" />
                            {/*<Checkbox labelClasses='mt-4 mb-4'*/}
                                      {/*inputClasses=''*/}
                                      {/*checkboxTxt={translate('conditions.addNews')}*/}
                                      {/*htmlFor='agree'*/}
                                      {/*onInputChange={this.onChange}*/}
                                      {/*testString={testString}*/}
                                      {/*value={this.state.agree}*/}
                                      {/*required />*/}
                            <Button className="btn-padding btn-color" testString={testString} buttonTxt={translate('label.buttons.sendMessage')} />
                        </div>
                    </form>
                    <span className="close"><i className="fa fa-times" aria-hidden="true" data-action="close" /></span>
                </div>
            </div>
        );
    }
}

function mapStateToProps(store){
    return {
        auth: store.auth,
        profileStore: store.profile,
        notificationStore: store.notification
    }
}

export default  connect(mapStateToProps, { showPopup })(Template);
