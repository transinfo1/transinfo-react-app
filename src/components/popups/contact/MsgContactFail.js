import React, { Component } from 'react';

import translate from '../../../utils/translate';

class MsgContactFail extends Component {
    render(){
        return(
            <div className="pp">
                <div data-test="popup.contactMsgFail" className="wrapper pp-message-fail">
                    <div className="row">
                        <div className="col-24">
                            <i className="fa fa-exclamation pp-icon mb-4" aria-hidden="true"></i>
                        </div>
                        <div className="col-24">
                            <h1 className="mb-4 mt-3">
                                {translate('message.errors.contact')}
                            </h1>
                        </div>
                    </div>
                    <span className="close"><i className="fa fa-times" aria-hidden="true" data-action="close"></i></span>
                </div>
            </div>
        );
    }
};

export default MsgContactFail;