import React, {Component} from 'react';
import {connect} from 'react-redux';

import { updateProfile } from '../../../actions/profile';
import { hidePopup } from '../../../actions/popups';
import { requestAsAuthenticatedUser, updateUserInfo } from '../../../utils/authentication';
import { sendNotification } from '../../../utils/notification';
import { onInputChange } from '../../../utils/onInputChange';
import translate from '../../../utils/translate';
import Checkbox from '../../../components/form/Checkbox';
import InputText from '../../../components/form/InputText';
import Button from '../../../components/form/Button';
import { checkForTrueVal } from '../../../utils/helpers';
import Notification from '../../../components/notification';
import {whilePending} from '../../../utils/helpers';

class Template extends Component {
  constructor(props){
    super(props);

    const { firstName, lastName } = props.authStore.data.user;

    // Initial component state
    this.state = {
        id: '',
        firstName: firstName || '',
        lastName: lastName || '',
        agree: true,
        regulations: true,
        companyAnswers: {
            "wcw1": false,
            "wcw2": false,
            "wcw4": false,
            "wcw3": false,
            "wcw5": false,
            "wcw6": false
        },
        positionAnswers: {
            "wpw1": false,
            "wpw2": false,
            "wpw3": false,
            "wpw4": false,
            "wpw5": false,
            "wpw6": false
        },
        labels: {
          "wcw1": translate('label.profile.aboutCompany.companyAnswer1'),
          "wcw2": translate('label.profile.aboutCompany.companyAnswer2'),
          "wcw4": translate('label.profile.aboutCompany.companyAnswer4'),
          "wcw3": translate('label.profile.aboutCompany.companyAnswer3'),
          "wcw5": translate('label.profile.aboutCompany.companyAnswer5'),
          "wcw6": translate('label.profile.aboutCompany.companyAnswer6'),
          "wpw1": translate('label.profile.aboutPosition.positionAnswer1'),
          "wpw2": translate('label.profile.aboutPosition.positionAnswer2'),
          "wpw3": translate('label.profile.aboutPosition.positionAnswer3'),
          "wpw4": translate('label.profile.aboutPosition.positionAnswer4'),
          "wpw5": translate('label.profile.aboutPosition.positionAnswer5'),
          "wpw6": translate('label.profile.aboutPosition.positionAnswer6')
        }
    };

    // Bindings
      this.onChange = this.onChange.bind(this);
      this.onBlur = this.onBlur.bind(this);
      this.onChangeList = this.onChangeList.bind(this);
      this.onFormSubmit = this.onFormSubmit.bind(this);
  }

    onChange(event){
    const info = onInputChange(event, this.state);
    this.setState(info);
    }

    onBlur() {
        this.setState({
            ...this.state,
            firstName: this.state.firstName.trim(),
            lastName: this.state.lastName.trim()
        })
    }

    onChangeList(event, state, listType){
        const info = onInputChange(event, state);
        const answerList = {};
        answerList[listType] = info;
        this.setState(answerList);
    }

  componentDidMount(){
      this.getProfile();
  }
    getProfile(){
        const {authStore, profileStore} = this.props;
        if(authStore.isAuthenticated && profileStore.fetched) {

            let data = profileStore.data;

            this.setState({
                id: data.id,
                firstName: data.rpcUserDTO.firstName ? data.rpcUserDTO.firstName : '',
                lastName: data.rpcUserDTO.lastName ? data.rpcUserDTO.lastName : '',
                birthDate: data.birthDate ? data.birthDate : '',
                companyAnswers: {
                    "wcw1": data.wcw1 ? data.wcw1 : false,
                    "wcw2": data.wcw2 ? data.wcw2 : false,
                    "wcw4": data.wcw4 ? data.wcw4 : false,
                    "wcw3": data.wcw3 ? data.wcw3 : false,
                    "wcw5": data.wcw5 ? data.wcw5 : false,
                    "wcw6": data.wcw6 ? data.wcw6 : false
                },
                positionAnswers: {
                    "wpw1": data.wpw1 ? data.wpw1 : false,
                    "wpw2": data.wpw2 ? data.wpw2 : false,
                    "wpw3": data.wpw3 ? data.wpw3 : false,
                    "wpw4": data.wpw4 ? data.wpw4 : false,
                    "wpw5": data.wpw5 ? data.wpw5 : false,
                    "wpw6": data.wpw6 ? data.wpw6 : false
                }
            })
        }
    }
    onFormSubmit(event){
        event.preventDefault();

          const {profileStore} = this.props;

          const answers = {
              "wcw1": this.state.companyAnswers.wcw1,
              "wcw2": this.state.companyAnswers.wcw2,
              "wcw3": this.state.companyAnswers.wcw3,
              "wcw4": this.state.companyAnswers.wcw4,
              "wcw5": this.state.companyAnswers.wcw5,
              "wcw6": this.state.companyAnswers.wcw6,
              "wpw1": this.state.positionAnswers.wpw1,
              "wpw2": this.state.positionAnswers.wpw2,
              "wpw3": this.state.positionAnswers.wpw3,
              "wpw4": this.state.positionAnswers.wpw4,
              "wpw5": this.state.positionAnswers.wpw5,
              "wpw6": this.state.positionAnswers.wpw6
          };

          const userInfo = {
              firstName: this.state.firstName,
              lastName: this.state.lastName
          };

          const newInfo = {
              ...userInfo,
              profileFilled: true
          };

          const newUser = {
              ...profileStore.data,
              ...answers,
              rpcUserDTO: {
                  ...profileStore.data.rpcUserDTO,
                  ...userInfo
              }
          };

          const newProfile = {
              "first_name": this.state.firstName,
              "last_name": this.state.lastName,
              ...answers
          };

          let notificationsDelay = 3000;

        if(!userInfo.firstName || !userInfo.lastName) {
            return sendNotification(translate('message.errorSelectVal'), 'error', notificationsDelay);
        }

          // Validate if at least one value is selected
          let companyList = !!checkForTrueVal(this.state.companyAnswers);
          let positionList = !!checkForTrueVal(this.state.positionAnswers);

          // Show error message if all values are empty
          if(!companyList || !positionList){
              return sendNotification(translate('message.errorSelectVal'), 'error', notificationsDelay);
          }

      // Proceed if validation is passed

        whilePending(true, '.btn-padding');
        requestAsAuthenticatedUser("/users/profile/" + profileStore.data.id + "/requiredProfileData", newProfile, 'put', () => {
          updateUserInfo(newInfo);
          this.props.updateProfile(newUser);
          sendNotification(translate('message.success.changesSaved'), 'success', notificationsDelay)
          setTimeout(() => {
              this.props.hidePopup();
              whilePending(false, '.btn-padding');
          }, 3000);
          whilePending(false);
        }, () => {
          sendNotification(translate('message.errors.savingFailure'), 'error', notificationsDelay);
          whilePending(false, '.btn-padding');
        });

  }

    renderCompanyAnswers(list, testString){
        return Object.keys(list.companyAnswers).map((item) => {
            return(
                <li className={list.companyAnswers[item]} key={item}>
                    <Checkbox checkboxTxt={list.labels[item]}
                              htmlFor={item}
                              onInputChange={event => this.onChangeList(event, list.companyAnswers, 'companyAnswers')}
                              testString={testString + '.companyDetails'}
                              value={list.companyAnswers[item]} />
                </li>
            )
        })
    }
    renderPositionAnswers(list, testString){
        return Object.keys(list.positionAnswers).map((item) => {
            return(
                <li className={list.positionAnswers[item]} key={item}>
                    <Checkbox checkboxTxt={list.labels[item]}
                              htmlFor={item}
                              onInputChange={event => this.onChangeList(event, list.positionAnswers, 'positionAnswers')}
                              testString={testString + '.positionDetails'}
                              value={list.positionAnswers[item]} />
                </li>
            )
        })
    }

    render(){
      const testString = 'popup.userProfile';
      const { authStore } = this.props;
      const social = (authStore.data.provider !== 'wordpress');

        return(
            <div className="pp">
                <div className="wrapper pp-user-details">
                    <h1 className="head-dark">{translate('account.hello')}</h1>
                    <Notification data={this.props.notificationStore} />
                    <form data-test="popup.userProfile" onSubmit={event => this.onFormSubmit(event)}>
                        <div className="text-center">
                            <p className="pt-3">{translate('label.profile.whoAreYou')}</p>
                        </div>
                        {!social ?
                            <div className="form-group pt-0">
                                {/*<p>{translate('label.profile.whoAreYou')}</p>*/}
                                <div className="d-flex flex-column flex-md-row">
                                    <InputText groupClasses="w-100 mr-md-2"
                                               labelTxt={translate('label.name')}
                                               htmlFor="firstName"
                                               value={this.state.firstName}
                                               onInputChange={this.onChange}
                                               onBlur={this.onBlur}
                                               testString={testString}
                                               validationMessage={translate('message.validation.noSpecialsandNumbers')}
                                               required
                                               formGroupClass={false}
                                               labelClasses="ml-3"
                                    />

                                    <InputText groupClasses="w-100"
                                               labelTxt={translate('label.surname')}
                                               htmlFor="lastName"
                                               value={this.state.lastName}
                                               onInputChange={this.onChange}
                                               onBlur={this.onBlur}
                                               testString={testString}
                                               validationMessage={translate('message.validation.noSpecialsandNumbers')}
                                               required
                                               formGroupClass={false}
                                               labelClasses="ml-3"
                                    />
                                </div>
                            </div>
                        : null}
                        <div className="form-group white pb-0">
                            <h3 className="">{translate('label.profile.aboutCompany.companyQuestion')}</h3>
                            <ul className="form-check border-bottom pb-4 px-md-4">
                                {this.renderCompanyAnswers(this.state, testString)}
                            </ul>
                        </div>
                        <div className="form-group white">
                            <h3 className="">{translate('label.profile.aboutPosition.positionQuestion')}</h3>
                            <ul className="form-check px-md-4">
                                {this.renderPositionAnswers(this.state, testString)}
                            </ul>
                        </div>
                        <div className="form-group form-group-last pb-0 agree">
                            <div className="row">
                                <div className="col-24 col-md-14">
                                    <Checkbox labelClasses='mt-1 mb-1'
                                              inputClasses=''
                                              checkboxTxt={translate('conditions.profile')}
                                              htmlFor='agree'
                                              onInputChange={this.onChange}
                                              testString={testString}
                                              value={this.state.agree}
                                              required />
                                    <Checkbox labelClasses='mt-1 mb-1'
                                              inputClasses=''
                                              checkboxTxt={translate('conditions.agreeWith') + "<a href='/regulations'> " + translate('conditions.regulations') + " *</a>" }
                                              htmlFor='regulations'
                                              onInputChange={this.onChange}
                                              testString={testString}
                                              value={this.state.regulations}
                                              required />
                                </div>
                                <div className="col-24 col-md-10">
                                  <Button className="btn btn-color btn-padding btn-padding-sides"
                                          buttonTxt={translate('label.buttons.confirm')}
                                          testString={testString + '.interestsDetails'} />
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (store) => {
  return {
    profileStore: store.profile,
    authStore: store.auth,
    notificationStore: store.notification
  }
}

export default connect(mapStateToProps, { hidePopup, updateProfile })(Template);