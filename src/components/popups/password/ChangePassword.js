import React, {Component} from 'react';
import {connect} from 'react-redux';
import axios from 'axios';
import getConfig from '../../../utils/getConfig';
import translate from '../../../utils/translate';
import {showPopup} from '../../../actions/popups';
import {sendNotification} from '../../../utils/notification';
import {browserHistory} from 'react-router';
import InputPassword from '../../form/InputPassword';
import Notification from '../../../components/notification';
import {whilePending} from '../../../utils/helpers';

class ChangePassword extends Component {

    constructor(props){
        super(props);

        this.state = {
            credentials: {
                password: '',
                confirmPassword: '',
            },
            showPassword: false,
            passwordType: 'password'
        };

        this.onInputChange = this.onInputChange.bind(this);
        this.onFormSubmit =  this.onFormSubmit.bind(this);
        this.onPasswordShow = this.onPasswordShow.bind(this);
    }

    onPasswordShow(){
        if(this.state.showPassword) {
            this.setState({ showPassword: false, passwordType: 'password' })
        } else {
            this.setState({ showPassword: true, passwordType: 'text' });
        }
    }

    onInputChange(event){
        const field = event.target.name;
        const credentials = this.state.credentials;

        if(event.target.type === 'checkbox'){
            credentials[field] = event.target.checked;
        } else {
            credentials[field] = event.target.value;
        }

        this.setState({ credentials: credentials });
    }

    onFormSubmit(event){
        event.preventDefault();
        const { password, confirmPassword} = this.state.credentials;

        if(password !== confirmPassword) {
            sendNotification(translate('message.errors.passwordsNotMatch'), 'error', 3000);
        } else {

            const {key, username} = this.props.popupsStore.params;
            let requestParams = {
                password: password,
                key: key,
                username: username
            }

            whilePending(true, '.btn-padding');

            axios.post(getConfig().restApiHostName + '/users/confirmKey', requestParams).then(response => {
                if (response.data === 'Updated') {
                    sendNotification(translate('account.confirmKey.confirmKeySuccess'), 'success', 3000)
                    setTimeout(() => {
                        this.props.showPopup('login');
                        whilePending(false, '.btn-padding');
                    }, 3000);
                    whilePending(false);
                }
            }).catch(error => {
                let msgText = 'message.errors.error';
                let msgText2 = '';
                switch (error.response.data.code) {
                    case 'password_too_short':
                        msgText = 'message.validation.passwordRequirements';
                        break;
                    case 'expired_key':
                        msgText = 'account.confirmKey.confirmKeyExpired';
                        msgText2 = 'account.usePasswordReset'
                        break;
                    case 'invalid_key':
                        msgText = 'account.confirmKey.confirmKeyInvalid';
                        msgText2 = 'account.usePasswordReset'
                        break;
                    default:
                        break;
                }

                let delay = 5000;
                let msgs = translate(msgText)

                if(msgText2.length > 0) {
                    msgs += translate(msgText2)
                    setTimeout(() => {
                        this.props.showPopup('remindPassword');
                    }, delay)
                }

                whilePending(false, '.btn-padding');

                sendNotification(msgs, 'error', delay);

            })
        }

    }

    render(){
        let testString = 'popup.passwordChange';
        return(
            <div className="pp">
                <div className="wrapper pp-password-set">
                    <h2 className="head">{translate('label.writeNewPassword')}</h2>

                    <Notification data={this.props.notificationStore} className="mt-4" />

                    <form autoComplete="off" data-test="popup.passwordChange" className="mt-4 row" onSubmit={this.onFormSubmit}>

                        <InputPassword groupClasses="form-password form-password-col col-md-24"
                                       labelTxt={translate('label.newPassword')}
                                       htmlFor="password"
                                       testString={testString + '.input'}
                                       onInputChange={this.onInputChange}
                                       value={this.state.credentials.password} />

                        <InputPassword groupClasses="form-password form-password-col col-md-24"
                                       labelTxt={translate('label.confirmPassword')}
                                       htmlFor="confirmPassword"
                                       testString={testString + '.input'}
                                       onInputChange={this.onInputChange}
                                       value={this.state.credentials.confirmPassword} />

                        <div className="form-group col-24 mb-0">
                            <button data-test="popup.passwordChange.button.send" className="btn btn-color btn-padding mb-3 mt-3  w-100">{translate('label.buttons.save')}</button>
                        </div>
                    </form>
                    <span className="close"><i className="fa fa-times" aria-hidden="true" data-action="close"></i></span>
                </div>
            </div>
        );
    }

    componentWillUnmount() {
        browserHistory.push('/');
    }
}

const mapStateToProps = (store) => {
    return {
        popupsStore: store.popups,
        notificationStore: store.notification
    };
};

export default connect(mapStateToProps, {showPopup})(ChangePassword);

