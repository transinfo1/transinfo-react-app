import React, {Component} from 'react';
import {connect} from 'react-redux';
import axios from 'axios';
import getConfig from '../../../utils/getConfig';
import translate from '../../../utils/translate';
import {showPopup, hidePopup} from '../../../actions/popups';
import {sendNotification} from '../../../utils/notification';
import Notification from '../../../components/notification';
import InputEmail from '../../../components/form/InputEmail';
import {whilePending} from '../../../utils/helpers';
import {getLocale} from '../../../utils/storeData';

class RemindPassword extends Component {

    constructor(props){
        super(props);

        this.state = {
            credentials: {
                username: '',
            },
        };

        this.onInputChange = this.onInputChange.bind(this);
        this.onFormSubmit =  this.onFormSubmit.bind(this);
    }

    onInputChange(event){
        const field = event.target.name;
        const credentials = this.state.credentials;

        if(event.target.type === 'checkbox'){
            credentials[field] = event.target.checked;
        } else {
            credentials[field] = event.target.value;
        }

        this.setState({ credentials: credentials });
    }

    onFormSubmit(event){
        event.preventDefault();

        let msgDelay = 5000;

        whilePending(true, '.btn-padding');
        let $ = window.jQuery;
        axios.post(getConfig().restApiHostName + '/users/passwordRecovery', $.param({
            usernameOrEmail: this.state.credentials.username,
            locale: getLocale()
        }), {
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        }).then(response => {
            if (response.data === 'Success send email') {
                sendNotification(translate('message.account.success.passwordResetEmailSent'), 'success', msgDelay);
                setTimeout(() => {this.props.hidePopup()}, msgDelay);
                whilePending(false, '.btn-padding');
            }
        }).catch(error => {
            sendNotification(translate('message.account.errors.passwordResetFailure'), 'error', msgDelay);
            whilePending(false, '.btn-padding');
        })
    }

    render(){
        const testString = "popup.forgottenPassword";
        return(
            <div className="pp">
                <div className="wrapper pp-password">

                    <h2 className="head">{translate('account.forgottenPassword')}</h2>

                    <form data-test="popup.forgottenPassword" className="mt-4" onSubmit={this.onFormSubmit}>
                        <Notification data={this.props.notificationStore} className="mt-2" />
                        <div className="row">
                            <InputEmail groupClasses="col-md-18"
                                        htmlFor="username"
                                        labelTxt={translate('label.email')}
                                        testString={testString}
                                        onInputChange={this.onInputChange}
                                        value={this.state.credentials.username}
                                        required />
                            <div className="form-group col-md-6 mb-md-0">
                                <button data-test="popup.forgottenPassword.button.send" className="btn btn-color btn-padding mb-0 w-100">
                                    {translate('label.buttons.send')}
                                </button>
                            </div>
                        </div>
                        <div className="row">
                            <p className="col-24 mb-0" dangerouslySetInnerHTML={ {__html: translate('account.forgottenPasswordDescription')} } />
                        </div>
                    </form>
                    <span className="close"><i className="fa fa-times" aria-hidden="true" data-action="close" /></span>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (store) => {
    return {
        profileStore: store.profile,
        notificationStore: store.notification
    }
}

export default connect(mapStateToProps, {showPopup, hidePopup})(RemindPassword);