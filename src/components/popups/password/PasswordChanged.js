import React from 'react';
import SocialLogin from '../login/Social';
import TransLogin from '../login/WordPress';
import translate from '../../../utils/translate';

const PasswordChanged = () => {
    return(
        <div className="pp">
            <div className="wrapper pp-password-changed">
                <h2 className="head">
                    {translate('message.account.success.changePassword')}
                </h2>
                <SocialLogin iconsLeft />
                <p className="or"><span>{translate('main.or')}</span></p>
                <TransLogin secondaryLogin btnClasses="mr-2" />
                <span className="close"><i className="fa fa-times" aria-hidden="true" data-action="close"></i></span>
            </div>
        </div>
    );
};

export default PasswordChanged;
