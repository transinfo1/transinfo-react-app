import React, {Component} from 'react';
import {connect} from 'react-redux';
import axios from 'axios';
import translate from '../../../utils/translate';
import { sendNotification } from '../../../utils/notification';
import getConfig from '../../../utils/getConfig';
import {getLocale} from '../../../utils/storeData';
import Notification from '../../../components/notification';

class MessageSuccess extends Component {
    resendEmail() {
        let $ = window.jQuery;
        axios.post(getConfig().restApiHostName + '/users/passwordRecovery', $.param({
            usernameOrEmail: this.props.popupsStore.params,
            locale: getLocale()
        }), {
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        }).then(response => {
            if (response.data === 'Success send email') {
                sendNotification(translate('message.account.success.emailResendSuccess'), 'success', 3000);
            }
        }).catch(error => {
            sendNotification(translate('message.errors.emailResendFailure'), 'error', 3000);
        })
    }

    render() {
        return (
            <div className="pp">
                <div className="wrapper pp-message-success">
                    <div className="row">
                        <div className="col-24">
                            <img src="/static/img/icons/thx-check.png" alt="Register success" />
                        </div>
                        <div className="col-24">
                            <h1 className="mb-4 mt-4">{translate('message.confirmAccountFromEmail')}</h1>
                        </div>
                        <div className="col-24">
                            <p className="mb-4 pb-3">
                                {translate('message.completeAccount')} <b data-test="popup.registerSuccess.email">{this.props.popupsStore.params}</b>.<br />
                                {translate('message.checkEmail')}
                            </p>
                            <p className="mb-0">
                                <a href="#" onClick={() => this.resendEmail()} data-test="popup.registerSuccess.resend">{translate('message.resendEmail')}</a>
                                <Notification data={this.props.notificationStore} className="mt-4" />
                            </p>
                        </div>
                    </div>
                    <span className="close"><i className="fa fa-times" aria-hidden="true" data-action="close" /></span>
                </div>
            </div>
        );
    }
};

const mapStateToProps = (store) => ({
    popupsStore: store.popups,
    notificationStore: store.notification
})

export default connect(mapStateToProps)(MessageSuccess);
