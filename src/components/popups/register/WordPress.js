import React, {Component} from 'react';
import { connect } from 'react-redux';
import translate from '../../../utils/translate';
import { sendNotification } from '../../../utils/notification';
import { getLocale } from '../../../utils/storeData';
import { showPopup } from '../../../actions/popups'
import axios from 'axios';
import getConfig from '../../../utils/getConfig';
import { onInputChange } from '../../../utils/onInputChange';

import Checkbox from '../../../components/form/Checkbox';
import InputEmail from '../../../components/form/InputEmail';
import Button from '../../../components/form/Button';
import {whilePending} from '../../../utils/helpers';

class WordPress extends Component {

    constructor(props){
        super(props);

        this.state = {
            credentials: {
                username: '',
                rules: true,
                userData: true
            }
        };

        this.onChange = this.onChange.bind(this);
        this.onFormSubmit =  this.onFormSubmit.bind(this);
    }
    onChange(event){
        const info = onInputChange(event, this.state.credentials);
        this.setState({credentials: info});
    }
    onFormSubmit(event){
        event.preventDefault();

        const guid = () => {
            const s4 = () => {
                return Math.floor((1 + Math.random()) * 0x10000)
                    .toString(16)
                    .substring(1);
            }
            return s4() + s4() + s4() + s4() + s4() + s4();
        }

        let requestParams = {
            username: guid(),
            email: this.state.credentials.username.trim(),
            locale: getLocale()
        }

        whilePending(true, '.btn-padding');

        axios.post(getConfig().restApiHostName + '/users/create', requestParams).then(() => {
            this.props.showPopup('registerSuccess', this.state.credentials.username);
            whilePending(false, '.btn-padding');
        }).catch(error => {
            let msgText = 'message.errors.error';
            switch (error.response.data.code) {
                case 'rest_invalid_param':
                    msgText = 'message.errors.wrongEmail';
                    break;
                case 'existing_user_email':
                    msgText = 'message.errors.emailExists';
                    break;
                default:
                    break;
            }
            sendNotification(translate(msgText), 'error', 5000);
            whilePending(false, '.btn-padding');
            window.dataLayer.push({
                typeaccount: 'email',
                messerr: translate(msgText),
                event: 'NewAccountErr'
            });
        })
    }

  render(){
      const testString = "popup.registerForm";
      return(
          <form data-test="popup.registerForm" onSubmit={this.onFormSubmit}>
              <InputEmail htmlFor="username"
                          labelTxt={translate('label.email')}
                          testString={testString}
                          onInputChange={this.onChange}
                          value={this.state.credentials.username}
                          required />
              <div className="form-group d-sm-flex flex-sm-row mt-4">
                  <div className="col-24 col-sm-15 p-0 agree">
                      <Checkbox testString={testString}
                                htmlFor="userData"
                                checkboxTxt={translate('conditions.profile')}
                                onInputChange={this.onChange}
                                value={this.state.credentials.userData}
                                required />

                      <Checkbox testString={testString}
                                htmlFor="rules"
                                checkboxTxt={translate('conditions.agreeWith') + "<a href='/regulations'> " + translate('conditions.regulations') + " *</a>" }
                                onInputChange={this.onChange}
                                value={this.state.credentials.rules}
                                required />
                  </div>
                  <div className="col-24 col-sm-9 px-0 pl-sm-3 mt-3 mt-sm-0">
                      <div className="form-group mb-0 d-flex flex-column">
                          <Button className="btn btn-color btn-padding"
                                  testString={testString}
                                  buttonTxt={translate('account.register')}
                                  testEnzyme="register-btn" />
                          <p data-test={testString + '.link.login'} className="mb-0 mt-2 login">
                              {translate('account.haveAccount')}
                              <a onClick={(e) => e.preventDefault()} href="#" data-popup="login" className="ml-2">{translate('navigation.login')}</a>
                          </p>
                      </div>
                  </div>
              </div>
          </form>
      );
  }
}

const mapStateToProps = (store) => ({
    store: store.auth
})

export default connect(mapStateToProps, {showPopup})(WordPress);