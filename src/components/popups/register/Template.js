import React, { Component } from 'react';
import { connect } from 'react-redux';
import SocialLogin from '../login/Social';
import WordPressRegister from './WordPress';
import translate from '../../../utils/translate';
import Notification from '../../../components/notification';

class Template extends Component {
       render(){
           return(
               <div className="pp">
                   <div className="wrapper pp-register">
                       <div className="row">
                           <div className="col-24">
                               <h2 className="head">{translate('account.registerAndJoin')}</h2>
                           </div>
                           <div className="w-100">
                               <SocialLogin iconsLeft />
                               <Notification data={this.props.notificationStore} />
                               <p className="or my-2"><span>{translate('main.or')}</span></p>
                               <WordPressRegister />
                           </div>
                           <div className="why w-100">
                               <h2 className="mb-4">{translate('account.registerReason.whyRegister')}</h2>
                               <ul className="d-flex flex-column flex-sm-row">
                                   <li className="pr-sm-5 mb-2 mb-sm-0">{translate('account.registerReason.whyRegisterP1')}</li>
                                   <li className="mb-2 mb-sm-0">{translate('account.registerReason.whyRegisterP2')}</li>
                                   <li className="pl-sm-5">{translate('account.registerReason.whyRegisterP3')}</li>
                               </ul>
                           </div>
                       </div>
                       <span data-test="popup.register.button.close" className="close"><i className="fa fa-times" aria-hidden="true" data-action="close"></i></span>
                   </div>
               </div>
           );
       }
}

const mapStateToProps = (store) => {
    return {
        notificationStore: store.notification
    };
};

export default connect(mapStateToProps)(Template);
