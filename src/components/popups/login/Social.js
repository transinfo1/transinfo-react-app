import React, {Component} from 'react';
import translate from '../../../utils/translate';
import {connect} from 'react-redux';
import { socialLoginFailed, setAuthenticatedUser } from '../../../actions/auth';
import { fetchProfile } from '../../../actions/profile';
import { sendNotification } from '../../../utils/notification';
import { isAuthenticated, fetchUserData } from '../../../utils/authentication';
import {whilePending} from '../../../utils/helpers';
import GoogleLogin from 'react-google-login';
import axios from 'axios';
import getConfig from '../../../utils/getConfig';
import * as storage from '../../../utils/storage';

class Social extends Component {
    constructor(props) {
        super(props);
        this.hash = storage.get('deviceId').split('-')[0].slice(1,7); // oneLogin 'state' hash - taken from deviceId hash
        this.googleResponseSuccess = this.googleResponseSuccess.bind(this);
        this.googleResponseFailure = this.googleResponseFailure.bind(this);
    }

    componentDidMount() {
        let params = this.props.popupStore.params;
        if(!isAuthenticated() && params.code && (params.state === this.hash || params.state === 'f04uM')) {
            whilePending(true);
            this.getTokenByCode(params.code, 'onelogin').then(response => {
                fetchUserData(response.data, 'onelogin').then(() => {

                    window.dataLayer.push({
                        provider: 'OneLogin',
                        message: 'success',
                        event: 'LogIn'
                    });

                    whilePending(false);
                    if(params.state === 'f04uM') {
                        window.location.href = '/forum/';
                    }
                }).catch(error => {
                    this.onError('OneLogin', error);
                })
            }).catch(error => {
                this.onError('OneLogin', error);
            })
        }
    }

    getTokenByCode(code, provider) {
        let $ = window.jQuery;
        return axios.post(getConfig().restApiHostName + '/token/get', $.param({
            code: code,
            provider: provider
        }), {
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        })
    }

    getFacebookLongLifeToken(shortLifeToken) {
        let $ = window.jQuery;
        return axios.post(getConfig().restApiHostName + '/token/fb/getLongLivedToken', $.param({
            fbExchangeToken: shortLifeToken
        }), {
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        })
    }

    facebookLoginPopup() {
        whilePending(true);
        window.FB.login(response => {
            this.facebookResponse(response)
        }, {
            scope: 'public_profile,email'
        })
    }

    facebookResponse(resp) {
        if (resp.status !== "unknown") {
            if(!resp.authResponse) {
                whilePending(false);
            }
            this.props.fetchProfile({
                accessToken: resp.authResponse.accessToken,
                provider: 'facebook'
            }).then(profileResp => {
                this.getFacebookLongLifeToken(resp.authResponse.accessToken).then(response => {

                    window.dataLayer.push({
                        provider: 'facebook',
                        message: 'success',
                        event: 'LogIn',
                    })

                    this.props.setAuthenticatedUser({
                        accessToken: response.data,
                        provider: 'facebook',
                        user: profileResp.action.payload.data.rpcUserDTO
                    })
                    whilePending(false);
                })
            }).catch(error => {
                this.onError('Facebook', error);
                whilePending(false);
            })
        } else {
            whilePending(false);
        }
    }

    googleResponseSuccess(resp) {
        whilePending(true);
        if (resp) {
            this.getTokenByCode(resp.code, 'google').then(response => {
                this.props.fetchProfile({
                    accessToken: response.data,
                    provider: 'google'
                }).then(profileResp => {

                    window.dataLayer.push({
                        provider: 'Google',
                        message: 'success',
                        event: 'LogIn',
                    })

                    this.props.setAuthenticatedUser({
                        accessToken: response.data,
                        provider: 'google',
                        user: profileResp.action.payload.data.rpcUserDTO
                    })
                    whilePending(false);
                }).catch(error => {
                    this.onError('Google+', error);
                })
            }).catch(error => {
                this.onError('Google+', error);
            })
        } else {
            whilePending(false);
        }
    }

    googleResponseFailure(resp) {
        if (resp.error === 'popup_closed_by_user') {
            console.log('Google+ login popup closed by user. Retrying after one second.')
            setTimeout(() => {
                document.querySelector('.btn-social-gp').click()
            }, 1000)
        }
    }


    oneLogin() {
        let params = [
            'redirect_uri=' + location.origin,
            'client_id=' + getConfig().auth.oneLogin.clientId, // client ID z prod z mobilnej trans.INFO: 11474527064r5sXitnNnPnrqN8z6JVB
            'response_type=code',
            'scope=companies.employees.me.read',
            'state=' + this.hash
        ];
        window.location.href = getConfig().auth.oneLogin.authServerUrl + "?" + params.join('&'); // DEV url: https://auth.dev-trans.rst.com.pl
    }

    onError(provider, error){
        this.props.socialLoginFailed(error);
        whilePending(false);
        sendNotification(provider + translate('message.account.errors.socialLoginFailed'), 'error', 7000);

        window.dataLayer.push({
            provider: provider,
            message: translate('message.account.errors.socialLoginFailed'),
            event: 'LogErr',
        });

    }

    render() {

        return (
            <div className="social-login d-flex flex-column flex-sm-row align-items-center my-3">
                  <div data-test="popup.facebookLogin" className="w-100 m-1">

                      <button className="btn btn-primary btn-social btn-social-fb fa fa-facebook w-100" onClick={() => this.facebookLoginPopup()}>Facebook</button>

                  </div>
                  <div data-test="popup.googleLogin" className="w-100 m-1">
                      <GoogleLogin
                          clientId={getConfig().auth.google.clientId}
                          onSuccess={this.googleResponseSuccess}
                          onFailure={this.googleResponseFailure}
                          className="btn btn-danger btn-social btn-social-gp fa fa-google-plus w-100"
                          scope="email profile"
                          buttonText="Google"
                          responseType="code"
                          autoLoad={false}
                          isSignedIn={false}
                          prompt="consent"
                      />

                      {/*google disabled*/}

                      {/*<button className="btn btn-danger btn-social btn-social-gp fa fa-google-plus w-100" onClick={() => this.google()}>Google</button>*/}

                  </div>

                <div className="w-100 m-1" data-test="popup.oneLogin" >
                    <button className="btn btn-primary btn-social btn-onelogin w-100" onClick={() => this.oneLogin()}>Trans</button>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (store) => ({
    store: store.auth,
    popupStore: store.popups
})

export default connect(mapStateToProps, {socialLoginFailed, setAuthenticatedUser, fetchProfile})(Social);
