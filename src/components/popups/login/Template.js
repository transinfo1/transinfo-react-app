import React, { Component } from 'react';
import translate from '../../../utils/translate';
import SocialLogin from './Social';
import WordPressLogin from './WordPress';
import Notification from '../../../components/notification';
import { connect } from 'react-redux';

class Template extends Component {
    render(){
        const testString = 'popup';
        return (
            <div className="pp">
                <div className="wrapper pp-login">
                    <div className="d-flex flex-column">
                        <h2 className="head">{translate('account.loginToContinue')}</h2>
                        <SocialLogin />
                        <Notification data={this.props.notificationStore} />
                        <p className="or"><span>{translate('main.or')}</span></p>
                        <WordPressLogin testString={testString} />
                    </div>
                    <span data-test="popup.login.button.close" className="close"><i className="fa fa-times" aria-hidden="true" data-action="close"></i></span>
                </div>
            </div>
        );
    }
};

const mapStateToProps = (store) => {
    return {
        notificationStore: store.notification
    };
};

export default connect(mapStateToProps)(Template);
