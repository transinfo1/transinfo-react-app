import React, {Component} from 'react';
import translate from '../../../utils/translate';
import {connect} from 'react-redux';

import {wordPressLogin} from '../../../actions/auth';
import InputEmail from '../../form/InputEmail';
import InputPassword from '../../form/InputPassword';
import Button from '../../form/Button';
import {sendNotification} from '../../../utils/notification';
import Checkbox from '../../form/Checkbox';
import {onInputChange} from '../../../utils/onInputChange';
import {whilePending} from '../../../utils/helpers';
import {fetchProfile} from '../../../actions/profile';
import moment from 'moment';

export class WordPress extends Component {
    constructor(props) {
        super(props);

        this.state = {
            credentials: {
                username: '',
                password: '',
                remember: true,
            }
        };

        this.onChange = this.onChange.bind(this);
        this.onFormSubmit = this.onFormSubmit.bind(this);
    }

    onChange(event) {
        const info = onInputChange(event, this.state.credentials);
        this.setState({credentials: info});
    }

    onFormSubmit(event) {
        event.preventDefault();
        whilePending(true, '.btn-padding');
        this.props.wordPressLogin(this.state.credentials).then(() => {
            whilePending(false, '.btn-padding');

            window.dataLayer.push({
                provider: 'email',
                message: 'success',
                event: 'LogIn',
            })

        }).catch(() => {
            this.onError();
            whilePending(false, '.btn-padding');
        });
    }

    onError() {
        const lockTime = this.props.store.responseBody.lockdowntime,
            failedLoginAttempt = this.props.store.responseBody.failedLoginAttempt,
            maxfailedLoginAttempts = this.props.store.responseBody.maxfailedLoginAttempts;

        const {status, response} = this.props.store;

        if (response && [401, 403].indexOf(status) >= 0) {
            if (lockTime >= 0) {
                sendNotification(translate('message.account.errors.reLoginAt') + moment().add(lockTime, 'seconds').format("HH:mm"), 'error', 5000);
                window.dataLayer.push({
                    typeaccount: 'email',
                    messerr: translate('message.account.errors.reLoginAt') + moment().add(lockTime, 'seconds').format("HH:mm"),
                    event: 'LoginErr'
                });
            } else if (failedLoginAttempt >= 1 && failedLoginAttempt <= maxfailedLoginAttempts) {
                sendNotification(translate('message.account.errors.loginFailed') + ' ' + translate('message.account.errors.failedLoginAttempt.failedLoginAttemptText1') + ' ' + failedLoginAttempt + ' ' + translate('message.account.errors.failedLoginAttempt.failedLoginAttemptText2') + ' ' + maxfailedLoginAttempts + ' ' + translate('message.account.errors.failedLoginAttempt.failedLoginAttemptText3'), 'error', 5000);
                window.dataLayer.push({
                    typeaccount: 'email',
                    messerr: translate('message.account.errors.loginFailed') + ' ' + translate('message.account.errors.failedLoginAttempt.failedLoginAttemptText1') + ' ' + failedLoginAttempt + ' ' + translate('message.account.errors.failedLoginAttempt.failedLoginAttemptText2') + ' ' + maxfailedLoginAttempts + ' ' + translate('message.account.errors.failedLoginAttempt.failedLoginAttemptText3'),
                    event: 'LoginErr'
                });
            } else {
                sendNotification(translate('message.account.errors.loginFailed'), 'error', 5000);
                window.dataLayer.push({
                    typeaccount: 'email',
                    messerr: translate('message.account.errors.loginFailed'),
                    event: 'LoginErr'
                });
            }
        }
    }

    render() {

        const {testString = '', secondaryLogin} = this.props;
        const inputGroup = (secondaryLogin) ? 'form-group d-flex flex-column flex-sm-row orientation-landscape' : null;
        const submitGroup = (secondaryLogin) ? 'd-flex flex-row align-items-center mr-2' : null;
        const submitBtn = (secondaryLogin) ? 'mr-2' : 'btn-full-width mt-3';
        const passwordHidden = (secondaryLogin) ? 'hidden-xs-up' : null;

        return (
            <form autoComplete="off" method="post" data-test="popup.loginForm" className="d-flex flex-column"
                  onSubmit={this.onFormSubmit}>
                <div className={inputGroup}>
                    <InputEmail htmlFor="username"
                                labelTxt={translate('label.email')}
                                testString={testString + '.loginForm'}
                                testEnzyme="username"
                                onInputChange={this.onChange}
                                value={this.state.credentials.username}
                                required/>
                    <InputPassword groupClasses="form-password-col"
                                   labelTxt={translate('label.password')}
                                   htmlFor="password"
                                   testString={testString + '.loginForm'}
                                   onInputChange={this.onChange}
                                   value={this.state.credentials.password}
                                   required/>
                    <div className="d-flex">
                        <Checkbox checkboxTxt={translate('account.rememberMe')}
                                  htmlFor='remember'
                                  onInputChange={this.onChange}
                                  testString={testString}
                                  value={this.state.credentials.remember}/>
                        <a className={'ml-auto ' + passwordHidden} data-test="popup.loginForm.link.external" href="#"
                           onClick={(e) => e.preventDefault()} data-popup="remindPassword">
                            {translate('account.forgottenPassword')}
                        </a>
                    </div>
                </div>
                <div className={submitGroup}>
                    <Button className={"btn-padding btn-color " + submitBtn}
                            testString={testString + '.loginForm'}
                            buttonTxt={translate('navigation.login')}
                            testEnzyme="login-btn"
                    />
                    <div className="external-links mt-4">
                        <h2 className="mb-3">{translate('account.noAccount')}</h2>
                        <a className="btn btn-padding w-100" data-test={testString + "login.button.register"} href="#"
                           onClick={(e) => e.preventDefault()} data-popup="register">{translate('account.register')}</a>
                    </div>
                </div>
            </form>
        );
    }
}

const mapStateToProps = (store) => ({
    store: store.auth
})

export default connect(mapStateToProps, {wordPressLogin, fetchProfile})(WordPress);
