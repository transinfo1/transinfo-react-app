import React from 'react';
import { Link } from 'react-router';
import translate from '../../utils/translate';

const ViewMore = ({ path, color, className = '', testString, testKey = '', modified = false, modifiedTxt = '', normalLink = false }) => {

  const iconVersion = (color) ? 'arrow' : 'arrow_white';

  if(normalLink) {
      return(
          <a className={'view-more ' + className} href={'/' + path} data-test={testString + '.readMore' + testKey}>
              {modified ? modifiedTxt : translate('main.seeMore')} <img className="ml-2" src={`/static/img/icons/${iconVersion}.svg`} alt="arrow icon"/>
          </a>
      );
  }

  return(
    <Link className={'view-more ' + className} to={'/' + path} data-test={testString + '.readMore' + testKey}>
        {modified ? modifiedTxt : translate('main.seeMore')} <img className="ml-2" src={`/static/img/icons/${iconVersion}.svg`} alt="arrow icon"/>
    </Link>
  );

};

export default ViewMore;
