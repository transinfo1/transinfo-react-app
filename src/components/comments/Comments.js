import React, {Component} from 'react';
import {connect} from 'react-redux';

import {fetchComments, fetchNextComments} from '../../actions/comments';

class Comments extends Component {

    componentWillMount() {
        this.props.fetchComments();
    }

    add() {
        this.props.fetchNextComments(2)
    }

    getCommentsByParent(parent = 0) {
        return this.props.store.data.filter((comment) => {
            return comment.parent === parent
        })
    }

    replies(parent) {
        if(!this.getCommentsByParent(parent).length) {
          return null
        }

        return (
          <ul>
            {this.getCommentsByParent(parent).map((comment) => {
                return (
                    <li key={comment.id}>
                        <div>
                            <b>{comment.id}</b>
                        </div>
                        <div>{comment.content}</div>
                    </li>
                )
            })}
          </ul>
        )
    }

    render() {
        return (
            <div>
                <button onClick={this.add.bind(this)}>ADD</button>

                <ul>
                  {this.getCommentsByParent().map((comment) => {
                      return (
                          <li key={comment.id}>
                              <div>
                              <b>{comment.id}</b>
                              </div>
                              <div>{comment.content}</div>
                              {this.replies(comment.id)}
                          </li>
                      )
                  })}
              </ul>
       </div>
        );
    }
}

const mapStateToProps = (store) => {
    return {store: store.comments};
};

export default connect(mapStateToProps, {fetchComments, fetchNextComments})(Comments);
