import React from 'react';
import { plural } from '../../utils/helpers';
import { getLang } from '../../utils/storeData';

export default ({type = 'comments', value, description = false, white=false, className = '', testString, testKey = ''}) => {

    const getDescription = (value, type) => {
     return plural(value, getLang(), type);
    };

    let img = type === 'comments' ? 'comment' : 'arrow-reply'
    let color = white ? '_white' : '';

    return (
        <div className={'amount ' + className} data-test={testString + '.' + type + 'Amount'  + testKey}>
            <img
              className="cloud"
              src={'/static/img/icons/' + img + color + '.svg'}
              alt="comments"
              />

            <span>
                {value ? value : 0}
                {description ? ' ' + getDescription(value, type) : ''}
            </span>
        </div>
    )
}

