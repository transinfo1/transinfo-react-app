import React, {Component} from 'react';
import translate from '../../utils/translate';
import {connect} from 'react-redux';
import {addComment} from '../../actions/comments';
import {isAuthenticated, getUser, requestAsAuthenticatedUser} from '../../utils/authentication';
import { sendNotification } from '../../utils/notification';
import Notification from '../../components/notification';
import {whilePending} from '../../utils/helpers';

class AddFrom extends Component {
    constructor(props) {
        super(props);

        this.state = {
            comment: ''
        };

        this.onInputChange = this.onInputChange.bind(this);
        this.onFormSubmit = this.onFormSubmit.bind(this);

        this.idForNotifications = this.props.postId + (Number(this.props.parentId) || 0)
    }


    onInputChange(event) {
        this.setState({comment: event.target.value.trim()});
        // console.warn("CHANGE VALUE : ", event.target.value)
    }

    onFormSubmit(event) {
        event.preventDefault();
        // console.warn("FORM VALUE : ", this.state.comment);
        let postId = this.props.postId;
        let currentForm = document.getElementById('commentForm' + postId + Number(this.props.parentId));
        if (isAuthenticated() && this.state.comment.length) {
            if(this.props.beforeSendCallback) {
              this.props.beforeSendCallback();
            }
            currentForm.children.comment.disabled = true;
            whilePending(true);

            const onSuccess = (response) => {
                currentForm.reset();
                currentForm.children.comment.disabled = false;
                whilePending(false);
                this.props.addComment(response.data)
            }

            const onFailure = (error) => {
                currentForm.reset();
                currentForm.children.comment.disabled = false;
                whilePending(false);

                switch (error.response.status) {
                    case 409:
                        sendNotification(translate('message.errors.commentDuplicate'), 'error', 3000, this.idForNotifications);
                        break;
                    case 403:
                        // do not show any errors, wait for refresh token
                        break;
                    default:
                        sendNotification(translate('message.errors.commentFailure'), 'error', 3000, this.idForNotifications);
                }
            }

            requestAsAuthenticatedUser(
                '/comments/create/', {
                    content: this.state.comment,
                    parent: this.props.parentId ? this.props.parentId : 0,
                    post: postId,
                    userId: getUser().id
                }, 'post', onSuccess, onFailure
            );
        }
    }

    render() {
        let testKey = this.props.testKey !== undefined ? this.props.testKey : '';
        let testString2 = this.props.parentId ? '.addReplyInput' : '.addCommentInput';
        return (
            <form className="add-comment" onSubmit={this.onFormSubmit} id={'commentForm' + this.props.postId + Number(this.props.parentId)}>

                <Notification data={this.props.notificationStore} className="my-3" id={this.idForNotifications} />

                <input type="text" className="form-control" placeholder={this.props.parentId ? translate('label.placeholder.answer') : translate('label.placeholder.comment')} name="comment"
                       data-popup="login"
                       data-test={this.props.testString + testString2 + testKey}
                       onChange={this.onInputChange}
                       required
                />
            </form>

        )
    }
}

const mapStateToProps = (store) => ({
    store: store.comments,
    notificationStore: store.notification
})

export default connect(mapStateToProps, {addComment})(AddFrom);
