import React from 'react';

import AuthorLink from './atoms/AuthorLink';
import Thumbnail from './atoms/Thumbnail';
import { authorName } from '../../utils/helpers';


export default ({author, className = '', testString, testKey = ''}) => {
    if(author === undefined || author === null ) {
        author = {
            avatar : null,
            userDeleted  : true
        }
    }

    let {avatar, userCaption} = author;

    return (
        <div className={'post-author ' + className}>
            <AuthorLink data={author} testString={testString + '.thumbnail'} testKey={testKey}>
                <Thumbnail path={avatar} name={authorName(author)} />
            </AuthorLink>

            <div className="post-author-info py-2 ml-2 w-100" itemProp="author" itemScope itemType="http://schema.org/Person">
                <AuthorLink data={author} testString={testString} testKey={testKey}>
                    {authorName(author)}
                </AuthorLink>

                <div className="role" data-test={testString + '.role' + testKey}>
                    {userCaption ? userCaption : ''}
                </div>
            </div>
        </div>
    )
}
