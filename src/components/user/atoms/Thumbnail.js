import React from 'react';

export default ({path = '', name = '', className = ''}) => (
    <img
        className={'thumbnail rounded-circle ' + className}
        src={path || '/static/img/default-avatar.png'}
        alt={name || 'thumbnail'}
    />
)
