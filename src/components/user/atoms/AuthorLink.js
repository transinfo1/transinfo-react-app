import React, {Component} from 'react';
import {Link} from 'react-router';

class AuthorLink extends Component {
    render() {

        const {data, disable = false, children, className = '', testString, testKey = ''} = this.props;

        if(disable || !data) {
          return (
            <span className={'link-user ' + className} data-test={testString + '.userLinkInactive' + testKey} itemProp="name">
                {children}
            </span>
          )
        }

        return (
                <Link to={`/user/${data.slug}`} className={'link-user ' + className} data-test={testString + '.userLink' + testKey}>
                    <span itemProp="name">{children}</span>
                </Link>
        )
    }

}

export default AuthorLink
