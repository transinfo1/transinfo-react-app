import React from 'react';
import translate from '../../utils/translate';

const GoToPost = ({ className = '', testString, path, testKey = '' }) => {
  return(
    <a data-test={testString + '.goToPost' + testKey} className={className} href={'/' + path}>
      <img className="mr-2" src="/static/img/icons/quot_arrow.svg" alt="arrow icon"/>
        {translate('functionalities.toArticle')}
    </a>
  );
};

export default GoToPost;
