import React from 'react';

const SocialLinks = ({classes = '', color}) => (
    <span className={'social-links d-flex flex-row ' + classes}>
        <a href="https://www.facebook.com/trans.info/" target="_blank" data-test="socialLinks.facebook">
            <img className="social-icon" src={'/static/img/icons/facebook_' + color + '.svg'} alt="Facebook" />
        </a>

        <a href="https://twitter.com/TransINFO_news" target="_blank" data-test="socialLinks.twitter">
            <img className="social-icon" src={'/static/img/icons/twitter_' + color + '.svg'} alt="Twitter" />
        </a>

        <a href="https://www.youtube.com/channel/UC2w2lDT1nMDvv8P_ZP1SPJQ" target="_blank" data-test="socialLinks.youtube">
            <img className="social-icon" src={'/static/img/icons/youtube_' + color + '.svg'} alt="Youtube" />
        </a>
    </span>
)

export default SocialLinks
