import React from 'react';
import {ShareButtons} from 'react-share';
import store from '../../store';

const {
    FacebookShareButton,
    LinkedinShareButton,
    TwitterShareButton,
} = ShareButtons;

const Share = ({colorful, vertical, data, testString, testKey = ''}) => {
    if (data === '0') {
        return null;
    }

    let iconColor = colorful ? 'color' : 'white';
    let classes = vertical ? 'flex-column justify-content-center m-auto' : 'flex-row';
    let shareUrl = document.location.href;
    let title = data.title

    return (
        <div className={'share d-flex ' + classes}>
            {/*<div className="hidden-md-down my-1">Udostępnij: </div>*/}
            <FacebookShareButton

                url={shareUrl}
                quote={title}
                className="item">
                <img src={`/static/img/icons/facebook_${iconColor}.svg`} alt="Facebook Share" className="share-btn social-icon" id={data.id} data-test={testString + '.facebookShare' + testKey}/>
            </FacebookShareButton>

            <TwitterShareButton
                url={shareUrl}
                quote={title}
                className="item">
                <img src={`/static/img/icons/twitter_${iconColor}.svg`} alt="Twitter Share" className="share-btn social-icon" data-test={testString + '.twitterShare' + testKey}/>
            </TwitterShareButton>

            <LinkedinShareButton
                url={shareUrl}
                quote={title}
                windowWidth={750}
                windowHeight={600}
                className="item">
                <img src={`/static/img/icons/linkedin_${iconColor}.svg`} alt="Linkedin Share" className="share-btn social-icon" data-test={testString + '.LinkedinShare' + testKey}/>
            </LinkedinShareButton>

            {(store.getState().language.lang === 'pl') ?
                <a href={'http://www.wykop.pl/dodaj/link/?url=' + shareUrl + '&title=' + title} className="item"
                   target="_blank">
                    <img src={`/static/img/icons/wykop_${iconColor}.svg`} alt="Wykop Share" className="share-btn social-icon" data-test={testString + '.wykopShare' + testKey}/>
                </a>
                : null }

        </div>
    );
};

export default Share;
