import React, {Component} from 'react';
import {connect} from 'react-redux';
import {requestAsAuthenticatedUser, getUser, isAuthenticated} from '../../utils/authentication';
import {setLike} from '../../actions/likes'


class Likes extends Component {

    constructor(props) {
        super(props);
        let {data} = props;

        let likeFromStore = this.getLikeFromStore();

        if(likeFromStore) {
            data = likeFromStore;
        }

        this.state = {
            likes: data.likes,
            unlikes: data.unlikes,
            currentUserLikeStatus: data.currentUserLikeStatus
        };
    }

    getLikeFromStore() {
        const {type, id} = this.props;
        let like = this.props.likesStore.data.filter(item => item.id === id && item.type === type)
        return like.length ? like[0] : null;
    }

    setLike(likeState) {
        // this.checkLikesStore();
        let {id, type} = this.props;
        if (!isAuthenticated()) {
            return;
        }
        let {likes, unlikes, currentUserLikeStatus} = this.state;
        let newLikes = likes;
        let newUnlikes = unlikes;
        let newCurrentUserLikeStatus = likeState;

        if (currentUserLikeStatus === null) {
            likeState ? newLikes++ : newUnlikes++
        } else {
            if (likeState) {
                if (currentUserLikeStatus === true) {
                    newLikes--;
                    newCurrentUserLikeStatus = null;
                } else {
                    newLikes++;
                    newUnlikes--;
                }
            } else {
                if (currentUserLikeStatus === false) {
                    newUnlikes--;
                    newCurrentUserLikeStatus = null;
                } else {
                    newUnlikes++;
                    newLikes--;
                }
            }
        }
        // creating 'like'
        const createStoreLike = () => (
            this.setState({
                likes: newLikes,
                unlikes: newUnlikes,
                currentUserLikeStatus: newCurrentUserLikeStatus
            })
        );
        requestAsAuthenticatedUser(
            '/likes', {
                entityId: id,
                type: type,
                unlike: !likeState,
                userId: getUser().id
            }, 'post', createStoreLike
        );


        this.props.setLike(id, type, newCurrentUserLikeStatus, newLikes, newUnlikes);

    }

    render() {
        let {colorful, testString, testKey = ''} = this.props;
        let {likes, unlikes, currentUserLikeStatus} = this.state;

        let itemClass = colorful ? ' bordered-item' : '';
        let iconColor = colorful ? 'color' : 'white';

        let likeFilled = currentUserLikeStatus === true ? '_filled' : '';
        let unlikeFilled = currentUserLikeStatus === false ? '_filled' : '';
        return (
            <div className="likes d-flex flex-row">
                <div className={'item' + itemClass}>
                    <img className="social-icon mr-2" src={`/static/img/icons/thumbup_${iconColor + likeFilled}.svg`}
                         alt="Like"
                         onClick={() => this.setLike(true)} data-popup="login"
                         data-test={testString + '.like' + testKey} id={this.props.id}/>
                    <span data-popup="login" data-test={testString + '.likesAmount' + testKey}>
                         {likes}
                       </span>
                </div>
                <div className={'item' + itemClass}>
                    <img className="social-icon mr-2"
                         src={`/static/img/icons/thumbdown_${iconColor + unlikeFilled}.svg`} alt="Unlike"
                         onClick={() => this.setLike(false)} data-popup="login"
                         data-test={testString + '.unlike' + testKey}/>
                    <span data-popup="login" data-test={testString + '.unlikesAmount' + testKey}>
                         {unlikes}
                       </span>
                </div>
            </div>
        )
    }
}


const mapStateToProps = (store) => {
    return {
        likesStore: store.likes,
    };

};

export default connect(mapStateToProps, {setLike})(Likes);
