import React from 'react';

import Likes from './Likes';
import Share from './Share';

import Amount from '../comments/Amount';
import translate from '../../utils/translate';

const SocialBar = ({colorful, vertical, wordShare, data, testString, testKey = '', horizontalLeft, horizontalRight, sticky}) => {

    let classes  = vertical ? 'flex-column align-items-center' : 'flex-row align-items-center';
    let textClass = (horizontalRight) ? ' ml-auto' : (horizontalLeft) ? ' ml-2' : '';
    return(
        <div className={'social-bar d-flex justify-content-between justify-content-lg-start ' + classes }>
            <Likes colorful={colorful} type="post" data={data.likes} id={data.id} testString={testString} testKey={testKey} />
            {vertical ? <div className="line mt-4 mb-2"></div> : ''}
            {sticky ? <Amount white value={data.commentsCount} className="p-1" /> : null}
            {wordShare ? <div className={'hidden-md-down text' + textClass}>{translate('functionalities.share')}:</div> : ''}
            <Share colorful={colorful} data={data} testString={testString} testKey={testKey} vertical={vertical} />
        </div>
    )
};

export default SocialBar;
