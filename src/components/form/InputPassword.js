import React, { Component } from 'react';

class InputPassword extends Component {
  constructor(props){
      super(props);

      this.state = {
          showPassword: false,
          passwordType: 'password'
      };

      this.onPasswordShow = this.onPasswordShow.bind(this);
  }
  onPasswordShow(){
    if(this.state.showPassword) {
      this.setState({ showPassword: false, passwordType: 'password' })
    } else {
      this.setState({ showPassword: true, passwordType: 'text' });
    }
  }
  render(){
    const {groupClasses = '', labelClasses = '', inputClasses = '',
          labelTxt, testString = '', onInputChange = '', htmlFor = '',
           pattern = '.*', placeholder = '', required = false, validationMessage = '', autoComplete='off'} = this.props;
    return(
      <div className={"form-group form-password " + groupClasses}>
          <label htmlFor="password" className={'col-form-label ' + labelClasses}>{labelTxt}</label>
          <input className={'form-control ' + inputClasses}
                 onChange={onInputChange}
                 name={htmlFor}
                 data-test={testString + '.input.password'}
                 type={this.state.passwordType}
                 placeholder={placeholder}
                 pattern={pattern}
                 title={validationMessage}
                 required={required}
                 autoComplete={autoComplete}
          />
          <img onClick={this.onPasswordShow}
               data-test={testString + '.img.showPassword'}
               src="/static/img/icons/password.svg"
               alt="Show password"
               className="img-password"
          />
      </div>
    )
  }
}

export default InputPassword;
