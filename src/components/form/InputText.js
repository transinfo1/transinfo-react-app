import React from 'react';

const InputText = ({ groupClasses = '', labelClasses = '',
                    wrapperClasses = '', inputClasses = '', htmlFor = '', labelTxt,
                    testString = '', onInputChange, onBlur = () => {}, value = '', placeholder = '',
                    testEnzyme = '', required = false, disabled = false, pattern = '.*', validationMessage = '',
                    formGroupClass = true, removeLabel = false, maxLength = 524288 }) => {

  return(
    <div className={(formGroupClass ? 'form-group ' : '') + groupClasses}>
        { !removeLabel ? <label className={'col-form-label ' + labelClasses} htmlFor={htmlFor}>{labelTxt}</label> : null }
        <div className={wrapperClasses}>
        <input className={'form-control ' + inputClasses}
               name={htmlFor}
               onChange={onInputChange}
               onBlur={onBlur}
               data-test={testString + '.input.' + htmlFor}
               data-enzyme={testEnzyme}
               type="text"
               value={value}
               placeholder={placeholder}
               required={required}
               pattern={pattern}
               title={validationMessage}
               maxLength={maxLength}
               disabled={disabled} />
        </div>
    </div>
  );
}

export default InputText;
