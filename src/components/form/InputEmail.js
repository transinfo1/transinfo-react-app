import React from 'react';

const InputEmail = ({ groupClasses = '', labelClasses = '',
                    inputClasses = '', htmlFor = '', labelTxt,
                    testString = '', onInputChange, value = '', placeholder = '',
                    testEnzyme = '', required = false, disabled = false, autocomplete = 'off' }) => {
  return(
    <div className={'form-group ' + groupClasses}>
        <label className={'col-form-label ' + labelClasses} htmlFor={htmlFor}>{labelTxt}</label>
        <input className={'form-control ' + inputClasses}
               name={htmlFor}
               onChange={onInputChange}
               data-test={testString + '.input.' + htmlFor}
               data-enzyme={testEnzyme}
               type="email"
               value={value}
               placeholder={placeholder}
               required={required}
               disabled={disabled}
               autoComplete={autocomplete}/>
    </div>
  );
}

export default InputEmail;
