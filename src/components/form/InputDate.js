import React from 'react';
import InputDatePicker from './InputDatePicker';

const InputDate = ({ groupClasses = '', labelClasses = '', inputClasses = '',
                     wrapperClasses = '', htmlFor = '', labelTxt = '', testString = '', onInputChange,
                     value = '', required = false, maxDate = '', minDate = '' }) => {


  return(
    <div className={'form-group ' + groupClasses}>
        <label htmlFor={htmlFor} className={"col-form-label " + labelClasses}>{labelTxt}</label>
        <div className={wrapperClasses}>

            <InputDatePicker
                class={"form-control " + inputClasses}
                name={htmlFor}
                type="date"
                data-test={testString + '.input.' + htmlFor}
                onChange={onInputChange}
                value={value}
                required={required}
                min={minDate}
                max={maxDate}
            />

        </div>
    </div>
  );
}


export default InputDate;