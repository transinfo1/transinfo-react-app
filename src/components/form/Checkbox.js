import React from 'react';

const Checkbox = ({ labelClasses = '', inputClasses = '', checkboxTxt = '',
                    htmlFor = '', onInputChange, testString = '', value = '', required = false}) => {
  return(
    <label className={"form-check-label " + labelClasses} htmlFor={htmlFor}>
        <input className={"form-check-input " + inputClasses}
               data-test={testString + '.checkbox.' + htmlFor}
               onChange={onInputChange}
               type="checkbox"
               name={htmlFor}
               checked={value}
               id={htmlFor}
               required={required}
                />
        <span dangerouslySetInnerHTML={ {__html: checkboxTxt } } />
    </label>
  )
};

export default Checkbox;
