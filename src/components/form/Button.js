import React from 'react';

const Button = ({ className = "", testString = "", buttonTxt = "", testEnzyme = "" }) => {
  return(
    <button data-enzyme={testEnzyme}
            data-test={testString + ".button.send"}
            className={'btn ' + className}>{buttonTxt}</button>
  );
}

export default Button;
