import React from 'react';
import DatePicker from 'react-datepicker';
import moment from 'moment';
import {getLang} from '../../utils/storeData';
import 'react-datepicker/dist/react-datepicker-cssmodules.css';


class InputDatePicker extends React.Component {
    constructor (props) {
        super(props)
        this.state = {
            startDate: moment(),
            nowDate: this.props.value
        };
        this.handleChange = this.handleChange.bind(this);
    }

    handleChange(date) {
        this.setState({
            startDate: date,
            nowDate: date
        });
    }

    render() {
        let $ = window.jQuery;
        $(function () {
            const datePickerInput = $('input[name=birthDate]');
            datePickerInput.parent().css('display', 'block').parent().css('display', 'block');
            datePickerInput.attr('data-test', 'editProfile.userDetails.input.birthDate');
            //data-test="editProfile.userDetails.input.birthDate"
        });
        return <DatePicker
                    dateFormat="YYYY-MM-DD"
                    name={this.props.name}
                    selected={this.state.startDate}
                    onChange={this.handleChange}
                    locale={getLang()}
                    className={this.props.class}
                    value={this.state.nowDate}
                    maxDate={moment()}
                />
    }
}

export default InputDatePicker;
