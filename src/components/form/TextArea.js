import React from 'react';

const TextArea = ({ groupClasses='', textareaClasses='', testString,
                    rows='', htmlFor='', placeholder='', onInputChange, value='',
                    required = false, maxLength = 524288}) => {
  return(
    <div className={"form-group " + groupClasses}>
        <textarea className={"form-control " + textareaClasses}
                  name={htmlFor}
                  rows={rows}
                  placeholder={placeholder}
                  onChange={onInputChange}
                  data-test={testString + '.textarea.' + htmlFor}
                  value={value}
                  maxLength={maxLength}
                  required={required}></textarea>
    </div>
  );
}

export default TextArea;


