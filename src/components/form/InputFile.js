import React from 'react';

const InputFile = ({ groupClasses = '', labelClasses = '',
                    inputClasses = '', htmlFor = '', labelTxt,
                    testString = '', onInputChange, value = '',
                    testEnzyme = '', required = false }) => {
  return(
    <div className={'form-group ' + groupClasses}>
        {labelTxt ? <label className={'col-form-label ' + labelClasses} htmlFor={htmlFor}>{labelTxt}</label> : null }
        <input className={'form-control ' + inputClasses}
               name={htmlFor}
               onChange={onInputChange}
               data-test={testString + '.input.' + htmlFor}
               data-enzyme={testEnzyme}
               type="file"
               accept="image/*"
               required={required} />
    </div>
  );
}

export default InputFile;
