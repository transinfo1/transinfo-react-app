import React, {Component} from 'react';
import axios from 'axios';
import getConfig from '../../utils/getConfig';
import {getLocale} from '../../utils/storeData';
import Loader from '../../components/asyncData/Loader';
import translate from '../../utils/translate';

class RecentPosts extends Component {

    constructor(props) {
        super(props);
        this.state = {
            posts: [],
            fetched: false
        }
        this.limit = props.limit || 2;
        this.slug = props.author.slug;

    }

    componentWillMount() {
        if(!this.slug) {
            console.log('RecentPosts: no author slug given');
            return false;
        }

        axios.get(getConfig().restApiHostName + '/posts', {
            params: {
                limit: this.limit,
                authorSlug: this.slug,
                langName: getLocale()
            }
        }).then(response => {
            this.setState({
                posts: response.data,
                fetched: true
            })
        }).catch(() => {
            this.setState({
                fetched: true
            })
        })

    }

    render() {
        if(!this.state.fetched) {
            return <Loader />
        }

        let className = this.props.className || '';

        return (
            <div id={'recentPosts-' + this.props.author.id}>
                {(this.props.title && this.state.posts.length > 0) &&
                    <h2 className="heading--semibold">
                        {translate('posts.lastArticles')}
                    </h2>
                }

                <ul className={'recent-posts ' + className}>
                    {this.state.posts.map((item, key) => {
                        return (
                            <li key={key}>
                                <a href={'/' + item.slug}>
                                    {item.type === 'videos' && <img src="/static/img/icons/play_small.svg" alt="play video" className="mr-1" />}
                                    {item.title}
                                </a>
                            </li>
                        );
                    })}
                </ul>
            </div>
        )
    }

}

export default RecentPosts;