export default (state = {
  visibility: false,
  name: '',
  params: {}
}, action) => {

    switch (action.type) {
        case "SHOW_POPUP":
            return {
              visibility: true,
              name: action.payload.name,
              params: action.payload.params
            }

        case "HIDE_POPUP":
            return {
              visibility: false,
              name: '',
              params: {},
            }

        default:
            return state;
    }
}