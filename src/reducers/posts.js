import {filterPosts} from '../utils/posts';
import {redirectToErrorPage} from '../utils/helpers';

export const posts = (state = {
    response: false,
    fetching: false,
    fetched: false,
    data: [],
    status: null,
    error: null
}, action) => {

    switch (action.type) {
        case "FETCH_POSTS_PENDING":
            return {
                ...state,
                fetching: true
            }

        case "FETCH_POSTS_REJECTED":
            let status = action.payload.response ? action.payload.response.status : 500;
            redirectToErrorPage(500, status);
            return {
                ...state,
                response: true,
                fetching: false,
                status: status,
                error: action.payload || action.payload.message
            }

        case "FETCH_POSTS_FULFILLED":
            const {payload} = action;
            let filteredData = payload.config.wall ? filterPosts(payload.data) : payload.data;
            return {
                ...state,
                response: true,
                fetching: false,
                fetched: true,
                data: [
                    ...state.data,
                    ...filteredData
                ]
            }

        default:
            return state;
    }
};

export const postsQuote = (state = {
    response: false,
    fetching: false,
    fetched: false,
    data: [],
    status: null,
    error: null
}, action) => {

    switch (action.type) {
        case "FETCH_POSTS_QUOTE_PENDING":
            return {
                ...state,
                fetching: true
            }

        case "FETCH_POSTS_QUOTE_REJECTED":
            let status = action.payload.response ? action.payload.response.status : 500;
            redirectToErrorPage(500, status);
            return {
                ...state,
                response: true,
                fetching: false,
                status: status,
                error: action.payload || action.payload.message
            }

        case "FETCH_POSTS_QUOTE_FULFILLED":
            return {
                ...state,
                response: true,
                fetching: false,
                fetched: true,
                data: [
                    ...state.data,
                    ...action.payload.data
                ]
            }

        default:
            return state;
    }
};
