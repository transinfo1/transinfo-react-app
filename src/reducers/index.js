import { combineReducers } from 'redux';
import { posts, postsQuote } from './posts';
import auth from './auth';
import language from './language';
import likes from './likes';
import {comments, featuredComments} from './comments';
import profile from './profile';
import notification from './notification';
import videoCategories from './categories';
import {videos, fetchCatVid, saveCatVid, bannerVideos } from './videos';
import relatedPosts from './relatedPosts';
import slugPosts from './slugPosts';
import popups from './popups';
import topAuthors from './topAuthors';
import search from './search';

import { routerReducer } from 'react-router-redux';

const reducers = combineReducers({
    posts,
    postsQuote,
    auth,
    language,
    likes,
     comments,
    featuredComments,
    profile,
    topAuthors,
    notification,
    videoCategories,
    videos,
    fetchCatVid,
    saveCatVid,
    bannerVideos,
    relatedPosts,
    slugPosts,
    search,
    popups,
    routing: routerReducer
});

export default reducers;
