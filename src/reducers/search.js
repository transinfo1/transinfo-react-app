import {redirectToErrorPage} from '../utils/helpers';

export default(state = {
    response: false,
    fetching: false,
    fetched: false,
    data: {
        post: {
            searchResults: [],
            total: 0
        },
        comment: {
            searchResults: [],
            total: 0
        },
        forum: {
            searchResults: [],
            total: 0
        }
    },
    status: null,
    error: null,
    suggestions: []
}, action) => {

    switch (action.type) {

        case 'SET_KEYWORDS':
            return {
                ...state,
                data: {
                    post: {
                        searchResults: [],
                        total: 0
                    },
                    comment: {
                        searchResults: [],
                        total: 0
                    },
                    forum: {
                        searchResults: [],
                        total: 0
                    }
                },
                keywords: action.keywords,
                redirectFromHeader: action.redirectFromHeader
            }

        case 'FETCH_SUGGESTIONS_FULFILLED':
            return {
                ...state,
                suggestions: [...action.payload.data]
            }

        case "FETCH_SEARCH_RESULT_PENDING":
            return {
                ...state,
                fetching: true,
            }

        case "FETCH_SEARCH_RESULT_REJECTED":
            let status = action.payload.response ? action.payload.response.status : 500;
            redirectToErrorPage(500, status);
            return {
                ...state,
                response: true,
                fetching: false,
                status: status,
                error: action.payload || action.payload.message
            }

        case "FETCH_SEARCH_RESULT_FULFILLED":
            const {payload} = action;
            return {
                ...state,
                response: true,
                fetching: false,
                fetched: true,
                suggestions: [],
                data: {
                    post: {
                        searchResults: payload.data.searchResultsMap.post.searchResults,
                        total: payload.data.searchResultsMap.post.total
                    },
                    comment: {
                        searchResults: payload.data.searchResultsMap.comment.searchResults,
                        total: payload.data.searchResultsMap.comment.total
                    },
                    forum: {
                        searchResults: payload.data.searchResultsMap.forum.searchResults,
                        total: payload.data.searchResultsMap.forum.total
                    }
                },
                type: payload.type,
                sortBy: payload.sortBy,
                onlyTitle: payload.onlyTitle,
                page: payload.page
            }

        default:
            return state;
    }
}