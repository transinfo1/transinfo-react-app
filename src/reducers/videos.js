import {redirectToErrorPage} from '../utils/helpers';

export const videos = (state = {
    response: false,
    fetching: false,
    fetched: false,
    data: [],
    status: null,
    error: null
}, action) => {

    switch (action.type) {
        case "FETCH_VIDEOS_PENDING":
            return {
                ...state,
                fetching: true
            }

        case "FETCH_VIDEOS_REJECTED":
            let status = action.payload.response ? action.payload.response.status : 500;
            redirectToErrorPage(500, status);
            return {
                ...state,
                response: true,
                fetching: false,
                status: status,
                error: action.payload || action.payload.message
            }

        case "FETCH_VIDEOS_FULFILLED":
            return {
                ...state,
                response: true,
                fetching: false,
                fetched: true,
                data: [
                  ...state.data,
                  ...action.payload.data
                ]
            }
        default:
            return state;
    }
};


export const fetchCatVid = (state = {
    response: false,
    fetching: false,
    fetched: false,
    videos: [],
    status: null,
    error: null
}, action) => {

    switch (action.type) {
        case "FETCH_CATEGORISED_VIDEOS_PENDING":
            return {
                ...state,
                fetching: true
            }

        case "FETCH_CATEGORISED_VIDEOS_REJECTED":
            let status = action.payload.response ? action.payload.response.status : 500;
            redirectToErrorPage(500, status);
            return {
                ...state,
                response: true,
                fetching: false,
                status: status,
                error: action.payload || action.payload.message
            }

        case "FETCH_CATEGORISED_VIDEOS_FULFILLED":
            return {
                ...state,
                response: true,
                fetching: false,
                fetched: true,
                categoryVideos: action.payload
            }

        default:
            return state;
    }
};

export const saveCatVid = (state = {
    videos: {}
}, action) => {
    switch (action.type) {
        case "SAVE_CATEGORISED_VIDEOS":
            return {
                ...state,
                videos: {
                    ...state.videos,
                    ...action.payload
                }
            }
        case "UPDATE_CATEGORISED_VIDEOS":
            return {
                ...state,
                videos: {
                    ...state.videos,
                    [action.catName]: [
                        ...state.videos[action.catName],
                        ...action.payload
                    ]
                }
            }
        default:
            return state
    }
};

export const bannerVideos = (state = {
    response: false,
    fetching: false,
    fetched: false,
    data: [],
    status: null,
    error: null
}, action) => {
    switch(action.type){
        case "FETCH_BANNER_VIDEOS_PENDING":
            return {
                ...state,
                fetching: true
            }

        case "FETCH_BANNER_VIDEOS_REJECTED":
            let status = action.payload.response ? action.payload.response.status : 500;
            redirectToErrorPage(500, status);
            return {
                ...state,
                response: true,
                fetching: false,
                status: status,
                error: action.payload || action.payload.message
            }

        case "FETCH_BANNER_VIDEOS_FULFILLED":
            return {
                ...state,
                response: true,
                fetching: false,
                fetched: true,
                data: [
                    ...state.data,
                    ...action.payload.data
                ]
            }
        default:
            return state
    }
};
