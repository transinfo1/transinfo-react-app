export const comments = (state = {
    fetching: false,
    fetched: false,
    data: [],
    error: null
}, action) => {

    switch (action.type) {
        case "FETCH_COMMENTS_PENDING":
            return {
                ...state,
                fetching: true
            };

        case "FETCH_COMMENTS_REJECTED":
            return {
                ...state,
                fetching: false,
                error: 'Request failed'
            }

        case "FETCH_COMMENTS_FULFILLED":
            return {
                ...state,
                fetching: false,
                fetched: true,
                data: [
                    ...state.data,
                    ...action.payload.data
                ]
            }

        default:
            return state;
    }
}

export const featuredComments = (state = {
    fetching: false,
    fetched: false,
    data: [],
    error: null,
    displayed: false
}, action) => {

    switch (action.type) {
        case "FETCH_FEATURED_COMMENTS_PENDING":
            return {
                ...state,
                fetching: true
            };

        case "FETCH_FEATURED_COMMENTS_REJECTED":
            return {
                ...state,
                fetching: false,
                error: 'Request failed'
            }

        case "FETCH_FEATURED_COMMENTS_FULFILLED":
            return {
                ...state,
                fetching: false,
                fetched: true,
                data: [
                    ...state.data,
                    ...action.payload.data
                ]
            }

        default:
            return state;
    }
}
