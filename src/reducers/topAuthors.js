import {redirectToErrorPage} from '../utils/helpers';

export default(state = {
    response: false,
    fetching: false,
    fetched: false,
    data: [],
    status: null,
    error: null
}, action) => {
    switch(action.type){
        case "FETCH_TOP_AUTHORS_PENDING":
            return {
                ...state,
                fetching: true
            }

        case "FETCH_TOP_AUTHORS_REJECTED":
            let status = action.payload.response ? action.payload.response.status : 500;
            redirectToErrorPage(500, status);
            return {
                ...state,
                response: true,
                fetching: false,
                status: status,
                error: action.payload || action.payload.message
            }

        case "FETCH_TOP_AUTHORS_FULFILLED":
            return {
                ...state,
                response: true,
                fetching: false,
                fetched: true,
                data: action.payload.data
            }
        default:
            return state
    }
};