import store from '../store';
import {logout} from '../actions/auth';

export default (state = {
    response: false,
    fetching: false,
    isAuthenticated: false,
    widgetAutoLogin: false,
    refreshing: false,
    refreshed: false,
    data: {},
    status: null,
    error: null
}, action) => {

    const {payload} = action;

    switch (action.type) {
        case "LOGIN_PENDING":
            return {
                ...state,
                fetching: true
            }

        case "LOGIN_REJECTED":
            return {
                ...state,
                response: true,
                fetching: false,
                isAuthenticated: false,
                status: payload.response.status || '500',
                error: payload.response.data.error || action.payload.message,
                responseBody: payload.response.data
            }
        case "LOGIN_FULFILLED":
            return {
                ...state,
                response: true,
                fetching: false,
                isAuthenticated: true,
                data: payload.data,
                status: payload.status,
                error: null
            }
        case "LOGOUT":
            return {
                ...state,
                response: false,
                fetching: false,
                isAuthenticated: false,
                widgetAutoLogin: false,
                data: {},
                status: null,
                error: null
            }
        case "REFRESH_TOKEN_PENDING":
            return {
                ...state,
                refreshing: true,
                refreshed: false,
            }

        case "REFRESH_TOKEN_REJECTED":
            console.log('REFRESH TOKEN REJECTED logout');
            store.dispatch(logout());
            return {
                ...state,
                response: true,
                fetching: false,
                isAuthenticated: false,
                refreshing: false,
                refreshed: false,
                status: payload.response.status || '500',
                error: payload.response.data.error || action.payload.message
            }

        case "REFRESH_TOKEN_FULFILLED":
            return {
                ...state,
                response: true,
                fetching: false,
                isAuthenticated: true,
                refreshing: false,
                refreshed: true,
                data: {
                    ...state.data,
                    accessToken: payload.data.accessToken,
                },
                status: status,
                error: null
            }
        case "WIDGET_AUTOLOGIN":
            return {
                ...state,
                widgetAutoLogin: true
            }
        default:
            return state;
    }
}




















// import { LOGIN_REQUEST, LOGIN_SUCCESS, LOGIN_FAIL,
//     LOGOUT_SUCCESS, TOKEN_CHECK, TOKEN_VALID,
//     TOKEN_EXPIRED } from '../actions/auth';
//
// export default (state = {
//     isFetching: false,
//     isAuthenticated: localStorage.getItem('access_token') ? true : false,
//     isValidToken: localStorage.getItem('access_token') ? true : false
// }, action) => {
//
//     switch(action.type){
//         case LOGIN_REQUEST:
//             return {
//                 ...state,
//                 isFetching: true,
//                 isAuthenticated: false
//             }
//         case LOGIN_SUCCESS:
//             return {
//                 ...state,
//                 isFetching: false,
//                 isAuthenticated: true,
//                 errorMessage: '',
//                 isValidToken: true
//             }
//         case LOGIN_FAIL:
//             return {
//                 ...state,
//                 isFetching: false,
//                 isAuthenticated: false,
//                 errorMessage: action.payload,
//                 isValidToken: false
//             }
//         case LOGOUT_SUCCESS:
//             return {
//                 ...state,
//                 isAuthenticated: false,
//                 isValidToken: false
//             }
//         case TOKEN_CHECK:
//             return {
//                 ...state,
//                 isFetching: true,
//                 isValidToken: true
//             }
//         case TOKEN_VALID:
//             return {
//                 ...state,
//                 isFetching: false,
//                 isAuthenticated: true,
//                 isValidToken: true
//             }
//         case TOKEN_EXPIRED:
//             return {
//                 ...state,
//                 isAuthenticated: false,
//                 isValidToken: false
//             }
//         default:
//             return state
//     }
//
// }
