export default (state = {
    fetched: false,
    updatedWithCurrentUser: false,
    data: []
}, action) => {
    switch (action.type) {
        case "SET_LIKE":
            let obj = action.payload;

            let selectedKey = null;
            state.data.forEach((item, key) => {
                if (item.id === obj.id && item.type === obj.type) {
                    selectedKey = key;
                }
            })

            if(null === selectedKey) {
                return {
                    ...state,
                    fetched: true,
                    data: [
                        ...state.data,
                        obj
                    ]
                }
            }

            return {
                ...state,
                fetched: true,
                updatedWithCurrentUser: true,
                data: [
                    ...state.data.slice(0, selectedKey),
                    {
                        ...state.data[selectedKey],
                        likes: obj.likes,
                        unlikes: obj.unlikes,
                        currentUserLikeStatus: obj.currentUserLikeStatus
                    },
                    ...state.data.slice(selectedKey + 1)
                ]
            }
        default:
            return state;
    }
}
