export default(state={
    visibility: false,
    type: null,
    alert: null,
    id: null
}, action) => {
  switch (action.type){
    case "SHOW_NOTIFICATION":
      console.log(action)
      return {
        visibility: true,
        type: action.payload.type,
        alert: action.payload.message,
        id: action.payload.id
      }
    case "HIDE_NOTIFICATION":
      return {
        visibility: false,
        type: null,
        alert: null,
        id: null
      }
    default:
      return state;
  }
}
