import {redirectToErrorPage} from '../utils/helpers';

export default (state = {
  response: false,
  fetching: false,
  fetched: false,
  data: {
      birthDate: null,
      companyName: null,
      email: null,
      id: null,
      positionName: null,
      rpcUserDTO: null,
      transId: '0-0',
      wcw1: false,
      wcw2: false,
      wcw3: false,
      wcw4: false,
      wcw5: false,
      wcw6: false,
      wpw1: false,
      wpw2: false,
      wpw3: false,
      wpw4: false,
      wpw5: false,
      wpw6: false,
  },
  status: null,
  error: null
}, action) => {
  switch(action.type){
    case "FETCH_PROFILE_PENDING":
      return {
        ...state,
        fetching: true
      }
    case "FETCH_PROFILE_REJECTED":
        let status = action.payload.response ? action.payload.response.status : 500;
        redirectToErrorPage(500, status);
      return {
        ...state,
        response: true,
        fetching: false,
        status: status,
        error: action.payload || action.payload.message
      }
      case "FETCH_PROFILE_FULFILLED":
        return {
          ...state,
          response: true,
          fetching: false,
          fetched: true,
          data: action.payload.data
        }
      default:
        return state;
  }
}
