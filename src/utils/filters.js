
// Filter videos by category
export const filterVideosCat = (videos, category) => {

  let list = [];

  videos.forEach((video) => {
    video.categories.filter((item) => {
      if(item.name === category.name){
        list.push(video);
      }
      return null;
    })
  });

  if(!list.length){
    return false;
  }

  return list;
}

// Filter posts by tag
export const filterPostsByTag = (posts, slug) => {

  let list = [];

  posts.forEach((post) => {
    post.tags.filter((item) => {
      if(item.slug === slug){
        list.push(post);
      }
      return null;
    })
  });

  if(!list.length){
    return false;
  }

  return list;
}

// Filter posts by author
export const filterPostsByAuthor = (posts, slug) => {

  let list = [];

  posts.forEach((post) => {
    if(post.author.slug === slug){
      list.push(post);
    }
  });

  if(!list.length){
    return false;
  }

  return list;
}

// Filter duplicat items
export const filterDuplicates = (data) => {
  let list = [];

  data.filter((item, index, arr) => {
    return (arr.map(obj => obj.id).indexOf(item.id) === index) ? list.push(item) : null;
  })

  return list;
}

//debounce function
export const debounce = (func, wait, immediate) => {
    var timeout;
    return function() {
        var context = this, args = arguments;
        var later = function() {
            timeout = null;
            if (!immediate) func.apply(context, args);
        };
        var callNow = immediate && !timeout;
        clearTimeout(timeout);
        timeout = setTimeout(later, wait);
        if (callNow) func.apply(context, args);
    };
};