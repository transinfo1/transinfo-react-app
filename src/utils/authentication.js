import getConfig from '../utils/getConfig';
import store from '../store';
import axios from 'axios';
import {setAuthenticatedUser, refreshToken, refreshWordpressToken, logout} from '../actions/auth';
import * as storage from '../utils/storage';

const getAuthStore = () => store.getState().auth;

export const isAuthenticated = () => {
    let authStore = getAuthStore()
    return authStore.data.accessToken && authStore.isAuthenticated
}

export const getAuthObj = () => {
    if (!isAuthenticated()) {
        return null;
    }
    return getAuthStore().data;
}

export const getAccessCredentials = () => {
    if (!isAuthenticated()) {
        return null
    }
    let credentials = getAuthStore().data;
    return {
        accessToken: credentials.accessToken,
        provider: credentials.provider,
    }
}

export const getUser = (returnIdNull = false) => {
    if (!isAuthenticated()) {
        return returnIdNull ? {id: null} : null;
    }
    return getAuthStore().data.user;
}

export const getAuthHeaders = (accessToken = null, provider = null, extraParams = null) => ({
    'Authorization': accessToken || getAccessCredentials().accessToken,
    'X-Provider': provider || getAccessCredentials().provider,
    'Content-type': extraParams === 'fileUpload' ? 'multipart/form-data' : 'application/json'
})

export const updateUserInfo = (data) => {

    const cred = getAccessCredentials();
    const user = getUser();

    const newUserObj = {
        ...cred,
        user: {
            ...user,
            ...data
        }
    };

    let autologin = Boolean(storage.get('auth').expires);

    store.dispatch(setAuthenticatedUser(newUserObj, !autologin));
};

export const requestAsAuthenticatedUser = (request, params, requestType = 'get', callbackSuccess = () => {}, callbackFailure = () => {}, accessToken = null, provider = null, extraParams) => {
    return axios({
        method: requestType,
        url: getConfig().restApiHostName + request,
        headers: getAuthHeaders(accessToken, provider, extraParams),
        data: params

    }).then(response => {
        if(response) {
            callbackSuccess(response);

        }
    }).catch(error => {
        if(error.response.status !== 403) {
            callbackFailure(error);
            if(error.response.status === 401) {
                store.dispatch(logout());
            }
        } else {
            if(!store.getState().auth.refreshing) {
                switch (getAccessCredentials().provider) {
                    case 'wordpress':
                        store.dispatch(refreshWordpressToken()).then(() => {
                            requestAsAuthenticatedUser(request, params, requestType, callbackSuccess, callbackFailure)
                        });
                        break;
                    case 'google':
                    case 'onelogin':
                        store.dispatch(refreshToken(getAccessCredentials().provider, getAuthStore().widgetAutoLogin)).then(() => {
                            requestAsAuthenticatedUser(request, params, requestType, callbackSuccess, callbackFailure)
                        });
                        break;
                    case 'facebook':
                        store.dispatch(logout());
                        break;
                    default:
                        break;
                }
            }
        }
    })
}

export const fetchUserData = (accessToken = null, provider = null, autoLogin = true) => {
    return requestAsAuthenticatedUser('/users/profile/me', null, 'get', wpResp => {
        store.dispatch(
            setAuthenticatedUser({
                accessToken: accessToken || getAccessCredentials().accessToken,
                provider: provider || getAccessCredentials().provider,
                user: wpResp.data.rpcUserDTO
            }, autoLogin)
        )
    }, undefined, accessToken, provider);
};