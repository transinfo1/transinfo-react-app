import store from '../store';

export default (path) => {
  let content = store.getState().language.content
  if(content) {
    let result = content;
    path.split('.').forEach(item => {
      result = result[item] || null;
    })
    return result ? result : path;
  }
  return path
}
