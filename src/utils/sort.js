export const sortByCreatedDateDesc = (a,b) => {
    if (a.dateGmt < b.dateGmt )
        return 1;
    if (a.dateGmt  > b.dateGmt )
        return -1;
    return 0;
}

export const sortByCreatedDateAsc = (a,b) => {
    if (a.dateGmt > b.dateGmt )
        return 1;
    if (a.dateGmt  < b.dateGmt )
        return -1;
    return 0;
}
