
export const onInputChange = (event, state) => {

  const field = event.target.name;
  const data = state;

  if(event.target.type === 'checkbox'){
    data[field] = event.target.checked;
  } else {
    data[field] = event.target.value;
  }

  return data;

}
