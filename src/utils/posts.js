import {sortByCreatedDateDesc} from './sort';

export const getFeaturedImage = (img = null) => {
    let file = !img ? '/static/img/default_image.jpg' : img;

    return {
        "backgroundImage": "url('" + file + "')"
    }
}

export const getLastCreatedDate = (data) => {
  switch (data.length) {
    case 0:
      return null
    case 1:
      return data[0].dateGmt
    default:
      return data.slice(-1)[0].dateGmt;
  }
}

export const filterPosts = (data) => {

    if(data.length === 0) {
        return [];
    }

    let sticky = data.filter(item => item.sticky);
    let stickyArticles = sticky.length >= 2 ? sticky.sort(sortByCreatedDateDesc).slice(0, 2) : data.sort(sortByCreatedDateDesc).slice(0, 2);
    let stickyVideo = data.filter(item => item.type === 'videos').sort(sortByCreatedDateDesc).slice(0, 1);

    if(stickyVideo.length === 0 && data.length >=3) {
        stickyVideo[0] = sticky.length > 3 ? sticky.sort(sortByCreatedDateDesc)[3] : data.sort(sortByCreatedDateDesc)[2];
    } else if(stickyVideo.length === 0) {
        stickyVideo[0] = sticky.length > 3 ? sticky.sort(sortByCreatedDateDesc).slice[3] : data.sort(sortByCreatedDateDesc)[0];
    }

    let stickyIds = [
      stickyArticles[0].id,
      stickyArticles[1].id,
      stickyVideo[0].id
    ]

    let wall = data.filter(item => stickyIds.indexOf(item.id) < 0).sort(sortByCreatedDateDesc)

    return stickyArticles.concat(stickyVideo, wall)
}
