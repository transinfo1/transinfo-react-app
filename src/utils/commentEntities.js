// Replace entities in comments

const replaceEntities = (content) => {
    return content.replace(/&amp;/g, "&")
                    .replace(/&gt;/g, ">")
                    .replace(/&lt;/g, "<")
                    .replace(/&#8216;/g, '"')
                    .replace(/&quot;/g, "'")
                    .replace(/&#8211;/g, "-")
                    .replace(/&#8220;/g, '"')
                    .replace(/&#8217;/g, '"')
                    .replace(/&#8222;/g, '"')
                    .replace(/&#8230;/g, '...')
                    .replace(/&#8242;/g, "'")
                    .replace(/&#8221;/g, '"');
};

export default replaceEntities