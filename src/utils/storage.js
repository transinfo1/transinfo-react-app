export const get = (key, storage = localStorage) => {
  try {
    let data = storage.getItem(key);
    if(data === null) {
      return undefined;
    }
    return JSON.parse(data);
  } catch(err) {
    return undefined;
  }
}

export const set = (key, value, storage = localStorage) => {
  try {
    let data = JSON.stringify(value);
    storage.setItem(key, data);
  } catch (err) {
    
  }
}

export const clear = (key, storage = localStorage) => {
  try {
    storage.removeItem(key)
  } catch (err) {

  }
}
