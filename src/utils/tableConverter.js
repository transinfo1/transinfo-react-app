let newTable = [];

const returnConvertedTable =  (array, currentIndex) => {

    array.map((item, index) => {

        let content;
        const arrayLength = array.length - 1;

        if (index === 0) {
            content = '<div class="table-row"><div class="table-head">' + item.cells[currentIndex].textContent + '</div>';
        } else if (index === arrayLength) {
            content = '<div class="table-cell">' + item.cells[currentIndex].textContent + '</div></div>';
        } else {
            content = '<div class="table-cell">' + item.cells[currentIndex].textContent + '</div>';
        }

        return newTable.push(content);

    })

};

export const tableConverter = () => {

    let tableBody = document.querySelector("table tbody");
    let tableHeadings = document.querySelector("table tbody tr:first-child");
    let $ = window.jQuery;

    if (!tableBody) { return; }

    let tableBodyArr = Array.prototype.slice.call(tableBody.rows);
    let currentIndex = 0;
    let totalItems = tableHeadings.cells.length;

    while (currentIndex < totalItems) {
        returnConvertedTable(tableBodyArr, currentIndex);
        currentIndex++;
    }

    $('table').after('<div class="table-converted">' + newTable.join(' ').toString() + '</div>');
};
