import store from '../store'
import {showNotification, hideNotification} from '../actions/notification';

export const sendNotification = (message, type, timeOut, id = null) => {
    store.dispatch(showNotification(message, type, id));
    setTimeout(() => {
      store.dispatch(hideNotification());
    }, timeOut);
}
