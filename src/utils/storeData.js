import getConfig from './getConfig';
import axios from 'axios';
// import {isAuthenticated, getUser, requestAsAuthenticatedUser} from './authentication';

import store from '../store'
// import {setComments} from '../actions/comments';

export const getLocale = () => (store.getState().language.locale)

export const getLang = () => (store.getState().language.lang)

export const getAllIds = (data) => {

  if(!data) {
    return false;
  }

  let result = [];
  data.forEach(item => {
    result.push(item.id)
  })
  return result
}

// const getNestedData = (data, objName) => {
//     let result = [];
//     data.forEach(item => {
//       if(item[objName] instanceof Array) {
//         item[objName].forEach(item => {
//             result.push(item)
//         })
//       } else {
//         result.push({
//           id: item.id,
//           type: item.type === 'videos' ? 'post' : item.type,
//           counts: item[objName],
//           currentUserLike: null
//         })
//       }
//     })
//
//     return result
// }

export const duplicatesFilter = (reducer, newData) => {
  let storeIds = getAllIds(store.getState()[reducer].data);
  return newData.filter(item => storeIds.indexOf(item.id) < 0)
}

export const fetchData = (reducer, url, params = null, headers = null, requestType = 'get') => (
  axios[requestType](getConfig().restApiHostName + url, params, headers).then((response) => {
      return {
      ...response,
      data: duplicatesFilter(reducer, response.data)
    }
  })
)

// export const setLikesForAuthenticatedUser = (data, type) => {
//   if(isAuthenticated() && data.length && !store.getState().auth.refreshing) {
//       requestAsAuthenticatedUser('/likes/' + type + '/' + getAllIds(data).join(',') + '?userId=' + getUser().id, null, 'get', response => {
//           response.data.forEach(item => {
//               store.dispatch(updateLike(item))
//           })
//       })
//   }
// }
//
//  export const setLikesAfterLogin = () => {
//    const {auth, likes, posts, comments} = store.getState()
//
//    if(auth.isAuthenticated && !likes.updatedWithCurrentUser) {
//      setLikesForAuthenticatedUser(posts.data, 'post')
//      setLikesForAuthenticatedUser(comments.data, 'comment')
//    }
//  }

// export const setNestedDataToStore = (data, type, postsQuotes = false) => {
//     type = type === 'videos' ? 'post' : type;
//
//     // set likes for loaded data
//     store.dispatch(setLikes(duplicatesFilter('likes', getNestedData(data, 'likes'))))
//     setLikesForAuthenticatedUser(data, type)
//
//     if(type === 'post' && !postsQuotes) {
//         // set comments for loaded posts
//         store.dispatch(setComments(duplicatesFilter('comments', getNestedData(data, 'lastComments'))))
//
//         // set likes for loaded posts.lastComments
//         data.forEach(item => {
//             store.dispatch(setLikes(duplicatesFilter('likes', getNestedData(item.lastComments, 'likes')), 'post'))
//             setLikesForAuthenticatedUser(item.lastComments, 'comment')
//         })
//     }
// }


