import translate from './translate';

export const authorName = (authorObj, brAsSeparator = false, getFirstName = false) => {
    if(!authorObj) {
        return translate('user.deleted');
    }
    if(authorObj.userDeleted) {
        return translate('user.deleted');
    }

    const {firstName, lastName} = authorObj;

    if(!firstName && !lastName) {
        return translate('user.guestName');
    }

    if(getFirstName) {
        return firstName;
    }

    let separator = brAsSeparator ? '<br />' : ' ';
    return firstName + separator + lastName;
};

export const checkForTrueVal = (list) => {
    for(let item in list) {
        if(list[item]){
            return true;
        }
    }
};

export const truncate = (data, maxLength) => {
    let wordCount = data.split(' ');
    let result = data;
    if(wordCount.length > maxLength){
        wordCount = wordCount.slice(0, maxLength);
        result = wordCount.join(" ") + "...</p>";
    }
    return result;
};

export const getTimestampModifiedByMinutes = (minutes = 1) => {
    let date = new Date();
    return date.setTime(date.getTime() + (minutes * 60 * 1000))
}

export const plural = (value, lang, type) => {

    let words = translate('counter.' + type);

    if(value > 20 && lang === 'pl') {
        let lastDigit = Number(value.toString().split('').pop());
        value = lastDigit === 1 ? value : lastDigit;
    }

    switch (value) {
        case 1:
            return words[0];
        case 2:
        case 3:
        case 4:
            return words[1];
        case 5:
        case 6:
        case 7:
        case 8:
        case 9:
            return words[2];
        default:
            return words[3];
    }

};

export const whilePending = (value, element = null) => {
    if(element) {
        var elements = document.querySelectorAll(element);
        for (var i = 0, l = elements.length; i < l; i++) {
            elements[i].disabled = value;
        }
    }

    let $ = window.jQuery;
    if(value) {

        $('.global-loader').addClass('global-loader-show');
    } else {
        $('.global-loader').removeClass('global-loader-show');
    }

}

export const redirectToErrorPage = (code, status) => {
    let path = '/' + code;
    if(window.location.pathname !== path && status !== 404){
        window.location.href = path;
    }
}