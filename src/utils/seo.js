import React from 'react';
import {Helmet} from 'react-helmet';
import {getLocale} from './storeData';
import store from '../store';

import * as storage from './storage';
import {getLang} from './storeData';

import isEqual from 'lodash.isequal';


export default (obj) => {

    let currentStore = store.getState();
    let authStore = currentStore.auth;
    let profileStore = currentStore.profile;
    let isAuth = authStore.isAuthenticated;
    let isFetched = profileStore.fetched;

        let zmienne = {
            event: 'pageView',
            pagePath: location.pathname,
            title: obj.title,
            type: isFetched ? 'LoggedIn' : 'Guest',
            userId: isFetched ? authStore.data.user.id : null,
            transId: profileStore.data.transId ? profileStore.data.transId : '0-0',
            wcw1: profileStore.data.wcw1,
            wcw2: profileStore.data.wcw2,
            wcw3: profileStore.data.wcw3,
            wcw4: profileStore.data.wcw4,
            wcw5: profileStore.data.wcw5,
            wcw6: profileStore.data.wcw6,
            wpw1: profileStore.data.wpw1,
            wpw2: profileStore.data.wpw2,
            wpw3: profileStore.data.wpw3,
            wpw4: profileStore.data.wpw4,
            wpw5: profileStore.data.wpw5,
            wpw6: profileStore.data.wpw6,
            author: obj.author || null,
            loc: storage.get('deviceId'),
            lang: getLang(),
            typeAccount: isAuth ? authStore.data.provider : null,
            eventType: obj.eventType || null,
            // messErr: obj.message || null,
            'gtm.uniqueEventId': null
        };

        window.dataLayer = window.dataLayer || []; // init dataLayer

        let tempDataLayer = [];

        window.dataLayer.forEach(item => {
            tempDataLayer.push({...item,"gtm.uniqueEventId": null})
        });

          if( !isEqual( tempDataLayer.slice(-1)[0], zmienne ) && tempDataLayer.slice(-1)[0].event !== "gtm.com" ) {
              window.dataLayer.push({
                  event: location.pathname === '/' ? 'pageView' : 'tilink',
                  pagePath: location.pathname,
                  title: obj.title,
                  type: isFetched ? 'LoggedIn' : 'Guest',
                  userId: isFetched ? authStore.data.user.id : null,
                  transId: profileStore.data.transId,
                  wcw1: profileStore.data.wcw1,
                  wcw2: profileStore.data.wcw2,
                  wcw3: profileStore.data.wcw3,
                  wcw4: profileStore.data.wcw4,
                  wcw5: profileStore.data.wcw5,
                  wcw6: profileStore.data.wcw6,
                  wpw1: profileStore.data.wpw1,
                  wpw2: profileStore.data.wpw2,
                  wpw3: profileStore.data.wpw3,
                  wpw4: profileStore.data.wpw4,
                  wpw5: profileStore.data.wpw5,
                  wpw6: profileStore.data.wpw6,
                  author: obj.author || null,
                  loc: storage.get('deviceId'),
                  lang: getLang(),
                  typeAccount: isAuth ? authStore.data.provider : null,
                  eventType: obj.eventType || null,
              });
          }

    function strip(html)
    {
        let tmp = document.createElement("DIV");
        tmp.innerHTML = html;
        return tmp.textContent || tmp.innerText || "";
    }
    let description = strip( obj.description );

  return (
    <Helmet
      titleTemplate="%s - trans.INFO"
    >
      <html lang={getLocale()} />
      <title>{obj.title}</title>
      // Search Engine -->
      <meta name="description" content={description} />
      <meta name="image" content={obj.avatar} />
      // Schema.org for Google -->
      <meta itemprop="name" content={obj.title} />
      <meta itemprop="description" content={description} />
      <meta itemprop="image" content={obj.avatar} />
      // Twitter -->
      <meta name="twitter:card" content="summary" />
      <meta name="twitter:title" content={obj.title} />
      <meta name="twitter:description" content={description} />
      <meta name="twitter:site" content="@TranseuSystem" />
      <meta name="twitter:image:src" content={obj.avatar} />
      {obj.type === 'videos' ? <meta name="twitter:player" content={'https://www.youtube.com/watch?v='+obj.video} /> : null}
      // Open Graph general (Facebook, Pinterest & Google+) -->
      <meta name="og:title" content={obj.title} />
      <meta name="og:description" content={description} />
      <meta name="og:image" content={obj.avatar} />
      <meta name="og:url" content={window.location.href} />
      <meta name="og:site_name" content="transINFO" />
      <meta name="og:locale" content={getLocale()} />
      {obj.type === 'videos' ? <meta name="og:video" content={'https://www.youtube.com/v/'+obj.video} /> : null}
      <meta name="og:type" content="website" />
      // Google News
      {obj.tags !== '' ? <meta name="news_keywords" content={obj.tags} /> : null}
      {obj.googleNews === true ? null : <meta name="Googlebot-News" content="noindex, nofollow" /> }

        {location.pathname.length <= 3 ? <link rel="alternate" hreflang="en" href="https://trans.info/en/" /> : null}
        {location.pathname.length <= 3 ? <link rel="alternate" hreflang="de" href="https://trans.info/de/" /> : null}
        {location.pathname.length <= 3 ? <link rel="alternate" hreflang="cs" href="https://trans.info/cs/" /> : null}
        {location.pathname.length <= 3 ? <link rel="alternate" hreflang="hu" href="https://trans.info/hu/" /> : null}
        {location.pathname.length <= 3 ? <link rel="alternate" hreflang="lt" href="https://trans.info/lt/" /> : null}
        {location.pathname.length <= 3 ? <link rel="alternate" hreflang="sk" href="https://trans.info/sk/" /> : null}
        {location.pathname.length <= 3 ? <link rel="alternate" hreflang="ro" href="https://trans.info/ro/" /> : null}
        {location.pathname.length <= 3 ? <link rel="alternate" hreflang="ru" href="https://trans.info/ru/" /> : null}
        {location.pathname.length <= 3 ? <link rel="alternate" hreflang="x-default" href="https://trans.info/"/> : null}

    </Helmet>
  )
}
