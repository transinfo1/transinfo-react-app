
export const showNotification = (message, type, id) => ({
  type: "SHOW_NOTIFICATION",
  payload: {message, type, id}
});

export const hideNotification = () => ({
  type: "HIDE_NOTIFICATION"
});
