import {fetchData, getLocale} from '../utils/storeData';
import { getUser } from '../utils/authentication';

export const fetchRelatedPosts = (limit, type, tagSlug) => ({
    type: "FETCH_RELATED_POSTS",
    payload: fetchData('relatedPosts', '/posts/limit/' + (limit + 1) + '/before/' + Date.now() + '/langName/' + getLocale(), {
      params: {
          type: type,
          tagSlug: tagSlug,
          currentUserId: getUser().id
      }
    })
})

export const fetchRelatedBySearch = (keywords) => ({
    type: "FETCH_RELATED_POSTS",
    payload: fetchData('relatedPosts', '/search/' + getLocale(), {
        params: {
            type: 'post',
            keywords: keywords
        }
    }).then(response => {
        console.log(response)
    })
})
