import getConfig from '../utils/getConfig';
import {getUser, getAccessCredentials} from '../utils/authentication';
import axios from 'axios';
import * as storage from '../utils/storage';
import {getTimestampModifiedByMinutes} from '../utils/helpers';

// storages helpers
const setLocalStorage = data => storage.set('auth', data);
const setCookie = (data = null) => {
    let string = data ? encodeURIComponent(JSON.stringify({
            accessToken: data.accessToken,
            provider: data.provider,
            userId: data.user.id
        })) : '';

    let date = new Date(getTimestampModifiedByMinutes(30*24*60)); // 30 days
    document.cookie = "forumAuth=" + string + "; expires=" + date.toUTCString() + "; path=/;";
}

const setStorages = (data, autoLogin = true) => {
    storage.get('remember') === false ? autoLogin = false : autoLogin = true ;
    if(!autoLogin) {
        data = {
            ...data,
            expires: getTimestampModifiedByMinutes() // 1 min
        }
    }
    setLocalStorage(data);
    setCookie(data);
}

// actions
export const wordPressLogin = ({username, password, remember}) => ({
    type: "LOGIN",
    payload: axios.post(getConfig().restApiHostName + '/users/login', {
        username: username,
        password: password
    }).then(response => {
      if(response.status === 200) {
          storage.set('remember', remember);
          setStorages(response.data, remember);

      }
      return response
    })
})

export const setAuthenticatedUser = (authDataObj, autoLogin = true) => {
    setStorages(authDataObj, autoLogin);
    return {
        type: "LOGIN_FULFILLED",
        payload: {
            data: authDataObj,
            status: 200
    }
}}

export const refreshToken = (provider, widgetAutoLogin = false) => {
    let $ = window.jQuery;
    let param = widgetAutoLogin ? '?autoLogin=true' : '';
    return {
        type: "REFRESH_TOKEN",
        payload: axios.post(getConfig().restApiHostName + '/token/refresh' + param, $.param({
            token: getAccessCredentials().accessToken,
            provider: provider
        }), {
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        }).then(response => {
            setStorages({
                accessToken: response.data,
                provider: getAccessCredentials().provider,
                user: getUser()
            },false)
            return {
                ...response,
                data: {
                    accessToken: response.data
                }
            };
        })
    }
}

export const setWidgetAutoLogin = () => ({
    type: "WIDGET_AUTOLOGIN"
})


export const refreshWordpressToken = () => ({
    type: "REFRESH_TOKEN",
    payload: axios.get(getConfig().restApiHostName + '/users/refreshAccessToken/' + getAccessCredentials().accessToken).then(response => {
        setStorages({
                accessToken: response.data.accessToken,
                provider: getAccessCredentials().provider,
                user: getUser()
            }, false)
        return response;
    })
});

export const setToken = (token, provider = null, userObj = null) => {
    setStorages({
        accessToken: token,
        provider: provider || getAccessCredentials().provider,
        user: userObj || getUser()
    })
    return {
        type: "REFRESH_TOKEN_FULFILLED",
        payload: {
            data: {
                accessToken: token
            }
        }
        }
}

export const socialLoginFailed = (error) => ({
    type: "LOGIN_REJECTED",
    payload: {
        response: {
          status: 401,
          data: {
            error: error
          }
        }
    }
})

export const logout = (reload = true) => {
    storage.clear('auth');
    storage.clear('remember');

    axios.get('forum/logout.php');

    setCookie();
    if(reload) {
        document.location.href = '/';
    }
    return {
    type: "LOGOUT"
    }
}
