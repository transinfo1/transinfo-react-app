import { getLocale } from '../utils/storeData';
import getConfig from './../utils/getConfig';
import axios from 'axios';
import * as storage from '../utils/storage';

export const setKeywords = (keywords, redirectFromHeader = false ) => ({
    type: 'SET_KEYWORDS',
    keywords,
    redirectFromHeader
})

export const fetchSearchResult = (obj) => ({
        type: "FETCH_SEARCH_RESULT",
        payload: axios.get(getConfig().restApiHostName + '/search/' + getLocale(), {
            params: {
                keywords: obj.keywords,
                limit: obj.limit || 10,
                teaserLimit: obj.teaserLimit || 3,
                type: obj.type || 'all',
                page: obj.page || 0,
                onlyTitle: obj.onlyTitle || false,
                sortBy: obj.sortBy || 'relevance',
                sortDirection: obj.sortDirection || 'desc',
                saveKeywords: obj.saveKeywords || false
            }
        }).then( res => {
            storage.set('searchParams', obj, sessionStorage)
            return {
                ...res,
                data: res.data,
                keywords: obj.keywords,
                type: obj.type || 'all',
                sortBy: obj.sortBy || 'relevance',
                page: obj.page || 0,
                onlyTitle: obj.onlyTitle || false,
                saveKeywords: obj.saveKeywords || false
            }
        })
}
)

export const fetchSuggestions = (keyword) => ({

        type: "FETCH_SUGGESTIONS",
        payload: axios.get(getConfig().restApiHostName + '/search/autocomplete/' + getLocale(), {
            params: {
                keywords: keyword
            }
        }).then( res => {
            return {
                ...res,
                data: res.data,
                keywords: keyword,
            }
        })
    }
)
