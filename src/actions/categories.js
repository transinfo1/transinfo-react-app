import axios from 'axios';
import getConfig from '../utils/getConfig';
import {getLocale} from '../utils/storeData';

export const fetchVideoCategories = (limit) => ({
  type: "FETCH_VIDEO_CATEGORIES",
  payload: axios.get(getConfig().restApiHostName + "/categories", {
    params: {
        limit: limit,
        type: 'videos',
        langName: getLocale(),
        onlyWithChildren: true
    }
  })
});
