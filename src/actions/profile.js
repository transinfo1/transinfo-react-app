import axios from 'axios';
import getConfig from '../utils/getConfig';
import { getAuthHeaders } from '../utils/authentication';

export const fetchProfile = (data) => ({
    type: "FETCH_PROFILE",
    payload: axios.get(getConfig().restApiHostName + '/users/profile/me', {
        headers: getAuthHeaders(data.accessToken, data.provider)
    })
})

export const updateProfile = (data) => ({
    type: "FETCH_PROFILE_FULFILLED",
    payload: {
        data: data
    }
})

export const setFetchingProfile = () => ({
    type: "FETCH_PROFILE_PENDING"
})

