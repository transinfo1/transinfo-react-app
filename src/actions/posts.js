import {fetchData, getLocale} from '../utils/storeData';
import { getUser } from '../utils/authentication';

export const fetchWallPosts = () => ({
    type: "FETCH_POSTS",
    payload: fetchData('posts', '/posts/wall/' + getLocale(), {
        wall: true,
        params: {
            currentUserId: getUser('returnIdNull').id,
            relatedPostsLimit: 2
        }
    })
});

export const fetchOnePost = (slug) => ({
    type: "FETCH_POSTS",
    payload: fetchData('posts', '/posts/' + slug, {
        params: {
            currentUserId: getUser('returnIdNull').id,
            relatedPostsLimit: 15
        }
    })
});

export const fetchNextPosts = (lastDate, limit) => ({
    type: "FETCH_POSTS",
    payload: fetchData('posts', '/posts/limit/' + limit + '/before/' + lastDate + '/langName/' + getLocale(), {
        params: {
            currentUserId: getUser('returnIdNull').id,
            relatedPostsLimit: 2
        }
    })
});

export const fetchAuthorPosts = (limit, slug) => ({
    type: "FETCH_POSTS_BY_SLUG",
    payload: fetchData('slugPosts', '/posts', {
      params: {
          limit: limit,
          authorSlug: slug,
          langName: getLocale(),
          currentUserId: getUser('returnIdNull').id,
          relatedPostsLimit: 2
      }
    })
});

export const fetchNextAuthorPosts = (limit, lastDate, slug) => ({
    type: "FETCH_POSTS_BY_SLUG",
    payload: fetchData('slugPosts', '/posts/limit/' + limit + '/before/' + lastDate + '/langName/' + getLocale(), {
      params: {
          authorSlug: slug,
          currentUserId: getUser('returnIdNull').id,
          relatedPostsLimit: 2
      }
    })
});

export const fetchTagPosts = (limit, slug) => ({
    type: "FETCH_POSTS_BY_SLUG",
    payload: fetchData('slugPosts', '/posts', {
      params: {
          limit: limit,
          tagSlug: slug,
          langName: getLocale(),
          currentUserId: getUser('returnIdNull').id,
          relatedPostsLimit: 2
      }
    })
});

export const fetchNextTagPosts = (limit, lastDate, slug) => ({
    type: "FETCH_POSTS_BY_SLUG",
    payload: fetchData('slugPosts', '/posts/limit/' + limit + '/before/' + lastDate + '/langName/' + getLocale(), {
      params: {
          tagSlug: slug,
          currentUserId: getUser('returnIdNull').id,
          relatedPostsLimit: 2
      }
    })
});

export const fetchQuotePosts = (limit, page, period, seed) => ({
    type: "FETCH_POSTS_QUOTE",
    payload: fetchData('postsQuote', '/posts/quotes/limit/' + limit + '/' + getLocale(), {
        params: {
            page: page,
            daysLimit: period,
            seed: seed,
            currentUserId: getUser('returnIdNull').id
        }
    })
});
