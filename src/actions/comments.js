import {getUser} from '../utils/authentication';
import {fetchData, getLocale} from '../utils/storeData';


export const setComments = (data) => ({
    type: "FETCH_COMMENTS_FULFILLED",
    payload: {data: data}
})

export const addComment = (commentObj) => ({
        type: "FETCH_COMMENTS_FULFILLED",
        payload: {
            data: [
                {
                    ...commentObj,
                    postId: commentObj.post_id,
                    dateGmt: commentObj.date_gmt,
                    repliesCount: 0,
                    author: getUser(),
                    justAdded: true,
                    likes: {
                        likes: 0,
                        unlikes: 0,
                        currentUserLikeStatus: null
                    }
                }
            ]
        }
    })

export const fetchComments = (id, date = null, parentId = 0, limit = 1000) => ({
    type: "FETCH_COMMENTS",
    payload: fetchData('comments', '/comments/' + id, {
      params: {
        after: parentId ? date : null,
        before: parentId ? null : date,
        parentId: parentId,
        limit: limit,
        direction: parentId ? 'ASC' : 'DESC',
        currentUserId: getUser('returnIdNull').id

      }
    })
});

export const fetchFeaturedComments = (limit, lastDate) => ({
    type: "FETCH_FEATURED_COMMENTS",
    payload: fetchData('featuredComments', '/comments/list/' + getLocale() + '?limit=' + limit + '&before=' + lastDate + '&direction=DESC&featured=true')
});
