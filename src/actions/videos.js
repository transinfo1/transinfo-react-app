import getConfig from '../utils/getConfig';
import axios from 'axios';
import {getLocale, fetchData} from '../utils/storeData';
import { getUser } from '../utils/authentication';


export const fetchVideos = (limit, lastDate) => ({
    type: "FETCH_VIDEOS",
    payload: axios.get(getConfig().restApiHostName + '/posts/limit/' + limit + '/before/' + lastDate + '/langName/' + getLocale(), {
        params: {
            type: 'videos',
            currentUserId: getUser('returnIdNull').id
        }
    })
});

export const fetchCategorisedVideos = (limit, lastDate, category) => ({
    type: "FETCH_CATEGORISED_VIDEOS",
    payload: axios.get(getConfig().restApiHostName + '/posts/limit/' + limit + '/before/' + lastDate + '/langName/' + getLocale(), {
        params: {
            type: 'videos',
            categorySlug: category,
            currentUserId: getUser('returnIdNull').id
        }
    })
});

export const fetchBannerVideos = (limit, lastDate) => ({
    type: "FETCH_BANNER_VIDEOS",
    payload: fetchData('bannerVideos', '/posts/limit/' + limit + '/before/' + lastDate + '/langName/' + getLocale(), {
        params: {
            type: 'videos',
            currentUserId: getUser('returnIdNull').id
        }
    })
});

export const saveCategorisedVideos = (videos) => ({
  type: "SAVE_CATEGORISED_VIDEOS",
  payload: videos
});

export const updateCategorisedVideos = (video, category) => ({
  type: "UPDATE_CATEGORISED_VIDEOS",
  catName: category,
  payload: video
});
