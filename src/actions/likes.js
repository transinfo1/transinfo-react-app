export const setLike = (id, type, currentUserLikeStatus = null, likes = 0, unlikes = 0) => {
    return {
        type: "SET_LIKE",
        payload: {id, type, currentUserLikeStatus, likes, unlikes}
    }
};
