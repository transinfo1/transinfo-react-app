import axios from 'axios';
import getConfig from '../utils/getConfig';
import {getLocale} from '../utils/storeData';

export const fetchTopAuthors = () => ({
    type: "FETCH_TOP_AUTHORS",
    payload: axios.get(getConfig().restApiHostName + "/users/awardedAuthor/randomList?limit=5&langName=" + getLocale())
});