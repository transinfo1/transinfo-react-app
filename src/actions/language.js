import * as storage from '../utils/storage';

export const getLangFromBrowser = () => (navigator.language || navigator.userLanguage).replace('-', '_').split('_')[0];

export const setLang = (lang = null) => {

    let defaultLang = 'en';

    if(!lang) {
        lang = storage.get('lang') || getLangFromBrowser() || defaultLang;
    }

    if(lang === 'cs') {
        lang = 'cz';
    }

    let locale = {
      pl: 'pl_PL',
      en: 'en_GB',
      es: 'es_ES',
      de: 'de_DE',
      cz: 'cs_CZ',
      sk: 'sk_SK',
      lt: 'lt_LT',
      ru: 'ru_RU',
      hu: 'hu_HU',
      ro: 'ro_RO'
    }

    try {
        let langFile = require('../../public/static/langs/' + lang + '.json');
        storage.set('lang', lang);

        return {
            type: "UPDATE_LANGUAGE",
            lang: lang,
            locale: locale[lang],
            payload: langFile
        }

    } catch (ex) {
        storage.set('lang', defaultLang);
        let langFile = require('../../public/static/langs/' + defaultLang + '.json');

        return {
            type: "UPDATE_LANGUAGE",
            lang: defaultLang,
            locale: locale[defaultLang],
            payload: langFile
        }
    }



}
