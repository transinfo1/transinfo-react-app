
export const showPopup = (popupName, params = {}) => ({
  type: "SHOW_POPUP",
  payload: {
    name: popupName,
    params: params
  }
})

export const hidePopup = () => {
  // if(selectPopup && store.getState().popups.name !== selectPopup) {
  //     return null
  // }

  return {
      type: "HIDE_POPUP",
  }
}
