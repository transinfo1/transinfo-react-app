import React from 'react';
import { Route, IndexRoute, browserHistory } from 'react-router';

import App from './app/App';
import Wall from './pages/wall';
import Post from './pages/post';
import ProfileEdit from './pages/profile/edit';
import Videos from './pages/video/';
import Contact from './pages/contact/Template';
import Code404 from './pages/errors/Code404';
import Code500 from './pages/errors/Code500';
import PostsFilter from './pages/postsFilter';
import People from './pages/people/Template';
import Policy from './pages/policy';
import Regulations from './pages/regulations';
import Search from './pages/search';
import WidgetAutoLogin from './pages/WidgetAutoLogin';
import store from './store';
import * as storage from './utils/storage';

import {showPopup} from './actions/popups';
import {setLang} from './actions/language';

const checkAuth = () => {
    if(!storage.get('auth')){
        browserHistory.push('/')
        store.dispatch(showPopup('login'))
    }
}

export default (
    <Route path='/' component={App}>
        <IndexRoute component={Wall} />
        <Route path="confirm/:username/:key" component={Wall} />
        <Route path="404" component={Code404} />
        <Route path="500" component={Code500} />
        <Route path="videos" component={Videos} />
        <Route path="contact" component={Contact} />
        <Route path="policy" component={Policy} />
        <Route path="regulations" component={Regulations} />
        <Route path="people" component={People} />
        <Route path="forum" component={Contact} />
        <Route path="search" component={Search} />
        <Route path="login/auto" component={WidgetAutoLogin} />
        <Route path="user/profile" component={ProfileEdit} onEnter={checkAuth} />
        <Route path=":type/:slug" component={PostsFilter} />

        <Route path="pl" component={Wall} onEnter={() => {store.dispatch(setLang('pl'))}} />
        <Route path="en" component={Wall} onEnter={() => {store.dispatch(setLang('en'))}} />
        <Route path="de" component={Wall} onEnter={() => {store.dispatch(setLang('de'))}} />
        <Route path="cz" component={Wall} onEnter={() => {store.dispatch(setLang('cz'))}} />
        <Route path="cs" component={Wall} onEnter={() => {store.dispatch(setLang('cs'))}} />
        <Route path="hu" component={Wall} onEnter={() => {store.dispatch(setLang('hu'))}} />
        <Route path="lt" component={Wall} onEnter={() => {store.dispatch(setLang('lt'))}} />
        <Route path="sk" component={Wall} onEnter={() => {store.dispatch(setLang('sk'))}} />
        <Route path="ro" component={Wall} onEnter={() => {store.dispatch(setLang('ro'))}} />
        <Route path="ru" component={Wall} onEnter={() => {store.dispatch(setLang('ru'))}} />

        <Route path=":postSlug" component={Post} />
        <Route path="*/*" component={Code404} />
    </Route>
);