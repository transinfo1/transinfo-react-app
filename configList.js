module.exports = {
    devConf:  {
        "env": "dev",
        "packageName": "transinfo_frontend_dev",
        "restApiHostName": "http://backend-dev.transinfo.dev.firetms.io",
        "auth": {
            "oneLogin": {
                "authServerUrl": "http://auth.dev-trans.rst.com.pl/oauth2/login",
                "clientId": "14963949470NHDEu4SA74pSgFwk817"
            },
            "google": {
                "clientId": "983191201652-0cum57752r41o66jnbdpi2j3244qvuho.apps.googleusercontent.com"
            }
        },
        languages: [
            "pl",
            "en",
            "de",
            "cz",
            "hu",
            "lt",
            "sk",
            "ro",
            "ru"
        ]
    },
    testConf: {
        "env": "dev",
        "packageName": "transinfo_frontend_test",
        "restApiHostName": "http://backend-test.transinfo.dev.firetms.io",
        "auth": {
            "oneLogin": {
                "authServerUrl": "http://auth.dev-trans.rst.com.pl/oauth2/login",
                "clientId": "14963949470NHDEu4SA74pSgFwk817"
            },
            "google": {
                "clientId": "983191201652-0cum57752r41o66jnbdpi2j3244qvuho.apps.googleusercontent.com"
            }
        },
        languages: [
            "pl",
            "en",
            "de",
            "cz",
            "hu",
            "lt",
            "sk",
            "ro",
            "ru"
        ]
    },
    awsDevConf:  {
        "env": "dev",
        "packageName": "transinfo_frontend_dev",
        "restApiHostName": "https://transinfo-frontend-ag-0.dev.r1.trans-dev.loc/api/rest/v1",
        "auth": {
            "oneLogin": {
                "authServerUrl": "http://auth.dev-trans.rst.com.pl/oauth2/login",
                "clientId": "14963949470NHDEu4SA74pSgFwk817"
            },
            "google": {
                "clientId": "983191201652-0cum57752r41o66jnbdpi2j3244qvuho.apps.googleusercontent.com"
            }
        },
        languages: [
            "pl",
            "en",
            "de",
            "cz",
            "hu",
            "lt",
            "sk",
            "ro",
            "ru"
        ]
    },
    awsRcConf:  {
        "env": "rc",
        "packageName": "transinfo_frontend_rc",
        "restApiHostName": "https://rc.trans.info/api/rest/v1",
        "auth": {
            "oneLogin": {
                "authServerUrl": "https://auth.rc-trans.rst.com.pl/oauth2/login",
                "clientId": "1504012847Iv174Go9BsyOMqzLutMm"
            },
            "google": {
                "clientId": "983191201652-0cum57752r41o66jnbdpi2j3244qvuho.apps.googleusercontent.com"
            }
        },
        languages: [
            "pl",
            "en",
            "de",
            "cz",
            "hu",
            "lt",
            "sk",
            "ro",
            "ru"
        ]
    },
    awsRcTiboConf:  {
        "env": "rcTibo",
        "packageName": "transinfo_frontend_rc_tibo",
        "restApiHostName": "http://tibo-fe-ag.service.rc-eu-west-1.aws.lt.trans/api/rest/v1",
        "auth": {
            "oneLogin": {
                "authServerUrl": "https://auth.rc-trans.rst.com.pl/oauth2/login",
                "clientId": "1504012847Iv174Go9BsyOMqzLutMm"
            },
            "google": {
                "clientId": "983191201652-0cum57752r41o66jnbdpi2j3244qvuho.apps.googleusercontent.com"
            }
        },
        languages: [
            "pl",
            "en",
            "de",
            "cz",
            "hu",
            "lt",
            "sk",
            "ro",
            "ru"
        ]
    },
    awsProdConf:  {
        "env": "prod",
        "packageName": "transinfo_frontend_prod",
        "restApiHostName": "https://trans.info/api/rest/v1",
        "auth": {
            "oneLogin": {
                "authServerUrl": "https://auth.system.trans.eu/oauth2/login",
                "clientId": "1504013305oTGc74AgkIXGn8A7gUXP"
            },
            "google": {
                "clientId": "983191201652-0cum57752r41o66jnbdpi2j3244qvuho.apps.googleusercontent.com"
            }
        },
        languages: [
            "pl",
            "en",
            "de",
            "cz",
            "hu",
            "lt",
            "sk",
            "ro",
            "ru"
        ]
    }
};